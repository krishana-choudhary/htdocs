<?php
/**
 * overriding
 */
class overriding
{

	function getoverriding()
	{
		echo "This From parents overriding </br>";
	}
}
class childoverriding extends overriding
{
	function getoverriding()
	{
		echo "This is from child overriding </br>";
	}
}
$result = new childoverriding;
$result->getoverriding();

/**
 * overloding
 */
// class overloding
// {
// 	public $a,$b;
//
// 	function add($a,$b)
// 	{
// 		$this->a=$a;
// 		$this->b=$b;
// 	}
// 	function show()
// 	{
// 		echo $this->a+$this->b;
// 	}
// 	function addition()
// 	{
// 		$this->show();
// 	}
// }
// $as = new overloding;
// $as->add(6,5);
// $as->addition();
// $as->show();


/**
 * overloding
 */
class overloding
{
	function add($a,$b)
	{
		echo ($a+$b)."</br>";
	}
	// function add($c,$d)
	// {
	// 	echo ($c+$d);
	// }
}
$result =new overloding();
$result->add(5,6);


/**
 * overriding with scope resolution & self & parent function call
 */
class overrdg
{
function startcar()
{
	echo "this is from parent class function</br>";
}
	function selfcf()
	{
		self::startcar();
	}
}
class childoverrdg extends overrdg
{

	function startcar()
	{
		echo"this is from child overriding class function</br>s";
	}
	function parentcb()
	{
		parent::selfcf();
	}
}
$result = new childoverrdg;
$result->startcar();
$result->parentcb();




?>
