<?php
/**
 * use traits in classes
 */
class dish
{

	public function veg()
	{
		echo "this is from dish veg</br>";
	}
	public function bread()
	{
		echo "this is from dish bread</br>";
	}
}
// now we need to sweets for seprate plate
/**
 * we canuse trait for every plate
 */
trait sweets
{
	function sweet()
	{
		echo "this is from trait sweets</br>";
	}
}
class dish2 extends dish
{
	use sweets;
	public function food()
	{
		echo "this is from dish2 function child class</br>";
	}
}
$obj = new dish2;
$obj->veg();
$obj->sweet();

$obj->food();


?>
