<?php
/**
 * inheritance is use for stop to the duplicatation of code
 * more than two & three onother classe functions code we can use in one  class
 */

class Parents
{
	private $data1;

	function parentsfun()
	{
	echo "this function is  from parents class<br>";
	}
}

class Child extends Parents
{

	function childfun()
	{
	echo "this function is from child class <br>";
	}
}
$fc = new Child;
$fc->childfun();
$fc->parentsfun();

echo "end 1st class</br>";
echo "<br>";

// 2nd class of inheritence
class Bike
{
	public $while,$race,$arace;

	function bikedt($while,$race,$arace)
	{
		$this->while=$while;
		$this->race=$race;
		$this->arace=$arace;
	}
	function bikeinfo()
	{
		echo "this is for motorbike :".' '."they have $this->while whiles & </br>".' '."$this->race is the race"."</br>";
	}

}

class Activa extends Bike
{
 function activadt()
 {
 echo "this for Activa :".' '."hey have $this->while while & <br>".' '."$this->arace is the race"."</br>";
 }
}
$cf = new Activa;
$cf->bikedt(2,80,120);
$cf->bikeinfo();
$cf->activadt();

echo "end second class</br>";
echo "<br>";

// 3nd class of inheritence
class Car
{
	private $wheel,$gear,$speed ;

	function parentscar($wheel,$gear,$speed )
	{
	echo "this function is  from parents car :- <br>".' '."they have $this->wheel $wheel wheels &,<br>" .' '. "$this->gear $gear gears &,<br>" .' '. "$this->speed $speed speeds<br>";
	}
}
class Racingcar extends Car
{
	private $wheel,$gear,$speed ;
		function childcar($wheel,$gear,$speed)
		{
			echo "this function is  from racingcar class :- <br>".' '."they have $this->wheel $wheel wheels &,<br>" .' '. "$this->gear $gear gears &,<br>" .' '. "$this->speed $speed speeds<br>";
		}
}

$fc = new Racingcar;
$fc->childcar(4,5,120);
$fc->parentscar(4,5,90);
echo "end third class<br>";
echo "<br>";

 // 4th class of inheritence
 class car1
 {
	 private $speed=80;
	 function changegear()
	 {
		 echo "change gear<br>";
	 }
 }
 class sportcar extends car1
 {
	 private $speed=120;

 }
 $fc = new sportcar;
 $fc->changegear();
 echo "end fourth class<br>";
 echo "<br>";


 // 5th class of inheritence
 class fan
 {
	private $speed=80;
	function fandetail()
	{
		echo "this is fan</br>";
	}
 }
 class tablefan extends fan
 {

 }
 class handfan extends fan
 {

 }
 $fc = new handfan;
 $fc->fandetail();
 echo "end fifth class<br>";
 echo "<br>";






 ?>
