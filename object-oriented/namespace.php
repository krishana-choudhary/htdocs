<?php
/* namespace */


/**
 * namespace
 */
namespace firstname
{
  class xyz
  {

    function __construct()
    {
      echo "this is from firstname class xyz";
    }
  }
}

namespace
{
  class xyz
  {

    function __construct()
    {
      echo " this is from global namespace ";
    }
  }
  // use firstname\xyz as result;
  // $obj = new result;
  $obj = new firstname\xyz;
  // $obj = new xyz;
}

 // 	namespace Manager;
	// 	class Userdata
	// 	{
	// 		public $uname;
	// 		public $uid;
  //
	// 		function __construct($uname,$uid)
	// 		{
	// 			echo $this->uname=$uname;
	// 			echo $this->uid=$uid;
	// 		}
	// 	}
  //
  //     use manager;
  //     $result = new manager\userdata('1','krishana');


 ?>
