<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       www.napstack.com
 * @since      1.0.0
 *
 * @package    Lawyers_Refer
 * @subpackage Lawyers_Refer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
