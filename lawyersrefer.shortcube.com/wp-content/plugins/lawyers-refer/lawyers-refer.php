<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              www.napstack.com
 * @since             1.0.0
 * @package           Lawyers_Refer
 *
 * @wordpress-plugin
 * Plugin Name:       Lawyers Refer
 * Plugin URI:        www.napstack.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Napstack
 * Author URI:        www.napstack.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       lawyers-refer
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-lawyers-refer-activator.php
 */
function activate_lawyers_refer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lawyers-refer-activator.php';
	Lawyers_Refer_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-lawyers-refer-deactivator.php
 */
function deactivate_lawyers_refer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-lawyers-refer-deactivator.php';
	Lawyers_Refer_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_lawyers_refer' );
register_deactivation_hook( __FILE__, 'deactivate_lawyers_refer' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-lawyers-refer.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_lawyers_refer() {

	$plugin = new Lawyers_Refer();
	$plugin->run();

}
run_lawyers_refer();
