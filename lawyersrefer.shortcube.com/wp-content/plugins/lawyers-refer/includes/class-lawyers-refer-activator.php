<?php

/**
 * Fired during plugin activation
 *
 * @link       www.napstack.com
 * @since      1.0.0
 *
 * @package    Lawyers_Refer
 * @subpackage Lawyers_Refer/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Lawyers_Refer
 * @subpackage Lawyers_Refer/includes
 * @author     Napstack <h@napstack.com>
 */
class Lawyers_Refer_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
