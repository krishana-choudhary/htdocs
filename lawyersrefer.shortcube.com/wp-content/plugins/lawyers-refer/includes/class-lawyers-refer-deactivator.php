<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www.napstack.com
 * @since      1.0.0
 *
 * @package    Lawyers_Refer
 * @subpackage Lawyers_Refer/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Lawyers_Refer
 * @subpackage Lawyers_Refer/includes
 * @author     Napstack <h@napstack.com>
 */
class Lawyers_Refer_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
