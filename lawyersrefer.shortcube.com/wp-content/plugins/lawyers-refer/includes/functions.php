<?php

# Enable Gravity Forms Password Field
add_filter( 'gform_enable_password_field', '__return_true' );

/**
 * Remove custom-forms class on signup form pages
 * @param  array - $classes - Default body classes
 * @return array - modified body classes
 */
function lr_remove_classes($classes = []) {

	if( !is_page() || !in_array(get_the_ID(), ['1775', '1781', '1952'] ) ) return $classes;

	$key = array_search('custom-forms', $classes);

	if( $key ) unset( $classes[$key] );

	return $classes;
}
add_filter('body_class', 'lr_remove_classes', 999);
