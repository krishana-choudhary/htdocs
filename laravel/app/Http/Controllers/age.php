<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * this function is use for with controller class present to only int. value on front end by condition java regular expresion
 */
class Age extends Controller
{

	function age_cb($age)
	{
		echo "Your age is:".' '.$age;
	}
}


 ?>
