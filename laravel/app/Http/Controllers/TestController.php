<?php
//
//this file is made by cmd :php artisan make:controller TestController
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
	function showdata()
	{
		// $students=['Jack','Jony','jeffery'];
		// $marks=['30','50','80'];
		// //this is first method
		// //return view('hello2')->with(['mystu'=>$students,'marks'=>$marks]);
		//
		// //this is second method
		// return view('hello2')->withmystu($students)->withmarks($marks);
		$students =['jack'=>'23','jony'=>'50','jeffery'=>'100'];
		return view('hello2')->with(['studn'=>$students]);
	}
}
