<?php
//In mamp laravel folder
// create to Middleware file by cmd code is :php artisan make:middleware test ENTER;

namespace App\Http\Middleware;

use Closure;

class test
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      //localhost ip address is 127.0.0.1 either ::1;
      //online server ip address is 192.135.0.0.1;
      $ip = $request->ip();
      if ($ip =='127.0.0.1') {
        return redirect('/');
      }
        return $next($request);


    //this redirection of file directory
    // $file = view('hello');
    // if (!isset($file)) {
    //   return redirect('/');
    // }
    //   return $next($request);
  }
}
