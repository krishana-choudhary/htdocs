<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* In this file we will work to both file hello.blade.php & class Hellolaravel will CallbackFilterIterator
   & routes will sign to all that what will do.*/

// This is by default
Route::get('/', function () {
   return view('welcome');
});

/* in this function use sayhello it will be use for url place
 This function sayhello with controller*/
Route::get('sayhello','Hellolaravel@index');

// This function hellocloser functions without controller
Route::get('saycloser',function(){
    return view('hellocloser');
});

// This functioin is use for value received by url without views folder file
Route::get('value/{fname}','Valuebyurl@value_by_url');

// This function is use for value received by url without controller & view folder file
Route::get('values/{price}', function($price){
 echo "your value is" .' ' . $price;
});

//This function is for use only present the intejior value on front end by conditions without controller class & without view folder
Route::get('int/{inte}',function($inte){
  echo "Your int value is:".' '.$inte."</br>";
  echo "It will be present only inte. value by condition java regular expresion  ";
})->Where(['inte'=>"[0-9]+"]);

//This fiunction is use for only presnt value with controller class method
Route::get('age/{age}','Age@age_cb')->where(['age'=>"[0-9]+"]);

//This function is use for optional perameters
Route::get('optional/{max}/{min?}',function ($max,$min='0'){
  echo "This is optional perameters</br>";
 echo "max value is:".' '.$max.' '."min value is ".' '.$min;
});

Route::get('middleware',function(){
  return view('middleware');
})->Middleware('test');

//this is by cmd created controller file
Route::get('hellocc','TestController@showdata');

// this class function is with blade function templating engion
Route::get('showcon/{price}/{max?}',function($price,$max='0'){
  echo "your value is:".$price.' '.$max;
  return view('contentmasrter');
})->where(['price' =>"[0-9]+"]);

Route::get('safly','Safemethod@showsafly');

Route::get('ifelse','Ifelse@showifelse');
