@extends('layout/master')
@section('body')

<p>This is from if else blade syntax</p>

  {{ isset($hi)?$hi :'data is not found'}} <!--this condition with blade syntax -->

	<!-- or we can use this condition with blade syntax or some little code-->
	{{$hi or 'data is not found'}}

</br>

  <?php $ud = 'data is not found'?>
	@if($hi=='hello')
	  {{$hi}}
	@elseif($hi=='hi')
		{{$hi}}
	@else
	{{$ud}}
	@endif



@endsection
