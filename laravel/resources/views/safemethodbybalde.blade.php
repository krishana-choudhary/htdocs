
@extends ('layout/master')

@section('body')
 <p>This is body content</p>

<!-- <?php echo $stude; ?> -->   <!-- It will display script -->
<!-- <h3>{{$stude}}</h3> -->   <!-- It will not display script -->
<!-- {!!$stude!!} -->        <!-- It will display script -->
<!-- @{{$stude}}                <!-It will tell to  angularjs that this is not part of laravel this is tell to only we want to run java files -->
@endsection
