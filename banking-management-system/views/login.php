<div class="row">
  <div class="col-sm-4"></div><!-- END .col-sm-4 -->

  <div class="col-sm-4">
    <h2 class="section-header text-center">Login</h2>

    <?php locate_template('components/login-form.php') ?>
  </div><!-- END .col-sm-4 -->

  <div class="col-sm-4"></div><!-- END .col-sm-4 -->
</div>
