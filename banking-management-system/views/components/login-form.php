<form action="<?php echo BMS_URL; ?>" method="post">
  <div class="form-group">
    <label for="email-address">Email address</label>
    <input type="email" class="form-control" id="email-address" name="email" aria-describedby="emailHelp" placeholder="Enter email" value="h@napstack.com">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>

  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="password">
  </div>

  <div class="form-check">
    <input type="checkbox" class="form-check-input" name="remember" id="remember-me">
    <label class="form-check-label" for="remember-me">Remember Me</label>
  </div>
  <button type="submit" class="btn btn-primary">Login</button>
  <input type="hidden" name="action" value="process_login">
</form>
