<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="<?php echo BMS_URL ?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo BMS_URL ?>css/main.css">
  <title>Document</title>
</head>
<body>
  <header id="header">
    <?php locate_template('components/nav.php'); ?>
  </header>
  <div id="page-wrap">
    <div class="container">
