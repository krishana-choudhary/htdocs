<?php

/**
 *
 */
class Initialize {

  public function __construct() {

  }

  public function init() {

      session_start();
      // session_destroy();
      $this->connect_db();
      $this->actions();
      $this->router();
      $this->users();

      $this->shutdown();
  }

  private function router() {

      $init = new Router();
      $init->load_template();
  }

  public function users() {

  }

  public function actions() {

    $init = new Actions();
    $init->process_login();
  }

  public function connect_db() {

    global $pdo;

    $host = "localhost";
    $user = "root";
    $pass = "root";
    $db   = "bms";

    try {
        $pdo = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
        // set the PDO error mode to exception
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
        die;
    }
  }

  public function shutdown() {

    global $pdo;

    $pdo = null;
  }
}
