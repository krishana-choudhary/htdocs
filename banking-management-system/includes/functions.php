<?php

function locate_template($template_name) {

  $path = BMS_PATH . 'views/' . $template_name;

  if( file_exists( $path ) ) { include( $path ); return; }

  echo 'File Not Found!';
}

function is_user_logged_in() {

    return Users::is_user_logged_in();
}
