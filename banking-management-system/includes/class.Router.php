<?php

class Router {

    /**
     *
     */
    public function load_template() {

      $this->include_header();

      $this->load_content();

      $this->include_footer();
    }

    /**
     *
     */
    public function include_header() {

        locate_template('header.php');
    }

    /**
     *
     */
    public function include_footer() {

        locate_template('footer.php');
    }

    /**
     *
     */
    public function load_content() {

        // Check if user is logged in
        if( !is_user_logged_in() ) {
          locate_template('login.php');
          return; }

        locate_template('main.php');  

    }
}
