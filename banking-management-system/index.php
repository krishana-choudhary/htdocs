<?php
/**
 * Project Name: Banking Management System
 * Version: 1.0
 * Author: Napstack
 */

// Basic Constants
define('BMS_PATH', rtrim(dirname(__FILE__), '/') . '/');
define('BMS_URL', 'http://localhost:8888/banking-management-system/');

// Include Files
include(BMS_PATH . 'includes/class.Initialize.php');
include(BMS_PATH . 'includes/class.Actions.php');
include(BMS_PATH . 'includes/class.Router.php');
include(BMS_PATH . 'includes/class.Users.php');
include(BMS_PATH . 'includes/functions.php');

$init_app = new Initialize();
$init_app->init();
