<?php
session_start();
 if (empty($_SESSION['name'])) {
   header('location:index.php');
 }
if (isset($_GET['logout'])) {
  header('location:index.php');
  session_destroy();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/style.css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
    <?php
    if (isset($_SESSION['pass'])) {
      echo $_SESSION['pass'];
      unset($_SESSION['pass']);

    }
     ?>
	  <div class="jumbotron">
      <?php
      if (isset($_SESSION['name'])) { ?>
	    <h3>Welcome <?php echo $_SESSION['name'] ?> to home page</h3>
	    <p>Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile-first projects on the web.</p>
      <a href="home.php?logout=1">log out</a>
      <?php } ?>
	  </div>
	</div>
</body>
</html>
