<?php
function confirm_database($fire){
	global  $con;
	if (!$fire){
		die ("data is not set: " .mysqli_error($con));
	}
}
//function for subject table
function select_data(){
	global  $con;
	$query = "SELECT * FROM subjects ORDER BY position ASC";
	$fire  = mysqli_query($con,$query);
	confirm_database($fire);
	return $fire;
}

/**
 * function for page table
 * @param  - integer - $result_id - Result ID from
 * @return [type]            [description]
 */
function page_data($result_id){

	global $con;

	$pages = "SELECT * FROM pages WHERE subject_id = {$result_id} ORDER BY position ASC";
	$page  = mysqli_query($con,$pages);

	confirm_database($page);

	return $page;
}
//function for content menu name
// function select_content_menu_name($subject_id){
// 	global $con;
// 	$query = "SELECT * FROM subjects WHERE id = {$subject_id}";
// 	$query_fire = mysqli_query($con,$query);
// 	confirm_database($query_fire);
// 	$row = mysqli_fetch_array($query_fire);
// 	return $row;
// }
