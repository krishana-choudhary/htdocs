<?php

function confirm_query($query){

	global  $con;

	if ( $query ) return true;

	return false;
}

function get_subjects() {

	$subjects = [];

	global  $con;

	$sql   = "SELECT * FROM subjects ORDER BY position ASC";
	$query = mysqli_query($con, $sql);

	if( !confirm_query($query) ) return $subjects;

	while( $subject = mysqli_fetch_assoc($query) ) {

		$subject['pages'] = get_pages($subject['id']);

		$subjects[] = $subject;
	}

	return $subjects;
}

function get_pages($subject_id) {

		$pages = [];

		global $con;

		$sql   = "SELECT * FROM pages WHERE subject_id = {$subject_id} ORDER BY position ASC";
		$query = mysqli_query($con, $sql);

		if( !confirm_query($query) ) return $pages;

		while( $page = mysqli_fetch_assoc($query) ) {
			$pages[] = $page;
		}

		return $pages;
}
