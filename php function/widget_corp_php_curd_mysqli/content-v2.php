<?php include 'includes/databasecon.php' ?>
<?php include 'includes/functions.php' ?>
<?php include 'includes/functions-v2.php' ?>
<?php include 'includes/header.php' ?>

<?php
  if (isset($_GET['subj'])){
     $set_subj = $_GET['subj'];
     $set_subj = "";
   }elseif (isset($_GET['page'])) {
     $set_page = $_GET['page'];
		 $set_subj = "";
   }else {
   	  $set_subj = "";
			$set_page = "";
   }
  //$row=select_content_menu_name($set_subj);
 ?>
 <div id="main">
	 <table id="structure">
		 <tr>
			 <td id="navigation">
        <ul class="subject-data">
        <?php $subjects = get_subjects(); ?>

        <?php if( !empty($subjects) ) : ?>
            <?php foreach( $subjects as $subject ) : ?>

                <li><a href="content-v2.php?subj=<?php echo $subject['id']; ?>"><?php echo $subject['menu_name']; ?></a></li>
                
                <?php if( !empty( $subject['pages'] ) ) : ?>
                  <ul class="subject-data">
                  <?php foreach( $subject['pages'] as $page ) : ?>
                    <li><a href="content-v2.php?page=<?php echo $page['id']; ?>"><?php echo $page['menu_name']; ?></a></li>
                  <?php endforeach; ?>
                  </ul>
                <?php endif; ?>

            <?php endforeach; ?>
        <?php endif; ?>
        </ul>
	 </td>
	 <td id="page">
	<h2>Content Area</h2>
	  <?php
		 echo $set_subj;
		 echo $set_page;
		 ?>
		 </ul>
	 </td>
 </tr>
</table>
</diV><!--end main-->
<?php require 'includes/footer.php' ?>
