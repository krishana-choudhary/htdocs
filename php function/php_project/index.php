  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <div class="row">
      <div class="container">
        <h3 class="text-center">Function Action Associations From VAriants Of Unknown Significance</h3>
        <br>
        <div class="col-sm-6 First-col">
          <p class="text-center">Top Molecular Function Associations</p>
          <hr/>
          //function
          <?php

          //  function current_year_cb() {
          //
	        //  return date('Y');
          //  }
          // add_shortcode('current_year', 'current_year_cb');


          function get_add_classes($width){
            $class ='';
            if ($width >= 0 && $width <= 20) { $class ='warning';}

            elseif ($width > 21 && $width <= 40) {$class ='info';}

            elseif ($width > 41 && $width <= 60) {$class ='success';}

            return $class;
           }

          $items =[
            ['label' =>'hello','value' => 5],
            ['label' =>'hello','value' => 10],
            ['label' =>'hi','value' => 15],
            ['label' =>'hello','value' => 20],
           ];

          foreach ($items as $item ):
            ?>
            <div class="progress">
              <div class="progress-bar progress-bar-<?php echo get_add_classes($item['value']);?>" role="progressbar" aria-valuenow="70"
                aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $item['value'];?>%">
              </div>
            </div>
            <p><small><?php echo $item['label'];?></small></p>
            <?php
           endforeach;
          ?>
        </div>
        <div class="col-sm-6">
          <p class="text-center">Top Molecular Function Associations</p>
          <hr/>
          <?php
          $items =[
            ['label' =>'hello','value' => 5],
            ['label' =>'hello world','value' => 40],
            ['label' =>'hello','value' => 15],
            ['label' =>'hello','value' => 20],
            ];
            foreach ($items as $item ){
              ?>
              <div class="progress">
                <div class="progress-bar progress-bar-<?php echo get_add_classes($item['value']);?>" role="progressbar" aria-valuenow="70"
                  aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $item['value'];?>%">
                </div>
              </div>
              <p><small><?php echo $item['label'];?></small></p>
              <?php
            };
              ?>
            </div>
          </div><!-- end container -->
        </div><!-- end.row -->


      <div class="container">
      <div class="row">
       <div class="container btn-info">
        <div class="col-sm-6">
          <?php
          $firstname ="krishana";
          $secondname ="kumar";
          $thirdname = $firstname;
          $thirdname.=$firstname.$secondname.'hello';
          echo "$thirdname </ br>";

         echo "</br>";
         $array1  = array(1,2,3,4,5,6,7,);?>
         <?php echo $array1[0];?>

         <?php $array2= array(4,'man','person', array('me','you'));?>
         <?php echo $array2[3][1];?>
         <?php $array2[3]='cat';?>
          </br>
         <?php echo $array2[3];?>
       </br>

          <?php $array3=['firstname' => 'developer', 'lastname' => 'Napstack']; ?>
         <?php echo $array3['firstname']." ".$array3['lastname']; ?></br>
         <?php $array3['firstname'] = "krishana";?>
         <?php echo $array3['firstname']." ".$array3['lastname'];
         ?>
          </div>

          <p>isset or not set</p>
          <?php
          //isset function its say true and false that var is set or not its dta type of wooleans
            $var1= true;
            $var2= false;
          ?>
           $var1 is true: <?php echo $var1;?></br>
           $var2 is false: <?php echo $var2;?></br>
           <?php
            $var3 = 1;
            $var4 = "cat";
            $var5 = "0";
           ?>
           $var3 is set: <?php echo isset($var3);?></br>
           $var4 is set: <?php echo isset($var4);?></br>
           $var5 is set: <?php echo isset($var5);?></br>
           $var6 is set: <?php echo !isset($var6);?></br>
         </br>
           $var3 empty:  <?php var_dump(empty($var3));?></br>
           $var4 empty:  <?php echo empty($var4);?></br>
           $var6 empty:  <?php var_dump(empty($var6));?></br>

           <p>if statements logical Expressions</p>
           <?php
            $vara=4;
            $varb=3;
            if ($vara > $varb) {
              echo "a is greter than b";
            }
            ?>

          <div class="col-sm-6">
            <?php
          echo "<p>array</p>";
          $array1  = array(1,2,3,4,5,6,7,);?>
           <?php echo $array1[0];?>
           <?php $array2= array(4,'man','person', array('me','you'));?>
           <?php echo $array2[3][1];?>
           <?php $array2[3]='you';?>
            </br>
           <?php echo $array2[3];?>
         </br>

            <?php $array3=['firstname' => 'developer', 'lastname' => 'Napstack']; ?>
           <?php echo $array3['firstname']." ".$array3['lastname']; ?></br>
           <?php $array3['firstname'] = "krishana";?>
           <?php echo $array3['firstname']." ".$array3['lastname'];

           echo "<p>typecasting</p>";

           //typecasting
             $var1 ="2 the world";
             $var2 =$var2+3;
             echo gettype($var1); echo "</br>";
             echo is_string($var1);echo "</br>";

             echo "<p>constants</P>";
             //constants
              $max_width = 980;
              define('MAX_WIDTH', 980);
              echo MAX_WIDTH;echo "</br>";
              $max_width +=1;
              echo "$max_width</br>";

              $a=array('krishana'=>55,'kumar'=>65);
              foreach ($a as $key => $d) {
                echo $key." has got secure ",$d." marks";
                echo "</br>";
              }


                echo "<p>ifelse statement with and&& opretors</p>";
                $a=5;
                $b=3;
                if ($a>$b) {
                  echo "a is greater than b</br>";
                }

                $c=20;
                $d=1;
                if (($a>$b) && ($c>$d)){
                  echo "a is greater than b And</br>";
                  echo "c is greater than d</br>";
                }
                if (($a>$b) || ($c>$d)){
                  echo "a is greater than b And</br>";
                  echo "c is greater than d</br>";
                }
                else {
                  echo "a is greater than b and cis greteater than d</br>";
                  }


                  unset($a);
                  if (!isset($a)) {
                    $a=100;
                  }
                  echo $a."</br>";

                  echo "<p>Switch:-</p></br>";
                  $switch= '3';
                  switch ($switch) {
                    case '3':
                    echo "Switch is equals 3</br></br>";
                    break;
                    case '2':
                    echo "switch is equals 2";
                    break;

                    default:
                    echo "values are not equals</br>";
                    break;
                  }
        echo "<p>while loop:-</p>";
        $abc=1;
        while ($abc <= 10) {
          if ($abc ==9) {
            echo "nine"."</br>";
          }
          else {
            echo $abc."</br>";
          }
          $abc++;
        }

        //foreach loops will be use with array
        echo " For Each Loops</br>";
        $array = array(
          'philiphs tv'=>2000, 'LCD'=>1224,'Watch'=>'priceless');
           foreach ($array as $key=>$value) {
           if (is_int($value)) {
           echo $key. ":$" .$value."</br>";
           }
           else {
           echo $key. ":" .$value;
          }
        }

        //for loop with continue
        echo "<p> for loop with continue</p></br>";
        for ($i=0; $i <=10 ; $i++) {
          if ($i==5) {
            continue;
          }
          else {
            echo "$i";
          }
        }

        //while loop with continue
        echo "<p> while loop with continue</p></br>";
        $head=0;
        $COUNTING=0;
        while ($head <= 3) {
          $flip=rand(0,1);
          $COUNTING++;
          if ($flip) {
            $head++;
            echo "HEAD";
          }else {
            $head=0;
            echo "<h3> - </h3>";
          }
        }
    echo "flip counts $COUNTING times</br>";


    //function with  concatination
    echo "<p>function with concatination </p>";
    function concatination($a,$b){
        $c = $a ." " .$b;
        return $c;
      }
      $d = concatination("hello","world");
      echo $d."</br>";

    //function with if condition
    echo "<p>function with array </p>";
    function condition($a,$b){
      $c = $a + $b;
      return $c;
    }
    $d = condition(4,5);
    echo $d."</br>";
    if (condition(6,4)==10) {
      echo "Yes.</br>";
      }else {
      echo "No Values Are Not Equals.</br>";
    };

    //Function with array
    echo "<p>Function With Array</p>";
    function with_array($a,$b){
      $c = $a+$b;
      $d = $a-$b;
      $e = array($c,$d);
      return $e;
    }
    $f = with_array(50,10);
     echo "Add : " .$f[0]."</br>";
     echo "Subt : " .$f[1]."</br>";





    //Function with Global Vari.
    echo "<p>Function With Global Var.</p>";
    $az = "Hello World";
    function Global_test(){
     GLOBAL $az;
     $z = "hello";
     echo $z;
    }
    Global_test();
    echo $az."</br>";


    //We Can Use Replace Global
    $w = "You Can Use It Replace Global var.  ";
    function replace_global($b){
      $b = "You Can Use It Replace Global var.1";
      return $b;
    }
    $w = replace_global($w);
    echo $w ."</br>";


    //We Can Use Replace Global
    echo "<p> Default Values</p>";
    function default_color($room="red",$bedroom="blue"){
      echo"My Room Color is {$room}"."</br>";
    }
    default_color()."</br>";?>


   <!--data received by get method-->
   <p>Url Values</p>
   <?php
    $id = 4;
    $name = 'Krishana Kumar';
    ?>
      <a style="color:red;"href="datareceived.php?name=<?php echo urlencode($name);?>&id=<?php echo $id; ?>"> Datareceived </a>

    <?php
    //data received by post method
    echo "<p>Data received by post method</p>";
    ?>
    <form action="datareceived.php" method="post">
      First name:<br>
      <input style="color:#000;" type="text" name="firstname" value=""><br>
      Last name:<br>
      <input style="color:#000;" type="text" name="lastname" value=""><br><br>
      <input style="color:red;"type="submit" value="Submit">
    </form>
    <?php
    echo "<p>for loop wiyh modifi</p></br>";
    for ($i=0; $i <6 ; $i++) {
    for ($io=0; $io <$i ; $io++) {
      echo "*";
      }
      echo "</br>";
    }
    //function with return fstatement

    echo"<p>function with return statement</p>";
       function num(){
         return 2;
       }
       $cb=num();
       echo $cb;

    // //cookies
    // $var = 0;
    // if (isset($_COOKIE['ra'])) {
    //   $var = $COOKIE['ra'];
    // }
    // echo $var;
    //  setcookie("ra", 0, time()+(60*60*24*7));

    ?>
      </div>
      </div><!-- End.container -->
    </div><!-- End.row -->


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  </body>
  </html>
