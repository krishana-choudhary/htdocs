
   /**
    * Callback function for [blog_post] shortcode
    *
    * @param array - $atts - array of passed attributes
    * @param string - $content - content passed in between shortcide tags
    * @return string - shortcode generated HTML
    */
 function blog_post_func( $atts ) {
   $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
   $atts = shortcode_atts( array(
   'posts_per_page'=>   9,
   ), $atts, 'blog_post' );

    $args = array(
      'post_type'     => 'post',
      'post_status'   => 'publish',
      'category_name' => 'behind-the-scenes',
      'posts_per_page'=>  $atts['posts_per_page'],
      'paged'         =>  $paged, );

   $query= new WP_Query( $args );
   if ( $query->have_posts() ) :
   while ( $query->have_posts() ) : $query->the_post();
   <article id="post-<?php the_ID(); ?>" <?php post_class();?>>
     <div class="main-column-item">
   <?php
   if (has_post_thumbnail() ):
       the_post_thumbnail();
     endif;
       ?>
       <div class="wraper-content">
       <header class="entry-header">
          <h1 class="entry-title"><?php the_title();?></h1>
           <img class="wpb_animate_when_almost_visible wpb_fadeInDown fadeInDown author-img" src="<?php echo get_stylesheet_directory_uri(); ?>img/author-img.jpg" alt="author img"></img>
           <p class="author-name">By <?php the_author();?></p>
         </header>

        <div class="entry-content">
          <?php the_excerpt();?>
        </div>
        <a href="<?php the_permalink(); ?>" class="item-permalinkbtn-sm btn-port">Read More</a>
      </div>
      </div><!--end .main-column-item-->
      </article>
      <?php
    endwhile;
   endif;
 }
 add_shortcode( 'blog_post', 'blog_post_func' );
