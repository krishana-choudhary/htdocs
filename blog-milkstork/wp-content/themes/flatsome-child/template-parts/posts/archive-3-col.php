<?php if ( have_posts() ) : ?>

<?php
	$ids = array();
	while ( have_posts() ) : the_post();
		array_push($ids, get_the_ID());
  endwhile; // end of the loop.

  // take two of first value to another array
  if (sizeof($ids) > 2) {
    $twiceIDs = implode(',', array_slice($ids, 0, 2));
    $ids = implode(',', array_slice($ids, 2));
  } else {
    $twiceIDs = implode(',', $ids);
    $ids = "";
  }
?>

<div class="row">
  <div class="col" style="padding-bottom: 40px;">
    <?php
      if(get_theme_mod('blog_after_header_content')){
        echo '<div class="blog_after_header_content">'.do_shortcode(get_theme_mod('blog_after_header_content')).'</div>';
      }
    ?>
  </div>
  <div class="col pb-0 the-2-posts">
    <?php echo do_shortcode('[blog_posts style="default" columns="2" columns__md="1" posts="2" title_size="large" readmore="READ NOW" readmore_style="link" show_date="false" excerpt_length="20" comments="false" image_height="56.25%" image_size="large" text_align="left"  ids="'.$twiceIDs.'"]'); ?>
  </div>

  <div class="col">
    <?php
      if(get_theme_mod('blog_middle_content')){
        echo '<div class="blog_middle_content">'.do_shortcode(get_theme_mod('blog_middle_content')).'</div>';
      }
    ?>
  </div>

  <?php if (strlen($ids)) { ?>
  <div class="col the-8-posts">
    <?php echo do_shortcode('[blog_posts style="default" type="row" columns="3" columns__md="1" posts="3" readmore="READ NOW" readmore_style="link" show_date="false" excerpt_length="10" comments="false" image_height="56.25%" image_size="medium" image_hover="zoom" text_align="left" ids="'.$ids.'"]'); ?>
  </div>
  <?php } ?>

  <div class="col text-center">
    <?php flatsome_posts_pagination(); ?>
  </div>
</div>



<?php else : ?>

	<?php get_template_part( 'template-parts/posts/content','none'); ?>

<?php endif; ?>