<?php
	do_action('flatsome_before_blog');
?>

<?php if(!is_single() && flatsome_option('blog_featured') == 'top'){ get_template_part('template-parts/posts/featured-posts'); } ?>
<div class="row align-center">
	<div class="col">
  <?php
    if(is_single()) {
      get_template_part( 'template-parts/posts/single');
    } else if(!is_single() && flatsome_option('blog_featured') == 'content') {
      get_template_part('template-parts/posts/featured-posts');
    } else {
      get_template_part( 'template-parts/posts/archive', flatsome_option('blog_style') );
    }
    if(is_single()) {
      if (comments_open()) {
        comments_template();
      } else {
        do_action('flatsome_before_comments');
      }
    }
  ?>
	</div> <!-- .large-9 -->
</div><!-- .row -->

<?php do_action('flatsome_after_blog');
