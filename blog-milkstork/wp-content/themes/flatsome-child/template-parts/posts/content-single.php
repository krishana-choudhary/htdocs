<div class="entry-content single-page">
  <div class="row align-center">
    <div class="col large-12 pb-0">
      <?php the_content(); ?>
    </div>
  </div>

	<?php
	wp_link_pages( array(
		'before' => '<div class="page-links">' . __( 'Pages:', 'flatsome' ),
		'after'  => '</div>',
	) );
	?>
</div><!-- .entry-content2 -->

<?php if ( get_theme_mod( 'blog_single_footer_meta', 1 ) ) : ?>
	<footer class="entry-meta text-<?php echo get_theme_mod( 'blog_posts_title_align', 'center' ); ?>">
    <div class="row align-center">
      <div class="col large-9  pb-0">
        <div class="row">
          <div class="col large-6 pb-0">
            <?php if ( get_theme_mod( 'blog_share', 1 ) ) {
              // SHARE ICONS
              echo '<div class="blog-share">';
              echo do_shortcode( '[share]' );
              echo '</div>';
            } ?>
          </div>
          <div class="col large-6 pb-0">
            <div class="blog-tag">
              <?php
              /* translators: used between list items, there is a space after the comma */
              $tag_list = get_the_tag_list( '', __( ', ', 'flatsome' ) );
              printf( $tag_list, get_permalink(), the_title_attribute( 'echo=0' ) );
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
	</footer><!-- .entry-meta -->
<?php endif; ?>

<?php if ( get_theme_mod( 'blog_author_box', 1 ) ) : ?>
	<div class="entry-author author-box">
		<div class="flex-row align-top">
			<div class="flex-col mr circle">
				<div class="blog-author-image">
					<?php
					$user = get_the_author_meta( 'ID' );
					echo get_avatar( $user, 90 );
					?>
				</div>
			</div><!-- .flex-col -->
			<div class="flex-col flex-grow">
				<h5 class="author-name uppercase pt-half">
					<?php echo esc_html( get_the_author_meta( 'display_name' ) ); ?>
				</h5>
				<p class="author-desc small"><?php echo esc_html( get_the_author_meta( 'user_description' ) ); ?></p>
			</div><!-- .flex-col -->
		</div>
	</div>
<?php endif; ?>

<?php if ( get_theme_mod( 'blog_single_next_prev_nav', 1 ) ) :
	flatsome_content_nav( 'nav-below' );
endif; ?>
