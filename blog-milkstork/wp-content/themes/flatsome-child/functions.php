<?php
// Add custom Theme Functions here
define('FLATSOME_CHILD_ROOT_DIR', get_stylesheet_directory());

include_once FLATSOME_CHILD_ROOT_DIR.'/inc/shortcodes/blog_posts.php';
include_once FLATSOME_CHILD_ROOT_DIR.'/inc/admin/options/blog/add-options-blog-layout.php';

if (!function_exists('milkstork_js_css')) {
  function milkstork_js_css () {
    wp_register_style( 'child-theme-css', get_stylesheet_directory_uri() . '/style.css'  );

    wp_enqueue_style( 'child-theme-css' );
  }
  add_action('wp_enqueue_scripts', 'milkstork_js_css');
}