<?php

/**
 * Plugin Name: Social Board
 * Plugin URI: http://axentmedia.com/wordpress-social-board/
 * Description: Combine all your social media feeds (Facebook, Twitter, Instagram, Google+, Flickr, YouTube, RSS, ...) into one single social stream and display on your website.
 * Tags: social stream, wordpress social plugin, social feed, social tabs, social wall, social timeline, social media, social networks, feed reader, facebook, twitter, google+, tumblr, delicious, pinterest, flickr, instagram, youtube, vimeo, stumbleupon, deviantart, rss, soundcloud, vk, linkedin, vine
 * Version: 3.4.7
 * Author: Axent Media <axentmedia@gmail.com>
 * Author URI: http://axentmedia.com/
 * License: http://codecanyon.net/licenses/standard
 * 
 * Copyright © 2016 Axent Media, All Rights Reserved.
 * Any type of usages require a commercial license.
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Define constants
define( 'SB_VERSION', '3.4.7' );
define( 'SB_DOCS', 'http://axentmedia.com/wordpress-social-board-docs/' );
define( 'SB_FILE', __FILE__ );
define( 'SB_DIRNAME', dirname( SB_FILE ) );
define( 'SB_LOGFILE', SB_DIRNAME.'/sb.log' );
define( 'SB_UPSERVER', 'http://wpupdate.axentmedia.com/' );

// starting session
add_action('wp', 'sb_start_session', 1);
function sb_start_session() {
    if ( ! session_id() ) {
        session_start();
    }
}

// Localization
load_plugin_textdomain(
    'social-board',
    false,  // deprecated
    basename( SB_DIRNAME ) . '/language/'  // Relative path to WP_PLUGIN_DIR where the .mo file resides.
);

// Define some global vars
$GLOBALS['sb_scripts'] = array();
$GLOBALS['sb_no_jquery'] = false;
$GLOBALS['sb_apipage_url'] = urlencode( admin_url( 'edit.php?post_type=sb_posts&page=sb_settings&tab=api' ) );

// Include the library file
if ( ! class_exists( 'SB_AdminPageFramework' ) ) {
    include( SB_DIRNAME . '/library/admin-page-framework/admin-page-framework.php' );
}
include( SB_DIRNAME . '/admin/SB_Bootstrap.php' );

// load Twitter lib
require SB_DIRNAME . "/library/twitteroauth/autoload.php";
use Abraham\TwitterOAuth\TwitterOAuth;

// load cache system
include( SB_DIRNAME . '/library/SimpleCache.php' );

// Social Board main class
class SocialBoard {
    public $attr, $final, $finalslide;
    public $echo = true;
    public $args = null;
    
    public function start() {
        add_action( 'wp_head', array(&$this, 'header') );
        add_action( 'wp_enqueue_scripts', array(&$this, 'add_styles') );
        // Add Shortcode
        add_shortcode( 'social_board', array('SocialBoard', 'social_board_shortcode') );
    }
    
    // Initialize by property
    function run() {
        $this->init( $this->attr, $this->echo, $this->args );
    }

    // Initialize by function
    function init( $attr, $echo = true, $args = null, $ajax_feed = array(), $loadmore = array() ) {
        
        $id = (@$attr['network']) ? substr( sha1( serialize($attr) ), 0, 5 ) : $attr['id'];
        if (@$attr['sb_type'])
            $attr['type'] = $attr['sb_type'];
        $type = (@$attr['type']) ? $attr['type'] : 'wall';
        if ( empty($id) ) die();
        
        // get options
        $this->sboption = (@$attr['network']) ? $attr['network'] : sb_options( $id );
        
        // get default setting & post options
        $defoption = array(
            'setting' => array(
                'theme' => 'sb-modern-light',
                'results' => '30',
                'words' => '40',
                'slicepoint' => '300',
                'commentwords' => '20',
                'titles' => '15',
                'userinfo' => 'top',
                'readmore' => '1',
                'order' => 'date',
                'filters' => '1',
                'loadmore' => '1',
                'lightboxtype' => 'media',
                'layout_image' => 'imgexpand',
                'links' => '1',
                'nofollow' => '1',
                'https' => false,
                'cache' => '360',
                'crawl' => '10',
                'timeout' => '15',
                'debuglog' => '0'
            ),
            'wallsetting' => array(
                'transition' => '400',
                'stagger' => '',
                'filter_search' => true,
                'originLeft' => 'true',
                'wall_width' => '',
                'wall_height' => '',
                'fixWidth' => 'false',
                'breakpoints' => array('5', '4', '4', '3', '2', '2', '1'),
                'itemwidth' => '230',
                'gutterX' => '10',
                'gutterY' => '10'
            ),
            'feedsetting' => array(
                'rotate_speed' => '100',
                'duration' => '4000',
                'direction' => 'up',
                'controls' => '1',
                'autostart' => '1',
                'pauseonhover' => '1',
                'width' => '280'
            ),
            'carouselsetting' => array(
                'cs_speed' => '400',
				'cs_pause' => '2000',
                'autoWidth'=> 'false',
                'cs_item' => array('4', '3', '2', '2', '1'),
                'cs_width' => '230',
                'cs_rtl' => 'false',
                'cs_controls' => 'true',
                'cs_auto' => 'false',
                'cs_loop' => 'true',
                'cs_pager' => 'true',
                'slideMargin' => '10',
                'slideMove' => '1'
            ),
            'timelinesetting' => array(
                'onecolumn' => 'false'
            )
        );
        
        // merge options
        $setoption = SB_AdminPageFramework::getOption( 'SB_Settings_Page' );
        $setoption = array_merge($defoption, $setoption);
        foreach ($defoption as $defoptKey => $defoptVal) {
            if ( isset($setoption[$defoptKey]) )
                $setoption[$defoptKey] = array_merge($defoptVal, $setoption[$defoptKey]);
        }
        
    	// the array of feeds to get
        $filters_order = array( 0 => array('facebook' => 1), array('twitter' => 1), array('google' => 1), array('tumblr' => 1), array('delicious' => 1), array('pinterest' => 1), array('flickr' => 1), array('instagram' => 1), array('youtube' => 1), array('vimeo' => 1), array('stumbleupon' => 1), array('deviantart' => 1), array('rss' => 1), array('soundcloud' => 1), array('vk' => 1), array('linkedin' => 1), array('vine' => 1) );
    	if ( ! empty($ajax_feed) && $ajax_feed != 'all' ) {
    		$this->feed_keys = $ajax_feed;
    	} else {
    		if (@$setoption['setting']['filters_order']) {
	    		// r3.4.0 < compatibility fix
	    		if ( ! is_array($setoption['setting']['filters_order'][0]) )
	    			$this->feed_keys = $filters_order;
	    		else
	    			$this->feed_keys = $setoption['setting']['filters_order'];
    		} else {
    			$this->feed_keys = $filters_order;
    		}
        }
        
        // define theme
        if ( ! $theme = @$attr['theme'] )
            $theme = $setoption['setting']['theme'];
        $themeoption = sb_options( $theme );

        // set some settings
        if ( $type == 'carousel' ) {
        	$attr['carousel'] = 'on';
        	$type = 'feed';
        }
        $label = (@$args['widget_id']) ? $args['widget_id'] : $type.$id;
        if ( $type == 'feed' ) {
            $is_feed = true;
            $settingsection = (@$attr['carousel']) ? 'carouselsetting' : 'feedsetting';
            $filterlabel = '';
        } else {
            $is_feed = false;
            $settingsection = $type.'setting';
            $filterlabel = ' filter-label';
        }
        $is_timeline = ( $type == 'timeline' ) ? true : false;
        $is_wall = ( $type == 'wall' ) ? true : false;

        $typeoption = $type;
        if ( $is_feed ) {
            if ( @$attr['position'] ){
                if ( @$attr['position'] != 'normal' )
                    $typeoption = 'feed_sticky';
            }
            if (@$attr['carousel'])
                $typeoption = 'feed_carousel';
        }

        // main container id
        if ( @$label )
            $attr_id = ' id="timeline_'.$label.'"';
        $class = array('sboard');

        // merge shortcode and widget attributes with related default settings
        if ( ! @$setoption[$settingsection] )
            $setoption[$settingsection] = array();
        $this->attr = $attr = array_merge($setoption['setting'], $setoption[$settingsection], $attr);
        
        // set results
        if ( ! $results = @$attr['results'] )
    		$results = 10;
    	if (@$args['liveresults'])
        if ( $results < $args['liveresults'] )
            $results = $args['liveresults'];
        if ( $results > 100 )
            $results = 100;
        $attr['results'] = $results;
        
        $attr_ajax = json_encode($attr);
        $attr['cache'] = (int)$attr['cache'];

        // set crawl time limit (some servers can not read a lot of feeds at the same time)
        $GLOBALS['crawled'] = 0;
        $crawl_limit = ($attr['cache'] == 0) ? 0 : (int)@$attr['crawl'];
        
        // Init cache
        $cache = new SB_SimpleCache;
        $cache->debug_log = @$attr['debuglog'];
        $cache->timeout = @$attr['timeout'];
        
        // add some js files
        $jquery = ( ! @$GLOBALS['sb_no_jquery'] ? array('jquery') : array() );
        if ( $is_timeline ) {
            wp_enqueue_script( 'sb-timeline', plugins_url( 'public/js/sb-timeline.js', SB_FILE ), $jquery, SB_VERSION, true);
        } else {
            if ( $is_feed ) {
                if (@$attr['carousel'])
                    wp_enqueue_script( 'sb-lightslider', plugins_url( 'public/js/sb-carousel.js', SB_FILE ), $jquery, SB_VERSION, true);
                else
                    wp_register_script( 'sb-newsticker', plugins_url( 'public/js/sb-rotating.js', SB_FILE ), $jquery, SB_VERSION, true);
            } else {
                wp_enqueue_script( 'sb-wall', plugins_url( 'public/js/sb-wall.js', SB_FILE ), $jquery, SB_VERSION, true);
            }
        }
        
        if ( $is_feed ) {
            if (@$attr['carousel']) {
                $class[] = 'sb-carousel';
                if (@$args['widget_id'])
                    $class[] = 'sb-widget';
            } else {
                $class[] = 'sb-widget';
                wp_enqueue_script( 'sb-newsticker' );
            }
        } elseif ($is_wall) {
            $class[] = 'sb-wall';
        }

        // set the block height
        $block_height = (@$attr['height']) ? $attr['height'] : 400;

        // if slideshow is active
        if (@$attr['lightboxtype'] == 'slideshow') {
            $slideshow = true;
            $class[] = 'sb-slideshow';
        }
        
        // load layout
        $layouts_path = SB_DIRNAME . '/layout/'.$themeoption['layout'].'.php';
        if ( ! file_exists($layouts_path) ) {
			$upload_dir = wp_upload_dir();
			$layouts_path = $upload_dir['basedir'].'/social-board/layouts/'.$themeoption['layout'].'.php';
        }
        include_once($layouts_path);
        if ( $themeoption['classname'] != $themeoption['layout'])
            $class[] = 'sb-'.$themeoption['layout'];

        $layoutclass = $themeoption['layout'].'Layout';
        $layoutobj = new $layoutclass;
        
        // load slide layout
        if ( isset($slideshow) ) {
            include_once( SB_DIRNAME . '/layout/slide/default.php' );
            $slidelayoutobj = new defaultSlideLayout;
        }
        
        $target = '';
        // nofollow links
        if (@$attr['nofollow'])
            $target .= ' rel="nofollow"';
        
        // open links in new window
        if (@$attr['links'])
            $target .= ' target="_blank"';
        $this->target = $layoutobj->target = $target;
        if ( isset($slideshow) )
            $slidelayoutobj->target = $target;
        
        // use https
        $protocol = (@$attr['https']) ? 'https' : 'http';
        // set output
        if ( ! empty($this->sboption['output']) ) {
            if ( ! is_array($this->sboption['output']) )
                $feedoutput = sb_explode($this->sboption['output']);
            else
				$feedoutput = $this->sboption['output'];
		} else {
			$feedoutput = array('title' => true, 'thumb' => true, 'text' => true, 'comments' => true, 'likes' => true, 'user' => true, 'share' => true, 'info' => true, 'stat' => true, 'meta' => true, 'tags' => false);
		}
        $layoutobj->output = $feedoutput;
        if ( isset($slideshow) )
            $slidelayoutobj->output = $feedoutput;
        
        if ( ! $ajax_feed) {
        // do some styling stuffs
        $dotboard = "#timeline_$label.sboard";
        if (@$themeoption['social_colors']) {
            $style = $layoutobj->sb_create_colors($themeoption['social_colors'], $filters_order, $type, $dotboard, $attr, @$themeoption["section_$typeoption"]);
        }
        
        if ($is_wall) {
            $dotitem2 = '.sb-item';
            $sbitem2 = "$dotboard $dotitem2";
            $sbgutter2 = "$dotboard .sb-gsizer";
            $sbgrid2 = "$dotboard .sb-isizer";
            $gutterX = (@$attr['gutterX']) ? $attr['gutterX'] : 10;
            $gutterY = (@$attr['gutterY']) ? $attr['gutterY'] : 10;
            if (@$attr['itemwidth']) {
                $itemwidth = (@$attr['itemwidth'] ? $attr['itemwidth'] : $defoption['wallsetting']['itemwidth']);
            }
            // if wall is set to block
            if (@$attr['fixWidth'] == 'false' || @$attr['fixWidth'] == 'block') {
                if ( ! @is_array($attr['breakpoints']) ) {
                    if (@$attr['breakpoints'] == '')
                        $attr['breakpoints'] = $defoption['wallsetting']['breakpoints'];
                    else
                        $attr['breakpoints'] = explode(',', trim($attr['breakpoints']) );
                }

                // calculate breakpoints
                $bpsizes = array(1200, 960, 768, 600, 480, 320, 180);
                foreach ($attr['breakpoints'] as $bpkey => $breakpoint) {
                    $gut = number_format( ($gutterX * 100) / $bpsizes[$bpkey], 3, '.', '');
                    $yut = number_format( ($gutterY * 100) / $bpsizes[$bpkey], 3, '.', '');
                    $bpyut = number_format($bpsizes[$bpkey] / (100/$yut), 3, '.', '');
                    $tw = number_format(100 - ( ($breakpoint - 1) * $gut), 3, '.', '');
                    if ($tw < 100) {
                        $bpgrid = number_format($tw / $breakpoint, 3, '.', '');
                        $bpgut = $gut;
                        $bpgridtwo = number_format( ($bpgrid * 2) + $bpgut, 3, '.', '');
                        $bpgridthree = number_format( ($bpgrid * 3) + ($bpgut * 2), 3, '.', '');
                    } else {
                        $bpgrid = 100;
                        $bpgut = 0;
                        $bpgridtwo = $bpgridthree = 100;
                    }
                    if (@$attr['fixWidth'] == 'false') {
	                    $bpcol[$bpkey] = "$sbitem2, $sbgrid2 { width: $bpgrid%; margin-bottom: {$bpyut}px; }
	                    $sbitem2.sb-twofold { width: $bpgridtwo%; }
	                    $sbitem2.sb-threefold { width: $bpgridthree%; }
	                    $sbgutter2 { width: $bpgut%; }";
                    } else {
	                    $bpcol[$bpkey] = '$("'.$sbitem2.', '.$sbgrid2.'").css({ "width": "'.$bpgrid.'%", "margin-bottom": "'.$bpyut.'px" });
	                    $("'.$sbitem2.'.sb-twofold").css({ "width": "'.$bpgridtwo.'%" });
	                    $("'.$sbitem2.'.sb-threefold").css({ "width": "'.$bpgridthree.'%" });
	                    $("'.$sbgutter2.'").css({ "width": "'.$bpgut.'%" });';
					}
                }
                
                if (@$attr['fixWidth'] == 'false') {
                	$mediaqueries = "$bpcol[0]
@media (min-width: 960px) and (max-width: 1200px) { $bpcol[1] }
@media (min-width: 768px) and (max-width: 959px) { $bpcol[2] }
@media (min-width: 600px) and (max-width: 767px) { $bpcol[3] }
@media (min-width: 480px) and (max-width: 599px) { $bpcol[4] }
@media (min-width: 320px) and (max-width: 479px) { $bpcol[5] }
@media (max-width: 319px) { $bpcol[6] }";
				}
            } else {
                $style[$sbitem2][] = 'width: '.$itemwidth.'px';
                $style[$sbitem2][] = 'margin-bottom: '.$gutterY.'px';
            }
        } else {
            if (@$attr['carousel']) {
                $dotitem2 = '.lslide';
                if (@$attr['cs_width']) {
                    $itemwidth = $attr['cs_width'];
                    $style["$dotboard $dotitem2"][] = 'width: '.$itemwidth.'px';
                }
            }
        }
        
        if ( $font_size = @$themeoption['font_size'] ) {
            $style["$dotboard, $dotboard a"][] = 'font-size: '.$font_size.'px';
            $style["$dotboard .sb-heading"][] = 'font-size: '.($font_size+1).'px !important';
        }
        
        if ( $is_feed && @$themeoption["section_$typeoption"]['title_background_color'] )
        if ( $themeoption["section_$typeoption"]['title_background_color'] != 'transparent') {
            $style["$dotboard .sb-heading, $dotboard .sb-opener"][] = 'background-color: '.$themeoption["section_$typeoption"]['title_background_color'].' !important';
        }
        if ( $is_feed && @$themeoption["section_$typeoption"]['title_color'] )
        if ( $themeoption["section_$typeoption"]['title_color'] != 'transparent')
            $style["$dotboard .sb-heading"][] = 'color: '.$themeoption["section_$typeoption"]['title_color'];
        
        if ( $is_feed )
            $csskey = "$dotboard .sb-content, $dotboard .toolbar";
        else
            $csskey = '#sb_'.$label;
            
        if ( @$themeoption["section_$typeoption"]['background_color'] ) {
            if ( $themeoption["section_$typeoption"]['background_color'] != 'transparent') {
                $bgexist = true;
                $style[$csskey][] = 'background-color: '.$themeoption["section_$typeoption"]['background_color'];
            }
        }
        
        if ( $is_timeline )
            $fontcsskey = "$dotboard .timeline-row";
        else
            $fontcsskey = "$dotboard .sb-item";
            
        if (@$themeoption["section_$typeoption"]['font_color'])
        if ($themeoption["section_$typeoption"]['font_color'] != 'transparent') {
            $rgbColorVal = sb_hex2rgb($themeoption["section_$typeoption"]['font_color']); // returns the rgb values separated by commas
            
            if ( $is_timeline ) {
                $style["$dotboard .timeline-row small"][] = 'color: '.$themeoption["section_$typeoption"]['font_color'];
            }
            
            $style["$fontcsskey .sb-title a"][] = 'color: '.$themeoption["section_$typeoption"]['font_color'];
            $style["$fontcsskey"][] = 'color: rgba('.$rgbColorVal.', 0.8)';
        }
        if (@$themeoption["section_$typeoption"]['link_color'])
        if ($themeoption["section_$typeoption"]['link_color'] != 'transparent') {
            $rgbColorVal = sb_hex2rgb($themeoption["section_$typeoption"]['link_color']); // returns the rgb values separated by commas
            $style["$fontcsskey a"][] = 'color: '.$themeoption["section_$typeoption"]['link_color'];
            $style["$fontcsskey a:visited"][] = 'color: rgba('.$rgbColorVal.', 0.8)';
        }

        if ( @$themeoption["section_$typeoption"]['background_image'] ) {
            $bgexist = true;
            $cssbgkey = $csskey;
            if ( $is_feed )
                $cssbgkey = "$dotboard .sb-content";
            $style[$cssbgkey][] = 'background-image: url('.$themeoption["section_$typeoption"]['background_image'].');background-repeat: repeat';
        }

        $location = null;
        if ( $is_feed ) {
            $class[] = @$attr['position'];
            if ( @$attr['position'] != 'normal' ) {
                $class[] = @$attr['location'];
                if ( ! @$attr['autoclose'] ) {
                    $class[] = 'open';
                    $active = ' active';
                }
                
                $locarr = explode('_', str_replace('sb-', '', @$attr['location']) );
                $location = $locarr[0];
            }
        }

        if (@$attr['carousel'] && @$attr['tabable'])
            unset($attr['tabable']);
            
        if (@$attr['carousel'])
            $attr['layout_image'] = 'imgnormal';
            
        if (@$attr['tabable'])
            $class[] = 'tabable';
        
        if ( (@$attr['filters'] or @$attr['controls']) && !@$attr['carousel']) {
            $style[$dotboard.' .sb-content'][] = 'border-bottom-left-radius: 0 !important;border-bottom-right-radius: 0 !important';
        }
        if ( (@$attr['showheader'] || ($location == 'bottom' && ! @$attr['tabable']) ) && $is_feed) {
            $style[$dotboard.' .sb-content'][] = 'border-top: 0 !important;border-top-left-radius: 0 !important;border-top-right-radius: 0 !important';
        }
        if ( $location == 'left' )
            $style[$dotboard.' .sb-content'][] = 'border-top-left-radius: 0 !important';
        if ( $location == 'right' )
            $style[$dotboard.' .sb-content'][] = 'border-top-right-radius: 0 !important';
        
    	// set block border
        if ( @$themeoption["section_$typeoption"]['border_color'] ) {
            if ( $themeoption["section_$typeoption"]['border_color'] != 'transparent') {
                $bgexist = true;
                if ( $is_feed ) {
                    $style[$dotboard.' .toolbar'][] = 'border-top: 0 !important';
                }
                $style[$csskey][] = 'border: '.@$themeoption["section_$typeoption"]['border_size'].'px solid '.$themeoption["section_$typeoption"]['border_color'];
            }
        } else {
            if (@$attr['carousel'])
                $style[$dotboard.' .sb-content'][] = 'padding: 10px 0 5px 0';
        }
        
        // set block padding if required
        if (@$bgexist) {
            $border_radius = @$themeoption["section_$typeoption"]['border_radius'];
            if ( ! $is_feed ) {
                if ($border_radius == '')
                    $border_radius = 7;
            }
            if ($border_radius != '') {
                $radius = 'border-radius: '.$border_radius.'px;-moz-border-radius: '.$border_radius.'px;-webkit-border-radius: '.$border_radius.'px';
                if ( ! $is_feed ) {
                    $style['#sb_'.$label][] = $radius;
                } else {
                    if ($location == 'bottom') {
                        $radiusval = ': '.$border_radius.'px '.$border_radius.'px 0 0;';
                        $style["$dotboard .sb-content, $dotboard.sb-widget, $dotboard .sb-heading"][] = 'border-radius'.$radiusval.'-moz-border-radius'.$radiusval.'-webkit-border-radius'.$radiusval;
                    } else {
                        $style["$dotboard .sb-content, $dotboard.sb-widget"][] = $radius;
                        $style[$dotboard.' .toolbar'][] = 'border-radius: 0 0 '.$border_radius.'px '.$border_radius.'px;-moz-border-radius: 0 0 '.$border_radius.'px '.$border_radius.'px;-webkit-border-radius: 0 0 '.$border_radius.'px '.$border_radius.'px';
                    }
                }
            }
            if ( $is_wall )
                $style['#sb_'.$label][] = 'padding: 10px';
        }

        if ($is_feed) {
            if (@$attr['width'] != '')
                $style["$dotboard"][] = 'width: '.$attr['width'].'px';
        }
        
        if ( @$attr['height'] != '' && ! $is_feed ) {
            $style[$csskey][] = 'height: '.$attr['height'].'px';
            if ( ! $is_feed ) {
                $style[$csskey][] = 'overflow: scroll';
                if ( $is_timeline )
                    $style[$csskey][] = 'padding-right: 0';
                $style[$dotboard][] = 'padding-bottom: 30px';
            }
        }
        } // end no ajax
        
        if ( @$theme ) {
            $class['theme'] = $themeoption['classname'];
        }
        
    	if ( ! $order = $attr['order'] )
            $order = 'date';
        
        $output = $ss_output = '';
        if ( ! $ajax_feed)
            $output .= "\n<!-- Social Board Plugin By Axent Media -->\t";

    	// Make sure feeds are getting local timestamps
    	if ( get_option( 'timezone_string' ) && strpos( get_option( 'timezone_string' ), 'UTC' ) === false )
    		date_default_timezone_set( get_option( 'timezone_string' ) );

        $GLOBALS['islive'] = false;
        if (@$attr['live']) {
            if (@$attr['live'] == 'users') {
                if ( is_user_logged_in() )
                    $GLOBALS['islive'] = true;
            } elseif (@$attr['live'] == 'all') {
                $GLOBALS['islive'] = true;
            }
        }
        
        if ($GLOBALS['islive']) {
            // Live update need cache to be disabled
            $forceCrawl = true;
        } else {
            // If a cache time is set in the admin AND the "cache" folder is writeable, set up the cache.
            if ( $attr['cache'] > 0 && is_writable( SB_DIRNAME . '/cache/' ) ) {
                $cache->cache_path = SB_DIRNAME . '/cache/';
                $cache->cache_time = $attr['cache'] * 60;
        		$forceCrawl = false;
        	} else {
        		// cache is not enabled, call local class
                $forceCrawl = true;
        	}
        }

        // if is ajax request
        if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if (@$_SESSION["$label-temp"]) {
            	$_SESSION[$label] = $_SESSION["$label-temp"];
                $_SESSION["$label-temp"] = array();
            }
            if (@$_REQUEST['action'] == "sb_liveupdate") {
                $_SESSION["$label-temp"] = @$_SESSION[$label];
                $_SESSION[$label] = array();
            }
        } else {
            $_SESSION[$label] = array();
            unset($_SESSION["$label-temp"]);
        }

        // Check which feeds are specified
        $feeds = array();
        foreach ($this->feed_keys as $value) {
            $key = key($value);
            for ($i = 1; $i <= 5; $i++) {
        		if ( $keyitems = @$this->sboption['section_'.$key][$key.'_id_'.$i] ) {
                    foreach ($keyitems as $key2 => $eachkey) {
                        if ( (@$_REQUEST['action'] == "sb_loadmore" && @$_SESSION[$label]['loadcrawl']) && ! @$_SESSION[$label]['loadcrawl'][$key.$i.$key2])
                            $load_stop = true;
                        else
                            $load_stop = false;
                        if ( ! @$load_stop) {
                        if ( $eachkey != '') {
         			        if ( ! @$attr['tabable'] || $ajax_feed ) {
                                if ( $crawl_limit && $GLOBALS['crawled'] >= $crawl_limit )
                                    break;
                                if ( $feed_data = $this->sb_get_feed( $key, $i, $key2, $eachkey, $results, $setoption, $this->sboption['section_'.$key], $cache, $forceCrawl, $label ) ) {
                                    $filteractive = (@$attr['default_filter'] == $key) ? ' active' : '';
                                    $feeds[$key][$i][$key2] = $feed_data;
                                    if (@$value[$key])
                                    	$filterItems[$key] = ($is_feed) ? '<span class="sb-hover sb-'.$key.$filterlabel.$filteractive.'" data-filter=".sb-'.$key.'"><i class="sb-micon sb-'.$key.'"></i></span>' : '<span class="sb-hover sb-'.$key.$filterlabel.$filteractive.'" data-filter=".sb-'.$key.'"><i class="sb-icon sb-'.$key.'"></i></span>';
                                }
                            } else {
                                $activeTab = '';
                                if ( @$attr['position'] == 'normal' || (@$attr['slide'] && ! @$attr['autoclose']) ) {
                                    if ( ! isset($fistTab) ) {
                                        if ( $feed_data = $this->sb_get_feed( $key, $i, $key2, $eachkey, $results, $setoption, $this->sboption['section_'.$key], $cache, $forceCrawl, $label ) ) {
                                            $feeds[$key][$i][$key2] = $feed_data;
                                        }
                                        $fistTab = true;
                                        $activeTab = ' active';
                                    }
                                }
                                
                                $fi = '
                    			<li class="'.$key.@$activeTab.'" data-feed="'.$key.'">';
                                if (@$attr['position'] == 'normal') {
                                    $fi .= '
                                    <span><i class="sb-icon sb-'.$key.'"></i></span>';
                                } else {
                                	$fi .= '
                                    <i class="sb-icon sb-'.$key.'"></i>';
                                    if ( $location != 'bottom' )
                                        $fi .= ' <span>'.ucfirst($key).'</span>';
                                }
                    			$fi .= '</li>';
                                $filterItems[$key] = $fi;
                            }
                        }
                        }
                    }
                }
            }
        }
        
        // set wall custom size if defined
        if ($is_wall) {
            if (@$attr['wall_height'] != '')
            	$style['#sb_'.$label][] = 'height: '.$attr['wall_height'].'px;overflow-y: scroll';
            if (@$attr['wall_width'] != '')
            	$style['#sb_'.$label][] = 'width: '.$attr['wall_width'].'px';
		}
		
        // set timeline style class
        if ( $is_timeline ) {
            $class[] = ($attr['onecolumn'] == 'true') ? 'timeline onecol' : 'timeline';
            $class[] = 'animated';
        }
        
        if ( ! $ajax_feed) {
            if ( @$style ) {
                $output .= '<style type="text/css">';
                if ( @$themeoption['custom_css'] )
                    $output .= $themeoption['custom_css']."\n";
                if ( @$mediaqueries )
                    $output .= $mediaqueries."\n";
                foreach ($style as $stKey => $stItem) {
                    $output .= $stKey.'{'.implode(';', $stItem).'}';
                }
                $output .= '</style>';
            }

            if ($is_wall || $is_timeline)
                $output .= '<div id="sb_'.$label.'">';
                
            if ( ! $is_feed && (@$attr['position'] == 'normal' || ! $is_timeline && ! @$attr['tabable'] ) ) {
                $filter_box = '';
                if ( ! empty($this->sboption['filtering_tabs']) ) {
					foreach ($this->sboption['filtering_tabs'] as $filtertab) {
						$filteractive = (@$filtertab['default_filter']) ? ' active' : '';
						if (@$filtertab['search_term'])
							$filterItems[] = '<span class="sb-hover sb-filter'.$filteractive.'" data-filter="'.$filtertab['search_term'].'"><i class="sb-filter-icon">'.(@$filtertab['filter_icon'] ? '<img src="' . $filtertab['filter_icon'] . '" alt="" title="'.@$filtertab['tab_title'].'">' : @$filtertab['tab_title']).'</i></span>';
					}
				}
                if ( @$attr['filters'] && @$filterItems ) {
                    if (@$attr['display_all'] != 'disabled') {
                    	$display_all = '<span class="sb-hover filter-label'.(@$attr['default_filter'] ? '' : ' active').'" data-filter="*" title="'.__( 'Show All', 'social-board' ).'"><i class="sb-icon sb-ellipsis-h"></i></span>';
	                    if (@$attr['display_all'] == '1')
	                    	$display_all_last = @$display_all;
	                    else
                    		$display_all_first = @$display_all;
                    }
                    $filter_box .= @$display_all_first.implode("\n", $filterItems).@$display_all_last;
                }
                if ( @$attr['filter_search'] )
                    $filter_box .= '<input type="text" class="sb-search" placeholder="'.__( 'Search...', 'social-board' ).'" />';
                if ($filter_box) {
                    $output .= '
            		<div class="filter-items sb-'.$themeoption['layout'].'">
                        '.$filter_box.'
            		</div>';
                }
            }

            $output .= '<div' . @$attr_id . ' class="' . @implode(' ', $class) . '" data-columns>' . "\n";
            if ($is_wall) {
                $output .= '<div class="sb-gsizer"></div><div class="sb-isizer"></div>';
            }
            if ( $is_feed ) {
                if (@$attr['tabable']) {
                        $minitabs = ( count($filterItems) > 5 ) ? ' minitabs' : '';
                        $output .= '
                        <div class="sb-tabs'.$minitabs.'">
                    		<ul class="sticky" data-nonce="'.wp_create_nonce( 'tabable' ).'">
                            '.implode("\n", $filterItems).'
                    		</ul>
                    	</div>';
                }
                if ( $is_feed ) {
                    if ( ! @$attr['tabable'] && @$attr['slide'] ) {
                        if ( $location == 'left' || $location == 'right' ) {
                            $opener_image = (@$themeoption["section_$typeoption"]['opener_image']) ? $themeoption["section_$typeoption"]['opener_image'] : plugins_url( 'public/img/opener.png', SB_FILE );
                            $output .= '<div class="sb-opener'.@$active.'" title="'.@$attr['label'].'"><img src="'.$opener_image.'" alt="" /></div>';
                        } else {
                            $upicon = '<i class="sb-arrow"></i>';
                        }
                    }
                    if ( @$attr['showheader'] || ($location == 'bottom' && ! @$attr['tabable']) )
                        $output .= '<div class="sb-heading'.@$active.'">'.@$attr['label'].@$upicon.'</div>';
                }
                
                $content_style = (!@$attr['carousel']) ? ' style="height: '.$block_height.'px"' : '';
                $output .= '<div class="sb-content"'.$content_style.'>';
                $output .= '<ul id="ticker_'.$label.'">';
            }
        }

        // Parsing the combined feed items and create a unique feed
        if ( ! empty($feeds) ) {
            foreach ($feeds as $feed_class => $feeditem) {
                foreach ($feeditem as $i => $feeds2) {
                foreach ($feeds2 as $ifeed => $feed) {
                $inner = '';
                // Facebook
                if ($feed_class == 'facebook') {
                    if (@$feed) {
                        $iframe = @$this->sboption['section_facebook']['facebook_iframe'];

                    if ($i != 3 && $i != 4 && ! empty($feed['data']) ) {
                    	$feednext = $feed['next'];
                    	$feed = $feed['data'];
                    }
                    // loop through feeds
                    if (@$feed)
                    foreach ($feed as $data) {
                    if (@$data) {
                        if ($i == 3 || $i == 4) {
                            $loadcrawl[$feed_class.$i.$ifeed] = @$data->paging->cursors->after;
                            $data = $data->data;
                        }
                    
                    // loop through feed items
                    foreach ($data as $entry) {
                        $url = $play = $text = $object_id = $source = $image2 = $mediasize = $embed = $type2 = '';
                        // create link
                        $idparts = @explode('_', @$entry->id);
                        if ( @count($idparts) > 1 )
                            $link = 'https://www.facebook.com/'.$idparts[0].'/posts/'.$idparts[1];
                        elseif (@$entry->from->id && @$entry->id)
                            $link = 'https://www.facebook.com/'.$entry->from->id.'/posts/'.$entry->id;
                        
                        if ( ! $link)
                            $link = @$entry->link;
                        if ( ! $link)
                            $link = @$entry->source;
                        
                        if ( $this->make_remove($link) ) {
                        
                        // body text
                        $content = array();
                        if (@$entry->message)
                            $content[] = $entry->message;
                        if (@$entry->description)
                            $content[] = $entry->description;
                        if (@$entry->story)
                            $content[] = $entry->story;
                        $content = implode(" \n", $content);
                        $text = (@$attr['words']) ? $this->word_limiter($content, $link) : @$this->format_text($content);
                        
                        // comments
                        $count = 0;
                        $comments_data = '';
                        $comments_count = ( @$this->sboption['section_facebook']['facebook_comments'] > 0 ) ? $this->sboption['section_facebook']['facebook_comments'] : 0;
                        if ( ! empty($entry->comments->data) && $comments_count ) {
                            foreach ( $entry->comments->data as $comment ) {
                                $count++;
                                $comment_message = (@$attr['commentwords']) ? $this->word_limiter(nl2br($comment->message), @$link, true) : nl2br($comment->message);
                                $nocommentimg = (@empty($comment->from) ) ? ' sb-nocommentimg' : '';
                                $comments_data .= '<span class="sb-meta sb-mention'.$nocommentimg.'">';
                                if ( ! @empty($comment->from) ) {
                                    $comment_picture = $protocol.'://graph.facebook.com/' . $comment->from->id . '/picture?type=square';
                                    $comments_data .= '<img class="sb-commentimg" src="'.$comment_picture.'" alt="" /><a href="https://www.facebook.com/' . $comment->from->id . '"'.$target.'>' . $comment->from->name . '</a> ';
                                }
                                $comments_data .= $comment_message . '</span>';
                                if ( $count >= $comments_count ) break;
                            }
                        }
                        // likes
                        $count = 0;
                        $likes_data = '';
                        $likes_count = ( @$this->sboption['section_facebook']['facebook_likes'] > 0 ) ? $this->sboption['section_facebook']['facebook_likes'] : 0;
                        if ( ! empty($entry->likes->data) && $likes_count ) {
                            foreach ( $entry->likes->data as $like ) {
                                $count++;
                                $like_title = (@$like->name) ? ' title="' . $like->name . '"' : '';
                                $likes_data .= '<img src="'.$protocol.'://graph.facebook.com/' . $like->id . '/picture?type=square"'.$like_title.' alt="">';
                                if ( $count >= $likes_count ) break;
                            }
                        }
                        
                        $meta = '';
                        if ($comments_data || $likes_data) {
                            $meta .= '
                            <span class="sb-metadata">';
                            if (@$feedoutput['comments'] && $comments_data)
                                $meta .= '
                                <span class="sb-meta">
                                    <span class="comments"><i class="sb-bico sb-comments"></i> '.@$entry->comments->summary->total_count.' '.ucfirst(__( 'comments', 'social-board' )).'</span>
                                </span>
                                ' . $comments_data;
                            if (@$feedoutput['likes'] && $likes_data)
                            $meta .= '
                                <span class="sb-meta">
                                    <span class="likes"><i class="sb-bico sb-star"></i> '.@$entry->likes->summary->total_count.' '.ucfirst(__( 'likes', 'social-board' )).'</span>
                                </span>
                                <span class="sb-meta item-likes">
                                    ' . $likes_data . '
                                </span>';
                            $meta .= '
                            </span>';
                        }
                        
                        $image_width = (@$this->sboption['section_facebook']['facebook_image_width']) ? $this->sboption['section_facebook']['facebook_image_width'] : 300;
                        $source = @$entry->picture;
                        $object_id = @$entry->object_id;
                        if ($iframe) {
                            $url = $source;
                            $image_width_iframe = 800;
                        }
                        if ( ! empty($entry->images) ) {
                            if ($image_width) {
                                // get closest image width
                                $closest = null;
                                foreach ($entry->images as $image) {
                                    if ($closest === null || abs($image_width - $closest) > abs($image->width - $image_width)) {
                                        $closest = $image->width;
                                        $source = $image->source;
                                    }
                                }
                            }
                            // set iframe image
                            if ($iframe) {
                                $closest = null;
                                foreach ($entry->images as $image2) {
                                    if ($closest === null || abs($image_width_iframe - $closest) > abs($image2->width - $image_width_iframe)) {
                                        $closest = $image2->width;
                                        $url = $image2->source;
                                        $mediasize = $image2->width.','.$image2->height;
                                    }
                                }
                            }
                        } else {
                            if (@$entry->attachments) {
                                $type2 = @$entry->attachments->data[0]->type;
                                if ($type2 == 'multi_share') {
									$image2 = @$entry->attachments->data[0]->subattachments->data[0]->media->image;
	                                if (@$image2->src) {
	                                    $source = $image2->src;
	                                }
								} else {
	                                $image2 = @$entry->attachments->data[0]->media->image;
	                                if ( ! $image2 ) {
	                                    $image2 = @$entry->attachments->data[0]->subattachments->data[0]->media->image;
	                                } else {
		                                if ( ! @$mediasize)
		                                	$mediasize = $image2->width.','.$image2->height;
									}
                                }
                            }
                            // get or create thumb
                            if ($image_width > 180 && @$type2 != 'multi_share') {
                                if (@$entry->full_picture) {
                                    $urlArr = explode('&url=', $entry->full_picture);
                                    if ($urlfb = @$urlArr[1]) {
                                        if (stristr($urlfb, 'fbcdn') == TRUE || stristr($urlfb, 'fbstaging') == TRUE) {
                                            $source = $entry->full_picture."&w=$image_width&h=$image_width";
                                        } else {
                                            $urlfbArr = explode('&', $urlfb);
                                            if (@$attr['image_proxy']) {
                                            	$token = md5(@$urlfbArr[0].@$_SERVER['SERVER_ADDR'].@$_SERVER['SERVER_ADMIN'].@$_SERVER['SERVER_NAME'].@$_SERVER['SERVER_PORT'].@$_SERVER['SERVER_PROTOCOL'].@strip_tags($_SERVER['SERVER_SIGNATURE']).@$_SERVER['SERVER_SOFTWARE'].@$_SERVER['DOCUMENT_ROOT']);
                                            	$imgStr = 'resize='.$image_width.'&refresh=3600&token='.$token.'&src='.@$urlfbArr[0];
                                            	$source = esc_url( add_query_arg( 'sbimg', base64_encode($imgStr), site_url('index.php') ) );
                                            } else {
												$source = urldecode(@$urlfbArr[0]);
											}
                                        }
                                    } else {
                                        $source = $entry->full_picture;
                                    }
                                } else {
                                    if ($object_id)
                                        $source = $protocol.'://graph.facebook.com/'.$object_id.'/picture?type=normal';
                                }
                            }

                            if (empty($source) ) {
                                if (@$image2->src)
                                    $source = $image2->src;
                            }

                            // set iframe image
                            if ($iframe) {
                                if (@$type2 == 'multi_share' && @$image2->src) {
                                	$url = $image2->src;
                                } else {
	                                if (@$entry->full_picture) {
	                                    $url = $entry->full_picture;
	                                } else {
	                                    if ($object_id)
	                                        $url = $protocol.'://graph.facebook.com/'.$object_id.'/picture?type=normal';
	                                }
                                }
                            }
                        }
                        
                        // set video
                        if (@$entry->type == 'video' || $i == 4 || stristr($type2, 'animated_image') == TRUE) {
                            $play = true;
                            $video_width = (@$this->sboption['section_facebook']['facebook_video_width']) ? $this->sboption['section_facebook']['facebook_video_width'] : 720;
                            if (@$entry->format) {
                                // get closest video width
                                $videosize = null;
                                foreach ($entry->format as $eformat) {
                                    if (abs($eformat->width) >= abs($video_width) ) {
                                        $videosize = $eformat;
                                        break;
                                    }
                                }
                                if ( ! $videosize)
                                	$videosize = end($entry->format);
                                $mediasize = $videosize->width.','.$videosize->height;
                            }
                            if ( ! @$mediasize)
                                $mediasize = '640,460';
                            if ($iframe) {
                                if (stristr($type2, 'animated_image') == TRUE) {
                                    if (stristr(@$entry->link, '.gif') == TRUE)
                                        $url = @$entry->link;
                                    else {
                                        if (stristr(@$entry->link, 'giphy.com') == TRUE) {
                                            $giphyID = substr($entry->link, strrpos($entry->link, '-') + 1);
                                            $url = 'http://i.giphy.com/'.$giphyID.'.gif';
                                        }
                                    }
                                } else {
                            		$msize = explode(',', $mediasize);
                            		if (@$entry->link)
                            			$vlink = $entry->link;
                            		else {
	                            		$vlink = 'https://www.facebook.com/'.(@$entry->from->id ? $entry->from->id : 'facebook').'/videos/'.$entry->id.'/';
									}
                            		$url = 'https://www.facebook.com/plugins/video.php?href='.urlencode($vlink).'&show_text=0&width='.$msize[0].'&height='.@$msize[1];
                                }
								
								if (stristr(@$entry->source, 'youtube.com/embed') == TRUE) {
									$url = $entry->source;
								} else {
									// if shared from youtube without embed
									if (strpos($url, 'youtube.com') == TRUE or strpos($url, 'youtu.be') == TRUE) {
										if (stristr($url, 'youtube.com/embed') === FALSE) {
											$url = $this->youtube_get_embedurl($url) . '?rel=0&wmode=transparent';
										}
									}
								}
                            }
                        }
                        
                        switch ($i) {
                            case 3:
                                $itemtype = 'image';
                                $type_icon = @$themeoption['type_icons'][4];
                            break;
                            case 4:
                                $itemtype = 'video-camera';
                                $type_icon = @$themeoption['type_icons'][6];
                            break;
                            default:
                                $itemtype = 'pencil';
                                $type_icon = @$themeoption['type_icons'][0];
                            break;
                        }
                        
                        // Set title
                        $title = (@$entry->name) ? $entry->name : (@$entry->title ? $entry->title : '');
                        if (empty($title) ) {
                            if (@$entry->attachments) {
                                $title = @$entry->attachments->data[0]->title;
                            }
                        }

                        $thetime = (@$entry->created_time) ? $entry->created_time : $entry->updated_time;
                        $sbi = $this->make_timestr($thetime, $link);
                        $itemdata = array(
                        'title' => (@$title) ? '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($title) : $title) . '</a>' : '',
                        'thumb' => (@$source) ? $source : '',
                        'thumburl' => $url,
                        'text' => @$text,
                        'meta' => @$meta,
                        'url' => @$link,
                        'iframe' => $iframe ? ( (@$entry->type == 'video' || $i == 4) ? 'iframe' : 'icbox') : '',
                        'date' => $thetime,
                        'user' => array(
                            'name' => @$entry->from->name,
                            'url' => (@$entry->from->id) ? 'https://www.facebook.com/' . $entry->from->id : '',
                            'image' => (@$entry->from->id) ? $protocol.'://graph.facebook.com/' . $entry->from->id . '/picture?type=square' : '',
                            // Status type
                            'status' => (@$entry->status_type) ? __( ucfirst( str_replace('_', ' ', $entry->status_type) ), 'social-board' ) : ''
                            ),
                        'type' => $itemtype,
                        'play' => @$play,
                        'icon' => array(@$themeoption['social_icons'][0], $type_icon)
                        );
							if (@$mediasize && ($iframe || isset($slideshow) ) )
								$itemdata['size'] = $mediasize;
							$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = @$this->format_text($content);
                                if ($url)
                                    $itemdata['thumb'] = $url;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                        }
                    } // end foreach
                    
                    if ($i != 3 && $i != 4) {
                        // facebook get last item date
                        if ( ! empty($feednext) )
                        	$loadcrawl[$feed_class.$i.$ifeed] = strtotime($thetime)-1;
                    }
                    
                    } // end $data
                    }
                    }
                }
        		// Twitter
                elseif ( $feed_class == 'twitter' ) {
                    if (@$feed) {
                        $iframe = @$this->sboption['section_twitter']['twitter_iframe'];
                        // if search/tweets
                        if ($i == 3) {
                    		$feed = $feed->statuses;
                    	}
	                    foreach ( $feed as $data ) {
	                        if ( isset($data->created_at) ) {
		                        if (@$_SESSION[$label]['loadcrawl'][$feed_class.$i.$ifeed] == $data->id_str)
		                            continue;
	                        $link = $protocol.'://twitter.com/' . $data->user->screen_name . '/status/' . $data->id_str;
	                        if ( $this->make_remove($link) ) {
								$url = $thumb = $mediasize = $play = $meta = '';
	                            if ( ! $status_text = @$data->retweeted_status->text)
	                                $status_text = @$data->text;
	                            if ( ! $status_full_text = @$data->retweeted_status->full_text)
	                                $status_full_text = @$data->full_text;
	                            $text = (@$status_text) ? $status_text : $status_full_text;
	                            $text = str_replace("\n", " \n", $text);
	                            $text = (@$attr['words']) ? $this->word_limiter($text) : @$this->format_text($text);
								$text = $this->twitter_add_links($text);

		                        // get image
		                        $entities = (@$data->entities) ? @$data->entities : @$data->extended_entities;
		                        if ($mediaobj = @$entities->media[0]) {
		                            $twitter_images = @$this->sboption['section_twitter']['twitter_images'];
		                            $media_url = (@$attr['https']) ? $mediaobj->media_url_https : $mediaobj->media_url;
		                            $thumb = $media_url . ':' . $twitter_images;
		                            if ($iframe) {
		                                $url = $media_url . ':large';
		                                $mediasize = $mediaobj->sizes->large->w.','.$mediaobj->sizes->large->h;
		                            }
		                        }

		                        // get video
		                        if ($extmediaobj = @$data->extended_entities->media[0]) {
		                            if (@$extmediaobj->type == 'video' || @$extmediaobj->type == 'animated_gif') {
		                                $play = true;
		                                if ($iframe) {
				                            $twitter_videos = (@$this->sboption['section_twitter']['twitter_videos']) ? $this->sboption['section_twitter']['twitter_videos'] : 'small';
				                            if ($videosize = @$extmediaobj->sizes->$twitter_videos) {
				                                // get the video size
				                                $mediasize = $videosize->w.','.$videosize->h;
				                            }
		                                    $url = 'https://twitter.com/i/videos/tweet/'.$data->id_str;
		                                }
		                            }
		                        }

		                        // set user
		                        if ( ! $user = @$data->retweeted_status->user) {
		                            $user = $data->user;
		                        }
	                        
	                        if (@$data->retweeted_status) {
	                            $meta .= '
	                            <span class="sb-metadata">
	                                <span class="sb-meta sb-tweet">
	                                    <a href="https://twitter.com/' . $data->user->screen_name . '"'.$target.'><i class="sb-bico sb-retweet"></i> ' . $data->user->name . ' '.ucfirst(__( 'retweeted', 'social-board' )).'</a>
	                                </span>
	                            </span>';
	                        }
	                        
	                        $sbi = $this->make_timestr($data->created_at, $link);
	                        $itemdata = array(
	                        'thumb' => $thumb,
	                        'thumburl' => $url,
	                        'iframe' => $iframe ? (@$play ? 'iframe' : 'icbox') : '',
	                        'text' => $text,
	                        'meta' => @$meta,
	                        'share' => (@$feedoutput['share']) ? '
	                        <span class="sb-share sb-tweet">
	                            <a href="https://twitter.com/intent/tweet?in_reply_to=' . $data->id_str . '&via=' . $data->user->screen_name . '"'.$target.'><i class="sb-bico sb-reply"></i></a>
	                            <a href="https://twitter.com/intent/retweet?tweet_id=' . $data->id_str . '&via=' . $data->user->screen_name . '"'.$target.'><i class="sb-bico sb-retweet"></i> ' . $data->retweet_count . '</a>
	                            <a href="https://twitter.com/intent/favorite?tweet_id=' . $data->id_str . '"'.$target.'><i class="sb-bico sb-star-o"></i> ' . $data->favorite_count . '</a>
	                        </span>' : null,
	                        'url' => $link,
	                        'date' => $data->created_at,
	                        'user' => array(
	                            'name' => '@'.$user->screen_name,
	                            'url' => 'https://twitter.com/'.$user->screen_name,
	                            'title' => $user->name,
	                            'image' => (@$attr['https']) ? $user->profile_image_url_https : $user->profile_image_url
	                            ),
	                        'type' => 'pencil',
	                        'play' => @$play,
	                        'icon' => array(@$themeoption['social_icons'][1], @$themeoption['type_icons'][0])
	                        );
								if (@$mediasize && ($iframe || isset($slideshow) ) )
									$itemdata['size'] = $mediasize;
								$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
	                            if ( isset($slideshow) ) {
	                                $text = (@$data->text) ? $data->text : $data->full_text;
	                                $text = @$this->format_text($text);
	                                $itemdata['text'] = $this->twitter_add_links($text);
									$itemdata['size'] = @$mediasize;
	                                if ($url)
	                                    $itemdata['thumb'] = $url;
	                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
	                            }
	                        }
	                        }
	                    } // end foreach
	                    
                    	// twitter get last id
                        if (@$_SESSION[$label]['loadcrawl'][$feed_class.$i.$ifeed] != @$data->id_str)
                            $loadcrawl[$feed_class.$i.$ifeed] = @$data->id_str;
                    }
        		}
                // Google+
                elseif ( $feed_class == 'google' ) {
                    $keyTypes = array( 'note' => array('pencil', 0), 'article' => array('edit', 1), 'activity' => array('quote-right', 2), 'photo' => array('image', 5), 'video' => array('video-camera', 6) );
                    $iframe = @$this->sboption['section_google']['google_iframe'];
                    
                    // google next page
                    $loadcrawl[$feed_class.$i.$ifeed] = @$feed->nextPageToken;
                    
                    if (@$feed->items) {
                    foreach ($feed->items as $item) {
                        $url = $play = $text = $textlong = $content = $contentlong = $image_url = $mediasize = '';
                        $link = @$item->url;
                        if ( $this->make_remove($link) ) {
                            // get text
                            if ($attachments = @$item->object->attachments[0]) {
                                $image_url = @$attachments->image->url;
                                if ($iframe && @$attachments->fullImage) {
                                    $url = @$attachments->fullImage->url;
                                    $mediasize = @$attachments->fullImage->width.','.@$attachments->fullImage->height;
                                }
                                if ($iframe && ! $mediasize) {
                                    $mediasize = @$attachments->image->width.','.@$attachments->image->height;
                                }
                                
                                if (@$attachments->objectType == 'photo') {
                                    if (@$attachments->displayName) {
                                        $text = (@$attr['words']) ? $this->word_limiter($attachments->displayName, $link) : $this->format_text($attachments->displayName);
                                        if ( isset($slideshow) )
                                            $textlong = @$this->format_text($attachments->displayName);
                                    }
                                } else {
                                    if (@$attachments->content)
                                        $content = (@$attr['words']) ? $this->word_limiter($attachments->content, $link) : @$this->format_text($attachments->content);
                                    $text = '<span class="sb-title"><a href="' . $attachments->url . '"'.$target.'>'.$attachments->displayName.'</a></span>'.@$content;
                                    
                                    if ( isset($slideshow) ) {
                                        if (@$attachments->content)
                                            $contentlong = @$this->format_text($attachments->content);
                                        $textlong = '<span class="sb-title"><a href="' . $attachments->url . '"'.$target.'>'.$attachments->displayName.'</a></span>'.@$contentlong;
                                    }
                                }
                                
                                if (@$attachments->objectType == 'video') {
                                    if (@$attachments->embed && $iframe) {
                                        $play = true;
                                        $url = $attachments->embed->url;
                                        // add 30% to media size
                                        $medias = explode(',', $mediasize);
                                        $mediasize = number_format($medias[0] * 1.3, 0, '.', '').','.number_format($medias[1] * 1.3, 0, '.', '');
                                    } else {
                                        $url = $image_url;
                                    }
                                }
                            } else {
                                $text = (@$attr['words']) ? $this->word_limiter($item->object->content, $link) : @$this->format_text($item->object->content);
                                if ( isset($slideshow) )
                                    $textlong = @$this->format_text($item->object->content);
                            }
                            
							$title = (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title);
							
							if ($title || $image_url || $text) {
							$sbi = $this->make_timestr($item->updated, $link);
							$itemdata = array(
							'thumb' => (@$image_url) ? $image_url : '',
							'thumburl' => $url,
							'title' => '<a href="' . $link . '"'.$target.'>' . $title . '</a>',
							'text' => $text,
							'iframe' => $iframe ? (@$play ? 'iframe' : 'icbox') : '',
							'meta' => (@$feedoutput['stat']) ? '
							<span class="sb-text">
								<span class="sb-meta">
									<span class="plusones">+1s ' . $item->object->plusoners->totalItems . '</span>
									<span class="shares"><i class="sb-bico sb-users"></i> ' . $item->object->resharers->totalItems . '</span>
									<span class="comments"><i class="sb-bico sb-comment"></i> ' . $item->object->replies->totalItems . '</span>
								</span>
							</span>' : null,
							'url' => @$link,
							'date' => @$item->published,
							'user' => array(
								'name' => $item->actor->displayName,
								'url' => $item->actor->url,
								'image' => @$item->actor->image->url
								),
							'type' => $keyTypes[$item->object->objectType][0],
							'play' => @$play,
							'icon' => array(@$themeoption['social_icons'][2], @$themeoption['type_icons'][$keyTypes[$item->object->objectType][1]])
							);
								if (@$mediasize && ($iframe || isset($slideshow) ) )
									$itemdata['size'] = $mediasize;
								$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
								if ( isset($slideshow) ) {
									$itemdata['text'] = $textlong;
									if ($url)
										$itemdata['thumb'] = $url;
									$this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
								}
							}
                        }
                    } // end foreach
                    }
                }
                elseif ( $feed_class == 'tumblr' ) {
                    $keyTypes = array( 'text' => array('pencil', 0), 'quote' => array('quote-right', 2), 'link' => array('link', 3), 'answer' => array('reply', 1), 'video' => array('video-camera', 6), 'audio' => array('youtube-play', 7), 'photo' => array('image', 4), 'chat' => array('comment', 9) );
                    $iframe = @$this->sboption['section_tumblr']['tumblr_iframe'];
                    
                    // tumblr next page start
                    $total_posts = @$feed->response->total_posts;
                    $loadcrawl[$feed_class.$i.$ifeed] = (@$_SESSION[$label]['loadcrawl'][$feed_class.$i.$ifeed]) ? $_SESSION[$label]['loadcrawl'][$feed_class.$i.$ifeed] + $results : $results;
                    
                    // blog info
                    $blog = $feed->response->blog;
                    
                    if (@$feed->response->posts) {
                        
                    foreach ($feed->response->posts as $item) {
                        $title = $thumb = $text = $textlong = $body = $object = $tags = $url = $mediasrc = $mediasize = $play = '';
                        
                        $link = @$item->post_url;
                        if ( $this->make_remove($link) ) {
                        
                        // tags
                        if ( @$feedoutput['tags'] ) {
                            if ( @$item->tags ) {
                                $tags = implode(', ', $item->tags);
                            }
                        }
                        
                        if ( @$item->title ) {
                            $title = '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>';
                        }
                        
                        // set image
                        if ($photoItem = @$item->photos[0]) {
                            if (@$photoItem->alt_sizes) {
                                foreach ($photoItem->alt_sizes as $photo) {
                                    if ($photo->width == @$this->sboption['section_tumblr']['tumblr_thumb'])
                                        $thumb = $photo->url;
                                }
                            }
                            // set iframe image
                            if ($iframe) {
                                if ($original = @$photoItem->original_size) {
                                    $url = $mediasrc = $original->url;
                                    $mediasize = $original->width.','.$original->height;
                                }
                            }
                        }
                        
                        if ($item->type == 'photo') {
                            $body = @$item->caption;
                        }
                        elseif ($item->type == 'video') {
                            $url = (@$item->video_type == 'tumblr') ? @$item->video_url : @$item->permalink_url;
                            if (@$item->thumbnail_url)
                                $thumb = $item->thumbnail_url;
                                
                            if ($iframe) {
                                // set player
                                if (@$item->player) {
                                    foreach ($item->player as $player) {
                                        if ($player->width == @$this->sboption['section_tumblr']['tumblr_video']) {
                                            $object = $player->embed_code;
											if (@$original->height) {
												$player_height = number_format( ($player->width * $original->height) / $original->width, 0, '.', '');
												$mediasize = $player->width.','.$player_height;
											}
											break;
										}
                                    }
                                }
                            }
                            $body = @$item->caption;
                            $play = true;
                        }
                        elseif ( $item->type == 'audio') {
                            $tit = @$item->artist . ' - ' . @$item->album . ' - ' . @$item->id3_title;
                            $title = '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($tit) : $tit) . '</a>';
                            if ( ! @$thumb)
                                $thumb = plugins_url( 'public/img/thumb-audio.png', SB_FILE );
                            $body = @$item->caption;
                            $object = @$item->embed;
                        }
                        elseif ( $item->type == 'link') {
                            $title = '<a href="' . $item->url . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>';
                            if (@$item->excerpt)
                                $excerpt = $item->excerpt." \n";
                            $text = $body = @$excerpt.@$item->description;
                            if ( ! @$thumb)
                                $thumb = @$item->link_image;
                            if ( ! $url) {
								$url = $item->link_image;
								$mediasize = $item->link_image_dimensions->width.','.$item->link_image_dimensions->height;
							}
                        }
                        elseif ( $item->type == 'answer') {
                            $text = $body = @$item->question." \n".@$item->answer;
                        }
                        elseif ( $item->type == 'quote') {
                            if (@$item->source)
                                $source = $item->source;
                            $text = $textlong = '<span class="sb-title">'.@$item->text.'</span>'.@$source;
                        }
                        elseif ( $item->type == 'chat') {
                            $text = $body = @$item->body;
                        }
                        // type = text
                        else {
                            if ( @$item->body )
                                $text = $body = $item->body;
                            
                            // find img
                            if ( ! @$thumb) {
                                $thumbarr = sb_getsrc($body);
                                $thumb = $thumbarr['src'];
                            }
                        }
                        
                        if ($iframe) {
                            if ( ! @$url )
                                $url = @$thumb;
                        }
                        
                        if ( empty($text) )
                            $text = @$item->summary;
                        $text = (@$attr['words']) ? $this->word_limiter($text, $link) : @$this->format_text($text);
                        
                        if ( isset($slideshow) && ! empty($body) && ! @$textlong ) {
                            $textlong = @$this->format_text($body);
                        }
                        
                        $sbi = $this->make_timestr($item->timestamp, $link);
                        $itemdata = array(
                        'title' => @$title,
                        'thumb' => @$thumb,
                        'thumburl' => $url,
                        'iframe' => $iframe ? ( (@$item->type == 'video' || $item->type == 'audio') ? 'inline' : 'icbox') : '',
                        'text' => @$text,
                        'tags' => @$tags,
                        'url' => @$link,
						'object' => @$object,
                        'date' => $item->date,
                        'play' => @$play,
                        'user' => array(
                            'name' => $blog->name,
                            'title' => $blog->title,
                            'url' => $blog->url,
                            'image' => $protocol.'://api.tumblr.com/v2/blog/'.$blog->name.'.tumblr.com/avatar/64'
                            ),
                        'type' => $keyTypes[$item->type][0],
                        'icon' => array(@$themeoption['social_icons'][3], @$themeoption['type_icons'][$keyTypes[$item->type][1]])
                        );
							if (@$mediasize && ($iframe || isset($slideshow) ) )
								$itemdata['size'] = $mediasize;
							$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = $textlong;
                                $itemdata['object'] = @$object;
                                if ($mediasrc)
                                    $itemdata['thumb'] = $mediasrc;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                        }
                    }
                    }
                }
                elseif ( $feed_class == 'delicious' ) {
                    if (@$feed) {
                    foreach ($feed as $item) {
                        $link = @$item->u;
                        if ( $this->make_remove($link) ) {
                        // tags
                        $tags = '';
                        if ( @$feedoutput['tags'] ) {
                            $tags = '';
                            if ( @$item->t ) {
                                $tags = implode(', ', $item->t);
                            }
                        }
                        
                        $sbi = $this->make_timestr($item->dt, $link);
                        $itemdata = array(
                        'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->d) : $item->d) . '</a>',
                        'text' => (@$attr['words']) ? $this->word_limiter(@$item->n, $link) : @$this->format_text($item->n),
                        'tags' => $tags,
                        'url' => $link,
                        'date' => $item->dt,
                        'user' => array('name' => $item->a),
                        'type' => 'pencil',
                        'icon' => array(@$themeoption['social_icons'][4], @$themeoption['type_icons'][0])
                        );
                        $this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = @$this->format_text($item->n);
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                        }
                    }
                    }
                }
                elseif ( $feed_class == 'pinterest' ) {
                    $iframe = @$this->sboption['section_pinterest']['pinterest_iframe'];
                    
                    $fcount = $ikey = 0;
                    $pinuser = @$feed[0]->data->user;
                    $pinuser_url = @$pinuser->profile_url;
                    $pinuser_image = str_replace('_30.', '_140.', @$pinuser->image_small_url);
                    if (@$attr['https']) {
                        $pinuser_url = str_replace('http:', 'https:', $pinuser_url);
                        $pinuser_image = str_replace('http:', 'https:', $pinuser_image);
                    }
                    
                    if ($items = @$feed[1]->channel->item)
                    foreach($items as $item) {
                        $link = @$item->link;
                        $pin = @$feed[0]->data->pins[$ikey];
                        $ikey++;
                        if ( $this->make_remove($link) ) {
                        $fcount++;
        
                        $cats = array();
                        if (@$item->category) {
                            foreach($item->category as $category) {
                                $cats[] = (string) $category;
                            }
                        }
                        
                        // fix the links in description
                        $pattern = "/(?<=href=(\"|'))[^\"']+(?=(\"|'))/";
                        if (preg_match($pattern, @$pin->description, $url1)) {
                            $description = preg_replace($pattern, "https://www.pinterest.com$url1[0]", @$pin->description);
                        } else {
                            $description = @$pin->description;
                        }
                        
                        // find img
                        $mediasrc = $meta = $mediasize = '';
                        $image_width = (@$this->sboption['section_pinterest']['pinterest_image_width']) ? $this->sboption['section_pinterest']['pinterest_image_width'] : 237;
                        if ($thumbobj = @$pin->images->{'237x'}) {
                            $thumb = $thumbobj->url;
                            if (@$attr['https'])
                                $thumb = str_replace('http:', 'https:', $thumb);
                            $bigthumb = str_replace('237x', '736x', $thumb);
                            if ($image_width == '736')
                                $thumb = $bigthumb;
                            if ($iframe) {
                                $mediasrc = $bigthumb;
                                $newwidth = 450;
                                $newheight = number_format( ($newwidth * $thumbobj->height) / $thumbobj->width, 0, '.', '');
                                $mediasize = $newwidth.','.$newheight;
                            }
                        }
                        
                        if (@$pin->is_video && @$pin->embed && $iframe) {
                            $mediasrc = @$pin->embed->src;
                            $mediasize = (@$pin->embed->width && @$pin->embed->height) ? $pin->embed->width.','.$pin->embed->height : $newwidth.','.$newheight;
                        }

                        // add meta
                        if (@$feedoutput['stat']) {
                            $meta .= '
                            <span class="sb-text">
                                <span class="sb-meta">
                                    <span class="shares"><i class="sb-bico sb-star-o"></i> ' . @$pin->repin_count . ' '.ucfirst(__( 'repin', 'social-board' )).'</span>
                                    <span class="comments"><i class="sb-bico sb-thumbs-up"></i> ' . @$pin->like_count . ' '.ucfirst(__( 'likes', 'social-board' )).'</span>
                                </span>
                            </span>';
                        }
                        
                        $sbi = $this->make_timestr($item->pubDate, $link);
                        $itemdata = array(
                        'title' => '<a href="' . @$pin->link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                        'text' => (string) (@$attr['words']) ? $this->word_limiter($description, $link) : $this->format_text($description),
                        'thumb' => $thumb,
                        'thumburl' => ($mediasrc) ? $mediasrc : $link,
                        'tags' => @implode(', ', $cats),
                        'url' => $link,
                        'iframe' => $iframe ? (@$pin->is_video ? 'iframe' : 'icbox') : '',
                        'date' => $item->pubDate,
                        'meta' => @$meta,
                        'user' => array(
                            'name' => @$pinuser->full_name,
                            'url' => @$pinuser_url,
                            'image' => @$pinuser_image
                            ),
                        'type' => 'pencil',
                        'play' => (@$pin->is_video) ? true : false,
                        'icon' => array(@$themeoption['social_icons'][5], @$themeoption['type_icons'][0])
                        );
							if (@$mediasize && ($iframe || isset($slideshow) ) )
								$itemdata['size'] = $mediasize;
							$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = $this->format_text($description);
                                if ($mediasrc)
                                    $itemdata['thumb'] = $mediasrc;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                            
							if ( $fcount >= $results ) break;
                        }
                    }
                }
                elseif ( $feed_class == 'flickr' ) {
                    // flickr next page
                    $loadcrawl[$feed_class.$i.$ifeed] = @$feed->photos->page+1;
                    
                    if (@$feed->photos->photo) {
                        $iframe = @$this->sboption['section_flickr']['flickr_iframe'];
                        
                        foreach ($feed->photos->photo as $media) {
                            $link = 'https://flickr.com/photos/' . $media->owner . '/' . $media->id;
                            if ( $this->make_remove($link) ) {
                                $text = $image = $url = $mediasize = '';
                                
                                // tags
                                $tags = '';
                                if ( @$feedoutput['tags'] ) {
                                    if ( @$media->tags ) {
                                        $tags = $media->tags;
                                    }
                                }
                                
                                if (@$attr['carousel'])
                                    $text = (@$attr['words']) ? $this->word_limiter($media->title, $link) : $media->title;
                                
                                $flickr_thumb = (@$this->sboption['section_flickr']['flickr_thumb']) ? $this->sboption['section_flickr']['flickr_thumb'] : 'm';
                                $image = $protocol.'://farm' . $media->farm . '.staticflickr.com/' . $media->server . '/' . $media->id . '_' . $media->secret . '_' . $flickr_thumb . '.jpg';
                                $author_icon = (@$media->iconserver > 0) ? $protocol.'://farm' . $media->iconfarm . '.staticflickr.com/' . $media->iconserver . '/buddyicons/' . $media->owner . '.jpg' : 'https://www.flickr.com/images/buddyicon.gif';
                                if ($iframe) {
                                    $url = $protocol.'://farm' . $media->farm . '.staticflickr.com/' . $media->server . '/' . $media->id . '_' . $media->secret . '_c.jpg';
                                    $mediasize = (@$media->width_c && @$media->height_c) ? $media->width_c.','.$media->height_c : '';
                                }
                                
                                $mediadate = (@$media->dateadded) ? $media->dateadded : $media->dateupload;
                                $sbi = $this->make_timestr($mediadate, $link);
                                $itemdata = array(
                                    'thumb' => $image,
                                    'thumburl' => $url,
                                    'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($media->title) : $media->title) . '</a>',
                                    'text' => $text,
                                    'tags' => $tags,
                                    'iframe' => $iframe ? 'icbox' : '',
                                    'url' => $link,
                                    'date' => $media->datetaken,
                                    'user' => array(
                                        'name' => @$media->ownername,
                                        'url' => $protocol.'://www.flickr.com/people/' . $media->owner . '/',
                                        'image' => $author_icon
                                        ),
                                    'type' => 'image',
                                    'icon' => array(@$themeoption['social_icons'][6], @$themeoption['type_icons'][5])
                                );
                                $this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                                if ( isset($slideshow) ) {
                                    $itemdata['text'] = $media->title;
                                    $itemdata['size'] = @$mediasize;
                                    if ($url)
                                        $itemdata['thumb'] = $url;
                                    $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                                }
                            }
                        }
                    }
                }
                // Instagram
                elseif ( $feed_class == 'instagram' ) {
                    $keyTypes = array( 'image' => array('camera', 5), 'video' => array('video-camera', 6) );
                    $iframe = @$this->sboption['section_instagram']['instagram_iframe'];
                    
                    // instagram next page
                    if ($i != 4) {
                        if ($i == 2) {
                            $next_id = @$feed->pagination->next_max_tag_id;
                        } else {
                            $next_id = @$feed->pagination->next_max_id;
                        }
                        $loadcrawl[$feed_class.$i.$ifeed] = @$next_id;
                    }
                    
                    // loop in feed items
                    if (@$feed->data) {
                    foreach ($feed->data as $item) {
                        $link = $url = @$item->link;
                        if ( $this->make_remove($link) ) {
                        $thumb = $mediasrc = $mediasize = '';
                        $instagram_images = (@$this->sboption['section_instagram']['instagram_images']) ? $this->sboption['section_instagram']['instagram_images'] : 'low_resolution';
                        if (@$item->images) {
                            $thumb = $item->images->{$instagram_images}->url;
                            
                            // set iframe image
                            $instagram_images_iframe = 'standard_resolution';
                            if ($iframe) {
                                $itemimages = $item->images->{$instagram_images_iframe};
                                $url = $mediasrc = $itemimages->url;
                                $mediasize = $itemimages->width.','.$itemimages->height;
                            }
                        }
                        
                        if (@$item->type == 'video' && $iframe) {
                            $instagram_videos = (@$this->sboption['section_instagram']['instagram_videos']) ? $this->sboption['section_instagram']['instagram_videos'] : 'low_resolution';
                            $itemvideos = $item->videos->{$instagram_videos};
                            $url = $mediasrc = $link . 'embed/';
                            $mediasize = $itemvideos->width.','.($itemvideos->height + 65);
                        }
                        
                        // tags
                        $tags = '';
                        if ( @$feedoutput['tags'] ) {
                            if ( @$item->tags ) {
                                $tags = implode(', ', $item->tags);
                            }
                        }
                        
                        // comments
                        $count = 0;
                        $comments_data = '';
                        $comments_count = ( @$this->sboption['section_instagram']['instagram_comments'] > 0 ) ? $this->sboption['section_instagram']['instagram_comments'] : 0;
                        if ( ! empty($item->comments->data) && $comments_count ) {
                            foreach ( $item->comments->data as $comment ) {
                                $count++;
                                $comment_message = (@$attr['commentwords']) ? $this->word_limiter($comment->text, $link, true) : $comment->text;
                                $comments_data .= '<span class="sb-meta sb-mention">';
                                if ( ! @empty($comment->from) )
                                    $comments_data .= '<img class="sb-commentimg" src="' . $comment->from->profile_picture . '" alt=""><a href="http://instagram.com/' . $comment->from->username . '"'.$target.'>' . $comment->from->full_name . '</a> ';
                                $comments_data .= $comment_message . '</span>';
                                if ( $count >= $comments_count ) break;
                            }
                        }
                        // likes
                        $count = 0;
                        $likes_data = '';
                        $likes_count = ( @$this->sboption['section_instagram']['instagram_likes'] > 0 ) ? $this->sboption['section_instagram']['instagram_likes'] : 0;
                        if ( ! empty($item->likes->data) && $likes_count ) {
                            foreach ( $item->likes->data as $like ) {
                                $count++;
                                $likes_data .= '<img src="' . $like->profile_picture . '" title="' . $like->full_name . '" alt="">';
                                if ( $count >= $likes_count ) break;
                            }
                        }
                        
                        $meta = $text = $textlong = '';
                        if ($comments_data || $likes_data || @$feedoutput['comments'] || @$feedoutput['likes']) {
                            $meta .= '
                            <span class="sb-metadata">';
                            if (@$feedoutput['comments']) {
                                $meta .= '
                                    <span class="sb-meta">
                                        <span class="comments"><i class="sb-bico sb-comments"></i> ' . $item->comments->count . ' '.__( 'comments', 'social-board' ).'</span>
                                    </span>';
                                if ($comments_data)
                                    $meta .= $comments_data;
                            }
                            if (@$feedoutput['likes']) {
                            $meta .= '
                                <span class="sb-meta">
                                    <span class="likes"><i class="sb-bico sb-star"></i> ' . $item->likes->count . ' '.__( 'likes', 'social-board' ).'</span>
                                </span>';
                            if ($likes_data)
                                $meta .= '
                                <span class="sb-meta item-likes">
                                    ' . $likes_data . '
                                </span>';
                            }
                            $meta .= '
                            </span>';
                        }
                        
                        $text = (@$attr['words']) ? $this->word_limiter(@$item->caption->text, $link) : @$this->format_text($item->caption->text);
                        if ( isset($slideshow) )
                            $textlong = @$this->format_text($item->caption->text);
                        
                        // Add links to all hash tags
                        $htreplace = $htsearch = array();
                        if ( @$item->tags ) {
                            foreach ($item->tags as $hashtag) {
                                $htsearch[] = '#'.$hashtag;
                                $htreplace[] = '<a href="https://instagram.com/explore/tags/'.$hashtag.'/"'.$target.'>#'.$hashtag.'</a>';
                            }
                            if (@$htsearch) {
                                $text = str_ireplace($htsearch, $htreplace, $text);

                                if ( isset($slideshow) )
                                    $textlong = str_ireplace($htsearch, $htreplace, $textlong);
                            }
                        }
                        
                        // create item
                        $sbi = $this->make_timestr($item->created_time, $link);
                        $itemdata = array(
                        'thumb' => @$thumb,
                        'thumburl' => $url,
                        'iframe' => $iframe ? (@$item->type == 'video' ? 'iframe' : 'icbox') : '',
                        'text' => $text,
                        'meta' => @$meta,
                        'tags' => $tags,
                        'url' => $link,
                        'date' => $item->created_time,
                        'user' => array(
                            'name' => $item->user->username,
                            'title' => @$item->user->full_name,
                            'url' => 'https://instagram.com/'.$item->user->username.'/',
                            'image' => @$item->user->profile_picture
                            ),
                        'type' => @$keyTypes[$item->type][0],
                        'play' => (@$item->type == 'video') ? true : false,
                        'icon' => array(@$themeoption['social_icons'][7], @$themeoption['type_icons'][$keyTypes[$item->type][1]])
                        );
							if (@$mediasize && ($iframe || isset($slideshow) ) )
								$itemdata['size'] = $mediasize;
							$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = $textlong;
                                if ($mediasrc)
                                    $itemdata['thumb'] = $mediasrc;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                        }
                    } // end foreach
                    
                    // next page timestamp only for /media/search
                    if ( $i == 4 && ! @$next_id)
                        $loadcrawl[$feed_class.$i.$ifeed] = @$item->created_time;
                    }
                }
                elseif ( $feed_class == 'youtube' ) {
                    $iframe = @$this->sboption['section_youtube']['youtube_iframe'];

                    // youtube next page
                    $loadcrawl[$feed_class.$i.$ifeed] = @$feed->nextPageToken;
                    
                    if (@$feed->items)
                    foreach ($feed->items as $item) {
                        $watchID = ($i == 3) ? @$item->id->videoId : @$item->snippet->resourceId->videoId;
                        $link = $protocol.'://www.youtube.com/watch?v='.$watchID;
                        $snippet = $item->snippet;
                        if ( $this->make_remove($link) ) {
                        $dateof = @$snippet->publishedAt;
                        $title = @$snippet->title;
                        $text = @$snippet->description;
                        $text = (@$attr['words']) ? $this->word_limiter(@$text, $link) : @$this->format_text($text);

                        $thumb = $mediasrc = $mediasize = '';
                        if ($iframe) {
                            $mediasrc = $protocol.'://www.youtube.com/embed/' . $watchID . '?rel=0&wmode=transparent';
                        }
                        $thumbnail = @$snippet->thumbnails->{$this->sboption['section_youtube']['youtube_thumb']};
                        if ( ! $thumbnail )
                            $thumbnail = @$snippet->thumbnails->{'medium'};
                        $thumb = @$thumbnail->url;
                        
						// user info
						$userdata = array(
							'name' => $snippet->channelTitle,
							'url' => 'https://www.youtube.com/channel/'.$snippet->channelId
						);
						if (@$feed->userInfo->thumbnails)
							$userdata['image'] = @$feed->userInfo->thumbnails->default->url;
						
                        $sbi = $this->make_timestr($dateof, $link);
                        $itemdata = array(
							'thumb' => $thumb,
							'thumburl' => ($mediasrc) ? $mediasrc : $link,
							'iframe' => $iframe ? 'iframe' : '',
							'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($title) : $title) . '</a>',
							'text' => $text,
							'url' => $link,
							'date' => $dateof,
							'user' => $userdata,
							'type' => 'youtube-play',
							'play' => true,
							'icon' => array(@$themeoption['social_icons'][8], @$themeoption['type_icons'][6])
                        );

                        $youtube_video = (@$this->sboption['section_youtube']['youtube_video']) ? $this->sboption['section_youtube']['youtube_video'] : '640-360';
                        $ytvsize = explode('-', $youtube_video);
                        $mediasize = "$ytvsize[0],".($ytvsize[1] + 100);
							if (@$mediasize && ($iframe || isset($slideshow) ) )
								$itemdata['size'] = $mediasize;
							$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = @$this->format_text(@$snippet->description);
                                if ($mediasrc)
                                    $itemdata['thumb'] = $mediasrc;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                        }
                    }
                }
                elseif ( $feed_class == 'vimeo' ) {
                    $iframe = @$this->sboption['section_vimeo']['vimeo_iframe'];
                    
                    if (@$feed) {
                        // vimeo next page
                        $loadcrawl[$feed_class.$i.$ifeed] = @$feed->page+1;
                        
                        if ($data = @$feed->data)
                        foreach ($data as $item) {
                            $link = @$item->link;
                            if ( $this->make_remove($link) ) {
                                $thumb = $mediasrc = $mediasize = '';
                                $vimeo_thumb = (@$this->sboption['section_vimeo']['vimeo_thumb']) ? $this->sboption['section_vimeo']['vimeo_thumb'] : '295';
                                if ($pictures = @$item->pictures->sizes) {
                                    foreach ($pictures as $photo) {
                                        if ($photo->width == $vimeo_thumb) {
                                            $thumb = $photo->link;
                                            break;
                                        }
                                    }
                                }
                                
                                $title = $item->name;
                                $id = preg_replace('/\D/', '', $item->uri);
                                if ($iframe || $slideshow) {
                                    $url = $mediasrc = 'https://player.vimeo.com/video/'. $id;
                                    $mediasize = $item->width.','.$item->height;
                                } else {
                                    $url = $link;
                                }
                                
                                $datetime = (@$item->created_time) ? @$item->created_time : @$item->modified_time;
                                $connections = @$item->metadata->connections;
                                $meta = '
                                <span class="sb-text">
                                    <span class="sb-meta">
                                        <span class="likes"><i class="sb-bico sb-thumbs-up"></i> ' . @$connections->likes->total . '</span>
                                        <span class="views"><i class="sb-bico sb-play-circle"></i> ' . @$item->stats->plays . '</span>
                                        <span class="comments"><i class="sb-bico sb-comment"></i> ' . @$connections->comments->total . '</span>
                                        <span class="duration"><i class="sb-bico sb-clock-o"></i> ' . @$item->duration . ' secs</span>
                                    </span>
                                </span>';
                                $user_name = @$item->user->name;
                                $user_url = @$item->user->link;
                                $user_image = @$item->user->pictures->sizes[1]->link;
                                
                                $sbi = $this->make_timestr($datetime, $link);
                                $itemdata = array(
                                'thumb' => @$thumb,
                                'thumburl' => @$url,
                                'iframe' => $iframe ? 'iframe' : '',
                                'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($title) : $title) . '</a>',
                                'text' => (@$attr['words']) ? $this->word_limiter($item->description, $link) : $item->description,
                                'meta' => (@$feedoutput['share']) ? $meta : null,
                                'url' => $link,
                                'date' => $datetime,
                                'user' => array(
                                    'name' => $user_name,
                                    'url' => $user_url,
                                    'image' => $user_image
                                    ),
                                'type' => 'video-camera',
                                'play' => true,
                                'icon' => array(@$themeoption['social_icons'][9], @$themeoption['type_icons'][6])
                                );
								if (@$mediasize && ($iframe || isset($slideshow) ) )
									$itemdata['size'] = $mediasize;
                                $this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                                if ( isset($slideshow) ) {
                                    $itemdata['text'] = $item->description;
                                    $itemdata['thumb'] = $mediasrc;
                                    $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                                }
                            } // end if $link
                        } // end foreach $data
                    }
                }
                elseif ( $feed_class == 'stumbleupon' ) {
                    $fcount = 0;
                    if (@$feed)
                    foreach ($feed as $dataKey => $data) {
                        if (@$this->sboption['section_stumbleupon']['stumbleupon_feeds'][$dataKey]) {
                        $channel = $data->channel;
                        $items = ( $dataKey == 'likes' ) ? $channel->item : $data->item;
                        foreach($items as $item) {
                            $link = @$item->link;
                            if ( $this->make_remove($link) ) {
                            $fcount++;
                            
                            // find user
                            $pattern = ( $dataKey == 'likes' ) ? '/http:\/\/www.stumbleupon.com\/stumbler\/(\w+)/i' : '/http:\/\/www.stumbleupon.com\/stumbler\/(\w+)\/comments/i';
                            $replacement = '$1';
                            $user_name = preg_replace($pattern, $replacement, $channel->link);
                            
                            $thumb = $text = '';
                            if ($description = (string) @$item->description) {
                                if (@$attr['words']) {
                                    $thumbarr = sb_getsrc($description);
                                    $thumb = $thumbarr['src'];
                                    $text = $this->word_limiter($description, $link);
                                }
                                else {
                                    $text = $description;
                                }
                            }

                            $sbi = $this->make_timestr($item->pubDate, $link);
                            $itemdata = array(
                            'thumb' => $thumb,
                            'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                            'text' => $text,
                            'url' => $link,
                            'date' => $item->pubDate,
                            'user' => array(
                                'name' => $user_name,
                                'url' => "http://www.stumbleupon.com/stumbler/$user_name",
                                'title' => @$channel->title
                                ),
                            'type' => ( $dataKey == 'likes' ) ? 'star-o' : 'comment-o',
                            'icon' => array(@$themeoption['social_icons'][10], @$themeoption['type_icons'][( $dataKey == 'likes' ) ? 8 : 9])
                            );
                            $this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                                if ( isset($slideshow) ) {
                                    $itemdata['text'] = $description;
                                    $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                                }
                            if ( $fcount >= $results ) break;
                            }
                        }
                        }
                    }
                }
                elseif ( $feed_class == 'deviantart' ) {
                    $fcount = 0;
                    $channel = @$feed->channel;
                    if (@$channel->item)
                    foreach($channel->item as $item) {
                        $link = @$item->link;
                        if ( $this->make_remove($link) ) {
                        $fcount++;

                        $description = $item->children('media', true)->description;
                        
                        $sbi = $this->make_timestr($item->pubDate, $link);
                        $itemdata = array(
                        'thumb' => @$item->children('media', true)->thumbnail->{1}->attributes()->url,
                        'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                        'text' => (@$attr['words']) ? $this->word_limiter($description, $link) : $description,
                        'tags' => '<a href="' . $item->children('media', true)->category . '"'.$target.'>' . $item->children('media', true)->category->attributes()->label . '</a>',
                        'url' => $link,
                        'date' => $item->pubDate,
                        'user' => array(
                            'name' => $item->children('media', true)->credit->{0},
                            'url' => $item->children('media', true)->copyright->attributes()->url,
                            'image' => $item->children('media', true)->credit->{1}),
                        'type' => 'image',
                        'icon' => array(@$themeoption['social_icons'][11], @$themeoption['type_icons'][4])
                        );
                        $this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                        if ( isset($slideshow) ) {
                            $itemdata['text'] = $description;
                            $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                        }
                        
                        if ( $fcount >= $results ) break;
                        }
                    }
                }
                elseif ( $feed_class == 'rss' ) {
                    $iframe = @$this->sboption['section_rss']['rss_iframe'];
                    
                    // RSS next page
                    $startP = @$_SESSION[$label]['loadcrawl'][$feed_class.$i.$ifeed];
                    $loadcrawl[$feed_class.$i.$ifeed] = $nextP = $startP + $results;
                    
                    $fcount = 0;
                    $MIMETypes = array('image/jpeg', 'image/jpg', 'image/gif', 'image/png');
                    if ( $channel = @$feed->channel ) { // rss
                        if (@$channel->item)
                        foreach($channel->item as $item) {
                            $link = @$item->link;
                            if ( $this->make_remove($link) ) {
                            	$fcount++;
                            
                            if ($fcount > $startP) {

                            $thumb = $url = '';
                            if (@$item->children('media', true)->thumbnail)
                            foreach($item->children('media', true)->thumbnail as $thumbnail) {
                                $thumb = @$thumbnail->attributes()->url;
                            }
                            if ( ! $thumb && @$item->children('media', true)->content) {
                                foreach($item->children('media', true)->content as $content) {
                                    $thumb = @$content->children('media', true)->thumbnail->attributes()->url;
                                    if ( @in_array($content->attributes()->type, $MIMETypes) )
                                        $url = @$content->attributes()->url;
                                }
                                if ( ! $thumb && $url) {
                                    $thumb = $url;
                                }
                            }
                            
                            if ( ! $thumb) {
                                if ( @in_array($item->enclosure->attributes()->type, $MIMETypes) )
                                    $thumb = @$item->enclosure->attributes()->url;
                            }
                            
                            if (@$item->category && @$feedoutput['tags'])
                            foreach($item->category as $category) {
                                $cats[] = (string) $category;
                            }
                            
                            // set Snippet or Full Text
                            $text = $description = '';
                            if (@$this->sboption['section_rss']['rss_text'])
                                $description = @$item->description;
                            else
                                $description = (@$item->children("content", true)->encoded) ? $item->children("content", true)->encoded : @$item->description;
                                
                            if (@$description) {
                                $description = preg_replace("/<script.*?\/script>/s", "", $description);
                                if (@$attr['words']) {
                                    if ( ! $thumb) {
                                        $thumbarr = sb_getsrc($description);
                                        $thumb = $thumbarr['src'];
                                    }
                                    $text = $this->word_limiter($description, $link);
                                } else {
                                    $text = $description;
                                }
                            }
                            if ($iframe) {
                                if ( ! $url)
                                    $url = (@$thumb) ? $thumb : '';
                            }
                            
                            // resize thumbnails
                            $image_width = @$this->sboption['section_rss']['rss_image_width'];
                            if (@$thumb && ! empty($image_width) ) {
                            	$token = md5(urlencode($thumb).@$_SERVER['SERVER_ADDR'].@$_SERVER['SERVER_ADMIN'].@$_SERVER['SERVER_NAME'].@$_SERVER['SERVER_PORT'].@$_SERVER['SERVER_PROTOCOL'].@strip_tags($_SERVER['SERVER_SIGNATURE']).@$_SERVER['SERVER_SOFTWARE'].@$_SERVER['DOCUMENT_ROOT']);
                            	$imgStr = 'resize='.$image_width.'&refresh=3600&token='.$token.'&src='.$thumb;
                            	$thumb = esc_url( add_query_arg( 'sbimg', base64_encode($imgStr), site_url('index.php') ) );
                            }
                            
                            $sbi = $this->make_timestr($item->pubDate, $link);
                            $itemdata = array(
                            'thumb' => (@$thumb) ? $thumb : '',
                            'thumburl' => $url,
                            'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                            'text' => $text,
                            'tags' => @implode(', ', $cats),
                            'url' => $link,
                            'iframe' => $iframe ? 'icbox' : '',
                            'date' => $item->pubDate,
                            'user' => array(
                                'name' => $channel->title,
                                'url' => $channel->link,
                                'image' => @$channel->image->url
                                ),
                            'type' => 'pencil',
                            'icon' => array(@$themeoption['social_icons'][12], @$themeoption['type_icons'][0])
                            );
	                            $this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
	                            if ( isset($slideshow) ) {
	                                $itemdata['text'] = @$this->format_text($description);
	                                if ($url)
	                                    $itemdata['thumb'] = $url;
	                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
	                            }
							} // end start point

                            	if ($fcount >= $nextP) break;
                            } // end make remove
                        } // end foreach
                    } elseif ( $entry = @$feed->entry ) { // atom
                        // get feed link
                        if (@$feed->link)
                        foreach($feed->link as $link) {
                            if (@$link->attributes()->rel == 'alternate')
                                $user_url = @$link->attributes()->href;
                        }
                        foreach($feed->entry as $item) {
                            $link = @$item->link[0]->attributes()->href;
                            if ( $this->make_remove($link) ) {
                            	$fcount++;
                            
                            if ($fcount > $startP) {

                            $title = (string) @$item->title;
                            $thumb = $url = '';
                            if (@$item->media)
                            foreach($item->media as $thumbnail) {
                                $thumb = @$thumbnail->attributes()->url;
                            }
                            if ( ! $thumb && @$item->link) {
                                foreach($item->link as $linkitem) {
                                    if (@$linkitem->attributes()->rel == 'enclosure') {
                                        if ( in_array(@$linkitem->attributes()->type, $MIMETypes) )
                                            $thumb = @$content->attributes()->url;
                                    }
                                }
                            }
                            
                            $cats = '';
                            if (@$item->category && @$feedoutput['tags']) {
                                foreach($item->category as $category) {
                                    $cats .= @$category->attributes()->term.', ';
                                }
                                $cats = rtrim($cats, ", ");
                            }

                            // set Snippet or Full Text
                            $text = $description = '';
                            if (@$this->sboption['section_rss']['rss_text']) {
                                $description = (string) @$item->summary;
                            } else {
                                $content = (string) @$item->content;
                                $description = ($content) ? $content : (string) @$item->summary;
                            }

                            if (@$description) {
                                if (@$attr['words']) {
                                    if ( ! $thumb) {
                                        $thumbarr = sb_getsrc($description);
                                        $thumb = $thumbarr['src'];
                                    }
                                    $text = $this->word_limiter($description, $link);
                                }
                                else {
                                    $text = $description;
                                }
                            }
                            if ($iframe)
                                $url = (@$thumb) ? $thumb : '';
                                
                            // resize thumbnails
                            $image_width = @$this->sboption['section_rss']['rss_image_width'];
                            if (@$thumb && ! empty($image_width) ) {
                            	$token = md5(urlencode($thumb).@$_SERVER['SERVER_ADDR'].@$_SERVER['SERVER_ADMIN'].@$_SERVER['SERVER_NAME'].@$_SERVER['SERVER_PORT'].@$_SERVER['SERVER_PROTOCOL'].@strip_tags($_SERVER['SERVER_SIGNATURE']).@$_SERVER['SERVER_SOFTWARE'].@$_SERVER['DOCUMENT_ROOT']);
                            	$imgStr = 'resize='.$image_width.'&refresh=3600&token='.$token.'&src='.$thumb;
                            	$thumb = esc_url( add_query_arg( 'sbimg', base64_encode($imgStr), site_url('index.php') ) );
                            }
                            
                            $sbi = $this->make_timestr($item->published, $link);
                            $itemdata = array(
                            'thumb' => @$thumb,
                            'thumburl' => $url,
                            'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($title) : $title) . '</a>',
                            'text' => @$text,
                            'tags' => @$cats,
                            'url' => $link,
                            'iframe' => $iframe ? 'icbox' : '',
                            'date' => $item->published,
                            'user' => array(
                                'name' => $feed->title,
                                'url' => @$user_url,
                                'image' => @$feed->logo
                                ),
                            'type' => 'pencil',
                            'icon' => array(@$themeoption['social_icons'][12], @$themeoption['type_icons'][0])
                            );
                            $this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = @$this->format_text($description);
                                if ($url)
                                    $itemdata['thumb'] = $url;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
									} // end start point
                            	if ($fcount >= $nextP) break;
                            } // end make remove
                        }
                    }
                }
                elseif ( $feed_class == 'soundcloud' ) {
                    if (@$feed)
                    foreach ($feed as $item) {
                        $link = @$item->permalink_url;
                        if ( $this->make_remove($link) ) {
                        // tags
                        $tags = '';
                        if ( @$feedoutput['tags'] ) {
                            if (@$item->tag_list)
                                $tags .= $item->tag_list;
                        }
                        
                        // convert duration to mins
                        $duration = '';
                        if (@$item->duration) {
                            $seconds = floor($item->duration / 1000);
                            $duration = floor($seconds / 60);
                        }
                        
                        $download = '';
                        if (@$item->download_url) {
                            $soundcloud_client_id = @$setoption['section_soundcloud']['soundcloud_client_id'];
                            $download .= '<span class="download"><i class="sb-bico sb-cloud-download"></i> ' . @$item->download_count . '</span>';
                        }
                        
                        if (@$item->artwork_url) {
                        	$artwork_url = str_replace('-large', '-t300x300', $item->artwork_url);
                        } else
                        	$artwork_url = '';
                        
                        $meta = '
                        <span class="sb-text">
                            <span class="sb-meta">
                                <span class="likes"><i class="sb-bico sb-thumbs-up"></i> ' . @$item->favoritings_count . '</span>
                                <span class="views"><i class="sb-bico sb-play-circle"></i> ' . @$item->playback_count . '</span>
                                <span class="comments"><i class="sb-bico sb-comment"></i> ' . @$item->comment_count . '</span>
                                <span class="duration"><i class="sb-bico sb-clock-o"></i> ' . @$duration . ' mins</span>
                                ' . $download . '
                            </span>
                        </span>';
                        
                        $sbi = $this->make_timestr($item->created_at, $link);
                        $itemdata = array(
                        'title' => '<a href="' . $link . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($item->title) : $item->title) . '</a>',
                        'text' => (@$attr['words']) ? $this->word_limiter(@$item->description, $link) : @$item->description,
                        'thumb' => $artwork_url,
                        'tags' => $tags,
                        'url' => $link,
                        'meta' => (@$feedoutput['meta']) ? $meta : null,
                        'date' => $item->created_at,
                        'user' => array(
                            'name' => $item->user->username,
                            'url' => $item->user->permalink_url,
                            'image' => $item->user->avatar_url
                            ),
                        'type' => 'youtube-play',
                        'icon' => array(@$themeoption['social_icons'][13], @$themeoption['type_icons'][7])
                        );
                        $this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = @$item->description;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                        }
                    }
                }
                elseif ( $feed_class == 'vk' ) {
                    if (@$feed) {
                        $iframe = @$this->sboption['section_vk']['vk_iframe'];
                        
                    // vk next page start
                    $offset = @$feed->offset;
                    $loadcrawl[$feed_class.$i.$ifeed] = ($offset == 0) ? $results : $results + $offset;
                    
                    if ($groups = @$feed->response->groups) {
                        foreach ($feed->response->groups as $group) {
                            $groupdata['-'.$group->id] = $group;
                        }
                    }
                    if ($profiles = @$feed->response->profiles) {
                        foreach ($feed->response->profiles as $profile) {
                            $userdata[$profile->id] = $profile;
                        }
                    }
                    if (@$feed->response)
                    foreach ($feed->response->items as $entry) {
                        $link = $protocol.'://vk.com/wall'.@$entry->owner_id.'_'.@$entry->id;
                        if ( $this->make_remove($link) ) {
                        
                        // body text
                        $text = @$entry->text;
                        if ( ! $text) {
                            if (@$entry->copy_history)
                                $text = $entry->copy_history[0]->text;
                        }
                        if ( isset($slideshow) ) {
                            $textlong = @$this->format_text($text);
                            $textlong = preg_replace('/#([^\s]+)/', '<a href="'.$protocol.'://vk.com/feed?section=search&q=%23$1"'.$target.'>#$1</a>', $textlong);
                        }
                        $text = (@$attr['words']) ? @$this->word_limiter($text, $link) : @$this->format_text($text);
                        // Add links to all hash tags
                        $text = preg_replace('/#([^\s]+)/', '<a href="'.$protocol.'://vk.com/feed?section=search&q=%23$1"'.$target.'>#$1</a>', $text);
                        
                        // user info
                        $user = (@$userdata[$entry->from_id]) ? $userdata[$entry->from_id] : $groupdata[$entry->from_id];
                        $user_name = (@$user->name) ? $user->name : $user->first_name.' '.$user->last_name;
                        $user_image = $user->photo_50;
                        $user_url = ($user->screen_name) ? $protocol.'://vk.com/' . $user->screen_name : $protocol.'://vk.com/id' . $entry->from_id;
                        
                        // get image
                        $image_width = @$this->sboption['section_vk']['vk_image_width'];
                        $attachments = @$entry->attachments;
                        if ( ! $attachments) {
                            if (@$entry->copy_history)
                                $attachments = $entry->copy_history[0]->attachments;
                        }
                        $source = $iframe2 = $play = $url = $mediasrc = $mediasize = '';
                        if ( ! empty($attachments) ) {
                            if ($image_width) {
                                foreach ($attachments as $attach) {
                                    if ($attach->type == 'photo') {
                                        $photo_width = "photo_$image_width";
                                        if ( ! @$attach->photo->{$photo_width} ) {
                                            $source = $this->vk_get_photo(@$attach->photo);
                                        } else {
                                            $source = @$attach->photo->{$photo_width};
                                        }
                                        if ($iframe) {
                                            $iframe2 = $iframe;
                                            $photo_width_iframe = "photo_1280";
                                            if ( ! @$attach->photo->{$photo_width_iframe} ) {
                                                $url = $mediasrc = $this->vk_get_photo(@$attach->photo);
                                            } else {
                                                $url = $mediasrc = @$attach->photo->{$photo_width_iframe};
                                                if ($attach->photo->width)
                                                    $mediasize = $attach->photo->width.','.$attach->photo->height;
                                            }
                                        }
                                        break;
                                    } elseif ($attach->type == 'link') {
                                        $source = (@$attach->link->image_big) ? $attach->link->image_big : @$attach->link->image_src;
                                        $url = @$attach->link->url;
                                        break;
                                    } elseif ($attach->type == 'video') {
                                        $play = true;
                                        $source = ($image_width <= 130) ? @$attach->video->photo_130 : @$attach->video->photo_320;
                                        break;
                                    } elseif ($attach->type == 'doc') {
                                        $source = $this->vk_get_photo(@$attach->doc);
                                        break;
                                    }
                                }
                            }
                        }
                        
                        $meta = (@$feedoutput['stat']) ? '
                        <span class="sb-text">
                            <span class="sb-meta">
                                <span class="likes"><i class="sb-bico sb-thumbs-up"></i>' . $entry->likes->count . '</span>
                                <span class="shares"><i class="sb-bico sb-retweet"></i> ' . $entry->reposts->count . '</span>
                                <span class="comments"><i class="sb-bico sb-comment"></i> ' . $entry->comments->count . '</span>
                            </span>
                        </span>' : null;
                        
                        $sbi = $this->make_timestr($entry->date, $link);
                        $itemdata = array(
                        'thumb' => (@$source) ? $source : '',
                        'thumburl' => $url,
                        'text' => @$text,
                        'meta' => @$meta,
                        'url' => @$link,
                        'iframe' => @$iframe2 ? 'icbox' : '',
                        'date' => $entry->date,
                        'user' => array(
                            'name' => @$user_name,
                            'url' => $user_url,
                            'image' => @$user_image
                            ),
                        'type' => 'pencil',
                        'play' => @$play,
                        'icon' => array(@$themeoption['social_icons'][14], ($i == 2) ? @$themeoption['type_icons'][4] : @$themeoption['type_icons'][0] )
                        );
							$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = @$textlong;
                                $itemdata['size'] = @$mediasize;
                                if ($mediasrc)
                                    $itemdata['thumb'] = $mediasrc;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                        }
                    } // end foreach
                    } // end $feed
                }
        		elseif ( $feed_class == 'linkedin' ) {
                    if (@$feed->values) {
                        $iframe = @$this->sboption['section_linkedin']['linkedin_iframe'];
                    
                    // linkedin next page
                    $loadcrawl[$feed_class.$i.$ifeed] = (@$feed->_start) ? @$feed->_start + @$feed->_count : $results;
                    
                    foreach ( $feed->values as $data ) {
                        if ( isset($data->timestamp) ) {
                        
                        $updateKey = explode('-', $data->updateKey);
                        $link = 'https://www.linkedin.com/nhome/updates?topic='.$updateKey[2];
                        if ( $this->make_remove($link) ) {
                        
                        $url = $thumb = $mediasrc = $title = $longtext = '';
                        $updateContent = $data->updateContent;
                        $share = $updateContent->companyStatusUpdate->share;
                        
                        if (@$share->content->title) {
                            $titleurl = (@$share->content->shortenedUrl) ? $share->content->shortenedUrl : $link;
                            $title = '<a href="' . $titleurl . '"'.$target.'>' . (@$attr['titles'] ? $this->title_limiter($share->content->title) : $share->content->title) . '</a>';
                        }
                        
                        if (@$share->comment)
                            $longtext .= $share->comment;
                        if (@$share->content->description) {
                            if ($longtext) $longtext .= " \n";
                            $longtext .= $share->content->description;
                        }
                        $text = (@$attr['words']) ? $this->word_limiter($longtext) : @$this->format_text($longtext);
                        
                        // comments
                        $count = 0;
                        $comments_data = '';
                        $comments_count = ( @$this->sboption['section_linkedin']['linkedin_comments'] > 0 ) ? $this->sboption['section_linkedin']['linkedin_comments'] : 0;
                        if ($updateComments = $data->updateComments)
                        if ( ! empty($updateComments->values) && $comments_count ) {
                            foreach ( $updateComments->values as $comment ) {
                                if ( ! @$comment->comment)
                                    continue;
                                $count++;
                                $comment_message = (@$attr['commentwords']) ? $this->word_limiter(nl2br($comment->comment), @$link, true) : nl2br($comment->comment);
                                if (@$comment->company->name)
                                    $comment_title = $comment->company->name;
                                else
                                    $comment_title = @$comment->person->firstName.' '.@$comment->person->lastName;
                                $comment_user_url = (@$comment->company->id) ? 'https://www.linkedin.com/company/'.$comment->company->id : @$comment->person->siteStandardProfileRequest->url;
                                $comment_user_img = (@$comment->person->pictureUrl) ? '<img class="sb-commentimg" src="' . $comment->person->pictureUrl . '" alt="" />' : '';
                                $comments_data .= '<span class="sb-meta sb-mention">'.$comment_user_img.'<a href="' . $comment_user_url . '"'.$target.'>' . $comment_title . '</a> ' . $comment_message . '</span>';
                                if ( $count >= $comments_count ) break;
                            }
                        }
                        // likes
                        $count = 0;
                        $likes_data = '';
                        $likes_count = ( @$this->sboption['section_linkedin']['linkedin_likes'] > 0 ) ? $this->sboption['section_linkedin']['linkedin_likes'] : 0;
                        if (@$data->likes)
                        if ( ! empty($data->likes->values) && $likes_count ) {
                            $like_title = array();
                            foreach ( $data->likes->values as $like ) {
                                if ( ! @$like->person)
                                    continue;
                                $count++;
                                if (@$like->person->firstName && @$like->person->lastName)
                                    $like_title[] = $like->person->firstName.' '.$like->person->lastName;
                                if ( $count >= $likes_count ) break;
                            }
                            $likes_data .= implode(', ', $like_title);
                        }
                        
                        $meta = '';
                        if ($comments_data || $likes_data) {
                            $meta .= '
                            <span class="sb-metadata">';
                            if (@$feedoutput['comments'] && $comments_data)
                                $meta .= '
                                <span class="sb-meta">
                                    <span class="comments"><i class="sb-bico sb-comments"></i> ' . @$updateComments->_total . ' '.ucfirst(__( 'comments', 'social-board' )).'</span>
                                </span>
                                ' . $comments_data;
                            if (@$feedoutput['likes'] && $likes_data)
                            $meta .= '
                                <span class="sb-meta">
                                    <span class="likes"><i class="sb-bico sb-star"></i> ' . @$data->numLikes . ' '.ucfirst(__( 'likes', 'social-board' )).'</span>
                                </span>
                                <span class="sb-meta item-likes">
                                    ' . $likes_data . '
                                </span>';
                            $meta .= '
                            </span>';
                        }
                        
                        // get image
                        if (@$share->content) {
                            $submittedImageUrl = @$share->content->submittedImageUrl;
                            if ($submittedImageUrl) {
                                if (@$attr['https'])
                                    $submittedImageUrl = str_replace('http:', 'https:', $submittedImageUrl);
                                $thumb = $submittedImageUrl;
                            } elseif (@$share->content->thumbnailUrl)
                                $thumb = $share->content->thumbnailUrl;
                            
                            if ($iframe && @$submittedImageUrl) {
                                $url = $mediasrc = $submittedImageUrl;
                            } else {
                                if (@$share->content->shortenedUrl)
                                    $url = $share->content->shortenedUrl;
                            }
                        }
                        
                        $sbi = $this->make_timestr($data->timestamp, $link);
                        $itemdata = array(
                        'title' => $title,
                        'thumb' => $thumb,
                        'thumburl' => $url,
                        'iframe' => $iframe ? 'icbox' : '',
                        'text' => $text,
                        'url' => $link,
                        'meta' => @$meta,
                        'date' => $data->timestamp,
                        'user' => array(
                            'name' => $updateContent->company->name,
                            'url' => 'https://www.linkedin.com/company/'.$updateContent->company->id,
                            'image' => ! empty($feed->company) ? ( (@$feed->company->squareLogoUrl) ? $feed->company->squareLogoUrl : $feed->company->logoUrl ) : ''
                            ),
                        'type' => 'pencil',
                        'icon' => array(@$themeoption['social_icons'][1], @$themeoption['type_icons'][0])
                        );
							$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = $longtext;
                                if ($mediasrc)
                                    $itemdata['thumb'] = $mediasrc;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                        }
                        }
                    } // end foreach
                    }
        		} // end linkedin
        		elseif ( $feed_class == 'vine' ) {
                    if (@$feed->data->records) {
                        $iframe = @$this->sboption['section_vine']['vine_iframe'];
                    
                    // vine next page
                    $loadcrawl[$feed_class.$i.$ifeed] = @$feed->data->nextPage;
                    
                    foreach ( $feed->data->records as $data ) {
                        if ( isset($data->created) ) {
                        
                        $link = $data->permalinkUrl;
                        if ( $this->make_remove($link) ) {
                        
                        $url = $thumb = $object = $text = '';
                        if (@$data->description) {
                            $text = (@$attr['words']) ? $this->word_limiter($data->description) : @$this->format_text($data->description);
                            $text = preg_replace('/#([\\d\\w]+)/', '<a href="https://vine.co/tags/$1">$0</a>', $text);
                        }
                        
                        // comments
                        $count = 0;
                        $comments_data = '';
                        $comments_count = ( @$this->sboption['section_vine']['vine_comments'] > 0 ) ? $this->sboption['section_vine']['vine_comments'] : 0;
                        if ($comments = $data->comments)
                        if ( ! empty($comments->records) && $comments_count ) {
                            foreach ( $comments->records as $comment ) {
                                if ( ! @$comment->comment)
                                    continue;
                                $count++;
                                $comment_message = (@$attr['commentwords']) ? $this->word_limiter(nl2br($comment->comment), @$link, true) : nl2br($comment->comment);
                                $comment_user_img = (@$comment->avatarUrl) ? '<img class="sb-commentimg" src="' . $comment->avatarUrl . '" alt="" />' : '';
                                $comments_data .= '<span class="sb-meta sb-mention">'.$comment_user_img.'<a href="https://vine.co/u/' . $comment->userId . '"'.$target.'>' . @$comment->username . '</a> ' . $comment_message . '</span>';
                                if ( $count >= $comments_count ) break;
                            }
                        }
                        // likes
                        $count = 0;
                        $likes_data = '';
                        $likes_count = ( @$this->sboption['section_vine']['vine_likes'] > 0 ) ? $this->sboption['section_vine']['vine_likes'] : 0;
                        if (@$data->likes)
                        if ( ! empty($data->likes->records) && $likes_count ) {
                            $like_title = array();
                            foreach ( $data->likes->records as $like ) {
                                if ( ! @$like->username)
                                    continue;
                                $count++;
                                $like_title[] = '<a href="https://vine.co/u/'.@$like->userId.'" title="'.@$like->created.'"'.$target.'>'.$like->username.'</a>';
                                if ( $count >= $likes_count ) break;
                            }
                            $likes_data .= implode(', ', $like_title);
                        }
                        
                        $meta = '';
                        if ($comments_data || $likes_data) {
                            $meta .= '
                            <span class="sb-metadata">';
                            if (@$feedoutput['comments'] && $comments_data)
                                $meta .= '
                                <span class="sb-meta">
                                    <span class="comments"><i class="sb-bico sb-comments"></i> ' . @$data->likes->count . ' '.ucfirst(__( 'comments', 'social-board' )).'</span>
                                </span>
                                ' . $comments_data;
                            if (@$feedoutput['likes'] && $likes_data)
                            $meta .= '
                                <span class="sb-meta">
                                    <span class="likes"><i class="sb-bico sb-star"></i> ' . @$data->comments->count . ' '.ucfirst(__( 'likes', 'social-board' )).'</span>
                                </span>
                                <span class="sb-meta item-likes">
                                    ' . $likes_data . '
                                </span>';
                            $meta .= '
                            </span>';
                        }
                        
                        // get image
                        if (@$data->thumbnailUrl)
                            $thumb = (@$attr['https']) ? str_replace('http:', 'https:', $data->thumbnailUrl) : $data->thumbnailUrl;
                        
                        $url = $link;
                        if ($iframe && @$data->shareUrl) {
                            $object = '<iframe src="'.$data->shareUrl.'/embed/simple" width="600" height="600" frameborder="0"></iframe><script src="https://platform.vine.co/static/scripts/embed.js"></script>';
                            $play = true;
                        }
                        
                        $sbi = $this->make_timestr($data->created, $link);
                        $itemdata = array(
                        'thumb' => $thumb,
                        'thumburl' => $url,
                        'iframe' => $iframe ? 'iframe' : '',
                        'text' => $text,
                        'url' => $link,
                        'meta' => @$meta,
                        'date' => $data->created,
                        'user' => array(
                            'name' => $data->username,
                            'url' => 'https://vine.co/u/' . $data->userId,
                            'image' => (@$attr['https']) ? str_replace('http:', 'https:', $data->avatarUrl) : $data->avatarUrl
                            ),
                        'type' => 'play-circle',
                        'play' => @$play,
                        'icon' => array(@$themeoption['social_icons'][1], @$themeoption['type_icons'][0])
                        );
							$mediasize = '560,460';
							if (@$mediasize && ($iframe || isset($slideshow) ) )
								$itemdata['size'] = $mediasize;
							$this->final[$sbi] = $layoutobj->sb_create_item($feed_class, $itemdata, $attr, $sbi, $i, $ifeed);
                            if ( isset($slideshow) ) {
                                $itemdata['text'] = $data->description;
                                if ($object)
                                    $itemdata['object'] = $object;
                                $this->finalslide[$sbi] = $slidelayoutobj->sb_create_slideitem($feed_class, $itemdata, $attr, $sbi);
                            }
                        }
                        }
                    } // end foreach
                    }
        		} // end vine

                $final = $this->final;
				
				// each network sorting
                if ( ! empty($final) ) {
                    krsort($final);
                    reset($final);
                    $ifeedclass = $feed_class.$i.$ifeed;
                    
                    if ( ! empty($loadmore) ) {
                        // filter last items
                        if ( $lastloaditem = $loadmore[$ifeedclass] ) {
                            $loadremovefrom = array_search( $lastloaditem, array_keys($final) );
                            if ( ! @$loadremovefrom) $loadremovefrom = 0;
                            if ( empty($_SESSION[$label]['loadcrawl'][$ifeedclass]) )
                                $loadremovefrom++;
                            $final = array_slice($final, $loadremovefrom);
                        }
                    }

                    $ranking[key($final)] = $ifeedclass;
                    $finals[$ifeedclass] = $final;
                    $rankcount[$ifeedclass] = count($final);
                }
                $final = $this->final = array();
                
                } // end foreach
                }
            } // end foreach $feeds

            if ( @$ranking ) {
                // defining limits by recent basis
                krsort($ranking);
                $rsum = 0;
                $rnum = count($ranking);
                for ($i = 1; $i <= $rnum; $i++) {
                    $rsum += $i;
                }
                $i = $rnum;
                foreach ($ranking as $cfeed) {
                    $rank[$cfeed] = number_format( ($i * 100) / $rsum, 0, '.', '');
                    $i--;
                }
            }

            if ( @$rankcount ) {
                $maxcountkey = array_search(max($rankcount), $rankcount);
                foreach ($rankcount as $rkey => $rval) {
                    $fresults[$rkey] = @number_format($rank[$rkey] * $results / 100, 0, '.', '');
                }
                foreach ($rankcount as $rkey => $rval) {
                    if ( $fresults[$rkey] > $rval ) {
                        $diffrankcount = $fresults[$rkey] - $rval;
                        $fresults[$rkey] -= $diffrankcount;
                        $fresults[$maxcountkey] += $diffrankcount;
                    }
                }
            }
            
            if ( @$finals ) {
                // filnal sorting and adding
                foreach ($finals as $fkey => $fval) {
                    $fcount = 0;
                    // limit last result
                    foreach ($fval as $key => $val) {
                        $fcount++;
                        $final[$key] = $val;
                        $loadmore[$fkey] = $key;
                        if ( $fcount >= $fresults[$fkey] ) break;
                    }
                }

                if ( array_sum($rankcount) <= $results && ! $is_feed && ( ! $GLOBALS['islive'] || @$_REQUEST['action'] == "sb_loadmore" ) ) {
                    // set next pages if exist
                    foreach ($rankcount as $rkey => $rval) {
                        if (@$loadcrawl[$rkey])
                            $_SESSION[$label]['loadcrawl'][$rkey] = $loadcrawl[$rkey];
                        else
                            $_SESSION[$label]['loadcrawl'][$rkey] = null;
                    }
                }
                
                krsort($final);
                if ( $order == 'random' )
                    $final = sb_shuffle_assoc($final);
                    
                // get related ads
                $args = array(
                    'meta_key' => 'board_id',
                    'meta_value' => $id,
                    'post_type' => 'sb_ads',
                    'posts_per_page' => 999999
                );
                $adposts = get_posts($args);
                foreach ($adposts as $adpost) {
                    $post_id = $adpost->ID;
                    $adData = array();
                    foreach( ( array ) get_post_custom_keys( $post_id ) as $sKey ) {
                        $adData[ $sKey ] = get_post_meta( $post_id, $sKey, true );
                    }
                    $ad_position = (@$adData['ad_position']) ? $adData['ad_position'] : 0;
                    $ads[$ad_position][] = $adData;
                }

                // Output board items
                if (@$attr['loadmore'] && ! $ajax_feed) {
                    $_SESSION[$label]['loadcount'] = 0;
                }
                
                $display_ads = false;
                if (@$attr['display_ads']) {
                    if (@$attr['display_ads'][$type]) {
                        $display_ads = true;
                    } else {
                        if (@$attr['carousel'] && @$attr['display_ads']['carousel'])
                            $display_ads = true;
                    }
                }
                
                $i = (@$_SESSION[$label]['loadcount']) ? $_SESSION[$label]['loadcount'] : 0;
                foreach ($final as $key => $val) {
                    if ( ! empty($ads[$i]) && $display_ads) {
                        foreach ($ads[$i] as $ad) {
                            $adstyle = '';
                            if (@$ad['ad_height'])
                                $adstyle .= 'height: '.$ad['ad_height'].'px;';
                            if ( isset($ad['ad_border_size']) )
                                $adstyle .= 'border-width: '.$ad['ad_border_size'].'px;';
                            if (@$ad['ad_border_color'] && @$ad['ad_border_size'])
                                $adstyle .= 'border-color: '.$ad['ad_border_color'].';';
                            if (@$ad['ad_background_color'])
                                $adstyle .= 'background-color: '.$ad['ad_background_color'].';';
                            if (@$ad['ad_text_align'])
                                $adstyle .= 'text-align: '.$ad['ad_text_align'].';';
                            if ($ad['ad_type'] == 'image')
                                $adstyle .= 'line-height: 0;';
                            $adtag = ($type != 'feed' || @$attr['carousel']) ? 'div' : 'li';
                            $adgrid = (@$ad['ad_grid_size']) ? ($ad['ad_grid_size'] == 'solo' ? '' : ' sb-'.$ad['ad_grid_size']) : '';
                            // get ad content
                            switch ($ad['ad_type']) {
                                case "text":
                                    $adinnerstyle = '';
                                    if (@$ad['ad_border_size']) {
                                        $border_radius = 5+$ad['ad_border_size']-1;
                                        $adinnerstyle .= 'border-radius: '.$border_radius.'px;-moz-border-radius: '.$border_radius.'px;-webkit-border-radius: '.$border_radius.'px;';
                                    }
                                    $adinnerstyle .= 'background-color: transparent !important;';
                                    $adcontent = '<div class="sb-inner"'.(@$adinnerstyle ? ' style="'.$adinnerstyle.'"' : '').'>'.nl2br($ad['ad_text']).'</div>';
                                break;
                                case "code":
                                    $adcontent = $ad['ad_custom_code'];
                                break;
                                case "image":
                                    if (@$ad['ad_text_align']) {
                                        if ($ad['ad_text_align'] == 'left')
                                            $admargin = 'margin-right: auto;';
                                        elseif ($ad['ad_text_align'] == 'right')
                                            $admargin = 'margin-left: auto;';
                                        else
                                            $admargin = 'margin: auto;';
                                    }
                                    $ad_link_target = (@$ad['ad_link_target'] == 'blank') ? ' target="_blank"' : '';
                                    $adcontent = '<a href="'.$ad['ad_link'].'"'.$ad_link_target.'><img src="'.$ad['ad_image'].'"'.(@$admargin ? ' style="'.$admargin.'max-height: 100%;"' : '').'></a>';
                                break;
                            }
                            if ( $is_timeline ) {
                                $adout = '
                                <div class="timeline-row">
                                    <div class="timeline-icon">
                                      <div class="bg-ad">
                                        <i class="sb-bico sb-wico sb-star"></i>
                                      </div>
                                    </div>
                                    <div class="timeline-content">
                                      <div class="panel-body sb-item sb-advert">
                                        <div class="sb-container"'.(@$adstyle ? ' style="'.$adstyle.'"' : '').'>';
                                        $adout .= $adcontent;
                                        $adout .= '
                                        </div>
                                      </div>
                                    </div>
                                </div>' . "\n";
                            } else {
                                $adout1 = '
                                <'.$adtag.' class="sb-item sb-advert'.$adgrid.'">
                                    <div class="sb-container"'.(@$adstyle ? ' style="'.$adstyle.'"' : '').'>';
                                    $adout1 .= $adcontent;
                                $adout1 .= '
                                    </div>
                                </'.$adtag.'>' . "\n";
                                $adout = (@$attr['carousel']) ? '<li>'.$adout1.'</li>' : $adout1;
                            }
                            $output .= $adout;
                        }
                    }
                    $i++;
                    $output .= $val;
                    
                    if ( isset($slideshow) ) {
                        $ss_output .= $this->finalslide[$key];
                    }
                } // end foreach $final
                if (@$attr['loadmore']) {
                    $_SESSION[$label]['loadcount'] = $i;
                }
            } else {
                if ( empty($loadmore) )
                    $output_error = '<p class="sboard-nodata"><strong>Social Board: </strong>There is no feed data to display!</p>';
            }
        } else {
            if ( empty($loadmore) )
                $output_error = '<p class="sboard-nodata"><strong>Social Board: </strong>There is no feed to show or there is a connectivity problem to the world wide web!</p>';
        }

        if (@$attr['loadmore']) {
            $_SESSION[$label]['loadmore'] = $loadmore;
        }
        
        if ($ajax_feed && $is_feed) {
            if (@$output_error)
                $output .= $output_error;
        }
        
    	if ( ! $ajax_feed) {
            if ( $is_feed ) {
                if (@$output_error)
                    $output .= $output_error;
                $output .= "</ul></div>";
                
                if ( ! @$attr['carousel']) {
                    if (@$attr['autostart']) {
                        $play_none = ' style="display: none;"';
                    } else {
                        $pause_none = ' style="display: none;"';
                    }
                    $controls = (@$attr['controls']) ? '
                        <div class="control">
                            <span class="sb-hover" id="ticker-next-'.$label.'"><i class="sb-bico sb-wico sb-arrow-down"></i></span>
                            <span class="sb-hover" id="ticker-prev-'.$label.'"><i class="sb-bico sb-wico sb-arrow-up"></i></span>
                            <span class="sb-hover" id="ticker-pause-'.$label.'"'.@$pause_none.'><i class="sb-bico sb-wico sb-pause"></i></span>
                            <span class="sb-hover" id="ticker-play-'.$label.'"'.@$play_none.'><i class="sb-bico sb-wico sb-play"></i></span>
                        </div>' : '';
                        
                    $filters = '';
                    if ( ! @$attr['tabable'] && @$filterItems && ! empty($feeds) ) {
                        $filters = (@$attr['filters']) ? '
                        <div class="filter">
                            <span class="sb-hover'.(@$attr['default_filter'] ? '' : ' active').'" data-filter="all"><i class="sb-bico sb-wico sb-ellipsis-h" title="'.__( 'Show All', 'social-board' ).'"></i></span>
                            '.implode("\n", $filterItems).'
                        </div>' : '';
                    }
                    
                    if (@$attr['filters'] or @$attr['controls'])
                    $output .= '
                    <div class="toolbar">
                        '.$controls.'
                        '.$filters.'
                    </div>'."\n";
                }
            }
        }

        if ($is_wall || $is_timeline) {
            if (@$output_error) {
                $output = str_replace(' timeline ', ' ', $output);
                $output .= $output_error;
            }
        }
        
        if ( ! $ajax_feed) {
            $output .= "</div>\n";
            $loadmoretxt = (@$attr['loadmore'] == 2) ? '' : '<p>'.__( 'Load More', 'social-board' ).'</p>';
            $loadmorecls = (@$attr['loadmore'] == 2) ? ' sb-infinite' : '';
            if ( ( ! $is_feed && ! @$output_error ) && @$attr['loadmore'] )
                $output .= '<div class="sb-loadmore'.$loadmorecls.'" data-nonce="'.wp_create_nonce( 'loadmore' ).'">'.$loadmoretxt.'</div>'."\n";
            if ($is_wall || $is_timeline)
                $output .= "</div>\n";

            $iframe_output = $iframe_slideshow = $iframe_media = '';
            if (@$attr['lightboxtype'] == 'slideshow') {
			$iframe_output = $iframe_slideshow = '
                $(".sb-inline").colorbox({
                    inline:true,
                    rel:"sb-inline",
                    href: function(){
                      return $(this).data("href");
                    },
					maxHeight:"95%",
                    width:"85%",
                    current:"slide {current} of {total}",
                    onComplete: function() {
                        var href, attrwidth, aspectratio, newheight = "";
						var winCurrentWidth = $(window).width();
						var winCurrentHeight = $(window).height();
						if (winCurrentWidth >= 768) {
							href = $(this).data("href");
							thumbimg = $(href + " .sb-inner .sb-thumb img," + href + " .sb-inner .sb-thumb iframe");
							attrwidth = thumbimg.attr("width");
							if (!attrwidth) {
								sizearrY = thumbimg.height();
								sizearrX = thumbimg.width();
								if (sizearrY) {
									var gapHeight = Math.round((winCurrentHeight * 5) / 100);
									var currentHeight = winCurrentHeight-gapHeight-30;
									if (currentHeight < sizearrY) {
										var newheight = currentHeight;
										
										aspectratio = sizearrX * newheight;
										newwidth = Math.round(aspectratio / sizearrY);
										sizearrX = newwidth;
										sizearrY = newheight;
										
										thumbimg.height(newheight);
									} else {
										var newheight = "500";
									}
									$(href + " .sb-inner .sb-body").innerHeight(newheight);
									
									if (thumbimg.height() > 500) {
										thumbimg.height(newheight);
									}
								}
								jQuery(this).colorbox.resize({innerHeight:newheight});
							}
						}
                    },
                    onLoad:function(){
                        $(".sb-slide .sb-thumb").empty();
                        var sizestr, href, inner, type, media, size = "";
						var wsize = sb_getwinsize();
						var bheight = (wsize.newHeight < 500) ? wsize.newHeight : 500;
                        href = $(this).data("href");
                        inner = $(href + " .sb-inner");
                        type = inner.data("type");
                        if (type) {
                            media = inner.data("media");
                            size = inner.data("size");
                            sizearr = size.split(",");
                            sizearrX = sizearr[0];
                            sizearrY = sizearr[1];
							thumb = inner.children(".sb-thumb");
							newConWidth = Math.round((wsize.newWidth * 70) / 100);
                            
							if ( (sizearrX && sizearrY) && (sizearrX > 400 || sizearrY > 400) ) {
								if (wsize.winCurrentWidth > 768) {
									if (sizearrY < 400) {
										thumb.width("50%");
										inner.children(".sb-body").width("50%").children(".sb-slide-footer").width("50%");
									}
									
									if (wsize.winCurrentHeight < sizearrY || newConWidth < sizearrX) {
										aspectratio = sizearrX * wsize.newHeight;
										sizearrX = Math.round(aspectratio / sizearrY);
										sizearrY = wsize.newHeight;
										
										if (sizearrX > newConWidth) {
											aspectratio = sizearrY * newConWidth;
											sizearrY = Math.floor(aspectratio / sizearrX);
											sizearrX = newConWidth;
											$(href + " .sb-inner .sb-body").innerHeight(sizearrY);
										} else {
											$(href + " .sb-inner .sb-body").innerHeight(wsize.newHeight);
										}
									} else {
										if (sizearrY && sizearrY > 400) {
											$(href + " .sb-inner .sb-body").innerHeight(sizearrY);
										}
									}
								} else {
									if (wsize.newWidth < sizearrX) {
										aspectratio = sizearrY * wsize.newWidth;
										sizearrY = Math.round(aspectratio / sizearrX);
										sizearrX = wsize.newWidth;
									} else if (newConWidth < sizearrX) {
										aspectratio = sizearrY * newConWidth;
										sizearrY = Math.round(aspectratio / sizearrX);
										sizearrX = newConWidth;
                                    }
                                    $(href + " .sb-inner .sb-body").innerHeight("auto");
								}
							} else {
								sizestr = "";
								thumb.width("50%");
								inner.children(".sb-body").width("50%").children(".sb-slide-footer").width("50%");
							}
							
                            if (type == "image") {
                                if ( (sizearrX && sizearrY) && (sizearr[0] > 400 || sizearr[1] > 400) ) {
                                    sizestr = " style=\'width:" + sizearrX + "px;height:" + sizearrY + "px\' width=\'" + sizearr[0] + "\' height=\'" + sizearr[1] + "\'";
                                    thumb.html("<img src=\'" + media + "\'" + sizestr + " alt=\'\'>");
                                } else {
                                    thumb.html("<span><img src=\'" + media + "\' class=\'sb-imgholder\' alt=\'\'></span>");
                                }
                            } else if (type == "video") {
                                if ( (sizearrX && sizearrY) && (sizearrX > 400 || sizearrY > 400) ) {
										sizestr = " style=\'width:" + sizearrX + "px;height:" + sizearrY + "px\' width=\'" + sizearr[0] + "\' height=\'" + sizearr[1] + "\'";
                                } else {
                                    sizestr = " width=\'560\' height=\'315\'";
                                }
                                var imedia = "<iframe" + sizestr + " src=\'" + media + "\' allowfullscreen=\'\' webkitallowfullscreen=\'\' mozallowfullscreen=\'\' autoplay=\'0\' wmode=\'opaque\' frameborder=\'0\'></iframe>";
								if (sizearr[1] && sizearr[1] > 400) {
									thumb.html(imedia);
								} else {
									thumb.html("<span>" + imedia + "</span>");
									$(href + " .sb-inner .sb-body").innerHeight(bheight);
								}
                            } else {
                                if (sizearrY && sizearrY > 400) {
									thumb.html(media);
								} else {
									thumb.html("<span>" + media + "</span>");
									if (wsize.winCurrentWidth > 768)
										$(href + " .sb-inner .sb-body").innerHeight(bheight);
								}
                            }
                        } else {
							$(href + " .sb-inner .sb-body").innerHeight(bheight);
						}
                    },
                    onClosed:function(){ $(".sb-slide .sb-thumb").empty(); }
                });';
            } else {
                $iframe_output = $iframe_media = '
				$(".sboard .sb-thumb .iframe").colorbox({
					iframe: true,
                    maxWidth: "85%",
                    maxHeight: "95%",
					width: function() {
                        var size = $(this).data("size");
                        if (size) {
                            sizearr = size.split(",");
				            return parseInt(sizearr[0])+10;
                        } else {
                            return 640;
                        }
					},
					height: function() {
                        var size = $(this).data("size");
                        if (size) {
                            sizearr = size.split(",");
                            return parseInt(sizearr[1])+10;
                        } else {
                            return 460;
                        }
					},
					onComplete: function() {
						var size = $(this).data("size");
                        if (size) {
    						var sizearr = size.split(",");
    						var iframebox = $( "#cboxLoadedContent iframe" );
    						if (iframebox.length) {
    							iframebox.attr("width", sizearr[0]).attr("height", sizearr[1]);
    						}
                        }
					}
				});
				$(".sboard .sb-thumb .icbox").colorbox({photo:true, maxWidth:"95%", maxHeight:"95%"});
				$(".sboard .sb-thumb .inline").colorbox({inline:true, maxWidth:"95%", maxHeight:"95%"});';
            }
        
        // Lazy load images
        $relayout_imgload = (@$attr['wall_relayout'] != '') ? ',
		     	appear: function() {
		     		$wall.isotope("layout");
		     	}' : '';
        $lazyload_output = '
			$(".sb-thumb img").lazyload({
				effect: "fadeIn",
				skip_invisible: true,
				container: $("#sb_'.$label.'")'.$relayout_imgload.'
			});';
		
        // loadmore ajax function
        $more_output = '';
        if (@$attr['loadmore']) {
            if (@$attr['loadmore'] == 2) {
                $fail_func = 'console.log';
                $more_output = '
                var sbwin = $(window);
                /* Each time the user scrolls */
                sbwin.scroll(function() {
                  /* End of the document reached? */
                  if ( $(document).height() - sbwin.height() == sbwin.scrollTop() ) {';
            } else {
                $fail_func = 'alert';
                $more_output = '
                jQuery("#sb_'.$label.'").on("click", ".sb-loadmore", function() {';
            }
                $more_output .= '
                  lmobj = $("#sb_'.$label.' .sb-loadmore");
                  lmnonce = lmobj.attr("data-nonce");';
                $more_output .= "$('#sb_".$label." .sb-loadmore').html('<p class=\"sb-loading\">&nbsp;</p>');";
                $more_output .= '
                  $.ajax({
                    type: "post",
                    url: "'.admin_url( 'admin-ajax.php' ).'",
                    data: {action: "sb_loadmore", attr: '.$attr_ajax.', nonce: lmnonce, label: "'.$label.'"},
                    cache: false
                    })
                    .done(function( response ) {
                        /* append and lay out items */';
                    if ( $is_wall ) {
                        $more_output .= '
                        var lmdata = $(response);
                        var $items = lmdata.filter(".sb-item");
                        var $slides = lmdata.filter(".sb-slide");
                        $wall.append( $items ).isotope( "appended", $items );
                        $("#sb_slides_'.$label.'").append( $slides );
                        $(window).trigger("resize");
                        $(window).trigger("scroll");';
    				} else {
    				    $more_output .= '
    				    $("#timeline_'.$label.'").append(response);';
    				}
                    $more_output .= $lazyload_output . $iframe_output . '
                        $("#sb_'.$label.' .sb-loadmore").html("'.$loadmoretxt.'");
                    })
                    .fail(function() {
                        '.$fail_func.'("Problem reading the feed data!");
                    });';
            if (@$attr['loadmore'] == 2) {
                $more_output .= '
                  }';
            }
                $more_output .= '
                });';
        }
        
        $output .= '
        <script type="text/javascript">
            jQuery(document).ready(function($) {
				function sb_getwinsize() {
					var wsize = {
						winCurrentWidth: $(window).width(),
						newWidth: 0,
						winCurrentHeight: $(window).height(),
						newHeight: 0
					};
					var gapWidth = Math.round((wsize.winCurrentWidth * 15) / 100);
					var currentWidth = wsize.winCurrentWidth-gapWidth;
					wsize.newWidth = currentWidth-10;
					
					var gapHeight = Math.round((wsize.winCurrentHeight * 5) / 100);
					var currentHeight = wsize.winCurrentHeight-gapHeight;
					wsize.newHeight = currentHeight-30;
					return wsize;
				}';

        $ticker_id_t = '';
        if ( $is_feed ) {
            if (@$attr['carousel']) {
                $cs_auto = (@$attr['cs_auto']) ? 'slider.play();' : '';
                if ( ! @is_array($attr['cs_item']) ) {
                    if ($attr['cs_item'] == '')
                        $attr['cs_item'] = $defoption['carouselsetting']['cs_item'];
                    else
                        $attr['cs_item'] = explode(',', trim($attr['cs_item']) );
                }
			$output .= '
    			var slider = $("#ticker_'.$label.'").lightSlider({
                    item: '.@$attr['cs_item'][0].',
                    autoWidth: '.@$attr['autoWidth'].',
                    slideMove: '.@$attr['slideMove'].',
                    slideMargin: '.@$attr['slideMargin'].',
                    mode: "slide",
                    pauseOnHover: true,
                    auto: '.(@$attr['cs_auto'] ? 'true' : 'false').',
                    loop: '.(@$attr['cs_loop'] ? 'true' : 'false').',
                    controls: '.(@$attr['cs_controls'] ? 'true' : 'false').',
                    rtl: '.@$attr['cs_rtl'].',
                    pager: '.(@$attr['cs_pager'] ? 'true' : 'false').',
                    speed: '.@$attr['cs_speed'].',
                    pause: '.@$attr['cs_pause'].',
                    responsive : [
                        {
                            breakpoint:960,
                            settings: {
                                item: '.@$attr['cs_item'][1].'
                              }
                        },
                        {
                            breakpoint:768,
                            settings: {
                                item: '.@$attr['cs_item'][2].'
                              }
                        },
                        {
                            breakpoint:600,
                            settings: {
                                item: '.@$attr['cs_item'][3].'
                              }
                        },
                        {
                            breakpoint:480,
                            settings: {
                                item: '.@$attr['cs_item'][4].'
                              }
                        }
                    ],
					onBeforeNextSlide: function (el, scene) {
						var slidetotal = el.getTotalSlideCount();
						var vsnum = el.find(".clone.left").length;
						for (i = 0; i < 4; i++) {
							var inum = scene+vsnum+i;
							if (inum > slidetotal)
								inum = inum - slidetotal;
							var lielem = $("#ticker_'.$label.' li").eq(inum).find(".sb-thumb .sb-crop");
							if ( typeof lielem.attr("data-original") !== "undefined" ) {
                                if ( lielem.attr("data-original") !== null )
    								$( "a[data-original=\'"+lielem.attr("data-original")+"\']" ).trigger("appear");
                            }
						}
					},
					onBeforePrevSlide: function (el, scene) {
						var slidetotal = el.getTotalSlideCount();
						for (i = 0; i < 4; i++) {
							var inum = scene-i;
							if (inum < 0)
								inum = inum + slidetotal;
							var lielem = $("#ticker_'.$label.' li").eq(inum).find(".sb-thumb .sb-crop");
							if ( typeof lielem.attr("data-original") !== "undefined" ) {
                                if ( lielem.attr("data-original") !== null )
    								$( "a[data-original=\'"+lielem.attr("data-original")+"\']" ).trigger("appear");
                            }
						}
					}
                });
                ' . $cs_auto . '
				$(".sb-thumb .sb-crop").lazyload({
					effect: "fadeIn",
					skip_invisible: true,
					threshold: '.$block_height.',
					placeholder: "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjAwIiBoZWlnaHQ9IjIwMCIgdmlld0JveD0iLTIzIC0yMyA4NCA4NCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBzdHJva2U9IiNmZmYiPiAgICA8ZyBmaWxsPSJub25lIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiPiAgICAgICAgPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMSAxKSIgc3Ryb2tlLXdpZHRoPSIyIj4gICAgICAgICAgICA8Y2lyY2xlIHN0cm9rZS1vcGFjaXR5PSIuNSIgY3g9IjE4IiBjeT0iMTgiIHI9IjE4Ii8+ICAgICAgICAgICAgPHBhdGggZD0iTTM2IDE4YzAtOS45NC04LjA2LTE4LTE4LTE4Ij4gICAgICAgICAgICAgICAgPGFuaW1hdGVUcmFuc2Zvcm0gICAgICAgICAgICAgICAgICAgIGF0dHJpYnV0ZU5hbWU9InRyYW5zZm9ybSIgICAgICAgICAgICAgICAgICAgIHR5cGU9InJvdGF0ZSIgICAgICAgICAgICAgICAgICAgIGZyb209IjAgMTggMTgiICAgICAgICAgICAgICAgICAgICB0bz0iMzYwIDE4IDE4IiAgICAgICAgICAgICAgICAgICAgZHVyPSIxcyIgICAgICAgICAgICAgICAgICAgIHJlcGVhdENvdW50PSJpbmRlZmluaXRlIi8+ICAgICAgICAgICAgPC9wYXRoPiAgICAgICAgPC9nPiAgICA8L2c+PC9zdmc+"
				});';
            }
			
            if ( ! @$attr['carousel']) {
                $ticker_id = '#ticker_'.$label;
                
				$ticker_lazyload_output = '
				$(".sb-thumb img").lazyload({
					effect: "fadeIn",
					skip_invisible: true,
					container: $("'.$ticker_id.'"),
					threshold: '.($block_height * 2).',
					failure_limit: '.$results.'
				});';
				
                $output .= '
				function sb_tickerlazyload() {
					var lielem = $("'.$ticker_id.' li:last-child");
					var lix = lielem.index();
					for (i = 0; i < 4; i++) {
						var inum = lix-i;
						var imgelem = $("'.$ticker_id.' li").eq(inum).find(".sb-thumb img");
						if (typeof imgelem.attr("data-original") !== "undefined" && imgelem.attr("data-original") !== null)
							$( "img[data-original=\'"+imgelem.attr("data-original")+"\']" ).trigger("appear");
					}
				}
				
                var $sbticker = $("'.$ticker_id.'").newsTicker({
                    row_height: '.$block_height.',
                    max_rows: 1,
                    speed: '.@$attr['rotate_speed'].',
                    duration: '.@$attr['duration'].',
                    direction: "'.@$attr['direction'].'",
                    autostart: '.@$attr['autostart'].',
                    pauseOnHover: '.@$attr['pauseonhover'].',
                    prevButton: $("#ticker-prev-'.$label.'"),
                    nextButton: $("#ticker-next-'.$label.'"),
                    stopButton: $("#ticker-pause-'.$label.'"),
                    startButton: $("#ticker-play-'.$label.'"),
                    start: function() {
                    	$("#timeline_'.$label.' #ticker-pause-'.$label.'").show();
                        $("#timeline_'.$label.' #ticker-play-'.$label.'").hide();
                    },
                    stop: function() {
                    	$("#timeline_'.$label.' #ticker-pause-'.$label.'").hide();
                        $("#timeline_'.$label.' #ticker-play-'.$label.'").show();
                    },
					movingUp: function() {
						$("'.$ticker_id.'").trigger("scroll");
					},
					movingDown: function() {
						sb_tickerlazyload();
					}
                });';
                if (@$attr['tabable'] && @$attr['autoclose'] ) {
				    $output .= '$sbticker.newsTicker("pause");';
				}
                $output .= $ticker_lazyload_output . '
				sb_tickerlazyload();';
				
				// Filtering rotating feed
				if ( ! @$attr['tabable'] && @$attr['filters'] ) {
                $output .= "
                $('#timeline_$label .filter span').click(function() {
            		/* fetch the class of the clicked item */
            		var ourClass = $(this).data('filter');
            		
            		/* reset the active class on all the buttons */
            		$('#timeline_$label .filter span').removeClass('active');
            		/* update the active state on our clicked button */
            		$(this).addClass('active');
            		
            		if (ourClass == 'all') {
            			/* show all our items */
            			$('$ticker_id').children('li.sb-item').show();
            		} else {
            			/* hide all elements that don't share ourClass */
            			$('$ticker_id').children('li:not(' + ourClass + ')').fadeOut('fast');
            			/* show all elements that do share ourClass */
            			$('$ticker_id').children('li' + ourClass).fadeIn('fast');
                        
        				setTimeout(function() {
                            $('$ticker_id').trigger('scroll');
        				}, 500);
            		}
            		return false;
            	});";
				}
            }
                
            if ( @$attr['slide'] && ! (@$attr['tabable'] && @$attr['position'] == 'normal') ) {
                if ( $location == 'left' || $location == 'right' ){
                    $getsizeof = 'Width';
                    $opener = 'sb-opener';
                    $padding = '';
                } else {
                    $getsizeof = 'Height';
                    $opener = 'sb-heading';
                    $padding = ( @$attr['showheader'] || ($location == 'bottom' && ! @$attr['tabable']) ) ? ' - 30' : '';
                }
                $openid = (@$attr['tabable']) ? "#timeline_$label .sb-tabs li" : "#timeline_$label .$opener";
                $output .= "
                /* slide in-out */
                var padding = $('#timeline_$label').outer$getsizeof();
                padding = parseFloat(padding)$padding;";
                $output .= ( @$attr['autoclose'] ) ? "$('#timeline_$label').animate({ '$location': '-='+padding+'px' }, 'fast' );" : '';
                $output .= "
                $('$openid').click(function(event) {
                    if ( $('#timeline_$label').hasClass('open') ) {
                        if ( $(this).hasClass( 'active' ) ) {
                            $('$openid').removeClass('active');
                            $('#timeline_$label').animate({ '$location': '-='+padding+'px' }, 'slow' ).removeClass('open');
                        } else {
                            $('$openid').removeClass('active');
                            $(this).addClass('active');
                        }
                    } else {
                        $(this).addClass('active');
                        $('#timeline_$label').animate({ '$location': '+='+padding+'px' }, 'slow' ).addClass('open');
                    }
                    event.preventDefault();
                });";
			}
            else { // only for normal tabable
                    $openid = "#timeline_$label .sb-tabs li";
                $output .= "
                $('$openid').click(function(event) {
                    $('$openid').removeClass('active');
                    if ( $('#timeline_$label').hasClass('open') ) {
                        if ( $(this).hasClass( 'active' ) ) {
                            $('#timeline_$label').removeClass('open');
                        } else {
                            $(this).addClass('active');
                        }
                    } else {
                        $(this).addClass('active');
                        $('#timeline_$label').addClass('open');
                    }
                    event.preventDefault();
                });";
            }
            
            if (@$ticker_id)
                $ticker_id_t = ' '.$ticker_id;
        } elseif ( $is_wall ) {
            if ( ! empty($feeds) ) {
                if (@$attr['stagger'])
                	$wallExt['stagger'] = 'stagger: '.$attr['stagger'];
                if (@$attr['default_filter'])
                	$wallExt['filter'] = 'filter: ".sb-'.$attr['default_filter'].'"';
                if ( ! empty($wallExt) )
                	$wallExtStr = implode(',', $wallExt);
                $columnWidth = (@$attr['fixWidth'] == 'false') ? '".sb-isizer"' : $itemwidth;
                $gutter = (@$attr['fixWidth'] == 'false') ? '".sb-gsizer"' : $gutterX;
                $percentPosition = (@$attr['fixWidth'] == 'false') ? 'true' : 'false';
                if (@$attr['fixWidth'] == 'block') {
                $output .= '
                function sb_setwallgrid($wall) {
                	var wallw = $wall.width();
                	if (wallw >= 960 && wallw < 1200) {
                		var ncol = '.$attr['breakpoints'][1].';
                		'.$bpcol[1].'
                	}
                	else if (wallw >= 768 && wallw < 959) {
                		var ncol = '.$attr['breakpoints'][2].';
                		'.$bpcol[2].'
                	}
                	else if (wallw >= 600 && wallw < 767) {
                		var ncol = '.$attr['breakpoints'][3].';
                		'.$bpcol[3].'
                	}
                	else if (wallw >= 480 && wallw < 599) {
                		var ncol = '.$attr['breakpoints'][4].';
                		'.$bpcol[4].'
                	}
                	else if (wallw >= 320 && wallw < 479) {
                		var ncol = '.$attr['breakpoints'][5].';
                		'.$bpcol[5].'
                	}
                	else if (wallw <= 319) {
                		var ncol = '.$attr['breakpoints'][6].';
                		'.$bpcol[6].'
                	} else {
						var ncol = '.$attr['breakpoints'][0].';
						'.$bpcol[0].'
					}
					var twgut = '.$gutterX.' * (ncol-1);
                	var itemw = (wallw - twgut) / ncol;
                	$wall.isotope({
                		masonry: {
                			columnWidth: parseFloat(itemw.toFixed(3)),
                			gutter: '.$gutterX.'
                		}
                	});
                }';
                }
                $output .= '
    			var $wall = $("#timeline_'.$label.$ticker_id_t.'").isotope({
                    itemSelector: ".sb-item",
                    layoutMode: "masonry",
					getSortData: {
                      dateid: function( itemElem ) {
                      	return $( itemElem ).attr("id");
                      }
					},
                    percentPosition: '.$percentPosition.',';
                    if (@$attr['fixWidth'] == 'false') {
                    	$output .= '
                    masonry: {
                      columnWidth: '.$columnWidth.',
                      gutter: '.$gutter.'
                    },';
                    }
                    $output .= '
                    transitionDuration: '.@$attr['transition'].',
                    originLeft: '.@$attr['originLeft'].',
                    '.@$wallExtStr.'
    			});';
    			if (@$attr['fixWidth'] == 'block')
    				$output .= 'sb_setwallgrid($wall);';
    			$output .= str_replace('.sb-thumb img', '#timeline_'.$label.$ticker_id_t.' .sb-thumb img', $lazyload_output) . '
				
				/* wall in cache */
				setTimeout(function() {
					$wall.isotope("layout");
                    $(window).trigger("resize");
                    $(window).trigger("scroll");
				}, 500);
                $(window).resize(function() {';
                if (@$attr['fixWidth'] == 'block')
                    $output .= 'sb_setwallgrid($wall);';
                $output .= '
                    setTimeout(function() {
                        $(window).trigger("scroll");
                    }, 500);
                });

    			/* Filter wall by networks */
				$(".filter-items").on("click", "span", function() {
                    $(".filter-label,.sb-filter").removeClass("active");
                    var filterValue = $(this).addClass("active").attr("data-filter");
					if ( $(this).hasClass( "filter-label" ) ) {
						$wall.isotope({ filter: filterValue });
	                    $wall.on( "arrangeComplete", function() {
	                        $(window).trigger("resize");
	                    });
                    }
    			});';
                
                // fix lazyload after live update interval
                if (@$GLOBALS['islive'] && ! $is_feed) {
                    $output .= '
                    $wall.on( "removeComplete", function() {
                        $(window).trigger("scroll");
                    });';
                }
                
                // filter wall with a text phrase
				if ( @$attr['filter_search'] ) {
                    $output .= '
                $("#sb_'.$label.$ticker_id_t.' .sb-search").keyup(function(){
                    var filterValue = $(this).val();
                    if (filterValue != "") {
                        $wall.isotope({
                            filter: function() {
                                return ($(this).text().search(new RegExp(filterValue, "i")) > 0);
                            }
                        });
                    } else {
                        $wall.isotope({ filter: "*" });
                    }
                	$wall.on( "arrangeComplete", function() {
                    	$(window).trigger("resize");
                	});
                });';
                }
                
                if ( ! empty($this->sboption['filtering_tabs']) ) {
                    $output .= '
                $("#sb_'.$label.$ticker_id_t.' .sb-filter").click(function(){
                    var filterTerm = $(this).attr("data-filter");
                    if (filterTerm != "") {
						var filterRegex = /^\.+[a-z]+-\d+-\d+$/ig;
						if (filterRegex.test(filterTerm)) {
							$wall.isotope({ filter: filterTerm });
						} else {
	                        $wall.isotope({
	                            filter: function() {
	                                return ($(this).text().search(new RegExp(filterTerm, "ig")) > 0);
	                            }
	                        });
                        }
                    	$wall.on( "arrangeComplete", function() {
                        	$(window).trigger("resize");
                    	});
                    }
                });';
                }
				
                $relayout_scroll = '$wall.isotope("layout");$(window).trigger("resize");';
                if (@$attr['wall_height'] != '') {
                    $output .= 'scrollStop(function () {
                        '.$relayout_scroll.'
                    }, "sb_'.$label.'");';
                } else {
                    $output .= 'scrollStop(function () {
                        '.$relayout_scroll.'
                    });';
                }
                $output .= $more_output;
            }
        } elseif ( $is_timeline ) {
			$output .= '
				$(".sb-thumb img").lazyload({
					effect: "fadeIn",
					skip_invisible: true
				});';
            if (@$more_output)
				$output .= $more_output;
        }

        // load tabs and rebuild feed ticker
        if (@$attr['tabable']) {
        	$output .= '
               $("#timeline_'.$label.' .sb-tabs").on("click", "li", function() {
                if ( $(this).hasClass( "active" ) ) {
                  feed = $(this).attr("data-feed");
                  tabnonce = $(this).parent().attr("data-nonce");
                  ';
               $output .= "
                  $('#timeline_".$label." .sb-content ul').html('<p class=\"sb-loading\"><i class=\"sb-icon sb-'+feed+'\"></i></p>');";
               $output .= '
                  $.ajax({
                    type: "post",
                    url: "'.admin_url( 'admin-ajax.php' ).'",
                    data: {action: "sb_tabable", feed: feed, attr: '.$attr_ajax.', nonce: tabnonce},
                    cache: false
                  })
                  .done(function( response ) {
                    $("#timeline_'.$label.$ticker_id_t.'").html(response);
                    $sbticker.newsTicker();
                    ' . $ticker_lazyload_output . $iframe_output . '
                  })
                  .fail(function() {
                    alert( "Problem reading the feed data!" );
                  });
                }
               });';
         }
         
         if (@$iframe) {
				if (@$attr['lightboxtype'] == 'slideshow') {
					if ( ! isset($GLOBALS['sb_scripts']['iframe_slideshow']) ) {
						$output .= $iframe_slideshow;
						$GLOBALS['sb_scripts']['iframe_slideshow'] = true;
					}
				} else {
					if ( ! isset($GLOBALS['sb_scripts']['iframe_media']) ) {
						$output .= $iframe_media;
						$GLOBALS['sb_scripts']['iframe_media'] = true;
					}
				}

				if ( isset($slideshow) ) {
					$colorbox_resize = 'width:"85%"';
					$slicepoint = (@$attr['slicepoint']) ? $attr['slicepoint'] : 300;
					$output .= '
					  $("div.sb-body .sb-text").expander({
						slicePoint: '.$slicepoint.',
						expandText: "'.__( 'read more (+)', 'social-board' ).'",
						userCollapseText: "'.__( 'read less (-)', 'social-board' ).'"
					  });';
				} else {
					$colorbox_resize = 'maxWidth:"95%", maxHeight:"95%"';
				}
				
				// resize colorbox on screen rotation
				$resize_part1 = '
                $(window).on("resize", function() {
                    if (jQuery("#cboxOverlay").is(":visible")) {
						var wsize = sb_getwinsize();
						var cbox = $( "#cboxLoadedContent" );';
					// Slide autosize
					if ( isset($slideshow) ) {
						$resize_part2 = '
						var slidespan = $("#cboxLoadedContent .sb-slide .sb-thumb");
						if (slidespan.length > 0) {
                            var slidethumb = $(".sb-slide .sb-thumb iframe, .sb-slide .sb-thumb img");
							if ( slidethumb.attr("height") ) {
								var cwidth = ( cbox.width() < slidethumb.attr("width") ) ? cbox.width() : slidethumb.width();
								var wwidth = Math.round((wsize.newWidth * 70) / 100);
								if (cwidth < wwidth && wsize.newHeight > slidethumb.attr("height")) {
									cwidth = wwidth;
								}
                                if (slidethumb.attr("width") < wwidth) {
                                    var newheight = slidethumb.attr("height") ? slidethumb.attr("height") : wsize.newHeight;
                                    cwidth = Math.floor( (slidethumb.attr("width") * newheight ) / slidethumb.attr("height") );
                                } else {
                                    if ( $(window).width() > 768 ) {
								        var newheight = Math.floor( (wwidth * slidethumb.attr("height") ) / slidethumb.attr("width") );
                                        slidethumb.width(wwidth);
                                    } else {
                                        var newheight = Math.floor( (wsize.newWidth * slidethumb.attr("height") ) / slidethumb.attr("width") );
                                    }
                                }
								slidethumb.height(newheight);
								if (slidethumb.width() < cwidth)
									slidethumb.width(cwidth);
							} else {
								var newheight = cbox.height() / 2;
                                slidethumb.css("max-height", newheight);
							}
							
							if ( $(window).width() >= 768 ) {
								if (slidespan.children("span").length > 0) {
									$(".sb-slide .sb-inner .sb-body").innerHeight(500);
								} else {
									$(".sb-slide .sb-inner .sb-body").innerHeight(newheight);
								}
							} else {
								var bheight = wsize.newHeight - newheight;
								if (bheight < 150) {
									bheight = 150;
								}
								$(".sb-slide .sb-inner .sb-body").css("height", "auto").css("min-height", bheight);
							}
						} else {
							var bheight = (wsize.newHeight < 500) ? wsize.newHeight : 500;
							$(".sb-slide .sb-inner .sb-body").innerHeight(bheight);
						}';
					}
					$resize_part3 = '
						var iframebox = $( "#cboxLoadedContent iframe" );
						if ( iframebox.length ) {
							var iframeWidth = iframebox.attr("width");
							var iframeHeight = iframebox.attr("height");
                            if ( $(window).width() <= 767 ) {
                                var pheight = Math.round( (iframeHeight / iframeWidth) * 95 );
                                jQuery.colorbox.resize({width: "95%", height: pheight+"%"});
                            } else {
								if ( cbox.children("div.sb-slide").length > 0) {
									jQuery.colorbox.resize({'.$colorbox_resize.'});
								} else {
									if ( iframeHeight > wsize.newHeight ) {
										var newWidth = Math.round( (wsize.newHeight * iframeWidth) / iframeHeight);
										iframeWidth = newWidth;
										iframeHeight = wsize.newHeight;
										
										if ( iframeWidth > wsize.newWidth ) {
											iframeWidth = wsize.newWidth;
											iframeHeight = wsize.newHeight;
										}
									}
									jQuery.colorbox.resize({ width: parseInt(iframeWidth)+10, height: parseInt(iframeHeight)+10 });
								}
							}
                        } else {
                            jQuery.colorbox.resize({'.$colorbox_resize.'});
                        }
                    }
                });';
				
			if ( isset($slideshow) ) {
				if ( ! isset($GLOBALS['sb_scripts']['resize_slideshow']) ) {
					$output .= $resize_part1.$resize_part2.$resize_part3;
					$GLOBALS['sb_scripts']['resize_slideshow'] = true;
				}
			} else {
				if ( ! isset($GLOBALS['sb_scripts']['resize_media']) && ! isset($GLOBALS['sb_scripts']['resize_slideshow']) ) {
					$output .= $resize_part1.$resize_part3;
					$GLOBALS['sb_scripts']['resize_media'] = true;
				}
			}
        }
            
            if (@$GLOBALS['islive'] && ! $is_feed) {
                $timeinterval = (@$attr['live_interval'] ? intval($attr['live_interval']) * 60000 : 60000); // 60000 = 1 Min
                $stdiv = ($is_wall) ? 'div.sb-item' : 'div.timeline-row';
                $output .= '
              setInterval(function(){
                  var stlen = $("#timeline_'.$label.' '.$stdiv.'").length;
                  $.ajax({
                    type: "post",
                    url: "'.admin_url( 'admin-ajax.php' ).'",
                    data: {action: "sb_liveupdate", attr: '.$attr_ajax.', nonce: "'.wp_create_nonce( 'liveupdate' ).'", results: stlen, label: "'.$label.'"},
                    cache: false
                  })
                  .done(function( data ) {
                    if (data != "") {
                        var $elems = $(data).filter("'.$stdiv.'");
                        if ( $elems.first().attr("id") != $("#timeline_'.$label.' '.$stdiv.'").first().attr("id") ) {
                            var rm = 0;
                            var rms = false;
                            var rmcount = $elems.length;
                            var items = [];
                            $elems.each(function() {
                                if ( $("#timeline_'.$label.' '.$stdiv.'#" + $(this).attr("id") ).length == 0 ) {
                                    items.push(this);
                                    rm++;
                                } else {
                                    rms = true;
                                }
                            });';
                        if ( $is_wall ) {
                            $output .= '
                            if (rm > 0) {
                                $wall.isotope( "remove", $("#timeline_'.$label.'").find("'.$stdiv.'").slice(-rm) );
                            }
                            if (rms == true || rm == rmcount ) {
                                $wall.prepend( items ).isotope( "prepended", items );
                            }
                            $wall.isotope({
                            	sortBy: "dateid",
                            	sortAscending: false
                            });';
                        } elseif ( $is_timeline ) {
                            $output .= '
                            if (rm > 0) {
                                $("#timeline_'.$label.'").find("'.$stdiv.'").slice(-rm).remove();
                            }
                            if (rms == true || rm == rmcount ) {
                                $("#timeline_'.$label.'").prepend(items);
                            }
                            $(window).trigger("scroll");';
                        }
                        $output .= $lazyload_output . $iframe_output.'
                        }
                    }
                  });
              }, '.$timeinterval.');';
            }

			$output .= '
            });
        </script>';
        }
        
        if ( ! $ajax_feed)
            $output .= ($forceCrawl) ? "\t<!-- End Social Board Plugin - cache is disabled. -->\n" : "\t<!-- End Social Board Plugin - cache is enabled - duration: " . $attr['cache'] . " minutes -->\n";
        $output = str_replace( array("\r\n","\r","\t","\n"), '', $output );
        
        // slideshow output
        if (@$attr['lightboxtype'] == 'slideshow' && @$ss_output) {
            if ( ! $ajax_feed)
	            $output .= '
	    		<div id="sb_slides_'.$label.'" style="display:none">
	                '.$ss_output.'
	    		</div>';
    		else
            	$output .= $ss_output;
        }
        
    	if ( $echo )
    		echo $output;
    	else
    		return $output;
    }

    // proper way to enqueue scripts and styles
    function add_styles() {
        wp_enqueue_style( 'social-board', plugins_url( 'public/css/styles.min.css', SB_FILE ), array(), SB_VERSION );
    }

	// add some js files
    public function header() {
        if ( ! @$GLOBALS['sb_no_jquery'])
        	wp_enqueue_script( 'jquery' );
        wp_enqueue_script( 'sb-utils', plugins_url( 'public/js/sb-utils.js', SB_FILE ), ( ! @$GLOBALS['sb_no_jquery'] ? array('jquery') : array() ), SB_VERSION, true );
	}
	
    // Function to retrieve data from feeds
    public function sb_get_feed( $feed_key, $i, $key2, $feed_value, $results, $setoption, $sboption, $cache, $forceCrawl = false, $sb_label = null ) {
        $feed_value = trim($feed_value);
        switch ( $feed_key ) {
            case 'facebook':
                $pageresults = 9; // the max results that is possible to fetch from facebook API - for group = 9 for page = 30 - we used 9 to support both
                $stepresults = ceil($results / $pageresults);
                $facebook_access_token = @$setoption['section_facebook']['facebook_access_token'];
                if ($locale = get_locale() )
                    $locale_str = '&locale='.$locale;
                
                if ($datetime_from = @$sboption['facebook_datetime_range']['from'])
                    $since_str = '&since='.strtotime($datetime_from);
                    
                if ($datetime_to = @$sboption['facebook_datetime_range']['to'])
                    $until_str = '&until='.strtotime($datetime_to);
                
                if ($i == 3 || $i == 4) {
                    if ($after = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                        $after_str = '&after='.$after;
                } else {
                    if ($until = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                        $until_str = '&until='.$until;
                }
                
                $afields = array('id','created_time','updated_time','link','from','source','message','description','story','comments.summary(true)','likes.summary(true)','picture','full_picture','attachments','object_id','type','status_type');
                // define the feed url
                if ($i == 1) {
                    // Page Feed
                    $feedType = (@$sboption['facebook_pagefeed']) ? $sboption['facebook_pagefeed'] : 'feed';
                    $afields[] = 'name';
                } elseif ($i == 2) {
                    // Group Feed
                    $feedType = 'feed';
                } elseif ($i == 3) {
                    $feedType = 'photos';
                    $afields[] = 'images';
                } elseif ($i == 4) {
                    $feedType = 'videos';
                    $afields[] = 'images';
                    $afields[] = 'title';
                    $afields[] = 'format';
                }
                
                $fields = implode(',', $afields);
                $feed_url = 'https://graph.facebook.com/v2.11/' . $feed_value . '/' . $feedType . '?limit=' . ( ($i == 2) ? $pageresults : $results ) . @$since_str . @$until_str . @$after_str . @$locale_str . '&fields=' . $fields . '&access_token=' . $facebook_access_token;
                $label = 'https://graph.facebook.com/' . $feed_value . '/' . $feedType . '?limit=' . $results;
                
                // if group feed
                if ($i == 2) {
                    // crawl the feed or read from the cache
                    $get_feed = TRUE;
                    if ( ! $forceCrawl ) {
                        if ( $cache->is_cached($label) ) {
                            $content = $cache->get_cache($label);
                            $get_feed = FALSE;
                        }
                    }
                    if ($get_feed) {
                        $feed = array();
                        for ($i = 1; $i <= $stepresults; $i++) {
                            $content = $cache->do_curl($feed_url);
                            $pagefeed = @json_decode($content);
                            if ( ! empty($pagefeed) ) {
                                $feed[] = $pagefeed->data;
                                if ( count($pagefeed->data) < $pageresults )
                                    break;
                                $feed_url = $pagefeed->paging->next;
                            }
                        }
               			if ( ! $forceCrawl )
                            $cache->set_cache($label, json_encode($feed));
                    } else {
                        $feed = @json_decode($content);
                    }
                } else {
                    $content = ( ! $forceCrawl ) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    if ( $pagefeed = @json_decode($content) ) {
                        if ( isset( $pagefeed->error ) ) {
                            if (@$this->attr['debuglog']) sb_log( 'Facebook error: '.@$pagefeed->error->message.' - ' . $feedType);
                            $feed[] = null;
                        } else {
                            if ($i == 3 || $i == 4) {
                                $feed[] = $pagefeed;
                            } else {
                                $feed['data'][] = $pagefeed->data;
                                $feed['next'] = $pagefeed->paging->next;
                            }
                        }
                    }
                }
            break;
            case 'twitter':
                switch($i)
                {
                	case 1:
                        $rest = 'statuses/user_timeline';
                        $params = array(
                            // define what type of tweets to filter from the feed
                            'exclude_replies' => (@$sboption['twitter_feeds']['replies']) ? 'false' : 'true',
                            'screen_name' => $feed_value
                            );
                        if ( ! @$sboption['twitter_feeds']['retweets'])
                            $params['include_rts'] = 'false';
                	break;
                	case 2:
                        $rest = "lists/statuses";
                        if ( is_numeric($feed_value) )
                            $params = array('list_id' => $feed_value);
                        else {
                            $feedvalarr = explode('/', $feed_value);
                            $params = array('owner_screen_name' => $feedvalarr[0], 'slug' => $feedvalarr[1]);
                            if (@$sboption['twitter_feeds']['retweets'])
                                $params['include_rts'] = 'true';
                        }
                	break;
                	case 3:
                		// The Search API is not complete index of all Tweets, but instead an index of recent Tweets. The index includes between 6-9 days of Tweets.
                        $rest = "search/tweets";
                        $feed_value = urlencode($feed_value);
                        if ( ! @$sboption['twitter_feeds']['retweets'])
                            $feed_value .= ' AND -filter:retweets';
                        $params = array('q' => $feed_value, 'include_entities' => 'true');
                	break;
                }
                // Specifies the number of Tweets to try and retrieve.
                // Deleted content is removed after the count has been applied. Retweets are also include in the count, even if include_rts is not supplied.
                $params['count'] = $results;
                $params['tweet_mode'] = 'extended';
                
                if ($id_from = @$sboption['twitter_id_range'][0])
                    $params['since_id'] = $id_from;
                    
                if ($id_to = @$sboption['twitter_id_range'][1])
                    $params['max_id'] = $id_to;
                    
                if ($max_id = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $params['max_id'] = $max_id;
        		
                // Fetch the feed
                $feed = $this->twitter_get_feed($rest, $params, $forceCrawl, $cache, $setoption);
    		break;
    		case 'google':
    			$google_api_key = @$setoption['section_google']['google_api_key'];
                if ($nextPageToken = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $pageToken = '&pageToken='.$nextPageToken;
                $feed_url = 'https://www.googleapis.com/plus/v1/people/' . $feed_value . '/activities/public?maxResults=' . $results . @$pageToken . '&key=' . $google_api_key;
                $content = ( ! $forceCrawl ) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content);
                if (@$feed->error) {
                    $feed = null;
                }
    		break;
            case 'flickr':
                $flickr_api_key = @$setoption['section_flickr']['flickr_api_key'];
                if ($nextPage = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $pageToken = '&page='.$nextPage;
                if ($i == 1) {
                    $feedType = 'flickr.people.getPublicPhotos';
                    $feedID = '&user_id='.$feed_value;
                } elseif ($i == 2) {
                    $feedType = 'flickr.groups.pools.getPhotos';
                    $feedID = '&group_id='.$feed_value;
                }
                $feed_url = 'https://api.flickr.com/services/rest/?method='.$feedType.'&api_key='.$flickr_api_key . $feedID . '&per_page=' . $results . @$pageToken . '&extras=date_upload,date_taken,owner_name,icon_server,tags,views&format=json&nojsoncallback=1';
                $content = ( ! $forceCrawl ) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content);
    		break;
            case 'delicious':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                    $feed_url = "http://feeds.del.icio.us/v2/json/" . $feed_value . '?count=' . $results;
                    $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    $feed = @json_decode($content);
                }
            break;
    		case 'pinterest':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                    // get json data
                    $json_uri = ($i == 1) ? 'users' : 'boards';
                    $feed_url = "https://api.pinterest.com/v3/pidgets/$json_uri/" . $feed_value . "/pins/";
                    $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    $feed[0] = @json_decode($content);
                    if (@$feed[0]->status == 'success') {
                        // get rss data
                        $rss_uri = ($i == 1) ? '/feed.rss' : '.rss';
                        $feed_url = "https://www.pinterest.com/" . $feed_value . "$rss_uri/";
                        $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                        $feed[1] = @simplexml_load_string(trim($content));
                    } else {
                    	if ( ! empty($feed[0]->message) )
                        	sb_log( 'Pinterest error: '.$feed[0]->message.' - ' . $feed_url);
                        $feed = null;
                    }
                }
    		break;
    		case 'instagram':
                $instagram_access_token = @$setoption['section_instagram']['instagram_access_token'];
                $max_str = 'max_id';
                $feed_url = '';
                if ($i == 1) {
                    $feed_url = 'https://api.instagram.com/v1/users/self/media/recent?count=' . $results;
                    $feed_token = $this->is_instagram_access_token($feed_value) ? $feed_value : $instagram_access_token;
                    $feed_url .= '&access_token=' . $feed_token;
                } elseif ($i == 2) {
                    $feed_url = 'https://api.instagram.com/v1/tags/' . urlencode($feed_value) . '/media/recent?count=' . $results;
                    $max_str = 'max_tag_id';
                } elseif ($i == 3) {
                    $feed_url = 'https://api.instagram.com/v1/locations/' . $feed_value . '/media/recent?access_token=' . $instagram_access_token;
                } elseif ($i == 4) {
                    $coordinates = explode(',', $feed_value);
                    $feed_url = 'https://api.instagram.com/v1/media/search?lat=' . $coordinates[0] . '&lng=' . $coordinates[1] . '&distance=' . $coordinates[2];
                    $max_str = 'max_timestamp';
                }
                if ($i != 1)
                    $feed_url .= '&access_token=' . $instagram_access_token;
                
                if (@$feed_url) {
                    if ($next_max_id = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                        $feed_url .= '&'. $max_str .'='.$next_max_id;
                    
                    $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    $feed = @json_decode($content);
                }
    		break;
    		case 'youtube':
                $google_api_key = @$setoption['section_google']['google_api_key'];
                if ($nextPageToken = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $pageToken = '&pageToken='.$nextPageToken;
                switch($i)
                {
                	case 1:
                    case 4:
                        $channel_filter = ($i == 1) ? 'forUsername' : 'id';
                        $user_url = 'https://www.googleapis.com/youtube/v3/channels?part=snippet,contentDetails&'.$channel_filter.'=' . $feed_value .'&key=' . $google_api_key;
                        $user_content = ( ! $forceCrawl) ? $cache->get_data($user_url, $user_url) : @$cache->do_curl($user_url);
                        if ($user_content) {
                            $user_feed = @json_decode($user_content);
                            if (@$user_feed->items[0])
                                $feed_url = 'https://www.googleapis.com/youtube/v3/playlistItems?playlistId=' . $user_feed->items[0]->contentDetails->relatedPlaylists->uploads;
                        }
                    break;
                    case 2:
                        $feed_url = 'https://www.googleapis.com/youtube/v3/playlistItems?playlistId=' . $feed_value;
                    break;
                    case 3:
                        $feed_url = 'https://www.googleapis.com/youtube/v3/search?q=' . rawurlencode($feed_value);
                    break;
                }
                if ($results > 50) $results = 50;
                if (@$feed_url) {
                    $feed_url .= '&part=snippet&maxResults=' . $results . @$pageToken . '&key=' . $google_api_key;
                    $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                    $feed = @json_decode($content);
					
					if (is_object($feed) && @$user_feed)
						$feed->userInfo = $user_feed->items[0]->snippet;
                }
                if (@$feed->error) {
                    $feed = null;
                }
    		break;
    		case 'vimeo':
                $vimeo_access_token = @$setoption['section_vimeo']['vimeo_access_token'];
                $feedtype = 'videos';
                $feed_url = 'https://api.vimeo.com/users/' . $feed_value . '/' . $feedtype . "?per_page=$results&access_token=$vimeo_access_token";
                if ($nextPage = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $feed_url .= '&page='.$nextPage;
                    
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content);
    		break;
    		case 'tumblr':
                $tumblr_api_key = @$setoption['section_tumblr']['tumblr_api_key'];
                $feed_url = "https://api.tumblr.com/v2/blog/" . $feed_value . ".tumblr.com/posts?api_key={$tumblr_api_key}&limit=$results";
                if ($posts_start = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $feed_url .= '&offset='.$posts_start;
                    
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content);
            break;
    		case 'stumbleupon':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
                $feedtypes = array('comments', 'likes');
                foreach ($feedtypes as $ftype) {
                    if (@$sboption['stumbleupon_feeds'][$ftype]) {
                        $feed_url = "http://www.stumbleupon.com/rss/stumbler/" . $feed_value . "/" . $ftype;
                        $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                        if ( $data = @simplexml_load_string(trim($content), 'SimpleXMLElement', LIBXML_NOCDATA) )
                            $feed[$ftype] = $data;
                    }
                }
                }
    		break;
    		case 'deviantart':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
					$feed_url = "https://backend.deviantart.com/rss.xml?type=deviation&q=by%3A" . $feed_value . "+sort%3Atime+meta%3Aall";
					$content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
					$feed = @simplexml_load_string(trim($content));
                }
    		break;
            case 'rss':
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_value, $feed_value) : $cache->do_curl($feed_value);
                $feed = @simplexml_load_string(trim($content));
            break;
            case 'soundcloud':
                if ( empty($_SESSION[$sb_label]['loadcrawl']) ) {
					$soundcloud_client_id = @$setoption['section_soundcloud']['soundcloud_client_id'];
					$feed_url = "http://api.soundcloud.com/resolve?url=https://soundcloud.com/$feed_value/tracks&client_id=" . $soundcloud_client_id . "&limit=$results";
					// Reference: https://developers.soundcloud.com/docs/api/reference#resolve
					$content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
					$feed = @json_decode($content);
                }
            break;
            case 'vk':
                $pagefeed = (@$sboption['vk_pagefeed']) ? $sboption['vk_pagefeed'] : 'all';
                $wall_by = ($i == 1) ? 'domain' : 'owner_id';
                $vk_service_token = @$setoption['section_vk']['vk_service_token'];
                $feed_url = "https://api.vk.com/method/wall.get?v=5.34&{$wall_by}={$feed_value}&count={$results}&extended=1&lang=en&filter={$pagefeed}&access_token={$vk_service_token}";
                if (@$this->attr['https'])
                    $feed_url .= '&https=1';
				if ($offset = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $feed_url .= '&offset='.$offset;
                else
                    $offset = 0;
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $content = @mb_convert_encoding($content, "UTF-8", "auto");
                $feed = @json_decode($content);
                if (is_object($feed))
                    $feed->offset = $offset;
            break;
            case 'linkedin':
                $linkedin_access_token = @$setoption['section_linkedin']['linkedin_access_token'];
                $feed_url = "https://api.linkedin.com/v1/companies/{$feed_value}/updates?oauth2_access_token={$linkedin_access_token}&count={$results}&format=json";
                
                $sboption['linkedin_pagefeed'] = 'status-update';
                if ($pagefeed = @$sboption['linkedin_pagefeed']) {
                    if ($pagefeed != 'all')
                        $feed_url .= '&event-type='.$pagefeed;
                }
                
                if ($offset = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $feed_url .= '&start='.$offset;
                    
                $company_url = "https://api.linkedin.com/v1/companies/{$feed_value}:(id,name,logo-url,square-logo-url)?oauth2_access_token={$linkedin_access_token}&format=json";
                $company_content = ( ! $forceCrawl) ? $cache->get_data($company_url, $company_url) : @$cache->do_curl($company_url);
                
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content);
                if ( is_object($feed) && isset($feed->_count) ) {
                    if ($company_content) {
                        $company_feed = @json_decode($company_content);
                        $feed->company = $company_feed;
                    }
                }
            break;
            case 'vine':
                $feed_url = "https://api.vineapp.com/timelines/users/{$feed_value}";
                if ($offset = @$_SESSION[$sb_label]['loadcrawl'][$feed_key.$i.$key2])
                    $feed_url .= '?page='.$offset;
                $content = ( ! $forceCrawl) ? $cache->get_data($feed_url, $feed_url) : $cache->do_curl($feed_url);
                $feed = @json_decode($content, false, 512, JSON_BIGINT_AS_STRING);
            break;
    	}
        
    	return @$feed;
    }
    
    /**
     * [social_board id="" type=""]
     */
    public static function social_board_shortcode( $atts ) {
        if (@$atts['network']) {
            $network2 = explode('{|}', $atts['network']);
            foreach ($network2 as $net2) {
                $network3 = explode('{/}', $net2);
                $network4 = explode('{;}', $network3[1]);
                
                $net5 = array();
                foreach ($network4 as $net4) {
                    $network5 = explode('{:}', $net4);
                    if (stristr($network5[0], '_id_') === FALSE) {
                        $network6 = $network5[1];
                    } else {
                        $network6 = explode('{,}', $network5[1]);
                    }
                    $net5[$network5[0]] = $network6;
                }
                
                $net3[$network3[0]] = $net5;
            }
            $atts['network'] = $net3;
        }
        $echo = (@$atts['echo']) ? true : false;
        $sb = new SocialBoard();
        return $sb->init( $atts, $echo );
    }
    
    // create time string for sorting and applying pinning options
    private function make_timestr($time, $link) {
        $timestr = ( is_numeric($time) ) ? $time : strtotime($time);
        if ( ! empty($this->sboption['pins']) ) {
            $pinscount = strlen(count($this->sboption['pins']) );
            $dkey = array_search($link, $this->sboption['pins']);
            if ($dkey !== false) {
                $dkey = str_pad($dkey, $pinscount, 0, STR_PAD_LEFT);
                $timestr = "9{$dkey}";
            }
        }
		$linkstr = sprintf("%u", crc32($link) );
        return $timestr.'-'.$linkstr;
    }
    
    // applying board items removal
    private function make_remove($link) {
        if ( ! empty($this->sboption['remove']) ) {
            if ( in_array($link, $this->sboption['remove']) )
                return false;
        }
        return true;
    }
    
    /**
     * Word Limiter
     *
     * Limits a string to X number of words.
     *
     * @param   $end_char   the end character. Usually an ellipsis
     */
    function word_limiter($text, $url = '', $comment = false) {
        $limit = ($comment) ? @$this->attr['commentwords'] : @$this->attr['words'];
        $end_char = '...';
        
        $str = trim( strip_tags($text) );
        $str1 = trim( strip_tags($text, '<a>') );
        $str1 = trim(preg_replace('#<([^ >]+)[^>]*>([[:space:]]|&nbsp;)*</\1>#', '', $str1)); // remove all empty HTML tags
    	if ($str == '') {
    		return $str;
    	}
        
        if (sb_word_count_utf8($str) < $limit) {
            return ($str1 == $str) ? $this->append_links($str1) : trim(preg_replace('/\s*\n+/', "<br>", $str1) );
        }
        
    	preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);
        if (strlen($str) == strlen($matches[0])) {
    		$end_char = '';
    	}
        $str = $this->append_links($matches[0]);
    	if (@$this->attr['readmore'] && $url)
            $end_char = ' <a href="' . $url . '"'.$this->target.' style="font-size: large;">' . $end_char . '</a>';
            
        return $str.$end_char;
    }
    
    // Title Limiter (limits the title of each item to X number of words)
    function title_limiter($str, $url = '') {
        $end_char = '...';
        $limit = (@$this->attr['titles']) ? $this->attr['titles'] : 15;
        $str = strip_tags($str);

    	if (trim($str) == '')
    	{
    		return $str;
    	}
        
        if (sb_word_count_utf8($str) < $limit) {
            return $str;
        }

    	preg_match('/^\s*+(?:\S++\s*+){1,'.(int) $limit.'}/', $str, $matches);

        if (strlen($str) == strlen($matches[0]))
    	{
    		$end_char = '';
    	}

        return rtrim($matches[0]).$end_char;
    }
    
    function append_links($str) {
        // make the urls hyper links
        $regex = '#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#';
        $str = preg_replace_callback($regex, array(&$this, 'links_callback'), $str);
        return trim(preg_replace('/\s*\n+/', "<br>", $str) );
    }
    
    function links_callback($matches) {
        return (@$matches[0]) ? '<a href="'.$matches[0].'"'.$this->target.'>'.$matches[0].'</a>' : '';
    }
    
    function format_text($str) {
        $str = trim( strip_tags($str) );
    	if ($str == '')
    	{
    		return $str;
    	}
        
        $str = $this->append_links($str);
        return $str;
    }
    
    function twitter_add_links($text) {
        // Add links to all @ mentions
        $text = preg_replace('/@([^\s\<.*?>]+)/', '<a href="https://twitter.com/$1"'.$this->target.'>@$1</a>', $text);
        // Add links to all hash tags
        $text = preg_replace('/#([^\s\<.*?>]+)/', '<a href="https://twitter.com/search/%23$1"'.$this->target.'>#$1</a>', $text);
        
        return $text;
    }
    
    function twitter_get_feed($rest, $params, $forceCrawl, $cache, $setoption) {
        $consumer_key = @trim($setoption['section_twitter']['twitter_api_key']);
        $consumer_secret = @trim($setoption['section_twitter']['twitter_api_secret']);
        $oauth_access_token = @trim($setoption['section_twitter']['twitter_access_token']);
        $oauth_access_token_secret = @trim($setoption['section_twitter']['twitter_access_token_secret']);
        $get_feed = TRUE;
        $label = 'https://api.twitter.com/1.1/'.$rest.'/'.serialize($params);
        if ( ! $forceCrawl ) {
            if ( $cache->is_cached($label) ) {
                $content = $cache->get_cache($label);
                $get_feed = FALSE;
            }
        }
        if ($get_feed) {
            $auth = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_access_token, $oauth_access_token_secret);
            $auth->setTimeouts($this->attr['timeout'], $this->attr['timeout']);
            $auth->setDecodeJsonAsArray(false);
            $content = $auth->get($rest, $params);
            if ( ! $content ) {
            	if (@$this->attr['debuglog'])
                    sb_log( 'Twitter error: An error occurs while reading the feed, please check your connection or settings.');
            }
            else {
                $feed = $content;
                if ( isset( $feed->errors ) ) {
                    foreach( $feed->errors as $key => $val ) {
                        if (@$this->attr['debuglog'])
                            sb_log( 'Twitter error: '.$val->message.' - ' . $rest);
                    }
                    $feed = null;
                }
            }
    		if ( ! $forceCrawl )
                $cache->set_cache($label, json_encode($content) );
        }
        else
            $feed = @json_decode($content);
                
        return @$feed;
    }
    
    function vk_get_photo($photo) {
        foreach ($photo as $ikey => $iphoto) {
            if (stristr($ikey, 'photo_') == TRUE) {
                $source = $iphoto;
            }
        }
        return @$source;
    }
    
    // Find a Youtube video link in a string and convert it into Embed Code
	function youtube_get_embedurl($url)
	{
		preg_match("#(http://www.youtube.com)?/(v/([-|~_0-9A-Za-z]+)|watch\?v\=([-|~_0-9A-Za-z]+)&?.*?)#i", $url, $matches);
		foreach ($matches as $matche) {
			if ( ! empty($matche) && strpos($matche, '/' ) === FALSE && strpos($matche, '=' ) === FALSE) {
				$vidID = $matche;
				break;
			}
		}
	    return 'https://www.youtube.com/embed/' . $vidID ;
    }
    
    function is_instagram_access_token($token) {
        $tokenArr = explode('.', $token);
        if ( count($tokenArr) == 3 ) {
            if ( is_numeric($tokenArr[0]) && is_numeric($tokenArr[1]) && ! is_numeric($tokenArr[2]) )
                return true;
        }
        return false;
    }
} // end class

// create the ajax callback for tabable widget
add_action("wp_ajax_sb_tabable", "sb_tabable");
add_action("wp_ajax_nopriv_sb_tabable", "sb_tabable");
function sb_tabable() {
    if ( ! session_id() ) {
        session_start();
    }
    if ( ! wp_verify_nonce( $_REQUEST['nonce'], "tabable")) {
        exit("No naughty business please!");
    }

    if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $sb = new SocialBoard();
        $sb->init( $_REQUEST['attr'], true, null, array( array($_REQUEST['feed'] => 1) ) );
    } else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }
    
    die();
}

// create the ajax callback for load more
add_action("wp_ajax_sb_loadmore", "sb_loadmore");
add_action("wp_ajax_nopriv_sb_loadmore", "sb_loadmore");
function sb_loadmore() {
    if ( ! session_id() ) {
        session_start();
    }
    if ( ! wp_verify_nonce( $_REQUEST['nonce'], "loadmore")) {
        exit("No naughty business please!");
    }
    
    if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $sb = new SocialBoard();
        $sb->init( $_REQUEST['attr'], true, null, 'all', $_SESSION[$_REQUEST['label']]['loadmore'] );
    } else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }
    
    die();
}

// create the ajax callback for live update
add_action("wp_ajax_sb_liveupdate", "sb_liveupdate");
add_action("wp_ajax_nopriv_sb_liveupdate", "sb_liveupdate");
function sb_liveupdate() {
    if ( ! session_id() ) {
        session_start();
    }
    if ( ! wp_verify_nonce( $_REQUEST['nonce'], "liveupdate")) {
        exit("No naughty business please!");
    }

    if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $args = array('liveresults' => @$_REQUEST['results']);
        $sb = new SocialBoard();
        $sb->init( $_REQUEST['attr'], true, $args, 'all', array() );
    } else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }
    
    die();
}

// Word counter function
function sb_word_count_utf8($str) {
    return count( preg_split('~[^\p{L}\p{N}\']+~u', $str) );
}

// Friendly dates (i.e. "2 days ago")
function sb_friendly_date( $date ) {
	// Make sure the server is using the right time zone
	if ( get_option( 'timezone_string' ) && strpos( get_option( 'timezone_string' ), 'UTC' ) === false )
		date_default_timezone_set( get_option( 'timezone_string' ) );
	
	// Get the time difference in seconds
	$post_time = ( is_numeric($date) ) ? $date : strtotime( $date );
    if ( strlen($post_time) > 10 ) $post_time = substr( $post_time, 0, -3 );
	$current_time = time();
	$time_difference = $current_time - $post_time;
	
	// Seconds per...
	$minute = 60;
	$hour = 3600;
	$day = 86400;
	$week = $day * 7;
	$month = $day * 31;
	$year = $day * 366;
	
	// if over 3 years
	if ( $time_difference > $year * 3 ) {
		$friendly_date = __( 'a long while ago', 'social-board' );
	}
	
	// if over 2 years
	else if ( $time_difference > $year * 2 ) {
		$friendly_date = __( 'over 2 years ago', 'social-board' );
	}
	
	// if over 1 year
	else if ( $time_difference > $year ) {
		$friendly_date = __( 'over a year ago', 'social-board' );
	}
	
	// if over 11 months
	else if ( $time_difference >= $month * 11 ) {
		$friendly_date = __( 'about a year ago', 'social-board' );
	}
	
	// if over 2 months
	else if ( $time_difference >= $month * 2 ) {
		$months = (int) $time_difference / $month;
		$friendly_date = sprintf( __( '%d months ago', 'social-board' ), $months );
	}
	
	// if over 4 weeks ago
	else if ( $time_difference > $week * 4 ) {
		$friendly_date = __( 'last month', 'social-board' );
	}
	
	// if over 3 weeks ago
	else if ( $time_difference > $week * 3 ) {
		$friendly_date = __( '3 weeks ago', 'social-board' );
	}
	
	// if over 2 weeks ago
	else if ( $time_difference > $week * 2 ) {
		$friendly_date = __( '2 weeks ago', 'social-board' );
	}
	
	// if equal to or more than a week ago
	else if ( $time_difference >= $day * 7 ) {
		$friendly_date = __( 'last week', 'social-board' );
	}
	
	// if equal to or more than 2 days ago
	else if ( $time_difference >= $day * 2 ) {
		$days = (int) $time_difference / $day;
		$friendly_date = sprintf( __( '%d days ago', 'social-board' ), $days );
	}
	
	// if equal to or more than 1 day ago
	else if ( $time_difference >= $day ) {
		$friendly_date = __( 'yesterday', 'social-board' );
	}
	
	// 2 or more hours ago
	else if ( $time_difference >= $hour * 2 ) {
		$hours = (int) $time_difference / $hour;
		$friendly_date = sprintf( __( '%d hours ago', 'social-board' ), $hours );
	}
	
	// 1 hour ago
	else if ( $time_difference >= $hour ) {
		$friendly_date = __( 'an hour ago', 'social-board' );
	}
	
	// 2–59 minutes ago
	else if ( $time_difference >= $minute * 2 ) {
		$minutes = (int) $time_difference / $minute;
		$friendly_date = sprintf( __( '%d minutes ago', 'social-board' ), $minutes );
	}
	
	else {
		$friendly_date = __( 'just now', 'social-board' );
	}
	
	// HTML 5 FTW
	return '<time title="' . date_i18n( get_option( 'date_format' ), $post_time ) . '" datetime="' . date( 'c', $post_time ) . '">' . ucfirst( $friendly_date ) . '</time>';
}

// i18n dates
function sb_i18n_date( $date, $format ) {
    $post_time = ( is_numeric($date) ) ? $date : strtotime( $date );
    return date_i18n( $format, $post_time );
}

function sb_explode( $output = array() ) {
    if ( ! empty($output) ) {
        $outputArr = explode(',', str_replace(' ', '', $output) );
        foreach ($outputArr as $val)
            $out[$val] = true;
        
        return $out;
    }
    return false;
}

function sb_options( $post_id ) {
    if ( ! is_numeric($post_id) ) {
        $args = array(
            'meta_key' => 'classname',
            'meta_value' => $post_id,
            'post_type' => 'sb_themes',
            'posts_per_page' => 999999
        );
        $posts = get_posts($args);
        $post_id = $posts[0]->ID;
    }
    
    // 1. To retrieve the meta box data - get_post_meta( $post->ID ) will return an array of all the meta field values.
    // or if you know the field id of the value you want, you can do $value = get_post_meta( $post->ID, $field_id, true );
    $postData = array();
    foreach( ( array ) get_post_custom_keys( $post_id ) as $sKey ) {    // This way, array will be unserialized; easier to view.
        $postData[ $sKey ] = get_post_meta( $post_id, $sKey, true );
    }
    
    return $postData;
}

// display different items according to user selections in admin widget
function sb_admin_inline_script() {
    if ( wp_script_is( 'jquery', 'done' ) ) {
    
    if ( @$sb_widget = get_option('widget_sb_widget') ){
    ?>
    <script type="text/javascript">
    var sb_widget_updated = function($) {
        <?php foreach ($sb_widget as $widgetKey => $widgetItem) {
        if ( is_int($widgetKey) ) {
            ?>
            var widgetElement = '[name="widget-sb_widget[<?php echo $widgetKey; ?>]';
            <?php
            if ($widgetItem['position'] == 'normal'){ ?>
                $(widgetElement+'[width]"]').val('');
                $(widgetElement+'[location]"]').closest("tr").hide();
                $(widgetElement+'[slide]"]').closest("tr").hide();
                $(widgetElement+'[autoclose]"]').closest("tr").hide();
            <?php } ?>
            var widget = $( '[id$=sb_widget-<?php echo $widgetKey; ?>]' );
            sb_widget_update(null, widget);
        <?php }
        } ?>
    }
    
    jQuery(document).ready(function($) {
        sb_widget_updated($);
    });
    
    function sb_widget_update(e, widget) {
        // selective
        widget.on('change', 'input[name$="[position]"]', function() {
            if ( jQuery(this).val() == 'normal' ) {
                widget.find('input[name$="[width]"]').val('');
                widget.find('select[name$="[location]"]').closest("tr").fadeOut('fast');
                widget.find('input[name$="[slide]"]').closest("tr").fadeOut('fast');
                widget.find('input[name$="[autoclose]"]').closest("tr").fadeOut('fast');
            } else {
                widget.find('input[name$="[width]"]').val('280');
                widget.find('select[name$="[location]"]').closest("tr").fadeIn('fast');
                widget.find('input[name$="[tabable]"]').closest("tr").fadeIn('fast');
                widget.find('input[name$="[slide]"]').closest("tr").fadeIn('fast');
                widget.find('input[name$="[autoclose]"]').closest("tr").fadeIn('fast');
            }
        });
        
        widget.on('change', 'select[name$="[location]"]', function() {
            var ischecked = true;
            if ( jQuery( this ).val() == 'sb-left' || jQuery( this ).val() == 'sb-right' ) {
                var ischecked = false;
            }
            widget.find('input[name$="[showheader]"]').prop('checked', ischecked);
        });
        widget.on('change', 'input[name$="[slide]"]', function() {
            var ischecked = jQuery( this ).prop('checked');
            widget.find('input[name$="[autoclose]"]').prop('disabled', ! ischecked);
        });
        widget.on('change', 'input[name$="[carousel]"]', function() {
            var ischecked = jQuery( this ).prop('checked');
            widget.find('input[name$="[tabable]"]').prop('disabled', ischecked);
            widget.find('input[name$="[override_settings]"]').val( (ischecked) ? 'cs_item=1&slideMove=1' : '' );
        });
    }

    // "widget" represents jQuery object of the affected widget's DOM element
    jQuery(document).on('widget-updated', sb_widget_update);
    jQuery(document).on('widget-added', sb_widget_update);
    </script>
    <?php } ?>
    
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        // clear cache
        $("#sb-clearcache").on("click", function() {
          nonce = $(this).attr("data-nonce");
          log = $(this).attr("data-log");
          var msgid = (log) ? 'clearlog' : 'clearcache';
          $('#sb-'+msgid+'-msg').html('<span class="sb-loading"></span>');
          $.ajax({
            type: "post",
            url: "<?php echo admin_url( 'admin-ajax.php' ); ?>",
            data: {action: "sb_clearcache", nonce: nonce, log: log},
            cache: false
            })
            .done(function( response ) {
                $("#sb-"+msgid+"-msg").html(response);
            })
            .fail(function() {
                alert( "Problem cleaning the cache!" );
          });
        });
        
        // wall item width
        function sb_set_fixWidth(fixed_width) {
        	if (fixed_width == 'true') {
				$("#fieldrow-wallsetting_breakpoints").fadeOut('fast');
				$("#fieldrow-wallsetting_itemwidth").fadeIn('fast');
			} else {
				$("#fieldrow-wallsetting_itemwidth").fadeOut('fast');
				$("#fieldrow-wallsetting_breakpoints").fadeIn('fast');
			}
        }
        $('[name="SB_Settings_Page[wallsetting][fixWidth]"]').on("change", function() {
        	var fixed_width = $(this).val();
        	sb_set_fixWidth(fixed_width);
        });
        var fixed_width = $('[name="SB_Settings_Page[wallsetting][fixWidth]"]:checked').val();
        sb_set_fixWidth(fixed_width);
		
        // Instagram access token
        $('[name="SB_Settings_Page[section_instagram][instagram_client_id]"]').on("keyup keypress blur change click", function() {
            var client_id = $(this).val();
            var redirect_uri = 'http://axentmedia.com/instagram-access-token/?return_uri=<?php echo $GLOBALS['sb_apipage_url'] ?>';
            $("#sb-instagram-token").attr('href', 'https://instagram.com/oauth/authorize/?client_id='+client_id+'&redirect_uri='+redirect_uri+'&response_type=token&scope=public_content');
        });
        $("#sb-reset-client").on("click", function() {
            $('[name="SB_Settings_Page[section_instagram][instagram_client_id]"]').val('0912694563ee4b26ad9095f3fdf5b889');
        });
        var query = window.location.hash;
        if (query.length && query.indexOf("access_token") > -1) {
            var token = window.location.hash.split("#access_token=")[1];
            $('[name="SB_Settings_Page[section_instagram][instagram_access_token]"]').val(token);
            // scroll page to instagram section
            var target = $('#sections-section_instagram');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 0);
            }
            $("#sb-instagram-token-msg").html('<span style="color:green;">Access Token is set. Do not forget to click the <strong>Save Changes</strong> button at the page footer.</span>');
        }
    });
    </script>
    <?php
    }
}
add_filter('admin_footer', 'sb_admin_inline_script');

// create the ajax callback for clear cache
add_action("wp_ajax_sb_clearcache", "sb_clearcache");
add_action("wp_ajax_nopriv_sb_clearcache", "sb_clearcache");
function sb_clearcache() {
    if ( ! wp_verify_nonce( $_REQUEST['nonce'], "clearcache")) {
        exit("No naughty business please!");
    }

    if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        // delete cached files
        if (@$_REQUEST['log']) {
            $file = SB_DIRNAME . '/sb.log';
            @unlink($file);
        } else {
            $path = "/cache/";
            if ( $cachefiles = @glob(SB_DIRNAME . $path ."*.cache*") ) {
                foreach($cachefiles as $file) {
                    @unlink($file); // Delete only .cache files through the loop
                }
            }
        }
        $cache = (@$_REQUEST['log']) ? 'Log' : 'Cache';
        echo '<span style="color:green;">'.$cache.' cleared successfully.</span>';
    }
    else {
        header("Location: ".$_SERVER["HTTP_REFERER"]);
    }
    
    die();
}

// Plugin activation
function sb_activate() {
    $version = get_option("sb_version");
    if ( ! $version) {
        add_option("sb_version", SB_VERSION, '', 'no');
        
        require_once SB_DIRNAME . "/admin/SB_Install.php";
        sb_data_insert();
    }
    elseif (version_compare($version, SB_VERSION, '<')) {
        update_option("sb_version", SB_VERSION);
        
        require_once SB_DIRNAME . "/admin/SB_Install.php";
        sb_data_update($version);
    }
}
register_activation_hook(__FILE__, 'sb_activate');

if (!isset($_GET['action']) || $_GET['action'] != 'deactivate') {
    add_action('admin_init', 'sb_activate');
}

// hex to rgb numerical converter for color styling
function sb_hex2rgb($hex, $str = true) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   } 
   // returns the rgb values separated by commas OR returns an array with the rgb values
   $rgb = ($str) ? "$r, $g, $b" : array($r, $g, $b);
   return $rgb;
}

// Shuffle associative and non-associative array
function sb_shuffle_assoc($list) {
  if (!is_array($list)) return $list;

  $keys = array_keys($list);
  shuffle($keys);
  $random = array();
  foreach ($keys as $key)
    $random[$key] = $list[$key];

  return $random;
}

// get all URLs from string
function sb_geturls($string) {
    $regex = '#\bhttps?://[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/))#';
    preg_match_all($regex, $string, $matches);
    return @$matches[0];
}

function sb_getsrc($html) {
    preg_match_all('/<img[^>]+>/i', $html, $rawimagearray, PREG_SET_ORDER);
    if ( isset($rawimagearray[0][0]) ) {
        preg_match('@src="([^"]+)"@', $rawimagearray[0][0], $match);
        $img['src'] = @array_pop($match);
        preg_match('@width="([^"]+)"@', $rawimagearray[0][0], $matchwidth);
        $img['width'] = @array_pop($matchwidth);
        
        return (@$img['width'] && $img['width'] < 10) ? false : $img;
    }
}

function sb_log($value) {
    // Create if not exists
    if ( ! file_exists(SB_LOGFILE)) {
        $fp = fopen(SB_LOGFILE, 'w');
        fclose($fp);
    }
    SB_AdminPageFramework_Debug::log( $value, SB_LOGFILE );
}

// modify widget
add_filter('widget_display_callback', 'sb_widget_custom_display', 10, 3);
function sb_widget_custom_display($instance, $widget, $args) {
    if ($widget instanceof SB_AdminPageFramework_Widget_Factory) {
    	$instance['sb_type'] = 'feed';
    	
	    if (@$instance['override_settings']) {
	        parse_str($instance['override_settings'], $aOverride);
	        $instance = array_merge($aOverride, $instance);
	        unset($instance['override_settings']);
	    }

	    // remove widget header if required
	    $instance['label'] = $instance['title'];
	    if ( @$instance['position'] == 'sticky' || @$instance['showheader'] ) {
	        unset($instance['title']);
	    }
	    
	    if ( @$instance['position'] == 'sticky' ) {
	        // remove from sidebar if is sticky
	        $sb = new SocialBoard();
	        $sb->attr = $instance;
	        $sb->args = $args;
	        add_action( 'wp_footer', array(&$sb, 'run') );
	        
	        return false;
	    }
	    else
	        return $instance;
    }
    else
		return $instance;
}

// function for using in template files
function social_board( $atts ) {
    $sb = new SocialBoard();
    return $sb->init( $atts, false );
}

// register the plugin for the first time
$sb = new SocialBoard();
$sb->start();

// remove possible filters
add_filter( 'the_content', 'sb_disable_wpautop', 0 );
function sb_disable_wpautop( $content ) {
	'sb_posts' === get_post_type() && remove_filter( 'the_content', 'wpautop' );
	return $content;
}

// image display proxy
add_action( 'template_redirect', 'sb_image_proxy');
function sb_image_proxy() {
    global $wp_query;
    
    $sbimg = $wp_query->get( 'sbimg' );
    if ( ! empty( $sbimg ) ) {
        // load image proxy
        include( SB_DIRNAME . '/library/ImageCache.php' );
        $imgcache = new SB_ImageCache;
        $imgcache->imageCacheDir = SB_DIRNAME . '/cache/';
        
        // convert string
        parse_str(base64_decode($sbimg), $imgArr);
        $imgcache->cacheImage($imgArr);
    }
}

function sb_custom_rewrite_tag() {
    add_rewrite_tag('%sbimg%', '([^&]+)');
}
add_action('init', 'sb_custom_rewrite_tag', 10, 0);

// integrations
add_action('plugins_loaded', 'sb_integration');
function sb_integration() {
    // Cornerstone
    if ( class_exists( 'Cornerstone_Plugin' ) ) {
        include( SB_DIRNAME . '/integration/cornerstone.php' );
    }
    
    // Visual Composer
    if ( defined( 'WPB_VC_VERSION' ) ) {
        include( SB_DIRNAME . '/integration/visualcomposer.php' );
    }
}
