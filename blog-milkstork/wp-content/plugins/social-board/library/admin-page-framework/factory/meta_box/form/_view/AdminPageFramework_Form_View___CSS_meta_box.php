<?php 
/**
	Admin Page Framework v3.8.15 by Michael Uno 
	Generated by PHP Class Files Script Generator <https://github.com/michaeluno/PHP-Class-Files-Script-Generator>
	<http://en.michaeluno.jp/social-board-admin>
	Copyright (c) 2013-2017, Michael Uno; Licensed under MIT <http://opensource.org/licenses/MIT> */
class SB_AdminPageFramework_Form_View___CSS_meta_box extends SB_AdminPageFramework_Form_View___CSS_Base {
    protected function _get() {
        return $this->_getRules();
    }
    private function _getRules() {
        return ".postbox .title-colon {margin-left: 0.2em;}.postbox .social-board-admin-section .form-table > tbody > tr > td,.postbox .social-board-admin-section .form-table > tbody > tr > th{display: inline-block;width: 100%;padding: 0;float: right;clear: right; }.postbox .social-board-admin-field { width: auto;}.postbox .social-board-admin-field {max-width: 100%;}.postbox .sortable .social-board-admin-field {max-width: 84%; width: auto;} .postbox .social-board-admin-section .form-table > tbody > tr > th {font-size: 13px;line-height: 1.5;margin: 1em 0px;font-weight: 700;}#poststuff .metabox-holder .postbox-container .social-board-admin-section-title h3 {border: none;font-weight: bold;font-size: 1.12em;margin: 1em 0;padding: 0;font-family: 'Open Sans', sans-serif; cursor: inherit; -webkit-user-select: inherit;-moz-user-select: inherit;user-select: inherit;text-shadow: none;-webkit-box-shadow: none;box-shadow: none;background: none;}#poststuff .metabox-holder .postbox-container .social-board-admin-collapsible-title h3 {margin: 0;}#poststuff .metabox-holder .postbox-container h4 {margin: 1em 0;font-size: 1.04em;}#poststuff .metabox-holder .postbox-container .social-board-admin-section-tab h4 {margin: 0;}@media screen and (min-width: 783px) {#poststuff #post-body.columns-2 #side-sortables .postbox .social-board-admin-section .form-table input[type=text]{width: 98%;}}";
    }
}
