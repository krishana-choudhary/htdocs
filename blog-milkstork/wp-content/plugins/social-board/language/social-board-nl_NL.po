msgid ""
msgstr ""
"Project-Id-Version: WordPress Social Board 3.0\n"
"POT-Creation-Date: 2015-06-15 01:34+0330\n"
"PO-Revision-Date: 2015-12-21 13:22+0330\n"
"Last-Translator: Axent Media <axentmedia@gmail.com>\n"
"Language-Team: Axent Media <axentmedia@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.1\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: ..\\\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Language: nl\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: admin\n"
"X-Poedit-SearchPathExcluded-1: library\n"

msgid "Tags"
msgstr "Tags"

msgid "Posted"
msgstr "Gepost"

msgid "Show All"
msgstr "Toon Alles"

msgid "comments"
msgstr "reacties"

msgid "likes"
msgstr "ik-leuks"

msgid "repin"
msgstr "Repin"

msgid "Load More"
msgstr "Laad meer"

msgid "a long while ago"
msgstr "een lange tijd geleden"

msgid "over 2 years ago"
msgstr "Meer dan 2 jaar"

msgid "over a year ago"
msgstr "meer dan een jaar geleden"

msgid "about a year ago"
msgstr "een jaar geleden"

msgid "%d months ago"
msgstr "%d maanden geleden"

msgid "last month"
msgstr "vorige maand"

msgid "3 weeks ago"
msgstr "3 weken geleden"

msgid "2 weeks ago"
msgstr "2 weken geleden"

msgid "last week"
msgstr "vorige week"

msgid "%d days ago"
msgstr "%d dagen geleden"

msgid "yesterday"
msgstr "gisteren"

msgid "%d hours ago"
msgstr "%d uur geleden"

msgid "an hour ago"
msgstr "een uur geleden"

msgid "%d minutes ago"
msgstr "%d minuten geleden"

msgid "just now"
msgstr "zojuist"

msgid "read more (+)"
msgstr "lees meer (+)"

msgid "read less (-)"
msgstr "lees minder (-)"

msgid "Search..."
msgstr "Zoeken..."

msgid "Timeline photos"
msgstr "Tijdlijn foto's"

msgid "Added photos"
msgstr "toegevoegde foto's"

msgid "Shared story"
msgstr "gedeelde verhaal"

msgid "Created note"
msgstr "Gemaakt note"

msgid "Mobile status update"
msgstr "Mobile status update"

msgid "Added video"
msgstr "Toegevoegd video"

msgid "Wall post"
msgstr "prikbordbericht"

msgid "Created event"
msgstr "Gemaakt event"
