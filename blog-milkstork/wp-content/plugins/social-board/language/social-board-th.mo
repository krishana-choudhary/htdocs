��    "      ,  /   <      �     �               !     /     ;     G     T     `     n  	   {     �     �  	   �     �     �     �     �  	   �     �     �                 
      	   +     5     ;     L     \     j     x  	   ~  B  �  '   �  3   �  *   '  -   R  2   �  2   �  !   �  !     $   *  '   O     w  3   �     �     �  B   �     '	     =	  0   P	     �	  -   �	  <   �	  0   
  !   9
     [
  3   t
  0   �
  	   �
  9   �
  -     "   K  +   n     �     �                                                     "   !                    	                                                                           
       %d days ago %d hours ago %d minutes ago %d months ago 2 weeks ago 3 weeks ago Added photos Added video Created event Created note Load More Mobile status update Posted Search... Shared story Show All Tags Timeline photos Wall post a long while ago about a year ago an hour ago comments just now last month last week likes over 2 years ago over a year ago read less (-) read more (+) repin yesterday Project-Id-Version: WordPress Social Board 3.0
POT-Creation-Date: 2015-06-15 01:09+0330
PO-Revision-Date: 2017-04-30 23:09+0330
Last-Translator: Axent Media <axentmedia@gmail.com>
Language-Team: Axent Media <axentmedia@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.1
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ..\
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=1; plural=0;
Language: th
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: admin
X-Poedit-SearchPathExcluded-1: library
 %d วันที่ผ่านมา %d ชั่วโมงที่ผ่านมา %d นาทีที่ผ่านมา %d เดือนที่ผ่านมา 2 สัปดาห์ที่ผ่านมา 3 สัปดาห์ที่ผ่านมา เพิ่มรูปภาพ เพิ่มวิดีโอ สร้างกิจกรรม หมายเหตุสร้าง มากกว่า อัปเดตสถานะมือถือ โพสต์ ค้นหา... เรื่องราวที่ใช้ร่วมกัน ทั้งหมด แท็กส์ รูปภาพบนไทม์ไลน์ โพสต์ผนัง เมื่อสักครู่นี้ เกี่ยวกับปีที่ผ่านมา ชั่วโมงที่ผ่านมา ความคิดเห็น เมื่อกี้ เมื่อเดือนที่แล้ว สัปดาห์ที่ผ่านมา ชอบ มากกว่า 2 ปีที่ผ่านมา กว่าปีที่ผ่านมา อ่านน้อยลง (-) อ่านเพิ่มเติม (+) Repin เมื่อวาน 