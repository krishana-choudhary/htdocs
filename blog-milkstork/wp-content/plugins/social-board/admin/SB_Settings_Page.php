<?php

/**
 * WordPress Social Board 3.4.1
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// create the setting page
class SB_Settings_Page extends SB_AdminPageFramework {
    
    /**
     * Sets up pages.
     */
    public function setUp() {
        /* ( optional ) this can be set via the constructor. For available values, see https://codex.wordpress.org/Roles_and_Capabilities */
        $this->setCapability( 'manage_options' );
        
        /* ( required ) Set the root page */
        $this->setRootMenuPageBySlug( 'edit.php?post_type=sb_posts' );
        
        $this->addSubMenuItems(
            array(
                'title' => __( 'Board Settings', 'social-board-admin' ),
                'page_slug' => 'sb_settings',
                'show_debug_info' => false
            ),
            array(
                'title' => __( 'Documentation', 'social-board-admin' ),
                'href' => SB_DOCS,
                'show_page_heading_tab' => false
            )
        );
        
        $this->setPluginSettingsLinkLabel( __( 'Settings', 'social-board-admin' ) );
        $this->oProp->aFooterInfo['sRight'] = '';
    }
    
    /**
     * The pre-defined callback method triggered when one of the added pages loads
     */
    public function load_SB_Settings_Page( $oAdminPage ) { // load_{instantiated class name}
    
        /* ( optional ) Determine the page style */
        $this->setPageHeadingTabsVisibility( false ); // disables the page heading tabs by passing false.
        $this->setInPageTabTag( 'h2' ); // sets the tag used for in-page tabs
    }
    
    /**
     * The pre-defined callback method that is triggered when the page loads.
     */
    public function load_sb_settings( $oAdminPage ) { // load_{page_slug}
        
        /*
         * Add in-page tabs
         * Page-heading tabs show the titles of sub-page items which belong to the set root page. 
         * In-page tabs show tabs that you define to be embedded within an individual page.
         */
        $this->addInPageTabs(
            /*
             * In-page tabs to display built-in field types
             * */
            'sb_settings', // set the target page slug so that the 'page_slug' key can be omitted from the next continuing in-page tab arrays.
            array(
                'tab_slug' => 'setting', // avoid hyphen(dash), dots, and white spaces
                'title' => __( 'Display Setting', 'social-board-admin' ),
                'order' => 1 // ( optional ) - if you don't set this, an index will be assigned internally in the added order
            ),     
            array(
                'tab_slug' => 'api',
                'title' => __( 'API Credentials', 'social-board-admin' )
            ),
            array(
                'tab_slug' => 'reset',
                'title' => __( 'Reset Options', 'social-board-admin' )
            ),
            array(
                'tab_slug' => 'licensing',
                'title' => __( 'Licensing', 'social-board-admin' )
            )
        );
        
        if ($debuglog = @$oAdminPage->getValue( array( 'setting', 'debuglog', 0 ) ) )
            $this->addInPageTabs(
                array(
                    'tab_slug' => 'debug',
                    'title' => __( 'Debug', 'social-board-admin' ),
                )
            );
        
        /*
         * Add setting sections
         * Section needs to be created prior to fields.
         * Use the addSettingSections() method to create sections and use the addSettingFields() method to create fields.
         */
        $this->addSettingSections(
            'sb_settings', // the target page slug
            array(
                'section_id'        => 'setting',
                'page_slug'         => 'sb_settings', // <-- the method remembers the last used page slug and the tab slug so they can be omitted from the second parameter.
                'tab_slug'          => 'setting', // <-- similar to the page slug, if the tab slug is the same as the previous one, it can be omitted.
                'title'             => __( 'General Setting', 'social-board-admin' ),
                'description'       => __( 'These options apply to all display modes.', 'social-board-admin' )
            ),
            array(
                'section_id'        => 'wallsetting',
                'tab_slug'          => 'setting',
                'title'             => __( 'Wall Display Setting', 'social-board-admin' ),
                'description'       => __( 'These options only apply to the wall display mode.', 'social-board-admin' )
            ),
            array(
                'section_id'        => 'feedsetting',
                'tab_slug'          => 'setting',
                'title'             => __( 'Rotating Feed Display Setting', 'social-board-admin' ),
                'description'       => __( 'These options only apply to the rotating feed display mode.', 'social-board-admin' )
            ),
            array(
                'section_id'        => 'carouselsetting',
                'tab_slug'          => 'setting',
                'title'             => __( 'Carousel Display Setting', 'social-board-admin' ),
                'description'       => __( 'These options only apply to the carousel display mode.', 'social-board-admin' )
            ),
            array(
                'section_id'        => 'timelinesetting',
                'tab_slug'          => 'setting',
                'title'             => __( 'Timeline Display Setting', 'social-board-admin' ),
                'description'       => __( 'These options only apply to the timeline display mode.', 'social-board-admin' )
            )
        );
        
        $this->addSettingSections(
            'sb_settings', // the target page slug
            array(
                'section_id'  => 'section_facebook',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'Facebook API Credentials',
                'description' => 'Required For All Facebook Feeds. <a href="'. SB_DOCS . '#facebook-api" target="_blank">Read the documentation</a> for more details on how to get your Facebook App Token.'
            ),
            array(
                'section_id'  => 'section_twitter',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'Twitter API Credentials',
                'description' => 'Required For All Twitter Feeds. <a href="'. SB_DOCS . '#twitter-api" target="_blank">Read the documentation</a> for more details on how to get your Twitter API Credentials.'
            ),
            array(
                'section_id'  => 'section_google',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'Google API Credentials',
                'description' => 'Required For All Google+ and YouTube Feeds. <a href="'. SB_DOCS . '#google-api" target="_blank">Read the documentation</a> for more details on how to get your Google API KEY.'
            ),
            array(
                'section_id'  => 'section_tumblr',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'Tumblr API Credentials',
                'description' => 'Required For All Tumblr Feeds. <a href="'. SB_DOCS . '#tumblr-api" target="_blank">Read the documentation</a> for more details on how to get your Tumblr API Key.'
            ),
            array(
                'section_id'  => 'section_flickr',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'Flickr API Credentials',
                'description' => 'Required For All Flickr Feeds. <a href="'. SB_DOCS . '#flickr-api" target="_blank">Read the documentation</a> for more details on how to get your Flickr API Key.'
            ),
            array(
                'section_id'  => 'section_instagram',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'Instagram API Credentials',
                'description' => 'In order to display your Instagram photos/videos you need an Access Token from Instagram. <a href="'. SB_DOCS . '#instagram-api" target="_blank">Read the documentation</a> for more details on how to get your Instagram Access Token.'
            ),
            array(
                'section_id'  => 'section_linkedin',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'LinkedIn API Credentials',
                'description' => 'Required For All LinkedIn Feeds. <a href="'. SB_DOCS . '#linkedin-api" target="_blank">Read this documentation</a> to find out how to obtain your LinkedIn Access Token.'
            ),
            array(
                'section_id'  => 'section_vimeo',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'Vimeo API Credentials',
                'description' => 'Required For All Vimeo Feeds. <a href="'. SB_DOCS . '#vimeo-api" target="_blank">Read this documentation</a> to find out how to obtain your Vimeo Access Token.'
            ),
            array(
                'section_id'  => 'section_vk',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'VK API Credentials',
                'description' => 'Required For All VK Feeds. <a href="'. SB_DOCS . '#vk-api" target="_blank">Read the documentation</a> for more details on how to get your VK Service Token.'
            ),
            array(
                'section_id'  => 'section_soundcloud',
                'page_slug'   => 'sb_settings',
                'tab_slug'    => 'api',
                'title'       => 'SoundCloud API Credentials',
                'description' => 'Required For All SoundCloud Feeds. <a href="'. SB_DOCS . '#soundcloud-api" target="_blank">Read the documentation</a> for more details on how to get your SoundCloud Client ID.'
            )
        );
        
        $oAdminPage->addSettingSections(
            'sb_settings', // the target page slug
            array(
                'tab_slug'          => 'reset',
                'section_id'        => 'section_reset',
                'title'             => __( 'Reset Default Options', 'social-board-admin' )
            ),
            array(
                'tab_slug'          => 'licensing',
                'section_id'        => 'section_licensing',
                'title'             => __( 'Register Social Board', 'social-board-admin' )
            )
        );
        
        /*
         * ( optional ) Adds setting fields into the meta box.
         */
        $this->addSettingFields(
            array(
                'section_id' => 'setting',
                'field_id' => 'theme',
                'type' => 'select',
                'title' => __( 'Board Theme', 'social-board-admin' ),
                'label' => sb_getPostTitles('sb_themes'),
                'attributes' => array(
                    'select' => array(
                        'style' => "width: 150px;",
                    ),
                ),
                'description' => __( 'Select a theme to style your Social Board.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'setting',
                'field_id' => 'results',
                'type' => 'number',
                'title' => __( 'Results Limit', 'social-board-admin' ),
                'default' => 30,
                'description' => __( 'Limiting the maximum number of results to display on the board - Maximum 100.<br /><strong>Note: </strong>There is a restriction for number of feed output results on each social network. For example, if you enter 100 as "Results Limit", you will only get 50 items from YouTube in each request.<br />Facebook Group: 20, Tumblr: 50, Pinterest = 25, YouTube: 50, Vimeo: 20, Stumbleupon: 10, Deviantart: 60, RSS: 10 mostly', 'social-board-admin' )
            ),
            array(
                'section_id' => 'setting',
                'field_id' => 'words',
                'type' => 'number',
                'title' => __( 'Words Limit', 'social-board-admin' ),
                'default' => 40,
                'description' => __( 'Limiting the description words count to display for each item on the board - Leave empty for no limit.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'setting',
                'field_id' => 'commentwords',
                'type' => 'number',
                'title' => __( 'Comment Words Limit', 'social-board-admin' ),
                'default' => 20,
                'description' => __( 'Limiting the comment words count to display for each item - Leave empty for no limit.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'setting',
                'field_id' => 'titles',
                'type' => 'number',
                'title' => __( 'Title Limit', 'social-board-admin' ),
                'default' => 15,
                'description' => __( 'Limit of the title words count to display for each item on the board - Leave empty for no limit.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'setting',
                'field_id' => 'userinfo',
                'title' => __( 'User info position & detail', 'social-board-admin' ),
                'description' => __( 'This will define, how to display the user info for each item on the Social Board.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'top',
                'label' => array(
                    'top' => 'Name/Title/Thumbnail at top',
                    'bottom' => 'Name/Title at bottom'
                )
            ),
            array(
                'section_id' => 'setting',
                'field_id' => 'lightboxtype',
                'title' => __( 'Lightbox content', 'social-board-admin' ),
                'description' => __( 'This will define, how to display the Social Board item information in lightbox window.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'media',
                'label' => array(
                    'media' => 'Single media only',
                    'slideshow' => 'Slideshow detailed'
                )
            ),
            array(
                'section_id' => 'setting',
                'field_id' => 'layout_image',
                'title' => __( 'Item image style', 'social-board-admin' ),
                'description' => __( 'This will define, how to display the image for each item on the Social Board.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'imgexpand',
                'label' => array(
                    'imgexpand' => 'Expanded image at top',
                    'imgnormal' => 'Normal image at middle'
                )
            ),
            array (
                'field_id' => 'readmore',
                'type' => 'checkbox',
                'title' => __( 'Read more', 'social-board-admin' ),
                'label' => __( 'Link long blocks of text', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, the (&#8230;) character at the bottom of each feed item will be linked to the rest of the content.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'order',
                'type' => 'select',
                'title' => __( 'Ordering', 'social-board-admin' ),
                'label' => array(
                    'date' => __( 'Date', 'social-board-admin' ),
                    'random' => __( 'Random', 'social-board-admin' ),
                ),
                'default' => 'date',
                'attributes'    => array(
                    'select' => array(
                        'style' => "width: 150px;",
                    ),
                ),
                'description' => __( 'Select whether to order the results depending on date of each item or randomly.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'loadmore',
                'title' => __( 'Load more', 'social-board-admin' ),
                'label' => __( 'Load more social items (Next page)', 'social-board-admin' ),
                'description' => __( 'If "Load more button" is checked, a load more bar will appear at the bottom of the social network wall and loads more social items if clicked.<br>If "Infinite scroll" is checked, the new social items appear each time you scroll to the end of page.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'media',
                'label' => array(
                    '0' => 'Disabled',
                    '1' => 'Load more button',
                    '2' => 'Infinite scroll'
                )
            ),
            array(
                'field_id' => 'links',
                'type' => 'checkbox',
                'title' => __( 'Links', 'social-board-admin' ),
                'label' => __( 'Open Links In New Window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all links will open in a new window else they will be open in the parent window.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'nofollow',
                'type' => 'checkbox',
                'title' => __( 'Nofollow links', 'social-board-admin' ),
                'label' => __( 'Add nofollow attribute on links', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all links will have the status of nofollow.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'https',
                'type' => 'checkbox',
                'title' => __( 'Use https', 'social-board-admin' ),
                'label' => __( 'Load images over https', 'social-board-admin' ),
                'default' => false,
                'description' => __( 'If checked, all links and images will be getting over https.', 'social-board-admin' )
            ),
            array(
                'field_id'      => 'display_ads',
                'title'         => __( 'Displaying Ads', 'social-board-admin' ),
                'type'          => 'checkbox',
                'label'         => array(
                    'wall' => __( 'Wall', 'social-board-admin' ) . ' (' . __( 'Display ads on wall', 'social-board-admin' ) . ')',
                    'timeline' => __( 'Timeline', 'social-board-admin' ) . ' (' . __( 'Display ads on timeline', 'social-board-admin' ) . ')',
                    'feed' => __( 'Rotating Feed', 'social-board-admin' ) . ' (' . __( 'Display ads in rotating feed', 'social-board-admin' ) . ')',
                    'carousel' => __( 'Carousel', 'social-board-admin' ) . ' (' . __( 'Display ads in slides', 'social-board-admin' ) . ')'
                ),
                'default' => array(
                    'wall' => true,
                    'timeline' => false,
                    'feed' => false,
                    'carousel' => false,
                ),
                'description'   => __( 'If any of these items is checked, the ads (from Manage Ads) will be displayed on it.', 'social-board-admin' ),
                'after_label'   => '<br />',
            ),
            array(
                'field_id' => 'filters',
                'type' => 'checkbox',
                'title' => __( 'Filter', 'social-board-admin' ),
                'label' => __( 'Filter networks', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, a navigation bar of all active network icons will appear at the bottom of the rotating feed or above the social network wall. These icons will allow the user to filter the social board items.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'display_all',
                'title' => __( 'Display Show All', 'social-board-admin' ),
                'description' => __( 'Show All button will appear at the first or end of the social network filtering navigation bar.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => '',
                'label' => array(
                    '' => 'At the first',
                    '1' => 'At the end',
                    'disabled' => 'Disabled'
                )
            ),
            array(
                'field_id' => 'default_filter',
                'type' => 'select',
                'title' => __( 'Filtered by default', 'social-board-admin' ),
                'label' => array(
                    '' => __( 'Show All', 'social-board-admin' ),
                    'facebook' => __( 'Facebook', 'social-board-admin' ),
                    'twitter' => __( 'Twitter', 'social-board-admin' ),
                    'google' => __( 'Google', 'social-board-admin' ),
                    'tumblr' => __( 'Tumblr', 'social-board-admin' ),
                    'delicious' => __( 'Delicious', 'social-board-admin' ),
                    'pinterest' => __( 'Pinterest', 'social-board-admin' ),
                    'flickr' => __( 'Flickr', 'social-board-admin' ),
                    'instagram' => __( 'Instagram', 'social-board-admin' ),
                    'youtube' => __( 'YouTube', 'social-board-admin' ),
                    'vimeo' => __( 'Vimeo', 'social-board-admin' ),
                    'stumbleupon' => __( 'Stumbleupon', 'social-board-admin' ),
                    'deviantart' => __( 'Deviantart', 'social-board-admin' ),
                    'rss' => __( 'RSS', 'social-board-admin' ),
                    'soundcloud' => __( 'SoundCloud', 'social-board-admin' ),
                    'vk' => __( 'VK', 'social-board-admin' ),
                    'linkedin' => __( 'LinkedIn', 'social-board-admin' ),
                    'vine' => __( 'Vine', 'social-board-admin' )
                ),
                'default' => '',
                'attributes'    => array(
                    'select' => array(
                        'style' => "width: 150px;",
                    ),
                ),
                'description' => __( 'Which filtering button to be selected by default.', 'social-board-admin' )
            ),
            array(
                'field_id'      => 'filters_order',
                'type'          => 'inline_mixed', // Multiple Sortable Checkboxes
                'title'         => __( 'Filter networks ordering/visibility', 'social-board-admin' ),
                'sortable'      => true,
                'content'       => $this->___getRowCheckboxes( 0, array( 'facebook' => 1 ) ), // checked by default
                array( // Second field
                    'content'   => $this->___getRowCheckboxes( 1, array( 'twitter' => 1 ) )
                ),
                array( // Third field
                    'content'   => $this->___getRowCheckboxes( 2, array( 'google' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 3, array( 'tumblr' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 4, array( 'delicious' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 5, array( 'pinterest' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 6, array( 'flickr' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 7, array( 'instagram' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 8, array( 'youtube' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 9, array( 'vimeo' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 10, array( 'stumbleupon' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 11, array( 'deviantart' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 12, array( 'rss' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 13, array( 'soundcloud' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 14, array( 'vk' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 15, array( 'linkedin' => 1 ) )
                ),
                array(
                    'content'   => $this->___getRowCheckboxes( 16, array( 'vine' => 1 ) )
                ),
                'description' => __( 'Order of the filtering navigation bar that appear at the bottom of the rotating feed or above the social network wall. <strong>Do not forget to save the changes.</strong>', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
            ),
            array(
                'field_id' => 'live',
                'title' => __( 'Stream Auto Update', 'social-board-admin' ),
                'description' => __( 'If enabled, the social board will get updated automatically without the need to refresh the page - <strong>If enabled, the "Cache Time" value will be ignored and considered as <code>0</code></strong>.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'off',
                'label' => array(
                    'all' => 'Enable for all',
                    'users' => 'Enable only for logged-in users',
                    'off' => 'Disable'
                )
            ),
            array(
                'field_id' => 'live_interval',
                'type' => 'number',
                'title' => __( 'Auto update interval', 'social-board-admin' ),
                'default' => 3,
                'description' => __( 'The time delay for updating the Social Boards (in minutes). Considering that there are some limits on social networks about the number of requests sent to their API, the minimum allowed value is <code>1</code>.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'cache',
                'type' => 'number',
                'title' => __( 'Cache Time', 'social-board-admin' ),
                'default' => 360,
                'description' => __( 'The time delay for caching of Social Boards (in minutes) - reduces up download time. <code>0</code> Zero value means no caching. Make sure the `cache` folder inside the plugin folder is writable by the server (depending on your web hosting server, permissions set to 755, 775, or 777).<div style="padding-top: 10px; display: block;"><a class="button button-small" name="sb-clearcache" id="sb-clearcache" type="button" data-nonce="'.wp_create_nonce( 'clearcache' ).'">Clear Cache</a> &nbsp; <label id="sb-clearcache-msg"></label></div>', 'social-board-admin' )
            ),
            array(
                'field_id' => 'crawl',
                'type' => 'number',
                'title' => __( 'Crawl per request', 'social-board-admin' ),
                'default' => 10,
                'description' => __( 'Number of feed crawls per each request. <code>0</code> Zero value means to crawl all at once. (depending on your web hosting server resources, some servers can not read a lot of feeds at the same time).', 'social-board-admin' )
            ),
            array(
                'field_id' => 'timeout',
                'type' => 'number',
                'title' => __( 'API connection timeout', 'social-board-admin' ),
                'default' => 15,
                'description' => __( 'Social API request connection timeout (in seconds). (depending on your web hosting server resources, some servers can not wait to process very long time).', 'social-board-admin' )
            ),
            array (
                'field_id' => 'debuglog',
                'type' => 'checkbox',
                'title' => __( 'Debug Log', 'social-board-admin' ),
                'default' => false,
                'label' => __( 'Enable/disable debug log', 'social-board-admin' ),
                'description' => __( 'If checked, all plugin errors will be logged in a file named "sb.log" in plugin folder. The contents of the "sb.log" file will be also shown in <strong>Debug tab</strong>. Make sure the `sb.log` file inside the plugin folder is existed and writable by the server (depending on your web hosting server, permissions set to 644, 655, or 666).<div style="padding-top: 10px; display: block;"><a class="button button-small" name="sb-clearcache" id="sb-clearcache" type="button" data-log="log" data-nonce="'.wp_create_nonce( 'clearcache' ).'">Clear Log</a> &nbsp; <label id="sb-clearlog-msg"></label></div>', 'social-board-admin' )
            ),
            array(
                'section_id' => 'wallsetting',
                'field_id' => 'transition',
                'type' => 'number',
                'title' => __( 'Transition Duration', 'social-board-admin' ),
                'default' => 400,
                'description' => __( 'Duration of the transition when items change position or appearance -  in milliseconds - 0 means no transitions.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'stagger',
                'title' => __( 'Stagger', 'social-board-admin' ),
                'type' => 'number',
                'default' => '',
                'description' => __( 'Staggers item transitions, so items transition incrementally after one another - in milliseconds - Leave empty to disable it.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'filter_search',
                'type' => 'checkbox',
                'title' => __( 'Search Filtering', 'social-board-admin' ),
                'label' => __( 'Filter wall by search', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, a search box will appear above the social network wall allowing the user to filter the social board items using a search phrase.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'originLeft',
                'type' => 'select',
                'title' => __( 'Filter Direction', 'social-board-admin' ),
                'label' => array(
                    'true' => __( 'Left To Right', 'social-board-admin' ),
                    'false' => __( 'Right To Left', 'social-board-admin' ),
                ),
                'default' => 'true',
                'attributes' => array(
                    'select' => array(
                        'style' => "width: 150px;",
                    ),
                ),
                'description' => __( 'Let layout to start render from left to right or, right to left.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'wall_relayout',
                'title' => __( 'Re-layout wall based on', 'social-board-admin' ),
                'description' => __( 'If "Page scroll" is enabled, the wall gaps will get filled when the visitor scroll the page but, if "Images load" is enabled, the wall gaps will get filled when a new image on the wall is loaded.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => '',
                'label' => array(
                    '' => 'Page scroll',
                    'imgload' => 'Images load'
                )
            ),
            array(
                'field_id' => 'wall_width',
                'title' => __( 'Wall Block Width', 'social-board-admin' ),
                'type' => 'number',
                'default' => '',
                'description' => __( 'Width of wall block (px) - Leave empty for full width.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'wall_height',
                'title' => __( 'Wall Block Height', 'social-board-admin' ),
                'type' => 'number',
                'default' => '',
                'description' => __( 'Height of wall block (px) - Leave empty for infinite height.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'fixWidth',
                'title' => __( 'Item Width Definition', 'social-board-admin' ),
                'description' => __( 'If "Fixed Width" is enabled, the "Item Width" value will be applied to each wall item - <strong>Responsive Breakpoints will be ignored</strong>.<br>If "Based on browser width" is enabled, the "Responsive Breakpoints" sets will be applied to wall items based on <strong>browser width</strong>.<br>If "Based on container width" is enabled, the "Responsive Breakpoints" sets will be applied to wall items based on <strong>wall container block width</strong>.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'false',
                'label' => array(
                    'true' => 'Fixed Width',
                    'false' => 'Based on browser width',
                    'block' => 'Based on container width'
                )
            ),
            array(
                'field_id' => 'breakpoints',
                'title' => __( 'Responsive Breakpoints', 'social-board-admin' ),
                'type' => 'number',
                'default' => __( '5', 'social-board-admin' ),
                'label' => __( 'More than 1200px', 'social-board-admin' ) . ': ',
                'attributes' => array(
                    'size' => 20,
                ),
                'capability' => 'manage_options',
                'delimiter' => '<br />',
                array(
                    'default' => '4',
                    'label' => __( '960px to 1200px', 'social-board-admin' ) . ': '
                ),
                array(
                    'default' => '4',
                    'label' => __( '768px to 960px', 'social-board-admin' ) . ': '
                ),
                array(
                    'default' => __( '3', 'social-board-admin' ),
                    'label' => __( '600px to 768px', 'social-board-admin' ) . ': '
                ),
                array(
                    'default' => __( '2', 'social-board-admin' ),
                    'label' => __( '480px to 600px', 'social-board-admin' ) . ': '
                ),
                array(
                    'default' => __( '2', 'social-board-admin' ),
                    'label' => __( '320px to 480px', 'social-board-admin' ) . ': '
                ),
                array(
                    'default' => __( '1', 'social-board-admin' ),
                    'label' => __( 'Less than 320px', 'social-board-admin' ) . ': '
                ),
                'description' => __( 'Defines the number of items (columns) shwoing on each row for different viewport/container sizes - <strong>Requires "Fixed Width" to be disabled</strong>.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'itemwidth',
                'title' => __( 'Item Width', 'social-board-admin' ),
                'type' => 'number',
                'default' => 250,
                'description' => __( 'Width of wall item (px) - Do not leave empty!', 'social-board-admin' )
            ),
            array(
                'field_id' => 'gutterX',
                'type' => 'number',
                'title' => __( 'Gutter X', 'social-board-admin' ),
                'default' => '10',
                'description' => __( 'The horizontal space between item elements - in pixels. This may changes automatically when "Fixed Width" is disabled.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'gutterY',
                'type' => 'number',
                'title' => __( 'Gutter Y', 'social-board-admin' ),
                'default' => '10',
                'description' => __( 'The vertical space between row elements - in pixels. This may changes automatically when "Fixed Width" is disabled.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'feedsetting',
                'field_id' => 'rotate_speed',
                'type' => 'number',
                'title' => __( 'Animation Speed', 'social-board-admin' ),
                'default' => 100,
                'description' => __( 'Defines the animation speed (in ms) of the rows moving up or down.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'duration',
                'type' => 'number',
                'title' => __( 'Animation Duration', 'social-board-admin' ),
                'default' => 4000,
                'description' => __( 'Defines the times (in ms) before the rows automatically move.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'direction',
                'type' => 'select',
                'title' => __( 'Animation Direction', 'social-board-admin' ),
                'label' => array( 
                    'up' => __( 'Up', 'social-board-admin' ),
                    'down' => __( 'Down', 'social-board-admin' ),
                ),
                'default' => 'up',
                'attributes'    => array(
                    'select' => array(
                        'style' => "width: 150px;",
                    ),
                ),
                'description' => __( 'Sets the direction of rotating feed movement to either "up" or "down".', 'social-board-admin' )
            ),
            array(
                'field_id' => 'controls',
                'type' => 'checkbox',
                'title' => __( 'Controls', 'social-board-admin' ),
                'label' => __( 'Stop/start rotating', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, a feed control bar will appear at the bottom of the rotating feed allowing user to stop/start the rotating feed or go to next/previous item.', 'social-board-admin' )
            ),
            array (
                'field_id' => 'autostart',
                'type' => 'checkbox',
                'title' => __( 'Auto Start', 'social-board-admin' ),
                'default' => true,
                'label' => __( 'Enable/disable auto start on load', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'pauseonhover',
                'type' => 'checkbox',
                'title' => __( 'Pause On Mouse Hover', 'social-board-admin' ),
                'default' => true,
                'label' => __( 'Enable/disable pause when mouse hovers the rotating feed element', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'width',
                'title' => __( 'Block Width', 'social-board-admin' ),
                'type' => 'number',
                'default' => 280,
                'description' => __( 'Width of feed block (px) - Leave empty for full width.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'height',
                'title' => __( 'Block Height', 'social-board-admin' ),
                'type' => 'number',
                'default' => 400,
                'description' => __( 'Height of feed block (px) - Do not leave empty.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'carouselsetting',
                'field_id' => 'cs_speed',
                'type' => 'number',
                'title' => __( 'Animation Speed', 'social-board-admin' ),
                'default' => 400,
                'description' => __( 'Defines the animation speed (in ms) of the rows moving left or right.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'carouselsetting',
                'field_id' => 'cs_pause',
                'type' => 'number',
                'title' => __( 'Transition Speed', 'social-board-admin' ),
                'default' => 2000,
                'description' => __( 'The time (in ms) between each auto transition.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'autoWidth',
                'title' => __( 'Auto Width', 'social-board-admin' ),
                'description' => __( 'If enabled, the "Slide Width" value will be applied to each slide - <strong>Number of items will be ignored</strong>.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'false',
                'label' => array(
                    'true' => 'Enable',
                    'false' => 'Disable'
                )
            ),
            array(
                'field_id' => 'cs_item',
                'title' => __( 'Number of items', 'social-board-admin' ),
                'type' => 'number',
                'default' => __( '3', 'social-board-admin' ),
                'label' => __( 'More than 960px', 'social-board-admin' ) . ': ',
                'attributes' => array(
                    'size' => 20,
                ),
                'capability' => 'manage_options',
                'delimiter' => '<br />',
                array(
                    'default' => '3',
                    'label' => __( '768px to 960px', 'social-board-admin' ) . ': '
                ),
                array(
                    'default' => __( '2', 'social-board-admin' ),
                    'label' => __( '600px to 768px', 'social-board-admin' ) . ': '
                ),
                array(
                    'default' => __( '2', 'social-board-admin' ),
                    'label' => __( '480px to 600px', 'social-board-admin' ) . ': '
                ),
                array(
                    'default' => __( '1', 'social-board-admin' ),
                    'label' => __( 'Less than 480px', 'social-board-admin' ) . ': '
                ),
                'description' => __( 'Defines the number of items shwoing in each slide for different viewport sizes - <strong>Requires Auto Width to be OFF</strong>.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'cs_width',
                'title' => __( 'Slide Width', 'social-board-admin' ),
                'type' => 'number',
                'default' => 250,
                'description' => __( 'Width of slide item (px) - Do not leave empty!', 'social-board-admin' )
            ),
            array(
                'field_id' => 'cs_rtl',
                'type' => 'select',
                'title' => __( 'Animation Direction', 'social-board-admin' ),
                'label' => array( 
                    'true' => __( 'Right to left', 'social-board-admin' ),
                    'false' => __( 'Left to right', 'social-board-admin' ),
                ),
                'default' => 'false',
                'attributes' => array(
                    'select' => array(
                        'style' => "width: 150px;",
                    ),
                ),
                'description' => __( 'Sets the direction of carousel feed movement to either "right" or "left".', 'social-board-admin' )
            ),
            array(
                'field_id' => 'cs_controls',
                'type' => 'checkbox',
                'title' => __( 'Controls', 'social-board-admin' ),
                'label' => __( 'Prev/next buttons', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, prev/next buttons will be displayed.', 'social-board-admin' ),
            ),
            array (
                'field_id' => 'cs_auto',
                'type' => 'checkbox',
                'title' => __( 'Auto Start', 'social-board-admin' ),
                'default' => false,
                'label' => __( 'If checked, the Slider will automatically start to play.', 'social-board-admin' ),
            ),
            array (
                'field_id' => 'cs_loop',
                'type' => 'checkbox',
                'title' => __( 'Loop slide', 'social-board-admin' ),
                'default' => true,
                'label' => __( 'If checked, will enable the ability to loop back to the beginning of the slide when on the last element - <strong>Slide Move will be ignored</strong>.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'slideMove',
                'title' => __( 'Slide Move', 'social-board-admin' ),
                'type' => 'number',
                'default' => 1,
                'description' => __( 'Number of slides to be moved at a time - <strong>Requires Loop slide to be OFF</strong>.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'cs_pager',
                'type' => 'checkbox',
                'title' => __( 'Enable pager', 'social-board-admin' ),
                'default' => false,
                'label' => __( 'Enable/disable pager option.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'slideMargin',
                'title' => __( 'Slide Margin', 'social-board-admin' ),
                'type' => 'number',
                'default' => 5,
                'description' => __( 'Spacing between each slide.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'timelinesetting',
                'field_id' => 'onecolumn',
                'title' => __( 'Display layout', 'social-board-admin' ),
                'description' => __( 'To display timeline in one column or based on browser screen width.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'false',
                'label' => array(
                    'true' => 'One Column',
                    'false' => 'Based on browser screen width'
                )
            )
        );
        
        $this->addSettingFields(
            array(
                'section_id' => 'section_facebook',
                'field_id' => 'facebook_access_token',
                'type' => 'text',
                'title' => __( 'Any Valid App Token', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
                'description' => __( 'Use a long-lived App Token - e.g. <code>&lt;numeric part&gt;|&lt;alphanumeric part&gt;</code>.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_twitter',
                'field_id' => 'twitter_api_key',
                'type' => 'text',
                'title' => __( 'API Key', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
            ),
            array(
                'field_id' => 'twitter_api_secret',
                'type' => 'text',
                'title' => __( 'API Secret', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
            ),
            array(
                'field_id' => 'twitter_access_token',
                'type' => 'text',
                'title' => __( 'OAuth Access Token', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
            ),
            array(
                'field_id' => 'twitter_access_token_secret',
                'type' => 'text',
                'title' => __( 'OAuth Access Token Secret', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
            ),
            array(
                'section_id' => 'section_google',
                'field_id' => 'google_api_key',
                'title' => __( 'Google API KEY', 'social-board-admin' ),
                'type' => 'text',
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
                'description' => __( 'You can obtain it from <a href="https://code.google.com/apis/console/" target="_blank">Google APIs Console</a> - required', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_tumblr',
                'field_id' => 'tumblr_api_key',
                'title' => __( 'Tumblr API Key', 'social-board-admin' ),
                'type' => 'text',
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
                'description' => __( 'You can obtain your API Key by <a href="https://www.tumblr.com/oauth/register" target="_blank">registering an application</a> on Tumblr website - required', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_flickr',
                'field_id' => 'flickr_api_key',
                'title' => __( 'Flickr API Key', 'social-board-admin' ),
                'type' => 'text',
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
                'description' => __( 'You can obtain your API Key by <a href="https://www.flickr.com/services/apps/create/noncommercial/" target="_blank">registering an application</a> on Flickr App Garden - required', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_instagram',
                'field_id' => 'instagram_client_id',
                'title' => __( 'Client ID - <small>click to change</small>', 'social-board-admin' ),
                'type' => 'text',
                'default' => '0912694563ee4b26ad9095f3fdf5b889',
                'attributes' => array(
                    'style' => "width: 300px;",
                ),
                'description' => __( 'Click the button below and log into your Instagram account to get your Access Token.<br>You can also obtain your Access Token from <a href="http://axentmedia.com/instagram-access-token/" target="_blank">this website</a><div style="padding-top: 10px; display: block;"><a class="button button-primary" name="sb-instagram-token" id="sb-instagram-token" type="button" href="https://instagram.com/oauth/authorize/?client_id='.$oAdminPage->getValue( array( 'section_instagram', 'instagram_client_id', 0 ), '0912694563ee4b26ad9095f3fdf5b889' ).'&redirect_uri=http://axentmedia.com/instagram-access-token/?return_uri='.$GLOBALS['sb_apipage_url'].'&response_type=token&scope=public_content">Login and get my Access Token</a> <input class="button button-secondary" name="sb-reset-client" id="sb-reset-client" type="button" value="Reset Client ID"></div>', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_instagram',
                'field_id' => 'instagram_access_token',
                'title' => __( 'Access Token', 'social-board-admin' ),
                'type' => 'text',
                'attributes' => array(
                    'style' => "width: 300px;"
                ),
                'description' => __( '<label id="sb-instagram-token-msg"></label>', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_linkedin',
                'field_id' => 'linkedin_access_token',
                'type' => 'text',
                'title' => __( 'Access Token', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "width: 250px;"
                )
            ),
            array(
                'section_id' => 'section_vimeo',
                'field_id' => 'vimeo_access_token',
                'type' => 'text',
                'title' => __( 'Vimeo Access Token', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "width: 250px;"
                )
            ),
            array(
                'section_id' => 'section_vk',
                'field_id' => 'vk_service_token',
                'title' => __( 'VK Service Token', 'social-board-admin' ),
                'type' => 'text',
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
                'description' => __( 'You can obtain your Service Token by <a href="https://vk.com/editapp?act=create" target="_blank">registering an application</a> on VK Developers website - required', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_soundcloud',
                'field_id' => 'soundcloud_client_id',
                'title' => __( 'SoundCloud Client ID', 'social-board-admin' ),
                'type' => 'text',
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
                'description' => __( 'You can obtain your Client ID from <a href="http://soundcloud.com/you/apps/" target="_blank">SoundCloud API Service</a> - required', 'social-board-admin' )
            )
        );
        
        $this->addSettingFields(
            array( // Reset Submit button
                'section_id' => 'section_reset',
                'field_id' => 'reset_default',
                'title' => __( 'Reset Default', 'social-board-admin' ),
                'type' => 'submit',
                'label' => __( 'Reset', 'social-board-admin' ),
                'reset' => true,
                'attributes' => array(
                    'class' => 'button button-secondary',
                ),
                'description' => __( 'If you press this button, a confirmation message will appear and then if you press it again, it resets all the options to the default settings.', 'social-board-admin' ),
            )
        );
        
        $license_desc = 'Enter your CodeCanyon purchase key to enable and receive automatic updates.
        <p><strong>Each website using this plugin needs a legal license (1 license = 1 website).</strong><br>
        You can find more information on envato licenses <a href="http://codecanyon.net/licenses/standard" target="_blank">clicking here</a>.<br>
        If you need to buy a new license of this plugin, <a href="http://axentmedia.com/wordpress-social-board/" target="_blank">click here</a>.</p>
        <p><a href="http://axentmedia.com/wordpress-social-board-docs/#licensing" target="_blank">Where I can find my purchase code?</a></p>';
        $this->addSettingFields(
            array(
                'section_id' => 'section_licensing',
                'field_id' => 'license_key',
                'type' => 'text',
                'title' => __( 'License/Purchase Key', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "width: 250px;"
                ),
                'description' => __( $license_desc, 'social-board-admin' )
            )
        );
    }

    private function ___getRowCheckboxes( $rowid, array $fields ) {
        $_aSavedCheckboxes = $this->oUtil->getElement( 
            $this->oProp->aOptions, 
            array( 'setting', 'filters_order', $rowid ),
            $fields
        );
        return $this->___getCheckboxLabels( $_aSavedCheckboxes );
    }
    
    // Get Social Checkboxes
    private function ___getCheckboxLabels($aSavedCheckboxes) {
        if ( ! is_array($aSavedCheckboxes) )
        	$aSavedCheckboxes = array($aSavedCheckboxes => 1);

        $_aCheckboxes = array();
        foreach($aSavedCheckboxes as $_sKey => $_iValue) {
            $_aCheckboxes[] = array(
                'field_id'  => $_sKey,
                'type'      => 'checkbox',
                'label'     => $this->___getCheckboxLabelByKey( $_sKey ),
                'value'     => $_iValue,
            );
        }
        return $_aCheckboxes;
    }
    
    // Get Social Label
    private function ___getCheckboxLabelByKey($sKey) {
        $_aLabels = array(
            'facebook' => __( 'Facebook', 'social-board-admin' ),
            'twitter' => __( 'Twitter', 'social-board-admin' ),
            'google' => __( 'Google', 'social-board-admin' ),
            'tumblr' => __( 'Tumblr', 'social-board-admin' ),
            'delicious' => __( 'Delicious', 'social-board-admin' ),
            'pinterest' => __( 'Pinterest', 'social-board-admin' ),
            'flickr' => __( 'Flickr', 'social-board-admin' ),
            'instagram' => __( 'Instagram', 'social-board-admin' ),
            'youtube' => __( 'YouTube', 'social-board-admin' ),
            'vimeo' => __( 'Vimeo', 'social-board-admin' ),
            'stumbleupon' => __( 'Stumbleupon', 'social-board-admin' ),
            'deviantart' => __( 'Deviantart', 'social-board-admin' ),
            'rss' => __( 'RSS', 'social-board-admin' ),
            'soundcloud' => __( 'SoundCloud', 'social-board-admin' ),
            'vk' => __( 'VK', 'social-board-admin' ),
            'linkedin' => __( 'LinkedIn', 'social-board-admin' ),
            'vine' => __( 'Vine', 'social-board-admin' )
        );
        return isset( $_aLabels[ $sKey ] ) ? $_aLabels[ $sKey ] : '';
    }

    // Built-in Field Types Page
    public function do_sb_settings() { // do_{page slug}
        if (@$_GET['tab'] != 'debug' && @$_GET['tab'] != 'reset')
            submit_button();
    }
    
    // Filter the page contents.
    public function content_sb_settings( $sContent ) { // content_ + {page slug}
        if (@$_GET['tab'] == 'api')
            $msg = __( 'Some networks require <code>authentication</code> to provide feed data. <a href="'. SB_DOCS . '#api-credentials" target="_blank">Read the documentation</a> for more details.</code>', 'social-board-admin' );
        elseif (@$_GET['tab'] == 'setting')
            $msg = __( 'All changes here will only effect the plugin <code>default settings</code>. You can use inline attributes in shortcodes to override these default options.', 'social-board-admin' );
        else
            $msg = '';
        return '<p>' . $msg . '</p>' . $sContent;
    }
    
    public function do_sb_settings_debug() { // do_ + page slug + _ + tab slug
        $file = SB_DIRNAME . '/sb.log';
        if ( file_exists($file) ) {
            $_aSBLog = file_get_contents( $file );
            echo '<div style="overflow: scroll;min-height: 800px">' . nl2br($_aSBLog) . '</div>';
        }
    }
}

/* End of file ./admin/SB_Settings_Page.php */