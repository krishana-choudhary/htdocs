<?php

/**
 * Social Board 3.3.7
 * Copyright 2017 Axent Media (support@axentmedia.com)
 */

// create a custom post type for social boards manipulation options
class SB_MetaBox_Filters extends SB_AdminPageFramework_MetaBox {

    /**
     * The tab slug to add to the page.
     */
    public $sTabSlug    = 'filtering_sections';
    
    /**
     * The section slug to add to the tab.
     */
    public $sSectionID  = 'filtering_tabs';
    
    public function start_SB_MetaBox_Filters() {
    	add_filter('admin_footer', 'sb_filters_inline_script');
    }
    
    /*
     * ( optional ) Use the setUp() method to define settings of this meta box.
     */
    public function setUp() {
        $this->oProp->bShowDebugInfo = false;
        
        // Section
        $this->addSettingSections(
            array(
                'section_id'        => $this->sSectionID,
                'tab_slug'          => $this->sTabSlug,
                'section_tab_slug'  => 'filtering_tabs_sections',
                'title'             => __( 'Extra filtering tabs', 'social-board-admin' ),
                'description' => __( 'Here you can add your custom filtering tabs.', 'social-board-admin' ),
                'repeatable'        => true,
                'sortable'          => true
            )
        );   

        /*
         * ( optional ) Adds setting fields into the meta box.
         */
        $this->addSettingFields(
        	$this->sSectionID, // the target section ID
             array(
                'field_id'      => 'tab_title',
                'type'          => 'section_title',
                'label'         => __( 'Title', 'social-board-admin' ),
                'attributes'    => array(
                    'size' => 10,
                    'type' => 'text' // change the input type 
                )
            ),
            array(
                'field_id'        => 'search_term',
                'title'         => __( 'Search Term / Filter Keyword', 'social-board-admin' ),
                'description' => __( 'Here you can add your custom filtering tabs. You can use a manual search term e.g. <code>paris</code> [OR] copy/paste a feed filter keyword from the Social Board Configuration section e.g. <code>.facebook-1-0</code> or <code>.rss-1-1</code>.', 'social-board-admin' ),
                'type'            => 'text',
                'attributes'      => array(
                    'fieldset'  => array(
                        'style' => 'min-width: 200px;'
                    )
                )
            ),
            array(
                'field_id'        => 'filter_icon',
                'title'         => __( 'Filter Icon', 'social-board-admin' ),
                'type'            => 'image',
                'description' => __( 'If icon is not set, the filter title will be used.', 'social-board-admin' ),
                'allow_external_source' => false,
			    'attributes'      => array(
			    	'style' => 'max-width: 200px;'
			    )
            )
        );
    }
    
    public function do_SB_MetaBox_Filters() { // do_{instantiated class name}
        echo '<a href="'. SB_DOCS . '#filters" target="_blank">Read the documentation</a> for more details.';
    }
}

function sb_filters_inline_script() {
    if ( wp_script_is( 'jquery', 'done' ) ) {
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        // set filtering keywords
        $( document ).bind( 'social-board-admin_added_repeatable_field', function( oEvent, sFieldType, sID ) {
            var arrfilterID = sID.split('_');
            var newID = sID.replace("field-", "filter-key-");
            $('#'+sID+' .filter-key').html('.'+arrfilterID[1]+'-'+arrfilterID[4]+'-'+arrfilterID[6]);
        });
        
        // set default
        var feeds = $('input[name*="section_"][name*="_id_"]');
        sb_filters_update($, feeds);
    });
    
    // set ad type function
    function sb_filters_update($, feeds) {
		feeds.each(function() {
		    var feedID = $(this).attr('id');
		    if (feedID) {
		    	var arrID = feedID.split('_');
		    	$('#field-'+feedID+' .social-board-admin-repeatable-field-buttons').append('<span style="margin-left: 50px"><small>Filter keyword:</small> <code class="filter-key">'+'.'+arrID[1]+'-'+arrID[4]+'-'+arrID[6]+'</code></span>');
		    }
		})
    }
    </script>
    <?php
    }
}

?>