<?php

/**
 * WordPress Social Board 3.4.0
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// create an admin widget
class SB_Widget extends SB_AdminPageFramework_Widget {

    /**
     * The user constructor.
     * 
     * Alternatively you may use start_{instantiated class name} method.
     */
    public function start() {}
    
    /**
     * Sets up arguments.
     * 
     * Alternatively you may use set_up_{instantiated class name} method.
     */
    public function setUp() {
        $this->oProp->bShowDebugInfo = false;
        $this->setArguments(
            array(
                'description' => __( 'Displays a Social Board on your website.', 'social-board-admin' ),
                'classname' => 'widget_sb'
            )
        );
    }

    /**
     * Sets up the form.
     * 
     * Alternatively you may use load_{instantiated class name} method.
     */
    public function load() {
        $this->addSettingFields(
            array(
                'field_id'      => 'title',
                'type'          => 'text',
                'title'         => __( 'Title', 'social-board-admin' )
            ),
            array(
                'field_id'      => 'id',
                'type'          => 'select',
                'title'         => __( 'Social Board', 'social-board-admin' ),
                'description'   => __( 'Select the Social Board that you are going to add on your website.', 'social-board-admin' ),
                'label'         => sb_getPostTitles('sb_posts'),
                'attributes'    => array(
                    'select' => array(
                        'style' => "width: 150px;"
                    )
                )
            ),
            array (
                'field_id' => 'theme',
                'type' => 'select',
                'title' => __( 'Board Theme', 'social-board-admin' ),
                'label' => sb_getPostTitles('sb_themes'),
                'attributes' => array(
                    'select' => array(
                        'style' => "width: 150px;",
                    ),
                ),
                'description' => __( 'Select a theme to style your Social Board.', 'social-board-admin' ),
            ),
            array (
                'field_id' => 'sb_type',
                'type'  => 'select',
                'title' => __( 'Display Mode', 'social-board-admin' ),
                'label' => array(
                    'feed'     => __( 'Rotating Feed', 'social-board-admin' ),
                    'wall'     => __( 'Wall', 'social-board-admin' ),
                    'timeline' => __( 'Timeline', 'social-board-admin' )
                ),
                'default' => 'feed',
                'attributes'    => array(
                    'select' => array(
                        'style' => "width: 150px;"
                    )
                ),
                'hidden'      => true,
                'description' => __( 'Type of displaying on the website.', 'social-board-admin' )
            ),
            array(
                'field_id'    => 'position',
                'title'       => __( 'Display Position', 'social-board-admin' ),
                'description' => __( 'How the widget display on the website.', 'social-board-admin' ),
                'type'        => 'radio',
                'default'     => 'normal',
                'label'       => array(
                    'normal'    => 'Normal Widget',
                    'sticky'    => 'Sticky Widget',
                )
            ),
            array (
                'field_id' => 'carousel',
                'type' => 'checkbox',
                'title' => __( 'Carousel', 'social-board-admin' ),
                'label' => __( 'Check to enable carousel style', 'social-board-admin' ),
                'default' => 0,
                'description' => __( 'This will enable/disable carousel mode.', 'social-board-admin' ),
            ),
            array (
                'field_id' => 'tabable',
                'type' => 'checkbox',
                'title' => __( 'Ajax Tabbed', 'social-board-admin' ),
                'label' => __( 'Check to enable ajax social tabs', 'social-board-admin' ),
                'default' => 0,
                'description' => __( 'This will enable/disable using of social network tabs instead of filter networks feature. Each tab loads in Ajax mode.', 'social-board-admin' ),
            ),
            array (
                'field_id' => 'location',
                'type' => 'select',
                'title' => __( 'Location', 'social-board-admin' ),
                'label' => array( 
                    'sb-bottom_left' => __( 'Bottom Left', 'social-board-admin' ),
                    'sb-bottom_right' => __( 'Bottom Right', 'social-board-admin' ),
                    'sb-left' => __( 'Left', 'social-board-admin' ),
                    'sb-right' => __( 'Right', 'social-board-admin' )
                ),
                'default' => 'bottom_left',
                'attributes' => array(
                    'select' => array(
                        'style' => "width: 150px;",
                    ),
                ),
                'description' => __( 'Where to be displayed on the website.', 'social-board-admin' ),
            ),
            array (
                'field_id' => 'slide',
                'type' => 'checkbox',
                'title' => __( 'Slide In/Out', 'social-board-admin' ),
                'label' => __( 'Check to enable slide in/out', 'social-board-admin' ),
                'default' => 1,
                'description' => __( 'This will enable/disable slide in/out feature.', 'social-board-admin' ),
            ),
            array (
                'field_id' => 'autoclose',
                'type' => 'checkbox',
                'title' => __( 'Slide Auto-Close', 'social-board-admin' ),
                'label' => __( 'Check to enable slide auto-close', 'social-board-admin' ),
                'default' => 1,
                'description' => __( 'This will enable/disable slide auto-close feature.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'showheader',
                'type' => 'checkbox',
                'title' => __( 'Header', 'social-board-admin' ),
                'label' => __( 'Display Header', 'social-board-admin' ),
                'default' => 1,
                'description' => __( 'If checked, a feed header showing title will appear at the top of the feed block in place of the widget title.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'width',
                'title' => __( 'Width', 'social-board-admin' ),
                'type' => 'text',
                'description' => __( 'Width of widget (px) - Leave empty for no width.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'height',
                'title' => __( 'Height', 'social-board-admin' ),
                'type' => 'text',
                'default' => 400,
                'description' => __( 'Height of widget (px) - Leave empty for no height.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'override_settings',
                'title' => __( 'Override Default Settings', 'social-board-admin' ),
                'type' => 'text',
                'description' => __( 'You can override the board default settings for this widget here by passing the parameters separated by &amp; - e.g. order=random&cache=0 - Leave empty to use board default settings.', 'social-board-admin' )
            )
        );
    }
    
    /**
     * Print out the contents in the front-end.
     * 
     * Alternatively you may use the content_{instantiated class name} method.
     */
    public function content( $sContent, $aArguments, $aFormData ) {
        $sb = new SocialBoard();
        $aFormData['sb_type'] = 'feed';
        if (@$aFormData['override_settings']) {
            parse_str($aFormData['override_settings'], $aOverride);
            $aFormData = array_merge($aOverride, $aFormData);
            unset($aFormData['override_settings']);
        }
        return $sContent . $sb->init( $aFormData, false, $aArguments );
    }
}
