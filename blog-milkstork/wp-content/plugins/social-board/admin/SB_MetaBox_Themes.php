<?php

/**
 * WordPress Social Board 3.4.1
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// create manage themes page in admin
class SB_MetaBox_Themes extends SB_AdminPageFramework_MetaBox {

    public function start_SB_MetaBox_Themes() {

        if ( ! class_exists('SB_AceCustomFieldType') )
            include_once( SB_DIRNAME . '/library/admin-page-framework/custom-field-types/ace-custom-field-type/AceCustomFieldType.php');
        $sClassName = get_class( $this );
        new SB_AceCustomFieldType( $sClassName );
    }
    
    /*
     * ( optional ) Use the setUp() method to define settings of this meta box.
     */
    public function setUp() {
        $this->oProp->bShowDebugInfo = false;

        $this->addSettingSections(
            array(
                'section_id' => 'section_wall',
                'section_tab_slug' => 'tabbed_sections',
                'title' => __( 'Wall Options', 'admin-page-framework-demo' ),
                'description' => __( 'These options are only dedicated to the Wall display mode.', 'admin-page-framework-demo' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_timeline',
                'title' => __( 'Timeline', 'admin-page-framework-demo' ),
                'description' => __( 'These options are only dedicated to the Timeline display mode.', 'admin-page-framework-demo' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_feed',
                'title' => __( 'Normal Feed', 'admin-page-framework-demo' ),
                'description' => __( 'These options are only dedicated to Normal Rotating Feed display mode.', 'admin-page-framework-demo' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_feed_sticky',
                'title' => __( 'Sticky Feed', 'admin-page-framework-demo' ),
                'description' => __( 'These options are only dedicated to Sticky Rotating Feed display mode.', 'admin-page-framework-demo' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_feed_carousel',
                'title' => __( 'Carousel Feed', 'admin-page-framework-demo' ),
                'description' => __( 'These options are only dedicated to Carousel Feed display mode.', 'admin-page-framework-demo' ),
                'show_debug_info' => false
            ),
            array(
                'section_tab_slug' => '', // reset the target section tab slug for the next call.
                'section_id' => '_default', // reset the target tab slug for the next use.
                'show_debug_info' => false
            )
        );
        
        /*
         * ( optional ) Adds setting fields into the meta box.
         */
        $this->addSettingFields(
            array(
                'field_id'      => 'classname',
                'type'          => 'text',
                'title'         => __( 'CSS Class Name', 'social-board-admin' ),
                'description'   => __( 'Select a name for the theme CSS <code>class</code> selector (without dot). You will also use this name in your shortcodes e.g.<br />[social_board id="123" type="wall" theme="class-name"]', 'social-board-admin' )
            ),
            array(
                'field_id' => 'layout',
                'type' => 'select',
                'title' => __( 'Board Layout', 'social-board-admin' ),
                'label' => sb_getFileTitles(),
                'attributes' => array(
                    'select' => array(
                        'style' => "width: 150px;",
                    ),
                ),
                'description' => __( 'Use a predefined layout or create your own layout and put in the <code>./wp-content/uploads/social-board/layouts/</code> folder to use here. <a href="'. SB_DOCS . '#themes" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
            ),
            array( // Size
                'field_id'      => 'font_size',
                'title'         => __( 'Font Size', 'social-board-admin' ),
                'description'   => __( 'In order to set a font size <code>(in px)</code> for the Social Board.', 'social-board-admin' ),
                'type'          => 'number',
                'default'       => 11
            )
        );
        
        $this->addSettingFields(
            array( // Multiple Color Pickers
                'field_id' => 'social_colors',
                'title' => __( 'Social Networks Color', 'social-board-admin' ),
                'type' => 'color',
                'label' => __( 'Facebook', 'social-board-admin' ),
                'description' => __( 'Here you can change the background colors used for each social network. To change one of the network colors, click on the relevant colored box and a colorpicker should be appeared. Then select the new color and click elsewhere on the screen to close the colorpicker widget. The colored box should now be updated with the new color.', 'social-board-admin' ),
                'delimiter' => '<br />',
                array(
                    'label' => __( 'Twitter', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Google +1', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Tumblr', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Delicious', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Pinterest', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Flickr', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Instagram', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'YouTube', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Vimeo', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Stumbleupon', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Deviantart', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'RSS', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'SoundCloud', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'VK', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'LinkedIn', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Vine', 'social-board-admin' ),
                )
            ),
            array( // Image Selector
                'field_id' => 'social_icons',
                'title' => __( 'Social Networks Icons', 'social-board-admin' ),
                'type' => 'image',
                'label' => __( 'Facebook', 'social-board-admin' ),
                'allow_external_source' => false,
                'attributes'    => array(
                    'style' => 'max-width:200px;',
                    'preview' => array(
                        'style' => 'max-width:100px;' // determines the size of the preview image. // margin-left: auto; margin-right: auto; will make the image in the center.
                    )
                ),
                array(
                    'label' => __( 'Twitter', 'social-board-admin' ),
                    'default' => '',
                    'allow_external_source' => true,
                ),
                array(
                    'label' => __( 'Google +1', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Tumblr', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Delicious', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Pinterest', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Flickr', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Instagram', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'YouTube', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Vimeo', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Stumbleupon', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Deviantart', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'RSS', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'SoundCloud', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'VK', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'LinkedIn', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Vine', 'social-board-admin' ),
                ),
                'description'   => __( 'Here you can change the icon used for each social network. To change one of the network icons, enter the direct url of the icon file or click on the relevant "Select Image" button and an "Upload Image" window should now appear.', 'social-board-admin' ),
            ),
            array( // Image Selector
                'field_id' => 'type_icons',
                'title' => __( 'Post Type Icons', 'social-board-admin' ),
                'type' => 'image',
                'label' => __( 'Note', 'social-board-admin' ),
                'allow_external_source' => false,
                'attributes'    => array(
                    'style' => 'max-width:200px;',
                    'preview' => array(
                        'style' => 'max-width:100px;' // determines the size of the preview image. // margin-left: auto; margin-right: auto; will make the image in the center.
                    ),
                ),
                array(
                    'label' => __( 'Article', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Quote', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Link', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Image', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Camera', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Video', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Audio', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Like', 'social-board-admin' ),
                ),
                array(
                    'label' => __( 'Comment', 'social-board-admin' ),
                ),
                'description'   => __( 'Here you can change the icon used for each post type. To change one of the type icons, enter the direct url of the icon file or click on the relevant "Select Image" button and an "Upload Image" window should now appear.', 'social-board-admin' ),
            ),
            array( // Text Area
                'field_id'      => 'custom_css',
                'type'          => 'ace',
                'title'         => __( 'Custom CSS', 'social-board-admin' ),
                'description'   => __( 'Custom CSS for styling the Social Board can be entered into this text field. Any CSS rules included in this text area will automatically be inserted into the page.', 'social-board-admin' ),
                'attributes'    => array(
                    'rows' => 6,
                    'cols' => 60
                ),
                'options'   => array(
                    'language'              => 'css',
                    'theme'                 => 'chrome',
                    'gutter'                => false,
                    'readonly'              => false,
                    'fontsize'              => 12,
                )
            )
        );
        
        $this->repeatable_options( array('section_wall', 'section_timeline', 'section_feed', 'section_feed_sticky', 'section_feed_carousel') );
    }
    
    /*
     * Built-in Field Types Page
     * */
    public function do_SB_MetaBox_Themes() { // do_{instantiated class name}
        ?>
            <p><?php _e( 'Do not forget to select a <code>.class</code> name for your theme. Use the selected name in the Custom CSS section.', 'social-board-admin' ) ?></p>
        <?php
    }
    
    /*
     * ( optional ) Use this method to validate submitted option values.
     */
    public function validation_SB_MetaBox_Themes( $aInput, $aOldInput, $oAdmin ) { // validation_{instantiated class name}
    
        $_bIsValid  = true;
        $_aErrors   = array();
        
        // Validate the submitted data.
        if ( strlen( trim( $aInput['classname'] ) ) < 3 ) {
            $_aErrors['classname'] = __( 'The field is missing or the entered text is too short! Type more than 2 characters.', 'social-board-admin' );
            $_bIsValid = false;
        }
        
        if ( ! $_bIsValid ) {
            $this->setFieldErrors( $_aErrors );
            $this->setSettingNotice( __( 'There was an error in your input data in the form fields.', 'social-board-admin' ) );    
            return $aOldInput;
        }

        return $aInput;
    }
    
    public function repeatable_options( $sections ) {
        
        foreach ($sections as $section_id) {
        if ($section_id == 'section_feed' || $section_id == 'section_feed_sticky' || $section_id == 'section_feed_carousel')
            $this->addSettingFields(
                array (
                    'section_id' => $section_id,
                    'field_id' => 'title_background_color',
                    'type' => 'color',
                    'title' => __( 'Header Background Color', 'social-board-admin' ),
                    'default'       => ($section_id == 'section_feed_sticky') ? '#F3F3F3' : 'transparent',
                    'description' => __( 'Select the new background color of the Social Board Title and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' )
                ),
                array (
                    'section_id' => $section_id,
                    'field_id' => 'title_color',
                    'type' => 'color',
                    'title' => __( 'Title Font Color', 'social-board-admin' ),
                    'default'       => ($section_id == 'section_feed_sticky') ? '#FFFFFF' : 'transparent',
                    'description' => __( 'Select the new font color of the Social Board Title and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' )
                )
            );
        if ($section_id == 'section_feed_sticky')
            $this->addSettingFields(
                array(
                    'field_id' => 'opener_image',
                    'type' => 'image',
                    'title' => __( 'Block Opener Image', 'social-board-admin' ),
                    'allow_external_source' => false,
                    'attributes'    => array(
                        'preview' => array(
                            'style' => 'max-width:100px;' // determines the size of the preview image. // margin-left: auto; margin-right: auto; will make the image in the center.
                        )
                    ),
                    'description' => __( 'To change the block opener icon, enter the direct url of the image file or click on the relevant "Select Image" button and an "Upload Image" window should now appear.', 'social-board-admin' )
                )
            );
            
        $this->addSettingFields(
            array (
                'section_id' => $section_id,
                'field_id' => 'background_color',
                'type' => 'color',
                'title' => __( 'Body Background Color', 'social-board-admin' ),
                'default'       => ($section_id == 'section_feed_sticky') ? '#FFFFFF' : 'transparent',
                'description' => __( 'Select the new background color of the Social Board and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' ),
            ),
            array (
                'field_id'      => 'border_color',
                'type'          => 'color',
                'title'         => __( 'Body Border Color', 'social-board-admin' ),
                'default'       => ($section_id == 'section_feed_sticky') ? '#545454' : 'transparent',
                'description'   => __( 'Select the new border color of the Social Board and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'border_size',
                'title'         => __( 'Body Border Size', 'social-board-admin' ),
                'description'   => __( 'In order to set a border size <code>(in px)</code> for the Social Board.', 'social-board-admin' ),
                'type'          => 'number',
                'default'       => ($section_id == 'section_feed_sticky') ? 3 : 1,
            ),
            array(
                'field_id'      => 'border_radius',
                'title'         => __( 'Body Border Radius', 'social-board-admin' ),
                'description'   => __( 'In order to set a border radius <code>(in px)</code> for the Social Board.', 'social-board-admin' ),
                'type'          => 'number',
                'default'       => 7,
            ),
            array(
                'field_id' => 'background_image',
                'type' => 'image',
                'title' => __( 'Body Background Image', 'social-board-admin' ),
                'allow_external_source' => false,
                'attributes'    => array(
                    'preview' => array(
                        'style' => 'max-width:100px;' // determines the size of the preview image. // margin-left: auto; margin-right: auto; will make the image in the center.
                    ),
                ),
                'description' => __( 'In order to set a background image for the Social Board, enter the direct url of the image file or click on the relevant "Select Image" button and an "Upload Image" window should now appear.', 'social-board-admin' )
            ),
            array (
                'field_id' => 'font_color',
                'type' => 'color',
                'title' => __( 'Font Color', 'social-board-admin' ),
                'default' => 'transparent',
                'description' => __( 'Select the new font color for the feed item and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' )
            ),
            array (
                'field_id' => 'link_color',
                'type' => 'color',
                'title' => __( 'Link Color', 'social-board-admin' ),
                'default' => 'transparent',
                'description' => __( 'Select the new color for the feed item\' links and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' )
            ),
            array (
                'field_id' => 'item_background_color',
                'type' => 'color',
                'title' => __( 'Item Background Color', 'social-board-admin' ),
                'default'       => ($section_id == 'section_feed_sticky') ? '#FFFFFF' : 'transparent',
                'description' => __( 'Select the new background color for the feed item and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' ),
            ),
            array (
                'field_id'      => 'item_border_color',
                'type'          => 'color',
                'title'         => __( 'Item Border Color', 'social-board-admin' ),
                'default'       => ($section_id == 'section_feed_sticky') ? '#545454' : 'transparent',
                'description'   => __( 'Select the new border color for the feed item and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'item_border_size',
                'title'         => __( 'Item Border Size', 'social-board-admin' ),
                'description'   => __( 'In order to set a border size <code>(in px)</code> for the feed item.', 'social-board-admin' ),
                'type'          => 'number',
                'default'       => ($section_id == 'section_feed_sticky') ? 3 : 1,
            )
        );
        }
    }
}
