<?php

/**
 * WordPress Social Board 3.3.1
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// create social board post type
class SB_Social_Boards extends SB_AdminPageFramework_PostType {
    
    /**
     * This method is called at the end of the constructor.
     * 
     * Use this method to set post type arguments and add custom taxonomies as those need to be done in the front-end as well.
     * Also, to add custom taxonomies, the setUp() method is too late.
     * 
     * ALternatevely, you may use the start_{instantiated class name} method, which also is called at the end of the constructor.
     */
    public function start() {
        
        $this->setPostTypeArgs(
            array(
                'labels' => array(
                    'name'               => 'Social Board',
                    'all_items'          => __( 'Manage Boards', 'social-board-admin' ),
                    'singular_name'      => 'Social Board',
                    'add_new'            => __( 'Add New Board', 'social-board-admin' ),
                    'add_new_item'       => __( 'Add New Social Board', 'social-board-admin' ),
                    'edit'               => __( 'Edit', 'social-board-admin' ),
                    'edit_item'          => __( 'Edit Social Board', 'social-board-admin' ),
                    'new_item'           => __( 'New Social Board', 'social-board-admin' ),
                    'view'               => __( 'View', 'social-board-admin' ),
                    'view_item'          => __( 'View Social Board', 'social-board-admin' ),
                    'search_items'       => __( 'Search Social Board', 'social-board-admin' ),
                    'not_found'          => __( 'No Social Board found', 'social-board-admin' ),
                    'not_found_in_trash' => __( 'No Social Board found in Trash', 'social-board-admin' ),
                    'parent'             => __( 'Parent Social Board', 'social-board-admin' ),
                    'plugin_listing_table_title_cell_link' => __( 'Social Boards', 'social-board-admin' ),
                ),
                'public'            => false,
                'rewrite'           => false,
                'menu_position'     => 80,
                'supports'          => array( 'title' ), // e.g. array( 'title', 'editor', 'comments', 'thumbnail' ),
                'taxonomies'        => array( '' ),
                'has_archive'       => false,
                'show_admin_column' => true, // this is for custom taxonomies to automatically add the column in the listing table.
                'show_ui'           => true,
                'show_in_nav_menus' => true,
                'menu_icon'         => $this->oProp->bIsAdmin ? plugins_url( 'public/img/menu-icon.png', SB_FILE ) : null, // do not call the function in the front-end.
                // ( framework specific key ) this sets the screen icon for the post type for WordPress v3.7.1 or below.
                'screen_icon' => dirname( SB_FILE  ) . '/public/img/screen-icon.png', // a file path can be passed instead of a url, plugins_url( 'asset/image/wp-logo_32x32.png', SB_FILE )
                'exclude_from_search' => true
            )
        );
    }
    
    /**
     * Automatically called with the 'wp_loaded' hook.
     */
    public function setUp() {
        $this->oProp->bShowDebugInfo = false;
        $this->oProp->aFooterInfo['sRight'] = '';
        
        if ( $this->oProp->bIsAdmin ) {
            $this->setAutoSave( false );
            $this->setAuthorTableFilter( true );
        }
    }
    
    /*
     * Built-in callback methods
     */
    public function columns_sb_posts( $aHeaderColumns ) { // columns_{post type slug}
        return array_merge(
            $aHeaderColumns,
            array(
                'cb'                => '<input type="checkbox" />', // Checkbox for bulk actions.
                'title'             => __( 'Name', 'admin-page-framework' ), // Post title. Includes "edit", "quick edit", "trash" and "view" links. If $mode (set from $_REQUEST['mode']) is 'excerpt', a post excerpt is included between the title and links.
                'author'            => __( 'Author', 'admin-page-framework' ), // Post author.
                'date'              => __( 'Date', 'admin-page-framework' ), // The date and publish status of the post.
                'wallcode'          => __( 'Wall Shortcode' ),
                'timelinecode'      => __( 'Timeline Shortcode' ),
                'feedcode'          => __( 'Rotating Feed Shortcode' ),
                'carouselcode'      => __( 'Carousel Shortcode' )
            )
        );
    }
    
    public function cell_sb_posts_wallcode( $sCell, $iPostID ) { // cell_{post type}_{column key}
        return sprintf( __( '[social_board id="%1$s" type="wall"]', 'social-board-admin' ), $iPostID );
    }
    
    public function cell_sb_posts_timelinecode( $sCell, $iPostID ) { // cell_{post type}_{column key}
        return sprintf( __( '[social_board id="%1$s" type="timeline"]', 'social-board-admin' ), $iPostID );
    }
    
    public function cell_sb_posts_feedcode( $sCell, $iPostID ) { // cell_{post type}_{column key}
        return sprintf( __( '[social_board id="%1$s" type="feed"]', 'social-board-admin' ), $iPostID );
    }
    
    public function cell_sb_posts_carouselcode( $sCell, $iPostID ) { // cell_{post type}_{column key}
        return sprintf( __( '[social_board id="%1$s" type="carousel"]', 'social-board-admin' ), $iPostID );
    }
}
