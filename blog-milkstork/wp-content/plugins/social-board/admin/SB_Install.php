<?php

/**
 * WordPress Social Board 3.3.1
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

$GLOBALS['themes'] = array(
    'sb-modern-light' => array(
        'modern',
        'Modern Light',
        'wall' => 'a:8:{s:16:"background_color";s:7:"#f3f3f3";s:12:"border_color";s:7:"#d9d9d9";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e5e5e5";s:16:"item_border_size";s:1:"1";}',
        'timeline' => 'a:8:{s:16:"background_color";s:0:"";s:12:"border_color";s:0:"";s:11:"border_size";s:1:"0";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e5e5e5";s:16:"item_border_size";s:1:"1";}',
        'feed' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#f2f2f2";s:12:"border_color";s:7:"#e5e5e5";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e2e2e2";s:16:"item_border_size";s:1:"1";}',
        'feed_sticky' => 'a:11:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#FFFFFF";s:12:"opener_image";s:0:"";s:16:"background_color";s:7:"#f2f2f2";s:12:"border_color";s:7:"#d6d6d6";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e2e2e2";s:16:"item_border_size";s:1:"1";}',
        'feed_carousel' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#f2f2f2";s:12:"border_color";s:7:"#e5e5e5";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e2e2e2";s:16:"item_border_size";s:1:"1";}'
        ),
    'sb-metro-dark' => array(
        'metro',
        'Metro Dark',
        'wall' => 'a:10:{s:16:"background_color";s:0:"";s:12:"border_color";s:0:"";s:11:"border_size";s:1:"1";s:13:"border_radius";s:1:"7";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#ffffff";s:10:"link_color";s:11:"transparent";s:21:"item_background_color";s:7:"#444444";s:17:"item_border_color";s:7:"#050505";s:16:"item_border_size";s:1:"0";}',
        'timeline' => 'a:10:{s:16:"background_color";s:0:"";s:12:"border_color";s:0:"";s:11:"border_size";s:1:"1";s:13:"border_radius";s:1:"7";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#ffffff";s:10:"link_color";s:11:"transparent";s:21:"item_background_color";s:7:"#444444";s:17:"item_border_color";s:7:"#000000";s:16:"item_border_size";s:1:"0";}',
        'feed' => 'a:12:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#2b2b2b";s:12:"border_color";s:7:"#000000";s:11:"border_size";s:1:"1";s:13:"border_radius";s:1:"7";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#ffffff";s:10:"link_color";s:11:"transparent";s:21:"item_background_color";s:7:"#444444";s:17:"item_border_color";s:7:"#000000";s:16:"item_border_size";s:1:"0";}',
        'feed_sticky' => 'a:13:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#FFFFFF";s:12:"opener_image";s:0:"";s:16:"background_color";s:7:"#2d2d2d";s:12:"border_color";s:7:"#000000";s:11:"border_size";s:1:"1";s:13:"border_radius";s:1:"7";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#FFFFFF";s:10:"link_color";s:11:"transparent";s:21:"item_background_color";s:7:"#545454";s:17:"item_border_color";s:7:"#000000";s:16:"item_border_size";s:1:"0";}',
        'feed_carousel' => 'a:12:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#2b2b2b";s:12:"border_color";s:7:"#000000";s:11:"border_size";s:1:"1";s:13:"border_radius";s:1:"7";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#ffffff";s:10:"link_color";s:11:"transparent";s:21:"item_background_color";s:7:"#444444";s:17:"item_border_color";s:7:"#000000";s:16:"item_border_size";s:1:"0";}'
        ),
    'sb-modern2-light' => array(
        'modern2',
        'Modern 2 Light',
        'wall' => 'a:8:{s:16:"background_color";s:7:"#f3f3f3";s:12:"border_color";s:7:"#d9d9d9";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e5e5e5";s:16:"item_border_size";s:1:"1";}',
        'timeline' => 'a:8:{s:16:"background_color";s:0:"";s:12:"border_color";s:0:"";s:11:"border_size";s:1:"0";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e5e5e5";s:16:"item_border_size";s:1:"1";}',
        'feed' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#f2f2f2";s:12:"border_color";s:7:"#e5e5e5";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e2e2e2";s:16:"item_border_size";s:1:"1";}',
        'feed_sticky' => 'a:11:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#FFFFFF";s:12:"opener_image";s:0:"";s:16:"background_color";s:7:"#f2f2f2";s:12:"border_color";s:7:"#d6d6d6";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e2e2e2";s:16:"item_border_size";s:1:"1";}',
        'feed_carousel' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#f2f2f2";s:12:"border_color";s:7:"#e5e5e5";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#e2e2e2";s:16:"item_border_size";s:1:"1";}'
        ),
    'sb-default-light' => array(
        'default',
        'Default Light',
        'wall' => 'a:8:{s:16:"background_color";s:7:"#f3f3f3";s:12:"border_color";s:7:"#d9d9d9";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:0:"";s:16:"item_border_size";s:1:"1";}',
        'timeline' => 'a:8:{s:16:"background_color";s:0:"";s:12:"border_color";s:0:"";s:11:"border_size";s:1:"0";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:11:"transparent";s:16:"item_border_size";s:1:"1";}',
        'feed' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#fcfcfc";s:12:"border_color";s:7:"#e5e5e5";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#050505";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:0:"";s:16:"item_border_size";s:1:"1";}',
        'feed_sticky' => 'a:11:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#FFFFFF";s:12:"opener_image";s:0:"";s:16:"background_color";s:7:"#FFFFFF";s:12:"border_color";s:7:"#e5e5e5";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#FFFFFF";s:17:"item_border_color";s:0:"";s:16:"item_border_size";s:1:"1";}',
        'feed_carousel' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#fcfcfc";s:12:"border_color";s:7:"#e5e5e5";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#050505";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:0:"";s:16:"item_border_size";s:1:"1";}'
        ),
    'sb-flat-light' => array(
        'flat',
        'Flat Light',
        'wall' => 'a:8:{s:16:"background_color";s:0:"";s:12:"border_color";s:0:"";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:0:"";s:16:"item_border_size";s:1:"1";}',
        'timeline' => 'a:8:{s:16:"background_color";s:0:"";s:12:"border_color";s:0:"";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:11:"transparent";s:16:"item_border_size";s:1:"2";}',
        'feed' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#ffffff";s:12:"border_color";s:7:"#cecece";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:0:"";s:16:"item_border_size";s:1:"2";}',
        'feed_sticky' => 'a:11:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:12:"opener_image";s:0:"";s:16:"background_color";s:7:"#ffffff";s:12:"border_color";s:7:"#545454";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:7:"#a3a3a3";s:16:"item_border_size";s:1:"2";}',
        'feed_carousel' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#ffffff";s:12:"border_color";s:7:"#cecece";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#000000";s:21:"item_background_color";s:7:"#ffffff";s:17:"item_border_color";s:0:"";s:16:"item_border_size";s:1:"2";}'
        ),
    'sb-modern-dark' => array(
        'modern',
        'Modern Dark',
        'wall' => 'a:8:{s:16:"background_color";s:7:"#2d2d2d";s:12:"border_color";s:7:"#280000";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#ffffff";s:21:"item_background_color";s:7:"#444444";s:17:"item_border_color";s:7:"#050505";s:16:"item_border_size";s:1:"1";}',
        'timeline' => 'a:8:{s:16:"background_color";s:7:"#2d2d2d";s:12:"border_color";s:7:"#280000";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#ffffff";s:21:"item_background_color";s:7:"#444444";s:17:"item_border_color";s:7:"#000000";s:16:"item_border_size";s:1:"1";}',
        'feed' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#2b2b2b";s:12:"border_color";s:7:"#000000";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#ffffff";s:21:"item_background_color";s:7:"#444444";s:17:"item_border_color";s:7:"#000000";s:16:"item_border_size";s:1:"1";}',
        'feed_sticky' => 'a:11:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#FFFFFF";s:12:"opener_image";s:0:"";s:16:"background_color";s:7:"#2d2d2d";s:12:"border_color";s:7:"#000000";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#FFFFFF";s:21:"item_background_color";s:7:"#545454";s:17:"item_border_color";s:7:"#000000";s:16:"item_border_size";s:1:"3";}',
        'feed_carousel' => 'a:10:{s:22:"title_background_color";s:7:"#dd3333";s:11:"title_color";s:7:"#ffffff";s:16:"background_color";s:7:"#2b2b2b";s:12:"border_color";s:7:"#000000";s:11:"border_size";s:1:"1";s:16:"background_image";s:0:"";s:10:"font_color";s:7:"#ffffff";s:21:"item_background_color";s:7:"#444444";s:17:"item_border_color";s:7:"#000000";s:16:"item_border_size";s:1:"1";}'
        )
    );

// plugin installation
function sb_data_insert() {
    global $wpdb, $themes;
    
    // add theme
    $themes_row = $wpdb->get_var("SELECT * FROM " . $wpdb->prefix . "posts WHERE `post_type` LIKE 'sb_themes'");
    if ( ! $themes_row) {
        foreach ($themes as $key => $theme) {
            sb_add_theme($key, $theme);
        }
    }
}

// plugin upgrade
function sb_data_update() {
    global $wpdb, $themes;
    
    // add themes if not exist
    foreach ($themes as $key => $theme) {
        $themes_row = $wpdb->get_var("SELECT * FROM " . $wpdb->prefix . "posts WHERE `post_type` LIKE 'sb_themes' AND `post_title` LIKE '" . $theme[1] . "'");
        if ( ! $themes_row) {
            sb_add_theme($key, $theme);
        }
    }
}

function sb_add_theme($key, $theme) {
    global $wpdb;
    $user_ID = get_current_user_id();
    $social_colors = 'a:17:{i:0;s:7:"#305790";i:1;s:7:"#06d0fe";i:2;s:7:"#c04d2e";i:3;s:7:"#2E4E65";i:4;s:7:"#2d6eae";i:5;s:7:"#cb1218";i:6;s:7:"#ff0185";i:7;s:7:"#295477";i:8;s:7:"#b80000";i:9;s:7:"#00a0dc";i:10;s:7:"#ec4415";i:11;s:7:"#495d51";i:12;s:7:"#d78b2d";i:13;s:7:"#ff3300";i:14;s:7:"#4c75a3";i:15;s:7:"#1884BC";i:16;s:7:"#39a97b";}';
    
    $wpdb->query("INSERT INTO `" . $wpdb->prefix . "posts` (`post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
                ('$user_ID', NOW(), NOW(), '', '" . $theme[1] . "', '', 'publish', 'closed', 'closed', '', '', '', '', NOW(), NOW(), '', 0, '', 0, 'sb_themes', '', 0)");
    $lastid = $wpdb->insert_id;
    $wpdb->query("INSERT INTO `" . $wpdb->prefix . "postmeta` (`post_id`, `meta_key`, `meta_value`) VALUES
	($lastid, 'classname', '" . $key . "'),
	($lastid, 'layout', '$theme[0]'),
	($lastid, 'font_size', '11'),
	($lastid, 'social_colors', '$social_colors'),
	($lastid, 'section_wall', '$theme[wall]'),
	($lastid, 'section_timeline', '$theme[timeline]'),
	($lastid, 'section_feed', '$theme[feed]'),
	($lastid, 'section_feed_sticky', '$theme[feed_sticky]'),
	($lastid, 'section_feed_carousel', '$theme[feed_carousel]');");
}
