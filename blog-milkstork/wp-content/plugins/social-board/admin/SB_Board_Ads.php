<?php

/**
 * WordPress Social Board 3.3.1
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// create board ads post type
class SB_Board_Ads extends SB_AdminPageFramework_PostType {
    
    /**
     * This method is called at the end of the constructor.
     * 
     * Use this method to set post type arguments and add custom taxonomies as those need to be done in the front-end as well.
     * Also, to add custom taxonomies, the setUp() method is too late.
     * 
     * ALternatevely, you may use the start_{instantiated class name} method, which also is called at the end of the constructor.
     */
    public function start() {
        
        $this->setPostTypeArgs(
            array(
                'labels' => array(
                    'name'               => 'Board Ads',
                    'all_items'          => __( 'Manage Ads', 'social-board-admin' ),
                    'singular_name'      => 'Board Ad',
                    'add_new'            => __( 'Add New Board Ad', 'social-board-admin' ),
                    'add_new_item'       => __( 'Add New Board Ad', 'social-board-admin' ),
                    'edit'               => __( 'Edit', 'social-board-admin' ),
                    'edit_item'          => __( 'Edit Board Ad', 'social-board-admin' ),
                    'new_item'           => __( 'New Board Ad', 'social-board-admin' ),
                    'view'               => __( 'View', 'social-board-admin' ),
                    'view_item'          => __( 'View Board Ad', 'social-board-admin' ),
                    'search_items'       => __( 'Search Board Ad', 'social-board-admin' ),
                    'not_found'          => __( 'No Board Ad found', 'social-board-admin' ),
                    'not_found_in_trash' => __( 'No Board Ad found in Trash', 'social-board-admin' ),
                    'plugin_listing_table_title_cell_link' => __( 'Board Ads', 'social-board-admin' )
                ),
                'public'              => false,
                'rewrite'             => false,
                'menu_position'       => 90,
                'supports'            => array( 'title' ), // e.g. array( 'title', 'editor', 'comments', 'thumbnail' ),
                'has_archive'         => false,
                'show_admin_column'   => true, // this is for custom taxonomies to automatically add the column in the listing table.
                'show_ui'             => true,
                'hierarchical'        => false,
                'show_in_nav_menus'   => false,
                'show_in_admin_bar'   => false,
                'show_in_menu'        => 'edit.php?post_type=sb_posts',
                'exclude_from_search' => true
            )
        );
    }
    
    /**
     * Automatically called with the 'wp_loaded' hook.
     */
    public function setUp() {
        $this->oProp->bShowDebugInfo = false;
        $this->oProp->aFooterInfo['sRight'] = '';
        
        if ( $this->oProp->bIsAdmin ) {
            $this->setAutoSave( false );
            $this->setAuthorTableFilter( true );
        }
    }
    
    /*
     * Built-in callback methods
     */
    public function columns_sb_ads( $aHeaderColumns ) { // columns_{post type slug}
        return array_merge(
            $aHeaderColumns,
            array(
                'cb'                => '<input type="checkbox" />', // Checkbox for bulk actions.
                'title'             => __( 'Name', 'social-board-admin' ), // Post title. Includes "edit", "quick edit", "trash" and "view" links. If $mode (set from $_REQUEST['mode']) is 'excerpt', a post excerpt is included between the title and links.
                'author'            => __( 'Author', 'social-board-admin' ), // Post author.
                'date'              => __( 'Date', 'social-board-admin' ), // The date and publish status of the post.
                'board'             => __( 'Board Name' ),
                'position'          => __( 'Block Position' ),
                'adtype'            => __( 'Type of Ad' ),
                'gridsize'          => __( 'Grid Size' )
            )
        );
    }
    
    public function cell_sb_ads_board( $sCell, $iPostID ) { // cell_{post type}_{column key}
        $board_id = get_post_meta( $iPostID, 'board_id', true );
        $post = get_post( $board_id );
        return sprintf( __( '%1$s', 'social-board-admin' ), $post->post_title );
    }
    
    public function cell_sb_ads_position( $sCell, $iPostID ) {
        return get_post_meta( $iPostID, 'ad_position', true );
    }
    
    public function cell_sb_ads_adtype( $sCell, $iPostID ) {
        $adtype = array('image' => 'Image Banner', 'text' => 'Simple Text', 'code' => 'HTML/JS Code');
        return $adtype[get_post_meta( $iPostID, 'ad_type', true )];
    }
    
    public function cell_sb_ads_gridsize( $sCell, $iPostID ) {
        $gridsize = array('solo' => 'Solo', 'twofold' => 'Two fold', 'threefold' => 'Three fold');
        return $gridsize[get_post_meta( $iPostID, 'ad_grid_size', true )];
    }
}
