<?php

/**
 * WordPress Social Board 3.4.0
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// Create a custom post type for boards - this class deals with front-end components so checking with is_admin() is not necessary.
include( SB_DIRNAME . '/admin/SB_Social_Boards.php' );
new SB_Social_Boards(
    'sb_posts',          // the post type slug
    null,                // the argument array. Here null is passed because it is defined inside the class.
    SB_FILE,             // the caller script path.
    'social-board-admin' // the text domain.
);

// Create a custom post type for themes
include( SB_DIRNAME . '/admin/SB_Themes.php' );
new SB_Themes(
    'sb_themes',         // the post type slug
    null,                // the argument array. Here null is passed because it is defined inside the class.
    SB_FILE,             // the caller script path.
    'social-board-admin' // the text domain.
);

// Create a custom post type for ads
include( SB_DIRNAME . '/admin/SB_Board_Ads.php' );
new SB_Board_Ads(
    'sb_ads',            // the post type slug
    null,                // the argument array. Here null is passed because it is defined inside the class.
    SB_FILE,             // the caller script path.
    'social-board-admin' // the text domain.
);

// Create widgets - this class also deals with front-end components so no need to check with is_admin().
include( SB_DIRNAME . '/admin/SB_Widget.php' );
new SB_Widget( __( 'Social Board', 'social-board-admin' ) ); // the widget title

// Create admin pages.
if ( is_admin() ) {
    add_action( 'admin_enqueue_scripts', 'load_admin_style' );
    
    // Create meta boxes with form fields that appear in post definition pages (where you create a post) of the given post type.
    include( SB_DIRNAME . '/admin/SB_MetaBox_Social_Boards.php' );
    new SB_MetaBox_Social_Boards(
        'posts_metabox',                                     // meta box ID
        __( 'Social Board Configurations', 'social-board-admin' ), // title
        array( 'sb_posts' ),                                 // post type slugs: post, page, etc - setting multiple slugs is possible
        'normal',                                            // context (what kind of metabox this is)
        'default'                                            // priority
    );
    
    // Create meta boxes for filtering tabs
    include( SB_DIRNAME . '/admin/SB_MetaBox_Filters.php' );
    new SB_MetaBox_Filters(
        'sb_metabox_filters',                                     // meta box ID
        __( 'Extra Filtering Tabs', 'social-board-admin' ), // title
        array( 'sb_posts' ),                                 // post type slugs: post, page, etc - setting multiple slugs is possible
        'normal',                                            // context (what kind of metabox this is)
        'default'                                            // priority
    );
    
    // Create meta boxes for manipulation section
    include( SB_DIRNAME . '/admin/SB_MetaBox_Social_Boards_Manipulate.php' );
    new SB_MetaBox_Social_Boards_Manipulate(
        'sb_metabox_social_boards_manipulate',
        __( 'Social Board Manipulation', 'social-board-admin' ),
        array( 'sb_posts' ),
        'side',
        'default'
    );
    new SB_MetaBox_Social_Boards_Output(
        'sb_metabox_social_boards_output',
        __( 'Social Board Output', 'social-board-admin' ),
        array( 'sb_posts' ),
        'side',
        'default'
    );
    
    // Create an admin sidebar in Manage Boards page
    include( SB_DIRNAME . '/admin/SB_MetaBox_Social_Boards_Side.php' );
    new SB_MetaBox_Social_Boards_Side(
        'sb_metabox_social_boards_side',
        __( 'Add to your website?', 'social-board-admin' ),
        array( 'sb_posts' ),
        'side',
        'default'
    );
    
    // Create Manage Themes page
    include( SB_DIRNAME . '/admin/SB_MetaBox_Themes.php' );
    new SB_MetaBox_Themes(
        'themes_metabox',
        __( 'Theme Settings', 'social-board-admin' ),
        array( 'sb_themes' ),
        'normal',
        'default'
    );
    
    // Create Manage Ads page
    include( SB_DIRNAME . '/admin/SB_MetaBox_Ads.php' );
    new SB_MetaBox_Ads(
        'sb_ads_metabox',
        __( 'Ad Settings', 'social-board-admin' ),
        array( 'sb_ads' ),
        'normal',
        'default'
    );
    
    // Create an example page group and add sub-pages including a page with the slug 'sb_settings'.
    include( SB_DIRNAME . '/admin/SB_Settings_Page.php' ); // Include the basic usage example that creates a root page and its sub-pages.
    new SB_Settings_Page(
        null,                       // the option key
        SB_FILE,                    // the caller script path.
        'manage_options',           // the default capability
        'social-board-admin'        // the text domain
    );
    
    // Checking for plugin updates
    sb_update_checker();
    add_action( 'in_plugin_update_message-social-board/social-board.php', 'sb_addUpgradeMessage' );
}

// Function for update checks
function sb_update_checker() {
    // Initializes the update checker
    if ( ! class_exists( 'PucFactory' ) )
        require SB_DIRNAME . '/library/plugin-update-checker/plugin-update-checker.php';
    $sbUpdateChecker = PucFactory::buildUpdateChecker(
        SB_UPSERVER . '?action=get_metadata&slug=social-board', // Metadata URL
        plugin_dir_path( dirname( __FILE__ ) ) . 'social-board.php', // Full path to the main plugin file
        'social-board' // Plugin slug. Usually it's the same as the name of the directory
    );
    // Add the license key to query arguments
    $sbUpdateChecker->addQueryArgFilter('sb_filter_update_checks');
}

// Function for filtering update checks
function sb_filter_update_checks($queryArgs) {
    $license_key = sb_license_key();
    if ( ! empty($license_key) ) {
        $queryArgs['license_key'] = $license_key;
    }
    return $queryArgs;
}

function sb_license_key() {
    $settings = SB_AdminPageFramework::getOption( 'SB_Settings_Page' );
    return @$settings['section_licensing']['license_key'];
}

// Shows message on WP plugins page with a link for updating from server
function sb_addUpgradeMessage() {
    $license_key = sb_license_key();
    if ( empty($license_key) ) {
		$url = esc_url( admin_url( 'edit.php?post_type=sb_posts&page=sb_settings&tab=licensing' ) );
		$redirect = sprintf( '<a href="%s" target="_blank">%s</a>', $url, __( 'settings', 'social-board-admin' ) );

		echo sprintf( ' ' . __( 'To receive automatic updates, license activation is required. Please visit %s to activate your plugin.', 'social-board-admin' ), $redirect );
	}
}

// load admin side stylesheets
function load_admin_style() {
    wp_enqueue_style( 'social-board-admin', plugins_url( 'public/css/admin-styles.min.css', SB_FILE ), false, SB_VERSION );
}

// grab the posts with type social boards
function sb_getPostTitles( $sPostTypeSlug ) {
    $_aArgs = array(
        'post_type' => $sPostTypeSlug,
    	'orderby' => 'ID',
    	'order' => 'ASC',
        'posts_per_page' => 999999
    );
    $_oResults    = new WP_Query( $_aArgs );
    $_aPostTitles = array();
    foreach( $_oResults->posts as $_iIndex => $_oPost ) {
        $_aPostTitles[$_oPost->ID] = $_oPost->post_title;
    }
    if ( empty($_aPostTitles) ) {
        $sPostLabel = ($sPostTypeSlug == 'sb_posts') ? 'Social Board' : 'Theme';
        $_aPostTitles[''] = '-- There is no '.$sPostLabel.' created --';
    }
    return $_aPostTitles;
}

// grab the layout files
function sb_getFileTitles( $path = '/layout/' ) {
    $_aFileTitles = array();
    
    // read the main layout folder
    $_aFileTitles = sb_getFiles(SB_DIRNAME . $path);
    
    // read/create extra layout folder
	$upload_dir = wp_upload_dir();
	$layouts_path = $upload_dir['basedir'].'/social-board/layouts';
	if ( ! file_exists($layouts_path) ) {
	    wp_mkdir_p($layouts_path);
	} else {
		$_aFileTitles = array_merge($_aFileTitles, sb_getFiles($layouts_path) );
	}

    if ( empty($_aFileTitles) ) {
        $_aFileTitles[''] = '-- There is no layout created --';
    }
    return $_aFileTitles;
}

// grab the files
function sb_getFiles($path) {
	$_aFileTitles = array();
    if ($handle = opendir($path) ) {
        while($file = readdir($handle) ) {
            if ($file !== '.' && $file !== '..')
            {
                $finfo = pathinfo($file);
                if (isset($finfo['extension']) ) {
                    if ($finfo['extension'] == 'php')
                        $_aFileTitles[$finfo['filename']] = $finfo['filename'];
                }
            }
        }
        closedir($handle);
    }
    return $_aFileTitles;
}
