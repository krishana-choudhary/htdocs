<?php

/**
 * WordPress Social Board 3.4.7
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// create a custom post type for social boards
class SB_MetaBox_Social_Boards extends SB_AdminPageFramework_MetaBox {

    /**
     * The start() method is called at the end of the constructor.
     */
    public function start() {
        if ( ! class_exists('DateTimeRangeCustomFieldType'))
            include( SB_DIRNAME . '/library/admin-page-framework/custom-field-types/date-time-custom-field-type/DateTimeRangeCustomFieldType.php' );
        $_sClassName = get_class( $this );
        new DateTimeRangeCustomFieldType( $_sClassName );
    }
    
    /*
     * ( optional ) Use the setUp() method to define settings of this meta box.
     */
    public function setUp() {
        $this->oProp->bShowDebugInfo = false;
        
        $tabDesc = 'You can enter multiple IDs by clicking <code>+</code> sign.';
        $this->addSettingSections(
            array(
                'section_id' => 'section_facebook',
                'section_tab_slug' => 'tabbed_sections',
                'title' => __( '<i class="sb-icon sb-facebook" title="Facebook"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc . '<br />Facebook requires a valid access token to provide the feed\'s data. <a href="'. SB_DOCS . '#facebook-api" target="_blank">Read the documentation</a> for more details.' , 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_twitter',
                'title' => __( '<i class="sb-icon sb-twitter" title="Twitter"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc . '<br />Twitter requires authentication to provides the feed\'s data. You can set the Twitter API Credentials in the <a href="'.admin_url( 'edit.php?post_type=sb_posts&page=sb_settings').'" target="_blank">Board Settings</a> page. <a href="'. SB_DOCS . '#twitter-api" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_google',
                'title' => __( '<i class="sb-icon sb-google" title="Google+"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc . '<br />Google+ requires an API Key to display feed data. <a href="'. SB_DOCS . '#google-api" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_tumblr',
                'title' => __( '<i class="sb-icon sb-tumblr" title="Tumblr"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc . '<br />Tumblr requires an API Key to provide the feed\'s data. <a href="'. SB_DOCS . '#tumblr-api" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_delicious',
                'title' => __( '<i class="sb-icon sb-delicious" title="Delicious"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc, 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_pinterest',
                'title' => __( '<i class="sb-icon sb-pinterest" title="Pinterest"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc, 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_flickr',
                'title' => __( '<i class="sb-icon sb-flickr" title="Flickr"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc . '<br />Flickr requires an API Key to provide the feed\'s data. <a href="'. SB_DOCS . '#flickr-api" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_instagram',
                'title' => __( '<i class="sb-icon sb-instagram" title="Instagram"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc . '<br />Instagram requires your API Access Token to provide the feed\'s data. <a href="'. SB_DOCS . '#instagram-api" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_youtube',
                'title' => __( '<i class="sb-icon sb-youtube" title="YouTube"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc . '<br />YouTube requires a Google API Key to display feed data. If you have previously generated and added an API Key for Google+ you wil not need to do it again for YouTube. <a href="'. SB_DOCS . '#google-api" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_vimeo',
                'title' => __( '<i class="sb-icon sb-vimeo" title="Vimeo"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc, 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_stumbleupon',
                'title' => __( '<i class="sb-icon sb-stumbleupon" title="Stumbleupon"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc, 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_deviantart',
                'title' => __( '<i class="sb-icon sb-deviantart" title="Deviantart"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc, 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_rss',
                'title' => __( '<i class="sb-icon sb-rss" title="RSS"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc, 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_soundcloud',
                'title' => __( '<i class="sb-icon sb-soundcloud" title="SoundCloud"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc . '<br />SoundCloud requires your API Client ID to provide the feed\'s data. <a href="'. SB_DOCS . '#soundcloud-api" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_vk',
                'title' => __( '<i class="sb-icon sb-vk" title="VK"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc, 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_linkedin',
                'title' => __( '<i class="sb-icon sb-linkedin" title="LinkedIn"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc . '<br />LinkedIn requires your API Access Token to provide the feed\'s data. <a href="'. SB_DOCS . '#linkedin-api" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_id' => 'section_vine',
                'title' => __( '<i class="sb-icon sb-vine" title="Vine"></i>', 'social-board-admin' ),
                'description' => __( $tabDesc, 'social-board-admin' ),
                'show_debug_info' => false
            ),
            array(
                'section_tab_slug' => '' // reset the target tab slug for the next use.
            )
        );
        
        $this->addSettingFields(
            // Facebook
            array(
                'section_id' => 'section_facebook',
                'field_id' => 'facebook_id_1',
                'title' => __( '1. Facebook Page Feed', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'The feed of posts (including status updates), photos and links on this page.<br />Enter the page ID - e.g. <code>80655071208</code>. You can find it in your Facebook page setting tab or obtain it from <a href="http://lookup-id.com/" target="_blank">this website</a>', 'social-board-admin' )
            ),
            array(
                'field_id'      => 'facebook_pagefeed',
                'title'         => __( 'Specific sets of posts', 'social-board-admin' ),
                'description'   => __( 'Get specific sets of posts for Facebook page feed. You will not have an exact number of results if you use filtering posts.', 'social-board-admin' ),
                'type'          => 'radio',
                'default'       => 'feed',
                'label'         => array(
                    'posts' => "Shows only the posts that were published by this page (Filters the others' posts from the total result)",
                    'tagged' => 'Shows all public posts by others in which the page has been tagged (Filters the posts published by this page from the total result)',
                    'feed' => 'Show both (Do not filter)'
                )
            ),
            array(
                'section_id' => 'section_facebook',
                'field_id' => 'facebook_id_2',
                'title' => __( '2. Facebook Group Feed', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'The feed of posts (including status updates), photos and links on this group.<br />Enter the group ID - e.g. <code>7091225894</code>. You can find it in your Facebook group setting tab or obtain it from <a href="http://lookup-id.com/" target="_blank">this website</a>', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_facebook',
                'field_id' => 'facebook_id_3',
                'title' => __( '3. Facebook Album/Page Photos', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Shows all photos this page is tagged in.<br />Enter the album ID - e.g. <code>947092091976094</code> or the page ID - e.g. <code>182472465104731</code>. <a href="'. SB_DOCS . '#facebook-id" target="_blank">Read the documentation</a> on how to find a Facebook album ID.', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_facebook',
                'field_id' => 'facebook_id_4',
                'title' => __( '4. Facebook Page Videos', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Shows all videos this page is tagged in.<br />Enter the page ID - e.g. <code>80655071208</code>. <a href="'. SB_DOCS . '#facebook-id" target="_blank">Read the documentation</a> on how to find a Facebook page ID.', 'social-board-admin' )
            ),
            /* Deprecated
            array(
                'section_id' => 'section_facebook',
                'field_id' => 'facebook_id_5',
                'title' => __( '5. Search Term/Hashtag', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'To search, enter the search term - e.g. <code>socialmedia</code> or hashtag - e.g. <code>#socialmedia</code><br /><strong>Note: </strong><small>Facebook public post search will be deprecated on April 30, 2015 and will be no longer available.</small>', 'social-board-admin' )
            ),
            */
            array( // Single date/time range picker
                'field_id' => 'facebook_datetime_range',
                'title' => __( 'Date Time Range', 'admin-page-framework-demo' ),
                'type' => 'date_time_range',
                'description' => __( 'Get posts in a certain date/time from Facebook feed.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'facebook_comments',
                'title' => __( 'Comments Count', 'social-board-admin' ),
                'type' => 'number',
                'default' => 3,
                'description' => __( 'Enter the number of comments to display for facebook album photos<br />Set to 0 to disable comments.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'facebook_likes',
                'title' => __( 'Likes', 'social-board-admin' ),
                'type' => 'number',
                'default' => 5,
                'description' => __( 'Number of likes to display - set to 0 to disable likes.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'facebook_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images/videos will be open in a lightbox window - (Iframe/Direct Link To Facebook)<br />* This feature will not work on all video services.', 'social-board-admin' )
            ),
            array(
                'field_id'      => 'facebook_image_width',
                'type'          => 'select',
                'title'         => __( 'Image width', 'social-board-admin' ),
                'description'   => __( 'Select the image width for Facebook gallery album posts. If the selected size was not available, a smaller size will be selected', 'social-board-admin' ),
                'default'       => '300',
                'label' => array(
                    '180'   => __( 'Thumb - 180px', 'social-board-admin' ),
                    '300'   => __( 'Tiny - 300px', 'social-board-admin' ),
                    '480'   => __( 'Very Small - 480px', 'social-board-admin' ),
                    '640'   => __( 'Small - 640px', 'social-board-admin' ),
                    '720'   => __( 'Medium - 720px', 'social-board-admin' ),
                    '800'   => __( 'Large - 800px', 'social-board-admin' ),
                    '960'   => __( 'Larger - 960px', 'social-board-admin' ),
                    '1280'  => __( 'X-Large - 1280px', 'social-board-admin' ),
                    '1600'  => __( 'XX-Large - 1600px', 'social-board-admin' )
                ),
                'attributes'    => array( // the 'attributes' element of the select field type has three keys: select, 'option', and 'optgroup'.
                    'select' => array(
                        'style' => "width: 200px;",
                    ),
                ),
            ),
            // Twitter
            array(
                'section_id' => 'section_twitter',
                'field_id' => 'twitter_id_1',
                'title' => __( '1. Twitter Username', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter a Twitter username without "@" - e.g. <code>username</code>', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_twitter',
                'field_id' => 'twitter_id_2',
                'title' => __( '2. Twitter List', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'To show a Twitter list enter the list ID - e.g. <code>123456</code><br />or enter the owner screen name, then "/" followed by the list slug - e.g. <code>username/list_name</code>', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_twitter',
                'field_id' => 'twitter_id_3',
                'title' => __( '3. Search Term/Hashtag', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'To search enter the search term - e.g. <code>socialmedia</code> or hashtag - e.g. <code>#socialmedia</code>', 'social-board-admin' ),
            ),
            array( // Multiple text fields
                'field_id'          => 'twitter_id_range',
                'title'             => __( 'ID Range', 'admin-page-framework-demo' ),
                'type'              => 'text',
                'label'             => __( 'Since ID', 'admin-page-framework-demo' ) . ': ',
                'attributes'        => array(
                    'size' => 20,
                ),
                'capability'        => 'manage_options',
                'delimiter'         => '<br />',
                array(
                    'label'         => __( 'Max ID', 'admin-page-framework-demo' ) . ': ',
                    'attributes'    => array(
                        'size' => 20,
                    )
                ),     
                'description'       => __( 'Get posts in a certain ID range from Twitter feed. <a href="'. SB_DOCS . '#tweet-id" target="_blank">Read the documentation</a> on how to find a Tweet ID.<br /><strong>Since ID: </strong>Returns results with an ID greater than (that is, more recent than) the specified ID.<br /><strong>Max ID: </strong>Returns results with an ID less than (that is, older than) or equal to the specified ID.', 'admin-page-framework-demo' ),
            ),
            array(
                'field_id'      => 'twitter_images',
                'type'          => 'select',
                'title'         => __( 'Image width', 'social-board-admin' ),
                'description'   => __( 'Select the image width for Twitter images.', 'social-board-admin' ),
                'default'       => 'small',
                'label' => array( 
                    'thumb'   => __( 'Thumb - 150px', 'social-board-admin' ),
                    'small'   => __( 'Small - 340px', 'social-board-admin' ),
                    'medium' => __( 'Medium - 600px', 'social-board-admin' ),
                    'large' => __( 'Large - 1024px', 'social-board-admin' ),
                ),
                'attributes'    => array( // the 'attributes' element of the select field type has three keys: select, 'option', and 'optgroup'.
                    'select' => array(
                        'style' => "width: 200px;",
                    ),
                ),
            ),
            array(
                'field_id' => 'twitter_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all media will be open in a lightbox window - (Iframe/Direct Link To Twitter).', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'twitter_feeds',
                'title'         => __( 'Feed Data', 'social-board-admin' ),
                'type'          => 'checkbox',
                'label'         => array(
                    'retweets' => __( 'Retweets', 'social-board-admin' ) . ' (' . __( 'Include feed item retweets', 'social-board-admin' ) . ')',
                    'replies' => __( 'Replies', 'social-board-admin' ) . ' (' . __( 'Include replies', 'social-board-admin' ) . ')'
                ),
                'default' => array(
                    'retweets' => true,
                    'replies' => true
                ),
                'description'   => __( 'Feed data to be gathered and displayed.', 'social-board-admin' ),
                'after_label'   => '<br />',
            ),
            array(
                'section_id' => 'section_google',
                'field_id' => 'google_id_1',
                'title' => __( 'Google+ Page/Profile ID or Username', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter the Google+ profile ID - e.g. <code>112592709425670873637</code> or the Google+ username - e.g. <code>+GoogleforEntrepreneurs</code>', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'google_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images/videos will be open in a lightbox window - (Iframe/Direct Link To Google).', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_tumblr',
                'field_id' => 'tumblr_id_1',
                'title' => __( 'Tumblr Username', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter a Tumblr username - e.g. <code>username</code>', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'tumblr_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open images in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images will be open in a lightbox window - (Iframe/Direct Link To Tumblr).', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'tumblr_embed',
                'type' => 'checkbox',
                'title' => __( 'Embed video', 'social-board-admin' ),
                'label' => __( 'Show videos embedded', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all videos will be displayed inline - (Embed/Direct Link To Tumblr).', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'tumblr_thumb',
                'type'          => 'select',
                'title'         => __( 'Thumbnail Width', 'social-board-admin' ),
                'description'   => __( 'Select the width of the thumbnail image.', 'social-board-admin' ),
                'default'       => '250',
                'label' => array( 
                    '75'   => __( 'Width: 75px', 'social-board-admin' ),
                    '100'   => __( 'Width: 100px', 'social-board-admin' ),
                    '250' => __( 'Width: 250px', 'social-board-admin' ),
                    '400' => __( 'Width: 400px', 'social-board-admin' ),
                    '500' => __( 'Width: 500px', 'social-board-admin' ),
                    '1280' => __( 'Width: 1280px', 'social-board-admin' ),
                ),
                'attributes'    => array(
                    'select' => array(
                        'style' => "width: 200px;",
                    ),
                ),
            ),
            array(
                'field_id'      => 'tumblr_video',
                'type'          => 'select',
                'title'         => __( 'Video Width', 'social-board-admin' ),
                'description'   => __( 'Width of inline video player.', 'social-board-admin' ),
                'default'       => '500',
                'label' => array( 
                    '250'   => __( 'Width: 250px', 'social-board-admin' ),
                    '400'   => __( 'Width: 400px', 'social-board-admin' ),
                    '500' => __( 'Width: 500px', 'social-board-admin' ),
                ),
                'attributes'    => array(
                    'select' => array(
                        'style' => "width: 200px;"
                    )
                )
            ),
            array(
                'section_id' => 'section_delicious',
                'field_id' => 'delicious_id_1',
                'title' => __( 'Delicious username', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter a Delicious username - e.g. <code>username</code>', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_pinterest',
                'field_id' => 'pinterest_id_1',
                'title' => __( '1. Pinterest username', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter a Pinterest username - e.g. <code>username</code>', 'social-board-admin' )
            ),
            array(
                'field_id' => 'pinterest_id_2',
                'title' => __( '2. Pinterest board', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'To show a Pinterest board enter the username, then "/" followed by the board name - e.g. <code>username/board_name</code>', 'social-board-admin' )
            ),
            array(
                'field_id'      => 'pinterest_image_width',
                'type'          => 'select',
                'title'         => __( 'Image width', 'social-board-admin' ),
                'description'   => __( 'Select the image width for Pinterest images.', 'social-board-admin' ),
                'default'       => '237',
                'label' => array(
                    '237'   => __( 'Thumb - 237px', 'social-board-admin' ),
                    '736'   => __( 'Large - 736px', 'social-board-admin' )
                ),
                'attributes' => array(
                    'select' => array(
                        'style' => "width: 200px;"
                    )
                )
            ),
            array(
                'field_id' => 'pinterest_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images/videos will be open in a lightbox window - (Iframe/Direct Link To Pinterest)', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_flickr',
                'field_id' => 'flickr_id_1',
                'title' => __( '1. Flickr User ID', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter a Flickr User ID - e.g. <code>46221135@N04</code> - You can obtain it from <a href="http://idgettr.com/" target="_blank">this website</a>', 'social-board-admin' )
            ),
            array(
                'section_id' => 'section_flickr',
                'field_id' => 'flickr_id_2',
                'title' => __( '2. Flickr Group ID', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'To use a flickr group enter the group ID - e.g. <code>34427465497@N01</code> - Use the same method mentioned above.', 'social-board-admin' )
            ),
            array(
                'field_id'      => 'flickr_thumb',
                'type'          => 'select',
                'title'         => __( 'Thumbnail Size', 'social-board-admin' ),
                'description'   => __( 'Size of thumbnail image.', 'social-board-admin' ),
                'default'       => 'm',
                'label' => array(
                    's' => __( 'small square 75x75', 'social-board-admin' ),
                    'q' => __( 'large square 150x150', 'social-board-admin' ),
                    't' => __( 'thumbnail, 100 on longest side', 'social-board-admin' ),
                    'm' => __( 'small, 240 on longest side' ),
                    'n'	=> __( 'small, 320 on longest side' ),
                    '-'	=> __( 'medium, 500 on longest side' ),
                    'z'	=> __( 'medium 640, 640 on longest side' ),
                    'c'	=> __( 'medium 800, 800 on longest side' ),
                    'b'	=> __( 'large, 1024 on longest side' ),
                    'h'	=> __( 'large 1600, 1600 on longest side' ),
                    'k'	=> __( 'large 2048, 2048 on longest side' ),
                    'o'	=> __( 'original image, either a jpg, gif or png, depending on source format' )
                ),
                'attributes'    => array( // the 'attributes' element of the select field type has three keys: select, 'option', and 'optgroup'.
                    'select' => array(
                        'style' => "width: 200px;",
                    ),
                ),
            ),
            array(
                'field_id' => 'flickr_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images will be open in a lightbox window - (Iframe/Direct Link To Flickr).', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_instagram',
                'field_id' => 'instagram_id_1',
                'title' => __( '1. Instagram User Feed', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( ' Get the most recent media published by the owner of the <a href="'. SB_DOCS . '#instagram-api">Access Token</a>. Enter <code>self</code> => To get the feed from owner of the Access Token that is set in Board Settings and/or <code>Access Token string</code> => extra Access Token from another Instagram account you want to get its feed.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'instagram_id_2',
                'title' => __( '2. Instagram search tag', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'To search by tag enter the tag name (contiguous) - e.g. <code>paris</code>', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'instagram_id_3',
                'title' => __( '3. Instagram location ID', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'To show the latest posts by a location, enter the location ID - e.g. 12345', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'instagram_id_4',
                'title' => __( '4. Instagram geographical location', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'To search by geographical location start with the latitude, longitude and distance in meters (up to a maximum of 5000) all separated by a "," - e.g. 48.858844,2.294351,2000<br /><a href="'. SB_DOCS . '#instagram-api" target="_blank">Read the documentation</a> for more details.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'instagram_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images/videos will be open in a lightbox window - (Iframe/Direct Link To Instagram).', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'instagram_images',
                'type'          => 'select',
                'title'         => __( 'Image width', 'social-board-admin' ),
                'description'   => __( 'Select the image width for Instagram images.', 'social-board-admin' ),
                'default'       => 'low_resolution',
                'label' => array( 
                    'thumbnail' => __( 'Thumbnail - 150px', 'social-board-admin' ),
                    'low_resolution' => __( 'Low Resolution - 306px', 'social-board-admin' ),
                    'standard_resolution' => __( 'Standard Resolution - 640px', 'social-board-admin' )
                ),
                'attributes'    => array( // the 'attributes' element of the select field type has three keys: select, 'option', and 'optgroup'.
                    'select' => array(
                        'style' => "width: 200px;",
                    ),
                ),
            ),
            array(
                'field_id' => 'instagram_videos',
                'type' => 'select',
                'title' => __( 'Video width', 'social-board-admin' ),
                'description' => __( 'Select the video width for Instagram videos.', 'social-board-admin' ),
                'default' => 'standard_resolution',
                'label' => array(
                    'low_resolution' => __( 'Low Resolution - 480px', 'social-board-admin' ),
                    'standard_resolution' => __( 'Standard Resolution - 640px', 'social-board-admin' )
                ),
                'attributes' => array( // the 'attributes' element of the select field type has three keys: select, 'option', and 'optgroup'.
                    'select' => array(
                        'style' => "width: 200px;",
                    ),
                ),
            ),
            array(
                'field_id' => 'instagram_comments',
                'title' => __( 'Comments', 'social-board-admin' ),
                'type' => 'number',
                'default' => 3,
                'description' => __( 'Number of comments to display - set to 0 to disable likes.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'instagram_likes',
                'title' => __( 'Likes', 'social-board-admin' ),
                'type' => 'number',
                'default' => 5,
                'description' => __( 'Number of likes to display - set to 0 to disable likes.', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_youtube',
                'field_id' => 'youtube_id_1',
                'title' => __( '1. YouTube username', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Returns a list of videos uploaded by this user - Enter a YouTube username - e.g. <code>username</code>', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'youtube_id_2',
                'title' => __( '2. YouTube playlist', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Returns a collection of playlist videos items - Enter the unique ID of the playlist for which you want to retrieve playlist videos items - e.g. <code>PLsBcifUwsKVXunQPoySupBM6QCcWliTKi</code>', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'youtube_id_3',
                'title' => __( '3. YouTube search term', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Returns a collection of search results that match the search term query you specified - e.g. <code>music</code>', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_youtube',
                'field_id' => 'youtube_id_4',
                'title' => __( '4. YouTube channel ID', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Returns a list of videos uploaded by this channel - Enter a YouTube channel ID - e.g. <code>UC1yP5nx6JNEBQI3ps2XFMpz</code>', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'youtube_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open video in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all videos will be open in a lightbox window - (Iframe/Direct Link To YouTube).', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'youtube_thumb',
                'type'          => 'select',
                'title'         => __( 'Thumbnail Size', 'social-board-admin' ),
                'description'   => __( 'Select YouTube image size.', 'social-board-admin' ),
                'default'       => 'medium',
                'label' => array(
                    'default' => __( 'Default - 120px × 90px', 'social-board-admin' ),
                    'medium' => __( 'Medium - 320px × 180px', 'social-board-admin' ),
                    'high' => __( 'High - 480px × 360px', 'social-board-admin' ),
                    'standard' => __( 'Standard - 640px × 480px', 'social-board-admin' ),
                    'maxres' => __( 'Max - 1280px × 720px', 'social-board-admin' )
                ),
                'attributes'    => array( // the 'attributes' element of the select field type has three keys: select, 'option', and 'optgroup'.
                    'select' => array(
                        'style' => "width: 200px;"
                    )
                )
            ),
            array(
                'field_id'      => 'youtube_video',
                'type'          => 'select',
                'title'         => __( 'Video light-box Size', 'social-board-admin' ),
                'description'   => __( 'Select a custom light-box size for YouTube videos. Social Board woll add extra 100px to the light-box height for better view.', 'social-board-admin' ),
                'default'       => '640-240',
                'label' => array( 
                    '1280-720' => __( 'Medium - 1280px × 720px', 'social-board-admin' ),
                    '854-480' => __( 'High - 854px × 480px', 'social-board-admin' ),
                    '640-360' => __( 'Default - 640px × 360px', 'social-board-admin' ),
                    '426-240' => __( 'Standard - 426px × 240px', 'social-board-admin' )
                ),
                'attributes'    => array( // the 'attributes' element of the select field type has three keys: select, 'option', and 'optgroup'.
                    'select' => array(
                        'style' => "width: 200px;"
                    )
                )
            ),
            array(
                'section_id' => 'section_vimeo',
                'field_id' => 'vimeo_id_1',
                'title' => __( 'Vimeo username', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter a Vimeo username - e.g. <code>abcdefg</code>', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'vimeo_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open video in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all videos will be open in a lightbox window - (Iframe/Direct Link To Vimeo).', 'social-board-admin' ),
            ),
            /*
            array(
                'field_id'      => 'vimeo_feeds',
                'title'         => __( 'Feed Data', 'social-board-admin' ),
                'type'          => 'checkbox',
                'label'         => array(
                    'videos' => __( 'Videos', 'social-board-admin' ) . ' (' . __( 'Videos created by user', 'social-board-admin' ) . ')',
                    'likes' => __( 'Likes', 'social-board-admin' ) . ' (' . __( 'Videos the user likes', 'social-board-admin' ) . ')',
                    'appears_in' => __( 'appears_in', 'social-board-admin' ) . ' (' . __( 'Videos that the user appears in', 'social-board-admin' ) . ')',
                    'all_videos' => __( 'all_videos', 'social-board-admin' ) . ' (' . __( 'Videos that the user appears in', 'social-board-admin' ) . ')',
                    'subscriptions' => __( 'Subscriptions', 'social-board-admin' ) . ' (' . __( 'Videos the user is subscribed to', 'social-board-admin' ) . ')',
                    'albums' => __( 'albums', 'social-board-admin' ) . ' (' . __( 'Albums the user has created', 'social-board-admin' ) . ')',
                    'channels' => __( 'channels', 'social-board-admin' ) . ' (' . __( 'Channels the user has created and subscribed to', 'social-board-admin' ) . ')',
                    'groups' => __( 'groups', 'social-board-admin' ) . ' (' . __( 'Groups the user has created and joined', 'social-board-admin' ) . ')',
                ),
                'default' => array(
                    'videos' => true,
                    'likes' => false,
                    'appears_in' => false,
                    'all_videos' => false,
                    'subscriptions' => false,
                    'albums' => false,
                    'channels' => false,
                    'groups' => false
                ),
                'description'   => __( 'Feed data to be gathered and displayed.', 'social-board-admin' ),
                'after_label'   => '<br />'
            ),
            */
            array(
                'field_id'      => 'vimeo_thumb',
                'type'          => 'select',
                'title'         => __( 'Thumbnail Size', 'social-board-admin' ),
                'description'   => __( 'Size of thumbnail image.', 'social-board-admin' ),
                'default'       => 'medium',
                'label' => array( 
                    'small'   => __( 'Small - 100x75', 'social-board-admin' ),
                    'medium'   => __( 'Medium - 200x150', 'social-board-admin' ),
                    'large' => __( 'Large - 640', 'social-board-admin' ),
                ),
                'attributes'    => array( // the 'attributes' element of the select field type has three keys: select, 'option', and 'optgroup'.
                    'select' => array(
                        'style' => "width: 200px;",
                    ),
                ),
            ),
            array(
                'section_id' => 'section_stumbleupon',
                'field_id' => 'stumbleupon_id_1',
                'title' => __( 'Stumbleupon username', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter a Stumbleupon username - e.g. <code>abcd</code>', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'stumbleupon_feeds',
                'title'         => __( 'Feed Data', 'social-board-admin' ),
                'type'          => 'checkbox',
                'label'         => array(
                    'comments' => __( 'Comments', 'social-board-admin' ) . ' (' . __( 'User comments', 'social-board-admin' ) . ')',
                    'likes' => __( 'Likes', 'social-board-admin' ) . ' (' . __( 'User likes', 'social-board-admin' ) . ')'
                ),
                'default' => array(
                    'comments' => true,
                    'likes' => true
                ),
                'description'   => __( 'Feed data to be gathered.', 'social-board-admin' ),
                'after_label'   => '<br />',
            ),
            array(
                'section_id' => 'section_deviantart',
                'field_id' => 'deviantart_id_1',
                'title' => __( 'Deviantart username', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter a Deviantart username - e.g. <code>abcd</code>', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_soundcloud',
                'field_id' => 'soundcloud_id_1',
                'title' => __( 'SoundCloud User Tracks', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'The list of tracks of the user.<br />Enter the SoundCloud Username - e.g. <code>mayerhawthorne</code>', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_rss',
                'field_id' => 'rss_id_1',
                'title' => __( 'RSS feed URL', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter the RSS feed URL - e.g. <code>http://feeds.bbci.co.uk/news/world/europe/rss.xml</code>', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'rss_text',
                'title'         => __( 'Text Info', 'social-board-admin' ),
                'description'   => __( 'Select Snippet for displaying a summary of the post or select Full Text to include any links or images in the feed - if available.', 'social-board-admin' ),
                'type'          => 'radio',
                'default'       => 0,
                'label'         => array(
                    0 => 'Full Text',
                    1 => 'Snippet'
                )
            ),
            array(
                'field_id' => 'rss_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images/videos will be open in a lightbox window - (Iframe/Direct Link To RSS Post)<br />* This feature will not work on all video services.', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'rss_image_width',
                'type'          => 'select',
                'title'         => __( 'Thumbnails width', 'social-board-admin' ),
                'description'   => __( 'Select the image width for RSS feed images. If you select any size different than original size, it will enable the Social Board image proxy that <strong>may slow up the process</strong>.', 'social-board-admin' ),
                'default'       => '',
                'label' => array(
                    ''  => __( 'Original width', 'social-board-admin' ),
                    '180'   => __( 'Thumb - 180px', 'social-board-admin' ),
                    '300'   => __( 'Tiny - 300px', 'social-board-admin' ),
                    '480'   => __( 'Very Small - 480px', 'social-board-admin' ),
                    '640'   => __( 'Small - 640px', 'social-board-admin' ),
                    '720'   => __( 'Medium - 720px', 'social-board-admin' ),
                    '800'   => __( 'Large - 800px', 'social-board-admin' ),
                    '960'   => __( 'Larger - 960px', 'social-board-admin' ),
                    '1200'  => __( 'X-Large - 1200px', 'social-board-admin' )
                ),
                'attributes'    => array( // the 'attributes' element of the select field type has three keys: select, 'option', and 'optgroup'.
                    'select' => array(
                        'style' => "width: 200px;",
                    ),
                ),
            ),
            // VK
            array(
                'section_id' => 'section_vk',
                'field_id' => 'vk_id_1',
                'title' => __( '1. VK Wall Domain', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Returns a list of posts on a user wall or community wall.<br />Enter the user or community short address - e.g. <code>wphelpme</code> or, ID of the user or community that owns the wall - e.g. <code>id99999999</code>.', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_vk',
                'field_id' => 'vk_id_2',
                'title' => __( '2. VK Wall Owner ID', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Returns a list of posts on a user wall or community wall.<br />Enter the ID of the user or community that owns the wall. By default, current user ID - e.g. <code>99999999</code>. Use a negative value <code>-</code> to designate a community ID - e.g. <code>-99999999</code>.', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'vk_pagefeed',
                'title'         => __( 'Filter to apply', 'social-board-admin' ),
                'description'   => __( 'Apply filter and get specific sets of posts from VK wall feed.', 'social-board-admin' ),
                'type'          => 'radio',
                'default'       => 'all',
                'label'         => array(
                    'owner' => 'Show only the posts by the wall owner',
                    'tagged' => 'Show the posts by someone else',
                    'all' => 'Show both posts by the wall owner and others'
                )
            ),
            array(
                'field_id'      => 'vk_image_width',
                'type'          => 'select',
                'title'         => __( 'Image width', 'social-board-admin' ),
                'description'   => __( 'Select the image width for VK gallery album posts. If the selected size was not available, a smaller size will be selected', 'social-board-admin' ),
                'default'       => '604',
                'label' => array(
                    '75'   => __( 'Thumb - 75px', 'social-board-admin' ),
                    '130'  => __( 'Small - 130px', 'social-board-admin' ),
                    '604'  => __( 'Medium - 604px', 'social-board-admin' ),
                    '807'  => __( 'Large - 807px', 'social-board-admin' ),
                    '1280' => __( 'Larger - 1280px', 'social-board-admin' ),
                    '2560' => __( 'X-Large - 2560px', 'social-board-admin' )
                ),
                'attributes' => array(
                    'select' => array(
                        'style' => "width: 200px;",
                    ),
                ),
            ),
            array(
                'field_id' => 'vk_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images/videos will be open in a lightbox window - (Iframe/Direct Link To VK)<br />* This feature will not work on all video services.', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_linkedin',
                'field_id' => 'linkedin_id_1',
                'title' => __( 'LinkedIn Company Page ID', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'The list of the company updates.<br />Enter the LinkedIn Company Page ID - e.g. <code>10354180</code>', 'social-board-admin' ),
            ),
            /*
            array(
                'field_id'      => 'linkedin_pagefeed',
                'title'         => __( 'Specific sets of updates', 'social-board-admin' ),
                'description'   => __( 'Filter the results to only return updates of the specified event type.', 'social-board-admin' ),
                'type'          => 'radio',
                'default'       => 'all',
                'label'         => array(
                    'all' => 'Show all',
                    'job-posting' => 'Job postings',
                    'new-product' => 'New products',
                    'status-update' => 'Status updates'
                )
            ),
            */
            array(
                'field_id' => 'linkedin_comments',
                'title' => __( 'Comments', 'social-board-admin' ),
                'type' => 'number',
                'default' => 3,
                'description' => __( 'Number of comments to display - set to 0 to disable likes.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'linkedin_likes',
                'title' => __( 'Likes', 'social-board-admin' ),
                'type' => 'number',
                'default' => 5,
                'description' => __( 'Number of likes to display - set to 0 to disable likes.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'linkedin_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images/videos will be open in a lightbox window - (Iframe/Direct Link To LinkedIn)', 'social-board-admin' ),
            ),
            array(
                'section_id' => 'section_vine',
                'field_id' => 'vine_id_1',
                'title' => __( 'Vine User Timeline', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'The list of user timeline updates.<br />For user timeline enter Vine username or ID - e.g. <code>974522892907909120</code>', 'social-board-admin' ),
            ),
            /*
            array(
                'field_id' => 'vine_comments',
                'title' => __( 'Comments', 'social-board-admin' ),
                'type' => 'number',
                'default' => 3,
                'description' => __( 'Number of comments to display - set to 0 to disable likes.', 'social-board-admin' ),
            ),
            */
            array(
                'field_id' => 'vine_likes',
                'title' => __( 'Likes', 'social-board-admin' ),
                'type' => 'number',
                'default' => 5,
                'description' => __( 'Number of likes to display - set to 0 to disable likes.', 'social-board-admin' ),
            ),
            array(
                'field_id' => 'vine_iframe',
                'type' => 'checkbox',
                'title' => __( 'Open in Lightbox', 'social-board-admin' ),
                'label' => __( 'Open media in a lightbox window', 'social-board-admin' ),
                'default' => true,
                'description' => __( 'If checked, all images/videos will be open in a lightbox window - (Iframe/Direct Link To Vine)', 'social-board-admin' ),
            ),
            array()
        );
    }
    
    public function do_SB_MetaBox_Social_Boards() {
        echo __( '<div class="admin-page-framework-section-title"><h3>Social Networks</h3></div>', 'social-board-admin' );
    }
}