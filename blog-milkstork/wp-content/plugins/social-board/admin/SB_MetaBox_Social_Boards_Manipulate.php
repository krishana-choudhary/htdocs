<?php

/**
 * Social Board 3.3.7
 * Copyright 2017 Axent Media (support@axentmedia.com)
 */

// create a custom post type for social boards manipulation options
class SB_MetaBox_Social_Boards_Manipulate extends SB_AdminPageFramework_MetaBox {

    /*
     * ( optional ) Use the setUp() method to define settings of this meta box.
     */
    public function setUp() {
        $this->oProp->bShowDebugInfo = false;
        
        /*
         * ( optional ) Adds setting fields into the meta box.
         */
        $this->addSettingFields(
            array(
                'field_id' => 'pins',
                'title' => __( 'Pinned Items', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'This section will allow you to pin/stick an item on top of the Social Board. Enter the links of social items that you are going to pin on top of the Social Board - Each link in a new line.', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "max-width: 180px;"
                )
            ),
            array(
                'field_id' => 'remove',
                'title' => __( 'Remove Items', 'social-board-admin' ),
                'type' => 'text',
                'repeatable' => true,
                'description' => __( 'Enter the links of social items that you are going to remove from the Social Board - Each link in a new line.', 'social-board-admin' ),
                'attributes' => array(
                    'style' => "max-width: 180px;"
                )
            )
        );
    }
    
    public function do_SB_MetaBox_Social_Boards_Manipulate() { // do_{instantiated class name}
        echo '<a href="'. SB_DOCS . '#manipulation" target="_blank">Read the documentation</a> for more details.';
    }
}

// create a custom post type for social boards output options
class SB_MetaBox_Social_Boards_Output extends SB_AdminPageFramework_MetaBox {

    /*
     * ( optional ) Use the setUp() method to define settings of this meta box.
     */
    public function setUp() {
        
        $title_Board_Output = 'Board Output';
        $desc_board_output = 'Select the content blocks that you want to be included in each item on the board. Each social network may have different type of content blocks available.';
        
        /*
         * ( optional ) Adds setting fields into the meta box.
         */
        $this->addSettingFields(
            array(
                'field_id'      => 'output',
                'title'         => __( $title_Board_Output, 'social-board-admin' ),
                'type'          => 'checkbox',
                'label'         => array(
                    'title' => __( 'Title', 'social-board-admin' ) . ' (' . __( 'Feed item title', 'social-board-admin' ) . ')',
                    'thumb' => __( 'Thumbnail', 'social-board-admin' ) . ' (' . __( 'Display thumbnail - if available', 'social-board-admin' ) . ')',
                    'text' => __( 'Text', 'social-board-admin' ) . ' (' . __( 'Post text', 'social-board-admin' ) . ')',
                    'comments' => __( 'Comments', 'social-board-admin' ) . ' (' . __( 'Display post comments', 'social-board-admin' ) . ')',
                    'likes' => __( 'Likes', 'social-board-admin' ) . ' (' . __( 'Display post likes', 'social-board-admin' ) . ')',
                    'user' => __( 'User', 'social-board-admin' ) . ' (' . __( 'Display user info', 'social-board-admin' ) . ')',
                    'share' => __( 'Share', 'social-board-admin' ) . ' (' . __( 'Include sharing links', 'social-board-admin' ) . ')',
                    'info' => __( 'Info', 'social-board-admin' ) . ' (' . __( 'Feed item icon & date', 'social-board-admin' ) . ')',
                    'stat' => __( 'Stat', 'social-board-admin' ) . ' (' . __( 'Display stat block', 'social-board-admin' ) . ')',
                    'meta' => __( 'Meta', 'social-board-admin' ) . ' (' . __( 'Feed meta data', 'social-board-admin' ) . ')',
                    'tags' => __( 'Tags', 'social-board-admin' ) . ' (' . __( 'Feed item tags', 'social-board-admin' ) . ')'
                ),
                'default' => array(
                    'title' => true,
                    'thumb' => true,
                    'text' => true,
                    'comments' => true,
                    'likes' => true,
                    'user' => true,
                    'share' => true,
                    'info' => true,
                    'stat' => true,
                    'meta' => true,
                    'tags' => false
                ),
                'description'   => __( $desc_board_output, 'social-board-admin' ),
                'after_label'   => '<br />',
                'show_debug_info' => false
            )
        );
    }
}
