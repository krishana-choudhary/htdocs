<?php

/**
 * WordPress Social Board 3.3.1
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// Create a custom post type for themes
class SB_Themes extends SB_AdminPageFramework_PostType {
    
    /**
     * This method is called at the end of the constructor.
     * 
     * Use this method to set post type arguments and add custom taxonomies as those need to be done in the front-end as well.
     * Also, to add custom taxonomies, the setUp() method is too late.
     * 
     * ALternatevely, you may use the start_{instantiated class name} method, which also is called at the end of the constructor.
     */
    public function start() {
        
        $this->setPostTypeArgs(
            array(       // argument - for the argument array keys, refer to : http://codex.wordpress.org/Function_Reference/register_taxonomy#Arguments
                'labels' => array(
                    'name'               => 'Theme',
                    'all_items'          => __( 'Manage Themes' ),
                    'singular_name'      => __( 'Theme', 'social-board-admin' ),
                    'add_new'            => __( 'Add New Theme', 'social-board-admin' ),
                    'add_new_item'       => 'Add New Theme',
                    'edit'               => __( 'Edit', 'social-board-admin' ),
              		'edit_item'          => __( 'Edit Theme' ),
                    'new_item'           => __( 'New Theme', 'social-board-admin' ),
                    'view'               => __( 'View', 'social-board-admin' ),
                    'view_item'          => __( 'View Theme', 'social-board-admin' ),
                    'search_items'       => 'Search Themes',
                    'not_found'          => 'No Theme found',
                    'not_found_in_trash' => 'No Theme found in Trash',
                    'plugin_listing_table_title_cell_link' => __( 'Themes', 'social-board-admin' )
                ),
                'public'              => false,
                'rewrite'             => false,
                'menu_position'       => 90,
                'supports'            => array( 'title' ),
                'has_archive'         => false,
                'show_admin_column'   => true,
                'show_ui'             => true,
                'hierarchical'        => false,
                'show_in_nav_menus'   => false,
                'show_in_admin_bar'   => false,
                'show_in_menu'        => 'edit.php?post_type=sb_posts',
                'exclude_from_search' => true
            )
        );
    }
    
    /**
     * Automatically called with the 'wp_loaded' hook.
     */
    public function setUp() {
        $this->oProp->aFooterInfo['sRight'] = '';
        
        if ( $this->oProp->bIsAdmin ) {
            $this->setAutoSave( false );
            $this->setAuthorTableFilter( true );
        }
    }

    /*
     * Built-in callback methods
     */
    public function columns_sb_themes( $aHeaderColumns ) { // columns_{post type slug}
        return array_merge(
            $aHeaderColumns,
            array(
                'cb'                => '<input type="checkbox" />', // Checkbox for bulk actions.
                'title'             => __( 'Name', 'admin-page-framework' ), // Post title. Includes "edit", "quick edit", "trash" and "view" links. If $mode (set from $_REQUEST['mode']) is 'excerpt', a post excerpt is included between the title and links.
                'author'            => __( 'Author', 'admin-page-framework' ), // Post author.
                'date'              => __( 'Date', 'admin-page-framework' ), // The date and publish status of the post.
            )
        );
    }
}
