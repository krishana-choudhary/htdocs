<?php

/**
 * WordPress Social Board 3.3.1
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// create an admin help sidebar
class SB_MetaBox_Social_Boards_Side extends SB_AdminPageFramework_MetaBox {
    public function setUp() {}
    
    public function do_SB_MetaBox_Social_Boards_Side() { // do_{instantiated class name}
        $postid = get_the_ID();
        ?>
        <h4>Add your Social Board to a post or page</h4>
        <p>Add or edit the post or page that you want your Social Board display on. Then, copy and paste the shortcode below into your post or page content.</p>
        <p><strong>Wall Shortcode:</strong><br />
        [social_board id="<?php echo $postid; ?>" type="wall"]<br />
        <strong>Timeline Shortcode:</strong><br />
        [social_board id="<?php echo $postid; ?>" type="timeline"]<br />
        <strong>Rotating Feed Shortcode:</strong><br />
        [social_board id="<?php echo $postid; ?>" type="feed"]<br />
        <strong>Carousel Shortcode:</strong><br />
        [social_board id="<?php echo $postid; ?>" type="feed" carousel="on"]</p>
        
        <h4>Add your Social Board as a widget</h4>
        <p>To add the Social Board as a widget into a widget enabled area, go to: <code>Appearance → Widgets</code> in the WordPress navigation.
        Find the widget with the title <strong>"Social Board"</strong> and simply drag it to your widget enabled area.
        Select one of the Social Boards from the dropdown box, set the other options and click Save.</p>

        <h4>Add your Social Board to a theme file</h4>
        <p>To add this Social Board to one of your theme PHP files, use the PHP snippet below.</p>
        <p style="font-family:Courier New;">&lt;?php if (function_exists('social_board')) echo social_board( array( 'id' =&gt; <?php echo $postid; ?>, 'type' =&gt; '<strong>&lt;Board Type&gt;</strong>' ) ); ?&gt;</p>
        <p>According to your desired board, the <strong>&lt;Board Type&gt;</strong> could be on of the following:<br />
        <code>feed</code> - <strong>Rotating Feed displaying mode</strong><br />
        <code>wall</code> - <strong>Wall displaying mode</strong><br />
        <code>timeline</code> - <strong>Timeline displaying mode</strong>
        </p>
        <?php
    }
}
