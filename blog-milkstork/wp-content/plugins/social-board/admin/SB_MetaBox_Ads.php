<?php

/**
 * WordPress Social Board 3.3.1
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// create manage ads page in admin
class SB_MetaBox_Ads extends SB_AdminPageFramework_MetaBox {

    public function start_SB_MetaBox_Ads() {

        if ( ! class_exists('SB_AceCustomFieldType') )
            include_once( SB_DIRNAME . '/library/admin-page-framework/custom-field-types/ace-custom-field-type/AceCustomFieldType.php');
        $sClassName = get_class( $this );
        new SB_AceCustomFieldType( $sClassName );
        
        add_filter('admin_footer', 'sb_ads_inline_script');
    }
    
    /*
     * ( optional ) Use the setUp() method to define settings of this meta box.
     */
    public function setUp() {
        $this->oProp->bShowDebugInfo = false;
        
        /*
         * ( optional ) Adds setting fields into the meta box.
         */
        $this->addSettingFields(
            array(
                'field_id' => 'board_id',
                'type' => 'select',
                'title' => __( 'Add To Board', 'social-board-admin' ),
                'label' => sb_getPostTitles('sb_posts'),
                'attributes' => array(
                    'select' => array(
                        'style' => "width: 250px;",
                    ),
                ),
                'description' => __( 'Select the board you want to add your ad block to.', 'social-board-admin' ),
            ),
            array(
                'field_id'      => 'ad_position',
                'type'          => 'number',
                'title'         => __( 'Ad Block Position', 'social-board-admin' ),
                'default'       => 0,
                'description'   => __( 'Define the numeric position of where you want to see the ad on the Social Board.', 'social-board-admin' )
            ),
            array(
                'field_id' => 'ad_type',
                'title' => __( 'Type of Ad', 'social-board-admin' ),
                'description' => __( 'Select the type of ad you would like to add as an advertisement block on your Social Board.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'image',
                'label' => array(
                    'image' => 'Image Banner',
                    'text' => 'Simple Text',
                    'code' => 'HTML/JS Code'
                )
            ),
            array(
                'field_id'      => 'ad_image',
                'type'          => 'image',
                'title'         => __( 'Banner Image', 'social-board-admin' ),
                'allow_external_source' => false,
                'attributes'    => array(
                    'preview' => array(
                        'style' => 'max-width: 100px;' // determines the size of the preview image. // margin-left: auto; margin-right: auto; will make the image in the center.
                    )
                ),
                'description'   => __( 'Enter the direct url of the image file or click on the relevant "Select Image" button and an "Upload Image" window should now appear.', 'social-board-admin' )
            ),
            array(
                'field_id'      => 'ad_link',
                'type'          => 'text',
                'title'         => __( 'Banner Link', 'social-board-admin' ),
                'description'   => __( 'Enter the link you would like to be redirected to after clicking the banner image.', 'social-board-admin' ),
                'attributes' => array(
                    'size' => 40
                )
            ),
            array(
                'field_id' => 'ad_link_target',
                'title' => __( 'Link Target', 'social-board-admin' ),
                'description' => __( 'Select the target for the above banner link.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'blank',
                'label' => array(
                    'blank' => 'Blank new window',
                    'self' => 'Self window'
                )
            ),
            array( // Text Area
                'field_id'      => 'ad_custom_code',
                'type'          => 'ace',
                'title'         => __( 'Custom Code', 'social-board-admin' ),
                'description'   => __( 'Any custom HTML/JS code included in this text area will automatically be inserted into the ad block.', 'social-board-admin' ),
                'attributes'    => array(
                    'rows' => 6,
                    'cols' => 60
                ),
                'options'   => array(
                    'language' => 'javascript',
                    'theme'    => 'chrome',
                    'gutter'   => false,
                    'readonly' => false,
                    'fontsize' => 12
                )
            ),
            array( // Text Area
                'field_id'      => 'ad_text',
                'type'          => 'textarea',
                'title'         => __( 'Text', 'social-board-admin' ),
                'description'   => __( 'Any text interted in this text area will automatically be added into the ad block.', 'social-board-admin' ),
                'attributes'    => array(
                    'rows' => 6,
                    'cols' => 60
                )
            ),
            array(
                'field_id' => 'ad_grid_size',
                'title' => __( 'Ad Grid Size', 'social-board-admin' ),
                'description' => __( 'Defines the number of columns to fill for this ad block - Works for wall layout only!', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'solo',
                'label' => array(
                    'solo' => 'Solo',
                    'twofold' => 'Two fold',
                    'threefold' => 'Three fold'
                )
            ),
            array(
                'field_id' => 'ad_text_align',
                'title' => __( 'Text Align', 'social-board-admin' ),
                'description' => __( 'Select the alignment for text/images in your ad block.', 'social-board-admin' ),
                'type' => 'radio',
                'default' => 'left',
                'label' => array(
                    'left' => 'Left',
                    'center' => 'Center',
                    'right' => 'Right'
                )
            ),
            array(
                'field_id'      => 'ad_height',
                'type'          => 'number',
                'title'         => __( 'Block Height', 'social-board-admin' ),
                'default'       => '',
                'description'   => __( 'Define a height for the ad block - Leave empty for auto height.', 'social-board-admin' )
            ),
            array (
                'field_id'      => 'ad_background_color',
                'type'          => 'color',
                'title'         => __( 'Block Background Color', 'social-board-admin' ),
                'default'       => '',
                'description'   => __( 'Select a background color for the ad block if you require and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' )
            ),
            array(
                'field_id'      => 'ad_border_size',
                'type'          => 'number',
                'title'         => __( 'Block Border Size', 'social-board-admin' ),
                'default'       => 1,
                'description'   => __( 'Define a border size for the ad block.', 'social-board-admin' )
            ),
            array (
                'field_id'      => 'ad_border_color',
                'type'          => 'color',
                'title'         => __( 'Block Border Color', 'social-board-admin' ),
                'default'       => '',
                'description'   => __( 'Select a border color for the ad block if you require and then click elsewhere on the screen to close the colorpicker widget.', 'social-board-admin' )
            )
        );
    }
}

function sb_ads_inline_script() {
    if ( wp_script_is( 'jquery', 'done' ) ) {
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        // set ad type
        var adform = $( '#sb_ads_metabox' );
        adform.on('change', 'input[type=radio][name="ad_type"]', function() {
            sb_adtype_update( $(this).val(), adform);
        });
        
        // set default
        <?php
        $def_adform = "sb_adtype_update( 'image', adform);";
        if ( isset($_GET['post']) ) {
            if ( $ad = sb_options($_GET['post']) ) {
                $def_adform = "sb_adtype_update( '$ad[ad_type]', adform);";
            }
        }
        echo $def_adform;
        ?>
    });
    
    // set ad type function
    function sb_adtype_update(type, adform) {
            if ( type == 'code' ) {
                adform.find('input[name$="ad_image"]').closest("tr").fadeOut('fast');
                adform.find('input[name$="ad_link"]').closest("tr").fadeOut('fast');
                adform.find('textarea[name$="ad_text"]').closest("tr").fadeOut('fast');
                adform.find('textarea[name$="ad_custom_code"]').closest("tr").fadeIn('fast');
            }
            else if ( type == 'text' ) {
                adform.find('input[name$="ad_image"]').closest("tr").fadeOut('fast');
                adform.find('input[name$="ad_link"]').closest("tr").fadeOut('fast');
                adform.find('textarea[name$="ad_custom_code"]').closest("tr").fadeOut('fast');
                adform.find('textarea[name$="ad_text"]').closest("tr").fadeIn('fast');
            } else {
                adform.find('textarea[name$="ad_custom_code"]').closest("tr").fadeOut('fast');
                adform.find('textarea[name$="ad_text"]').closest("tr").fadeOut('fast');
                adform.find('input[name$="ad_image"]').closest("tr").fadeIn('fast');
                adform.find('input[name$="ad_link"]').closest("tr").fadeIn('fast');
            }
    }
    </script>
    <?php
    }
}
