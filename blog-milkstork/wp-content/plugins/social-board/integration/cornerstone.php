<?php

/**
 * WordPress Social Board 3.3.0
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

class CS_Social_Board extends Cornerstone_Element_Base {

  public function data() {
    return array(
      'name'        => 'social-board',
      'title'       => __( 'Social Board', 'social-board-admin' ),
      'section'     => 'social',
      'description' => __( 'This will add Social Board into your Cornerstone created section.', 'social-board-admin' ),
      'empty'       => array( 'sb_id' => 'none' )
    );
  }

    public function controls() {

        $items = array();
        $choices = array();
        
        if ( $this->is_active() ) {
            $items = sb_getPostTitles('sb_posts');
        }
        
        foreach ($items as $itemKey => $itemValue) {
            $choices[] = array( 'value' => $itemKey,  'label' => $itemValue );
        }
        
        if ( empty( $choices ) ) {
            $choices[] = array( 'value' => 'none', 'label' => __( 'No Social Board items available.', 'social-board-admin' ), 'disabled' => true );
        }
        
        $this->addControl(
          'sb_id',
          'select',
          __( 'Select Social Board', 'social-board-admin' ),
          __( 'Select a previously created Social Board.', 'social-board-admin' ),
          $choices[0]['value'],
          array( 'choices' => $choices )
        );
        
        $this->addControl(
          'sb_type',
          'choose',
          __( 'Display Mode', 'social-board-admin' ),
          __( 'There are multiple display modes for different situations. Select the one that best suits your needs.', 'social-board-admin' ),
          'wall',
          array(
            'columns' => '2',
            'choices' => array(
              array( 'value' => 'wall',     'label' => __( 'Wall', 'social-board-admin' ),          'icon' => fa_entity( 'th' ) ),
              array( 'value' => 'timeline', 'label' => __( 'Timeline', 'social-board-admin' ),      'icon' => fa_entity( 'list-ul' ) ),
              array( 'value' => 'feed',     'label' => __( 'Rotating Feed', 'social-board-admin' ), 'icon' => fa_entity( 'bars' ) ),
              array( 'value' => 'carousel', 'label' => __( 'Carousel', 'social-board-admin' ),      'icon' => fa_entity( 'columns' ) )
            )
          )
        );
    }

    public function is_active() {
        return class_exists( 'SocialBoard' );
    }

    public function render( $atts ) {
    
        extract( $atts );
        $shortcode = '';
        $carousel = '';
        
        // Hookup the shortcode
        if ( $sb_type == 'carousel' ) {
            $carousel = ' carousel="on"';
            $sb_type = 'feed';
        }
        $shortcode = sprintf( '[social_board id="%1$d" type="%2$s"%3$s]', $sb_id, $sb_type, $carousel );
        
        return $shortcode;
    }
}

// add to elements
add_action( 'cornerstone_register_elements', 'cs_register_elements' );
function cs_register_elements() {
    cornerstone_add_element( 'CS_Social_Board' );
}
