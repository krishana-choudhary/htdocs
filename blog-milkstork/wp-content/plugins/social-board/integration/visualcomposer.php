<?php

/**
 * WordPress Social Board 3.3.0
 * Copyright 2014 Axent Media (axentmedia@gmail.com)
 */

// don't load directly
if ( ! defined('ABSPATH') ) die('-1');

class SB_VCExtendAddon {
    function __construct() {
        // We safely integrate with VC with this hook
        add_action( 'vc_before_init', array( $this, 'integrateWithVC' ) );
    }

    public function integrateWithVC() {

        $items = array();
        $choices = array();
        
        // get sb items
        $items = sb_getPostTitles('sb_posts');
        
        foreach ($items as $itemKey => $itemValue) {
            $choices[$itemValue] = $itemKey;
        }
        
        if ( empty( $choices ) ) {
            $choices[] = array( __( 'No Social Board items available.', 'social-board-admin' ) => '' );
        }
        
        // Lets call vc_map function to "register" our custom shortcode within Visual Composer interface.
        vc_map( array(
            "name" => __("Social Board", 'social-board-admin'),
            "description" => __("Adds a Social Board into your page", 'social-board-admin'),
            "base" => "social_board",
            "class" => "",
            "controls" => "full",
            "icon" => "sb-icon32",
            "category" => __('Social', 'social-board-admin'),
            "params" => array(
                array(
                  "type" => "dropdown",
                  "heading" => __('Social Board', 'social-board-admin'),
                  "param_name" => "id",
                  "value" => $choices,
                  "admin_label" => true,
                  "description" => __("Select a previously created Social Board.", "social-board-admin")
                ),
                array(
                  "type" => "dropdown",
                  "heading" => __('Display Mode', 'social-board-admin'),
                  "param_name" => "type",
                  "value" => array(
                    __("Wall", 'social-board-admin') => 'wall',
                    __("Timeline", 'social-board-admin') => 'timeline',
                    __('Rotating Feed', 'social-board-admin') => 'feed',
                    __('Carousel', 'social-board-admin') => 'carousel'
                  ),
                  "description" => __("There are multiple display modes for different situations. Select the one that best suits your needs.", "social-board-admin")
                )
            )
        ) );
    }
}

// Finally initialize code
new SB_VCExtendAddon();
