<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

define('WP_ALLOW_REPAIR', true);
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog_milkstork');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|*W VM~l0ns2:Lq%X}((hSkeG~C&Qa!ZLr%Ru`_<b)Bdx/I:MLetv9#1XRhx9Zw~');
define('SECURE_AUTH_KEY',  '/*a(GB}u>6rguF[`;X(zee ZaSJ:P:Ct<jjnJ]q:XKkz3[8C~E/HQdK]l)_^gEwc');
define('LOGGED_IN_KEY',    'akL34MK>kaX%<?%@@yJjf6(PB5M[{Vs686rFwHl7.sctWa|JGOo#<48V(I:]N^nT');
define('NONCE_KEY',        'cDe<JFK:%~o,kuD-G0j>`.M{Ep#snN);a0>n?Zg/z9z<5bfusl:;1FCd?jfsVRB$');
define('AUTH_SALT',        'A}-pdjn{b3gu-<q0DU| Tdz/QrICl]R3DY,#u*x/eeU%oHx8m9nM6BgxGjgZ*o<X');
define('SECURE_AUTH_SALT', 'Q,(I&eBz&mmy[u!{PqU;>w!H%;|1F)P%oi=R|5H`=]2[9*7_eFj7yX$EN}eh1QGE');
define('LOGGED_IN_SALT',   '!qV=(@<(LRZo7E%R3}VSAV+^$}t;H^KB+0U`sfppQ/RH`lr$Zqy~QZ)`!I9)xnN?');
define('NONCE_SALT',       'WP_Z9An2 &sh8:iS?av$N~vk{>9Z+hJRDYIL.;3`R.sVz).dzNL!N,-[K01U*KON');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
