<?php
  	/**
  	 * router
  	 */
  	class Router
  	{

  		function __construct()
  		{
				//$this->load_main();
  		}
			public function router_cb()
			{
				$this->include_header();
				$this->include_footer();
				$this->load_content();
				$this->load_direction();
			}

			public function include_header()
			{
				load_template('header.php');
				// include(FIELDir.'views/header.php');
			}
			public function include_footer()
			{
				load_template('footer.php');
				// include(FIELDir.'views/footer.php');
			}
			public function load_content()
			{
				load_template('login.php');
			}

      public function load_direction() {

          // Check if user is logged in
          if( !is_user_logged_in() ) {
            load_template('login.php');
            return; }

          load_template('main.php');

      }
  	}

 ?>
