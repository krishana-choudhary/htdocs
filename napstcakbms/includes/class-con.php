<?php
 /**
  * connection
  */
 class dbcon
 {

 	function __construct()
 	{
		//$this->db_con();
 	}
	public function db()
	{
		$this->db_con();
		$this->shutdown();
	}
	function db_con()
	{
		$servername = "localhost";
		$username = "root";
		$password = "root";
		$db       = "nap_bms";

try {
		global $conn;

    $conn = new PDO("mysql:host=$servername;dbname=$db",$username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected successfully";
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
	}
  public function shutdown() {

  global $conn;

  $pdo = null;
}

 }


 ?>
