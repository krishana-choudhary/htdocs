<?php

class Users {

    public function __construct() {

      $this->update_activity();
    }

    public function update_activity() {

        if( !isset( $session['BMS']['last_activity'] ) ) return false;

        $session['BMS']['last_activity'] = time();
    }

    public static function is_user_logged_in() {

        $session = $_SESSION;

        if( !isset( $session['BMS']['user'] ) ) return false;
        if( !isset( $session['BMS']['last_activity'] ) ) return false;

        $timout = 5;

        // Logout if idle time exceeds
        if( ($session['BMS']['last_activity'] + $timout) < time() ) {

            session_destroy();
            return false;
        }

        return true;
    }
}
