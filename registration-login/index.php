<?php
require_once "data-insert-session.php";

if (empty($_SESSION['fname'])) {
header('location:login.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/style.css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
    <?php if (isset($_SESSION['pass'])) { ?>
      <h3>
        <?php
        echo $_SESSION['pass'];
        unset($_SESSION['pass']);
        ?>
      </h3>
  <?php } ?>
	  <div class="jumbotron">
    <?php
    if (isset($_SESSION['fname'])) {
     ?>
	    <h3>Welcome <?php echo $_SESSION['fname']; ?> to home page</h3>
	    <p>Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile-first projects on the web.</p>
      <p><a href="index.php?logout='1'"> Logout </a></p>
      <?php } ?>
	  </div>
	</div>
</body>
<?php
 ?>
</html>
