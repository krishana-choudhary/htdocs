<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'easy_testimonials');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3Vfnz%p^/x;t|t)T@Hb ;=~8Z);eUl?->Fi ^ZT2>CDSjI8gxq*2p^=S9Oce7[)|');
define('SECURE_AUTH_KEY',  'O_XsJ}}^`hjWt,7SG`&J1ilOHAhgt@d+Fdc(k92uS`@Vk<ve%[#%BlWXCWb#*3VH');
define('LOGGED_IN_KEY',    'GuIsJT~Gcm]!!8Az)25oWWMRf$`4PY(QdJ<c!mPMZK#N4-ol-0BXq;Wxu6o]jwM1');
define('NONCE_KEY',        ':5B:=@!d;eK!}g]KtVoOTP)$7l%Nsq -GbQ^rTaFHajKP_c?@v=H<U$[U&mCLCU;');
define('AUTH_SALT',        'zqesW3{q%}2Kc0<Y5q*l@*}3;BE>[a#ZJkf{S.!K(QrFU|PW5<pw7$]PGR|;vm28');
define('SECURE_AUTH_SALT', 'M)wTxqAP8{nReF%>$T]4Ss?jX{DH tLRlslQCyw.^d4uNByt7t;py,7.n+[k20~c');
define('LOGGED_IN_SALT',   'Q6Z|^1UJ7@NF.I$G=g+foMY,a!b.a2$]U^~7BQ`]8)UcA[W<=t{c.fOxd|=(4$#0');
define('NONCE_SALT',       ':$v7+g:Ct~R1Zv_yGJ,I*W1$=MyBLX8?~FvkC3V7xOf^tr}EUBEVh&SkybCoU{<Y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
