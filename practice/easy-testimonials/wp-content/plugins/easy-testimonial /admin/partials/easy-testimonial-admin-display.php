<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       www ?.com
 * @since      1.0.0
 *
 * @package    Easy_Testimonial
 * @subpackage Easy_Testimonial/admin/partials
 */

 /**
  * Register a book post type.
  *
  * @link http://codex.wordpress.org/Function_Reference/register_post_type
  */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
