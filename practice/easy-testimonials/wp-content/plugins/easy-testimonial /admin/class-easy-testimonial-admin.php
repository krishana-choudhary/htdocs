<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       www ?.com
 * @since      1.0.0
 *
 * @package    Easy_Testimonial
 * @subpackage Easy_Testimonial/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Easy_Testimonial
 * @subpackage Easy_Testimonial/admin
 * @author     Krishana <Krishana@napstack.com>
 */
class Easy_Testimonial_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Easy_Testimonial_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Easy_Testimonial_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/easy-testimonial-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Easy_Testimonial_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Easy_Testimonial_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/easy-testimonial-admin.js', array( 'jquery' ), $this->version, false );

	}
/*this function for menu page */
	//  public function display_admin_page(){
	//  	 add_menu_page(
	//  		'easy testimonial Demo',//$page_title
	//  		'Easy testimonial',//$menu_title
	//  		'manage_options',//$capability
	//  		'easy-testimonial-admin',//$menu_slug
	//  		array($this,'showpage'),//$function
	//  		'',//$icon url
	//  		'6.0'//$position menu frome top
	//  	 );
	//  }
	//   public function showpage (){
	//   	// echo 'pass';
	//   	 include WPPB_PATH . '/admin/partials/easy_testimonial-admin-display.php';
	//   }

	}
	/**
	 * Register a book post type.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/register_post_type
	 */
	 function register_easy_testimonials() {
		$labels = array(
			'name'               => __( 'Easy Testimonials', 'easy-testimonial' ),
			'singular_name'      => __( 'Easy Testimonial', 'easy-testimonial' ),
			'menu_name'          => __( 'Easy Testimonils','easy-testimonial' ),
			'name_admin_bar'     => __( 'Easy Testimonial','easy-testimonial' ),
			'add_new'            => __( 'Add New', 'Easy Testimonial', 'easy-testimonial' ),
			'add_new_item'       => __( 'Add New Easy Testimonial', 'easy-testimonial' ),
			'new_item'           => __( 'New Easy Testimonial', 'easy-testimonial' ),
			'edit_item'          => __( 'Edit Easy Testimonial', 'easy-testimonial' ),
			'view_item'          => __( 'View Easy Testimonial', 'easy-testimonial' ),
			'all_items'          => __( 'All Easy Testimonials', 'easy-testimonial' ),
			'search_items'       => __( 'Search Easy Testimonials', 'easy-testimonial' ),
			'parent_item_colon'  => __( 'Parent Easy Testimonials:', 'easy-testimonial' ),
			'not_found'          => __( 'No easy testimonials found.', 'easy-testimonial' ),
			'not_found_in_trash' => __( 'No easy testimonials found in Trash.', 'easy-testimonial' )
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Description.', 'your-plugin-textdomain' ),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'easy-testimonials' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor','author', )
		);

		register_post_type( 'easy_testimonial', $args );
	}
		add_action( 'init', 'register_easy_testimonials' );
