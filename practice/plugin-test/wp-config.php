<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'plugin_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7Yy30#x6nVguq1K``|ctb&~li17<z2y}b.azvgh;aawlKQ[yn/kKUR3*,}PYz:fm');
define('SECURE_AUTH_KEY',  ' HhNn#q_g#Z&f#/]-nwYAj,Uw087 ED38<rQI6>a3GK2rRe]|wQ2?k%4[c;X_bC:');
define('LOGGED_IN_KEY',    'oE#{~0Uo;8qcgYx_+:,8zyl(6G~93)Cm6Sv)HaV6&ZxN5[vmNHi;[?V $?Ft?d<|');
define('NONCE_KEY',        'erEu8>iDYvb96,$r`uGqhhO5A=dByQPIi]75OhdT)k-)7Ko4~RbrhKBZcvvs}RKh');
define('AUTH_SALT',        'aM@@=FG*Cf;^}%WQ-M(x6UV#b+(+mr;8ax%h=Rcrc?}E+DQyf~bMn$17,IXj[x*m');
define('SECURE_AUTH_SALT', 'Cv.jzH{uP/-Y&__Lbw5o)+qI+Y{+{hk+|pj;c8Ka;zVSn7J+*/1j bp@@f<bk~$A');
define('LOGGED_IN_SALT',   'nz)cj:Q6__<HDm^*m|e@%]/f/uro>9k/aB8Yu~rs8LchOiW84/Q|HDQO2U?5Etlz');
define('NONCE_SALT',       '5lw<6z*e[a^$=uQAL5! VsCH+UQAm%XL/)CdpzfE0eyM%lCmmx2gvGr{<$u4[=uZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
