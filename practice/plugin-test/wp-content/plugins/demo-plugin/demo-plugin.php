<?php
/*
Plugin Name:DEMO Plugin
Plugin URI:www.napstack.com
Description:Just For Demo
Author:Napstack
Author URI:www.napstack.com
Version:1.0
 */
/**
 * Capital Word
 * @param  String -  $title Capital alpha.
 * @return String - title
 */
function capital_word($title){
return ucwords($title);
}
 add_filter( 'the_title', 'capital_word');


/**
 * Say Hello
 * @param  String $content Thankyou Message
 * @return String  Content - Say thanks
 */
 function say_hello($content){
   $content.='Thankyou For Reading';
   return($content);
 }
add_filter('the_content','say_hello');

?>
