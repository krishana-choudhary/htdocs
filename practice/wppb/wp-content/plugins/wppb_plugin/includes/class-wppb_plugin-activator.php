<?php

/**
 * Fired during plugin activation
 *
 * @link       krishana@napstack.com
 * @since      1.0.0
 *
 * @package    Wppb_plugin
 * @subpackage Wppb_plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wppb_plugin
 * @subpackage Wppb_plugin/includes
 * @author     krishana <krishana@napstack.com>
 */
class Wppb_plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
