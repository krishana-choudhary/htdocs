<?php

/**
 * Fired during plugin deactivation
 *
 * @link       krishana@napstack.com
 * @since      1.0.0
 *
 * @package    Wppb_plugin
 * @subpackage Wppb_plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wppb_plugin
 * @subpackage Wppb_plugin/includes
 * @author     krishana <krishana@napstack.com>
 */
class Wppb_plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
