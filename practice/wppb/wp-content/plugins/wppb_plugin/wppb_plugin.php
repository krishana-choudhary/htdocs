<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              krishana@napstack.com
 * @since             1.0.0
 * @package           Wppb_plugin
 *
 * @wordpress-plugin
 * Plugin Name:       wp_plugin
 * Plugin URI:        krishana@napstack.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            krishana
 * Author URI:        krishana@napstack.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wppb_plugin
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( PLUGIN_VERSION, '1.0.0' );

 /* custome Path create by krishana */
define( WPPB_PATH, plugin_dir_path( __FILE__ ) );
define( WPPB_URL, plugin_dir_url( __FILE__ ) );
// var_dump(WPPB_PATH, WPPB_URL); die;
/* end */

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wppb_plugin-activator.php
 */
function activate_wppb_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wppb_plugin-activator.php';
	Wppb_plugin_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wppb_plugin-deactivator.php
 */
function deactivate_wppb_plugin() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wppb_plugin-deactivator.php';
	Wppb_plugin_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wppb_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_wppb_plugin' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wppb_plugin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wppb_plugin() {

	$plugin = new Wppb_plugin();
	$plugin->run();

}
run_wppb_plugin();
