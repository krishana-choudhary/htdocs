<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       krishana@napstack.com
 * @since      1.0.0
 *
 * @package    Wppb_plugin
 * @subpackage Wppb_plugin/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
