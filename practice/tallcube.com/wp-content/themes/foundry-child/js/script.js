'use strict';

(function($) {

	jQuery('.cube-carousel').owlCarousel({
		nav: true,
		navText: ["<i class='ti-angle-left'>","<i class='ti-angle-right'>"],
		dots: false,
		// center: true,
		loop:true,
		items: 14,
		margin: 15,
		responsive : {
		    // breakpoint from 0 up
		    0 : {
		        items: 2,
		    },
				480:{
					items:2,

				},
		    // breakpoint from 992 up
		    992 : {
		        items: 3,
		    }
		}
	});

})(jQuery);


/*'use strict';

(function($) {

	jQuery('.cube-testimonials').owlCarousel({
		nav: true,
		navText: ["<i class='ti-angle-left'>","<i class='ti-angle-right'>"],
		dots: false,
		// center: true,
		loop:true,
		items: 3,
		margin: 15,
		responsive : {
		    // breakpoint from 0 up
		    0 : {
		        items: 2,
		    },
				480:{
					items:2,

				},
		    // breakpoint from 992 up
		    992 : {
		        items: 3,
		    }
		}
	});

})(jQuery);*/
