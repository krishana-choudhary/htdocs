<?php

/**
 * Enqueue Child Theme Script
 * @return Null
 */
function cube_enqueue_scripts() {
	 $handle    = 'cube-script';
	 $src       = get_stylesheet_directory_uri() . '/js/script.js';
	 $deps      = array();
	 $ver       = wp_get_theme()->Version;
	 $in_footer = true;

	 wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );

}
add_action( 'wp_enqueue_scripts', 'cube_enqueue_scripts', 999 );


/**
 * Callback function for cube_carousel shortcode
 * @param  array $atts    array of params passed through shortcode
 * @param  string $content Contect between the shortcode tag. eg: [cube_carousel]Content goes here...[/cube_carousel]
 * @return string          shortcode generated HTML
 */
function cube_carousel_cb($atts = [], $content = '') {

	$atts = shortcode_atts( array(
		'post_type' => 'portfolio',
		'posts_per_page' => 10,
		'rtl' => 'false',
		'title' => ''
	), $atts, 'cube_carousel' );

	$args = [
		'post_type' => $atts['post_type'],
		'posts_per_page' => $atts['post_per_page'],
	];

	$query = new WP_Query($args);

	ob_start();

	$wrapper_classes = 'cube-carousel-wrap';

	if( $atts['rtl'] == 'true') $wrapper_classes .= ' slider-rtl';

	?>

	<div class="<?php echo $wrapper_classes; ?>">
	<?php if( !empty( $atts['title'] ) ) : ?>
		<h3 class="cube-carousel-title"><?php echo $atts['title']; ?></h3>
	<?php endif; ?>
	<?php if( $query->have_posts() ) : ?>
		<div class="cube-carousel">
		<?php while( $query->have_posts() ) : $query->the_post(); ?>
			<?php $thumbnail = get_the_post_thumbnail_url(); ?>
			<div class="cube-item" style="background-image:url(<?php echo $thumbnail; ?>)">
				<div class="cube-item-content">
					<h3 class="item-title"><?php the_title();?></h3>
					<?php echo get_the_term_list( $post->ID, 'portfolio_category', '<ul class="item-cats"><li>', ' <span class="seperator">|</span> </li><li>', '</li></ul>' ); ?>
					<a href="<?php the_permalink(); ?>" class="item-permalink btn btn-sm btn-port">View More</a>
				</div><!-- END .cube-item-content -->
				<h4 class="item-outer-title"><?php the_title(); ?></h4>
			</div><!-- END .cube-item -->
		<?php endwhile; ?>
		</div><!-- END .cube-carousel -->
	<?php endif;  ?>
	</div><!-- END .cube-carousel-wrap -->

	<?php

	return ob_get_clean();

}

add_shortcode('cube_carousel', 'cube_carousel_cb');



/**
* Register a Case Studies
*
* @link http://codex.wordpress.org/Function_Reference/register_post_type
*/
function register_case_studies() {
	$labels = array(
		'name'               => __( 'Case Studies', 'your-plugin-textdomain' ),
		'singular_name'      => __( 'Case Study',  'your-plugin-textdomain' ),
		'menu_name'          => __( 'Case Studies', 'your-plugin-textdomain' ),
		'name_admin_bar'     => __( 'Case Studies',  'your-plugin-textdomain' ),
		'add_new'            => __( 'Add New', 'Case Study',  'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Case Study', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Case Study', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Case Study', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Case Study', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Case Studies', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Case Studies', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Case Studies:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No case studies found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No case studies found in Trash.', 'your-plugin-textdomain' )
	);
	$args = array(
		'labels'             => $labels,
    'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'case-study' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
		'menu_icon'          => 'dashicons-book',
	);

	register_post_type( 'case_study', $args );
}
add_action( 'init', 'register_case_studies' );
