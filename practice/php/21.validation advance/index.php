<?php

$errors  = array();

if (isset($_POST['submit'])) {

  $user         = $_POST['name'];
  $pass         = $_POST['password'];
  $password_len = strlen($pass);
  $email        = $_POST['email'];
  $phone        = $_POST['phone'];

  if(empty($user)) {
    $errors[] = "enter your name";
  }

  if(empty($pass)) {
    $errors[] = "enter your password";
  }

  if($password_len<=6) {
    $errors[] = "password should be more than six character";
  }

  if(empty($email)) {
    $errors[] = "enter your mail";
  }

  if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
     $errors[] = "enter valid email";
  }

  if(empty($phone)) {
    $errors[] = "enter your phone";
  }
  //
  // if( empty( $errors ) ) {
  //   $errors[] = "msg has been send";
  // }
}

mysqli_connect('localhost','admin','password','students');
$conn = new mysqli('localhost','admin','password','students');
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO students_record (name,email,address,joining_date)
VALUES ('name', 'password', 'email','phone')";

if ($conn->query($sql) === TRUE) {
    echo "Enter your detail";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>form validation</title>
    <style>
    input{
      padding: 5px;
    }
    form{
      padding: 30px;
    }
    </style>
  </head>
  <body>
<form action=""method="post">
  <p>user:</br>
    <input type="name" name="name"value=""/></p>
  <p>pass:</br>
  <input type="password"name="password"value=""/></p>
  <p>email:</br>
  <input type="text"name="email"value=""/></p>
  <p>phone:</br>
    <input type="text"name="phone"value=""/></p>
  <p></br>
    <input type="submit"name="submit"value="sign up"/></p>

  <?php if (!empty($errors)) : ?>
    <ul>
      <?php foreach( $errors as $error ) : ?>
        <li><?php echo $error; ?></li>
      <?php endforeach;  ?>
    </ul>
  <?php endif; ?>

  <?php if( !empty($_POST) && empty($errors)) : ?>
    <p>msg has been send</p>
  <?php endif; ?>

</form>
  </body>
</html>
