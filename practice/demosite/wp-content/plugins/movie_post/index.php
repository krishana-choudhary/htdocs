<?php
/*
plugin Name:Movies post type
plugin URI:http://www.?.com
description:just for demo
version:1.0
Author:Kishana
Author URI:http://www.?.com
*/
class JW_Movies_Post_Type{
  public function __construct()
  {
   $this->register_post_type();
   $this->taxonomies();
   $this->metaboxes();
  }
  public function register_post_type()
  {

    $args = array(
      'labels'=> array(
        'name'=>'Movies',
        'singular_name'=>'Movie',
        'add_new'=>'Add New Movie',
        'add_new_item'=>'Add New Movie',
        'edit_item'=>'Edit Item',
        'new_item'=>'Add New Item',
        'view_item'=>'View Movie',
        'search_items'=>'Search Movies',
        'not_found'=>'No Movies Found',
        'not_found_in_trash'=>'No Movies Found in Trash '
      ),
        'query_var' => 'movies',
        'rewrite' => array(
        'slug' => 'movies/',
      ),
      'public'=>true,
      //'menu_position'=>5
      'menu_icon'=> admin_url().'images/media-button-video.gif',
      'supports' => array(
        'title',
        'thumbnail',
        'excerpt',
        'custom-fields',


      )
    );
    register_post_type('jw_movie',$args);
  }
  public function taxonomies()
  {
  $taxonomies = array();
  $taxonomies['genre']=array(
   'hierarchical' => true,
   'query var' => 'movie_genre',
   'rewrite' => array(
     'slug' => 'movies/genre'
   ),
   'labels'=> array(
     'name'=>'Genre',
     'singular_name'=>'Genre',
     'edit_item'=>'Edit Item',
     'update_item'=>'Update Genre',
     'add_new_item'=>'New Genre',
     'new_item_name'=>'Add New Genre',
     'all_items'=>'View Movie',
     'search_items'=>'Search Genres',
     'popular_items'=>'popular Genres',
     'separate_items_with_comment'=>'Separate genres with comments',
     'add_or_remove_items'=>'Add or remove Genres',
     'choose_from_most_used'=>'choose from most genres',
   )
 );

 $taxonomies['studios']=array(
  'hierarchical' => true,
  'query var' => 'movie_studios',
  'rewrite' => array(
    'slug' => 'movies/studios'
  ),
  'labels'=> array(
    'name'=>'studios',
    'singular_name'=>'studios',
    'edit_item'=>'Edit Item',
    'update_item'=>'Update studios',
    'add_new_item'=>'New studios',
    'new_item_name'=>'Add New studios',
    'all_items'=>'View Movie',
    'search_items'=>'Search studios',
    'popular_items'=>'popular studios',
    'separate_items_with_comment'=>'Separate genres with comments',
    'add_or_remove_items'=>'Add or remove studios',
    'choose_from_most_used'=>'choose from most studios',
  )
);

  $this->register_all_taxonomies($taxonomies);
  }
  public function register_all_taxonomies($taxonomies)
  {
    foreach ($taxonomies as $name => $arr) {
      register_taxonomy($name,array('jw_movie'),$arr);
    }
  }
  public function metaboxes()
  {
    add_action('add_meta_boxes',function(){
add_meta_box('jw_movie_length','Movie Length','movie_length','jw_movie');
    });
    function movie_length($post)
    {
    $length = get_post_meta($post->ID,'jw_movie_length',true);
  ?>
<p>
<label for="jw_movie_length">Length:</label>
<input type="text" class="widefat" name="jw_movie_length" id="jw_movie_length" value="<?php echo esc_attr($length);?>"/>
</p>
  <?php
    }
    add_action('save_post',function($id){
      if (isset($_POST['jw_movie_length'])) {
        update_post_meta(
          $id,
          'jw_movie_length',
          strip_tags($_POST['jw_movie_length'])

        );
      }
    });
  }

}
add_action('init',function(){
  new JW_Movies_Post_Type();
  include dirname(__FILE__).'/movies_shortcodes.php';
});
?>
