<?php
  /*
  plugin Name:jw options
  plugin URI:http://www.?.com
  description:just for demo
  version:1.0
  Author:krishana
  Author URI:http://www.?.com
*/
class jw_options{
  public $option;
   public function __construct()
   {
    //  delete_option('jw_plugin_options');
     $this->options = get_option('jw_plugin_options');
     $this->register_settings_and_fields();
   }
   public function add_menu_page()
    {
      add_options_page('Theme_options','Theme_options','administrator',_FILE_, array('jw_options','display_options_page'));
   }
   public function display_options_page()
   {
     ?>
   <div class="wrap">
    <?php get_screen_icon()?>
   <h2>Theme options</h2>

   <form method="post" action="options.php" enctype="multipart/form-data">
<?php settings_fields('jw_plugin_options');?>
<?php do_settings_sections(_FILE_); ?>
<p class="submit">
  <input name="submit"type="submit"class="button-primary"value="save changes">
</p>
   </form>
   </div>
     <?php
   }

 public function register_settings_and_fields()
 {
 register_setting('jw_plugin_options','jw_plugin_options',array($this,'jw_validate_settings'));
 add_settings_section('jw_main_section','Main settings',array($this,'jw_main_section_cb'),_FILE_);
 add_settings_field('jw_banner_heading','Banner Heding:', array($this,'jw_banner_heading_setting'),_FILE_,'jw_main_section');
 add_settings_field('jw_logo','Your logo:', array($this,'jw_logo_setting'),_FILE_,'jw_main_section');
 add_settings_field('jw_color_scheme','Your Desired Color Scheme:', array($this,'jw_color_scheme_setting'),_FILE_,'jw_main_section');


 }
   public function jw_main_section_cb()
   {

   }
   public function jw_validate_settings($plugin_options)
   {
     if (!empty($_FILES['jw_logo_upload']['tmp_name'])) {
       $override = array('test_form' => false);
       $file = wp_handle_upload($_FILES['jw_logo_upload'],$override);
       $plugin_options['jw_logo'] = $file['url'];
     }else {
       $plugin_options['jw_logo']= $this->options['jw_logo'];
     }
     return $plugin_options;
   }

 /*
 *
 *input
 */
 public function jw_banner_heading_setting()
 {
echo "<input name='jw_plugin_options[jw_banner_heading]' type='text' value='{$this->options['jw_banner_heading']}'/>";

 }
 public function jw_logo_setting()
 {
echo '<input type="file" name="jw_logo_upload" /><br /><br />';
if (isset($this->options['jw_logo']) ) {
  echo "<img sr='{$this->options['jw_logo']}' alt=''/>";
}
 }
 public function jw_color_scheme_setting()
 {
   $items = array('red','green','blue','yellow','black','white');
   echo "<select name = 'jw_plugin_options[jw_color_scheme]'>";
   foreach ($items as $item) {
     $selected = ($this->options['jw_color_scheme']=== $item) ?'selected="selected"':'';
     echo "<option value='$item' $selected>$item</option>";
   }
   echo "</select>";
 }
   }
add_action('admin_menu', function(){
  jw_options::add_menu_page();
});
add_action('admin_init',function(){
  new jw_options();
})
 ?>
