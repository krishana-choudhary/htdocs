<?php

/**
 * Fired during plugin activation
 *
 * @link       krishan@napstack.com
 * @since      1.0.0
 *
 * @package    Keyword_Search
 * @subpackage Keyword_Search/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Keyword_Search
 * @subpackage Keyword_Search/includes
 * @author     krishana <krishan@napstack.com>
 */
class Keyword_Search_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
