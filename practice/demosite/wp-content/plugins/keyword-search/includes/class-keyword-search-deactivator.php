<?php

/**
 * Fired during plugin deactivation
 *
 * @link       krishan@napstack.com
 * @since      1.0.0
 *
 * @package    Keyword_Search
 * @subpackage Keyword_Search/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Keyword_Search
 * @subpackage Keyword_Search/includes
 * @author     krishana <krishan@napstack.com>
 */
class Keyword_Search_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
