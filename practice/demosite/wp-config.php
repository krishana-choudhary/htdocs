<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'demosite');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Y{S:9-*1$LfmDhSU_kvuZN}^t@V+L|8zArbRG 7)so^=OMTlAu4>O~k-q{`.}?^/');
define('SECURE_AUTH_KEY',  'F]97{{oSLX[@7teeDq~Vd-5(joR7*R79By<^Qu+H`pU|(t$hc%mdeWIcEpqPe8X>');
define('LOGGED_IN_KEY',    '%X6idH&x [ZdKZ`5KO8T7S/LU^/9++NhoQd+hbAP<t# vYxA!OBdvV^cx+,AEIhB');
define('NONCE_KEY',        'm:Cx_*u6ak V|N(m%F<IleY@*sYoAf~jYiLJg,ILg%N_UElhBypm iGUj2dP?7W7');
define('AUTH_SALT',        '.KicCS,Jxr%))~)((sZmqn,&G|pt14kjojr#}`11c(1rZd):m=iYEp5tkN[VZ~l<');
define('SECURE_AUTH_SALT', 'dFXZAA5*P*Y=9Wsu,lI(Oq)2|x9|70{!6#+,3xoErSvZ<m&*]mZ<}[b)3wJSGnn@');
define('LOGGED_IN_SALT',   '@(CHvdj;&:Ts)Yh`2rcY9C[p5 1FHFnZ`/H091l>DQ&L!t@K.*C_i5{^-o=m3i[+');
define('NONCE_SALT',       'Tpt $1Jb>) (OivsnG7LP!OH15b/BLe@Y`r$4~$_* QS0 VwB#7]}lO3l11[Jki^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
