<?php

/**
 * Fired during plugin activation
 *
 * @link       http://napstack.com/
 * @since      1.0.0
 *
 * @package    Geneftx_Api
 * @subpackage Geneftx_Api/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Geneftx_Api
 * @subpackage Geneftx_Api/includes
 * @author     Napstack <h@napstack.com>
 */
class Geneftx_Api_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
