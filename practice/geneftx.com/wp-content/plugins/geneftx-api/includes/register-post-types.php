<?php
/**
 * Register a db_gene post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function register_gene_db() {
  $labels = array(
    'name'               => __( 'Genes', 'geneftx-api' ),
    'singular_name'      => __( 'Gene', 'geneftx-api' ),
    'menu_name'          => __( 'Genes', 'geneftx-api' ),
    'name_admin_bar'     => __( 'Gene', 'geneftx-api' ),
    'add_new'            => __( 'Add New', 'gene', 'geneftx-api' ),
    'add_new_item'       => __( 'Add New Gene', 'geneftx-api' ),
    'new_item'           => __( 'New Gene', 'geneftx-api' ),
    'edit_item'          => __( 'Edit Gene', 'geneftx-api' ),
    'view_item'          => __( 'View Gene', 'geneftx-api' ),
    'all_items'          => __( 'All Genes', 'geneftx-api' ),
    'search_items'       => __( 'Search Genes', 'geneftx-api' ),
    'parent_item_colon'  => __( 'Parent Genes:', 'geneftx-api' ),
    'not_found'          => __( 'No genes found.', 'geneftx-api' ),
    'not_found_in_trash' => __( 'No genes found in Trash.', 'geneftx-api' )
  );

  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'geneftx-api' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'db-gene' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title' ),
    'menu_icon'          => 'dashicons-networking',
  );

  register_post_type( 'db_gene', $args );
}
add_action( 'init', 'register_gene_db' );

              /*END db_gene*/

  /**
   * Register a db_Variant post type.
   *
   * @link http://codex.wordpress.org/Function_Reference/register_post_type
   */
  function register_variant_db() {
    $labels = array(
      'name'               => __( 'Variants', 'geneftx-api' ),
      'singular_name'      => __( 'Variant', 'geneftx-api' ),
      'menu_name'          => __( 'Variants', 'geneftx-api' ),
      'name_admin_bar'     => __( 'Variant', 'geneftx-api' ),
      'add_new'            => __( 'Add New', 'variant', 'geneftx-api' ),
      'add_new_item'       => __( 'Add New Variant', 'geneftx-api' ),
      'new_item'           => __( 'New Variant', 'geneftx-api' ),
      'edit_item'          => __( 'Edit Variant', 'geneftx-api' ),
      'view_item'          => __( 'View Variant', 'geneftx-api' ),
      'all_items'          => __( 'All Variants', 'geneftx-api' ),
      'search_items'       => __( 'Search Variants', 'geneftx-api' ),
      'parent_item_colon'  => __( 'Parent Variants:', 'geneftx-api' ),
      'not_found'          => __( 'No variants found.', 'geneftx-api' ),
      'not_found_in_trash' => __( 'No variants found in Trash.', 'geneftx-api' )
    );

    $args = array(
      'labels'             => $labels,
      'description'        => __( 'Description.', 'geneftx-api' ),
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'db-variant' ),
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'supports'           => array( 'title' ),
      'menu_icon'          => 'dashicons-images-alt2',
    );

    register_post_type( 'db_variant', $args );
  }
  add_action( 'init', 'register_variant_db' );

                 /*END db_variant*/

    /**
     * Register a db_disease post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function register_disease_db() {
      $labels = array(
        'name'               => __( 'Diseases', 'geneftx-api' ),
        'singular_name'      => __( 'Disease', 'geneftx-api' ),
        'menu_name'          => __( 'Diseases', 'geneftx-api' ),
        'name_admin_bar'     => __( 'Disease', 'geneftx-api' ),
        'add_new'            => __( 'Add New', 'Disease', 'geneftx-api' ),
        'add_new_item'       => __( 'Add New Disease', 'geneftx-api' ),
        'new_item'           => __( 'New Disease', 'geneftx-api' ),
        'edit_item'          => __( 'Edit Disease', 'geneftx-api' ),
        'view_item'          => __( 'View Disease', 'geneftx-api' ),
        'all_items'          => __( 'All Diseases', 'geneftx-api' ),
        'search_items'       => __( 'Search Diseases', 'geneftx-api' ),
        'parent_item_colon'  => __( 'Parent Diseases:', 'geneftx-api' ),
        'not_found'          => __( 'No diseases found.', 'geneftx-api' ),
        'not_found_in_trash' => __( 'No diseases found in Trash.', 'geneftx-api' )
      );

      $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'geneftx-api' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'db-disease' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' ),
        'menu_icon'          => 'dashicons-universal-access',
      );

      register_post_type( 'db_disease', $args );
    }
    add_action( 'init', 'register_disease_db' );

                 /*END db_desease*/

    /**
     * Register a db_phenotype post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function register_phenotype_db() {
      $labels = array(
        'name'               => __( 'Phenotypes', 'geneftx-api' ),
        'singular_name'      => __( 'Phenotype', 'geneftx-api' ),
        'menu_name'          => __( 'Phenotypes', 'geneftx-api' ),
        'name_admin_bar'     => __( 'Phenotype', 'geneftx-api' ),
        'add_new'            => __( 'Add New', 'Phenotype', 'geneftx-api' ),
        'add_new_item'       => __( 'Add New Phenotype', 'geneftx-api' ),
        'new_item'           => __( 'New Phenotype', 'geneftx-api' ),
        'edit_item'          => __( 'Edit Phenotype', 'geneftx-api' ),
        'view_item'          => __( 'View Phenotype', 'geneftx-api' ),
        'all_items'          => __( 'All Phenotypes', 'geneftx-api' ),
        'search_items'       => __( 'Search Phenotypes', 'geneftx-api' ),
        'parent_item_colon'  => __( 'Parent Phenotypes:', 'geneftx-api' ),
        'not_found'          => __( 'No phenotypes found.', 'geneftx-api' ),
        'not_found_in_trash' => __( 'No phenotypes found in Trash.', 'geneftx-api' )
      );

      $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'geneftx-api' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'db-phenotype' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' ),
        'menu_icon'          => 'dashicons-groups',
      );

      register_post_type( 'db_phenotype', $args );
    }
    add_action( 'init', 'register_phenotype_db' );

                      /*END db_phenotype*/

    /**
     * Register a db_lab post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function register_lab_db() {
      $labels = array(
        'name'               => __( 'Labs', 'geneftx-api' ),
        'singular_name'      => __( 'Lab', 'geneftx-api' ),
        'menu_name'          => __( 'Labs', 'geneftx-api' ),
        'name_admin_bar'     => __( 'Lab', 'geneftx-api' ),
        'add_new'            => __( 'Add New', 'Lab', 'geneftx-api' ),
        'add_new_item'       => __( 'Add New Lab', 'geneftx-api' ),
        'new_item'           => __( 'New Lab', 'geneftx-api' ),
        'edit_item'          => __( 'Edit Lab', 'geneftx-api' ),
        'view_item'          => __( 'View Lab', 'geneftx-api' ),
        'all_items'          => __( 'All Lab', 'geneftx-api' ),
        'search_items'       => __( 'Search Labs', 'geneftx-api' ),
        'parent_item_colon'  => __( 'Parent Labs:', 'geneftx-api' ),
        'not_found'          => __( 'No labs found.', 'geneftx-api' ),
        'not_found_in_trash' => __( 'No labs found in Trash.', 'geneftx-api' )
      );

      $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'geneftx-api' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'db-lab' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' ),
        'menu_icon'          => 'dashicons-store',
      );

      register_post_type( 'db_lab', $args );
    }
    add_action( 'init', 'register_lab_db' );

                   /*END db_lab*/

    /**
     * Register a db_environment post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function register_environment_db() {
      $labels = array(
        'name'               => __( 'Environments', 'geneftx-api' ),
        'singular_name'      => __( 'Environment', 'geneftx-api' ),
        'menu_name'          => __( 'Environments', 'geneftx-api' ),
        'name_admin_bar'     => __( 'Environment', 'geneftx-api' ),
        'add_new'            => __( 'Add New', 'Environment', 'geneftx-api' ),
        'add_new_item'       => __( 'Add New Environment', 'geneftx-api' ),
        'new_item'           => __( 'New Environment', 'geneftx-api' ),
        'edit_item'          => __( 'Edit Environment', 'geneftx-api' ),
        'view_item'          => __( 'View Environment', 'geneftx-api' ),
        'all_items'          => __( 'All Environment', 'geneftx-api' ),
        'search_items'       => __( 'Search Environments', 'geneftx-api' ),
        'parent_item_colon'  => __( 'Parent Environments:', 'geneftx-api' ),
        'not_found'          => __( 'No environments found.', 'geneftx-api' ),
        'not_found_in_trash' => __( 'No environments found in Trash.', 'geneftx-api' )
      );

      $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'geneftx-api' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'db-environment' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' ),
        'menu_icon'          => 'dashicons-palmtree',
      );

      register_post_type( 'db_environment', $args );
    }
    add_action( 'init', 'register_environment_db' );

                    /*END db_environment*/

    /**
     * Register a db_nutrient post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function register_nutrient_db() {
      $labels = array(
        'name'               => __( 'Nutrients', 'geneftx-api' ),
        'singular_name'      => __( 'Nutrient', 'geneftx-api' ),
        'menu_name'          => __( 'Nutrients', 'geneftx-api' ),
        'name_admin_bar'     => __( 'Nutrient', 'geneftx-api' ),
        'add_new'            => __( 'Add New', 'Nutrient', 'geneftx-api' ),
        'add_new_item'       => __( 'Add New Nutrient', 'geneftx-api' ),
        'new_item'           => __( 'New Nutrient', 'geneftx-api' ),
        'edit_item'          => __( 'Edit Nutrient', 'geneftx-api' ),
        'view_item'          => __( 'View Nutrient', 'geneftx-api' ),
        'all_items'          => __( 'All Nutrient', 'geneftx-api' ),
        'search_items'       => __( 'Search Nutrients', 'geneftx-api' ),
        'parent_item_colon'  => __( 'Parent Nutrients:', 'geneftx-api' ),
        'not_found'          => __( 'No nutrients found.', 'geneftx-api' ),
        'not_found_in_trash' => __( 'No nutrients found in Trash.', 'geneftx-api' )
      );

      $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'geneftx-api' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'db-nutrient' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' ),
        'menu_icon'          => 'dashicons-image-filter',
      );

      register_post_type( 'db_nutrient', $args );
    }
    add_action( 'init', 'register_nutrient_db' );

                     /*END db_nutrient*/

    /**
     * Register a db_drug post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function register_drug_db() {
      $labels = array(
        'name'               => __( 'Drugs', 'geneftx-api' ),
        'singular_name'      => __( 'Drug', 'geneftx-api' ),
        'menu_name'          => __( 'Drugs', 'geneftx-api' ),
        'name_admin_bar'     => __( 'Drug', 'geneftx-api' ),
        'add_new'            => __( 'Add New', 'Drug', 'geneftx-api' ),
        'add_new_item'       => __( 'Add New Drug', 'geneftx-api' ),
        'new_item'           => __( 'New Drug', 'geneftx-api' ),
        'edit_item'          => __( 'Edit Drug', 'geneftx-api' ),
        'view_item'          => __( 'View Drug', 'geneftx-api' ),
        'all_items'          => __( 'All Drug', 'geneftx-api' ),
        'search_items'       => __( 'Search Drugs', 'geneftx-api' ),
        'parent_item_colon'  => __( 'Parent Drugs:', 'geneftx-api' ),
        'not_found'          => __( 'No drugs found.', 'geneftx-api' ),
        'not_found_in_trash' => __( 'No drugs found in Trash.', 'geneftx-api' )
      );

      $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'geneftx-api' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'db-drug' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' ),
        'menu_icon'          => 'dashicons-plus-alt',
      );

      register_post_type( 'db_drug', $args );
    }
    add_action( 'init', 'register_drug_db' );

                    /*END db_drug*/

    /**
     * Register a db_pathway post type.
     *
     * @link http://codex.wordpress.org/Function_Reference/register_post_type
     */
    function register_pathway_db() {
      $labels = array(
        'name'               => __( 'Pathways', 'geneftx-api' ),
        'singular_name'      => __( 'Pathway', 'geneftx-api' ),
        'menu_name'          => __( 'Pathways', 'geneftx-api' ),
        'name_admin_bar'     => __( 'Pathway', 'geneftx-api' ),
        'add_new'            => __( 'Add New', 'Pathway', 'geneftx-api' ),
        'add_new_item'       => __( 'Add New Pathway', 'geneftx-api' ),
        'new_item'           => __( 'New Pathway', 'geneftx-api' ),
        'edit_item'          => __( 'Edit Pathway', 'geneftx-api' ),
        'view_item'          => __( 'View Pathway', 'geneftx-api' ),
        'all_items'          => __( 'All Pathway', 'geneftx-api' ),
        'search_items'       => __( 'Search Pathways', 'geneftx-api' ),
        'parent_item_colon'  => __( 'Parent Pathways:', 'geneftx-api' ),
        'not_found'          => __( 'No pathways found.', 'geneftx-api' ),
        'not_found_in_trash' => __( 'No pathways found in Trash.', 'geneftx-api' )
      );

      $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'geneftx-api' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'db-pathway' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' ),
        'menu_icon'          => 'dashicons-location',
      );

      register_post_type( 'db_pathway', $args );
    }
    add_action( 'init', 'register_pathway_db' );

                    /*END db_phathway*/

 ?>
