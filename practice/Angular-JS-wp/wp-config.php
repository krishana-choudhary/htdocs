<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'Angular-JS-wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'C|@R1DdH?R%RiXETSyO=dWHOe.k-t|Mk2TL78;=u-{T:=~xL{Jgr@ShFkDl0U@[E');
define('SECURE_AUTH_KEY',  ';p`Os!3!`-[*a|A[Sx0ypi&G2Diyd[_yP5c6Ad)m1=n0Hj):iNqYZ5Kg#/{j.;cK');
define('LOGGED_IN_KEY',    'uDHc=qIwVV~~,HSw6@+XNIRj~% C#s40/;{M=G%)|5!Aui/^;Y9J(sK4,??76+=:');
define('NONCE_KEY',        'u}O*.81Vh=e6u@@m<X*8jIj_u$yzGmk<hZv<W~9R+~IV-=v[4174kax7|f/K-lY(');
define('AUTH_SALT',        'NnhC;#i:jo[vP_|VEjtNoO^eq.NO(Ty,$P<l} Ug<1=iY>Ky5(eLi<pQZCgra/S4');
define('SECURE_AUTH_SALT', '_td.5=xe6]G&Rspx>v~=HWXW(%q4V_CJN&Dr)pRpUSF=]LkgNj=ut.4j~.xw>tN^');
define('LOGGED_IN_SALT',   '<sY:YHrpG~7+=7d]Z~I<Co]wT@[h[C7Hp-:5mtF|IMf%^)w0W]2hpn1aU+D^:fgL');
define('NONCE_SALT',       '(4gjPqW$GTrC  RkE]%-tE~huQT9w%=>3xxnFQjkk9hwT7O`GBK6>M.3+QQ%MxG4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
