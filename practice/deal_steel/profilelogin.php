<?php session_start();
if (!isset($_SESSION["uid"])) {
  header("location:index.php");
}
 ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>steel deal</title>
    <link rel="stylesheet"href="css/bootstrap.min.css">
    <link rel="stylesheet"href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
    <script src="main.js"></script>

    <style>
#myNavbar{
      margin-left: 150px;
      color: #fff;
    }
.brand{
     margin-left: 20px;
   }
.navbar{
     padding: 5px;
     border-radius: 0px;
     background:#000000;
   }
.navbar-nav{
    padding-top: 15px;
}
#myNavbar a{
color:#acaca4;
}
.dropdown-menu{
  padding-top: 0px;
}

    </style>
  </head>
  <body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="https://www.steel.com">
<img src="product_images/logo.jpg" class="brand"alt="steel deal" width="70" height="70"  border="0">
</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav text-color">
        <li><a href="#">Home</a></li>
        <li><a href="#">About us </a></li>
        <li><a href="#">Contact us</a></li>
      </ul>
      <div class="navbar-form navbar-nav">
        <input type="text" class="form-control" id="search">
      <button class="btn btn-default" id="search_btn">search</button>
    </div>
      <ul class="nav navbar-nav navbar-right">
          <li><a href="#" id="cart_container" class="dropdown_toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span> Cart<span class="badge"> 0 </span> </a>
    <div class="dropdown-menu" style="width:400px";>
      <div class="panel panel-success">
      <div class="panel-heading">
        <div class="row">
          <div class="col-md-3">sl.no</div>
          <div class="col-md-3">product image</div>
          <div class="col-md-3">product name</div>
          <div class="col-md-3">price</div>
      </div>
      </div>
      <div class="panel-body" style="color:black">
        <div id="cart_msg">
        <!--<div class="row">
          <div class="col-md-3">sl.no</div>
          <div class="col-md-3">product image</div>
          <div class="col-md-3">product  name</div>
          <div class="col-md-3">price</div>
      </div>-->
    </div>
      </div>
      <div class="panel-footer"></div>
  </div>
    </div>
</li>
         <li><a href="#"class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span> <?php echo "Hi,". $_SESSION["name"]; ?></a>
           <ul class="dropdown-menu">
             <li><a href="cart.php" style="text-decoration:none;color:blue"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a>
               <li class="divider"></li>
             <li><a href="#" style="text-decoration:none;color:blue">change password</a></li>
             <li class="divider"></li>
             <li><a href="logout.php" style="text-decoration:none;color:blue">logout</a></li>
           </ul>
         </li>

      </ul>

  </div>
</div>
</nav>
<p></br></p>
<p></br></p>
<p></br></p>
<div id="section1" class="section">
<div class="container-fluid">
<div class="container">
  <div class="row">
    <div class="col-md-1">
    </div>
    <div class="col-md-2">
      <div id="get_category">
      </div>
    <!--  <div class="nav nav-pills nav-stacked">
        <li class="active"><a href="#">Categories</a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
        <li><a href="#"></a></li>
      </div>-->

      <!--<div class="nav nav-pills nav-stacked">
        <li class="active"><a href="#">Brands</a></li>
        <li><a href="#">iphone</a></li>
        <li><a href="#">cannon</a></li>
        <li><a href="#">acer</a></li>
      </div>-->
      <div id="get_brand">
      </div>
    </div>
    <div class="col-md-8">
      <div class="row">
        <div class="col-md-12"id="product_msg">
        </div>

      </div>
      <div class="panel panel-info success">
        <div class="panel-heading">Products</div>
        <div class="panel-body">
          <div id="get_products">
          </div>
        <!--<div class="col-md-4">
      <div class="panel panel-info">
        <div class="panel-heading">Budha</div>
        <div class="panel-body">
          <img src="img/budha.jpg" width="150">
      </div>
        <div class="panel-heading">$.500.00
        <button style="float:right;" class="btn btn-danger btn-xs">AddToCart</button>
        </div>
      </div>
    </div>-->
  </div>
<div class="panel-heading">
  <p>copyright</p>
</div>

</div>
</div>
<div class="col-md-1"></div>
  </div> <!--end .row-->
<!--<div class="row">
    <div class="col-md-12">
      <center>
        <ul class="pagination">
          <li><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
        </ul>
      </center>
    </div>
  </div>-->
</div> <!--end .container-->
</div> <!--end .fluid-->
</div> <!--end #id-->

<!---------------------------------->
<!----------------------------------->


  </body>
</html>
