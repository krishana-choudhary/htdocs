<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>steel deal</title>
    <link rel="stylesheet"href="css/bootstrap.min.css">
    <link rel="stylesheet"href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
    <script src="main.js"></script>

    <style>
#myNavbar{
      margin-left: 150px;
      color: #fff;
    }
.brand{
     margin-left: 20px;
   }
.navbar{
     padding: 5px;
     border-radius: 0px;
     background:#000000;
   }
.navbar-nav{
    padding-top: 15px;
}
#myNavbar a{
color: #fff;
}
.dropdown-menu{
  padding-top: 0px;
}
  </style>
  </head>
  <body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav text-color">
        <li><a href="#">Home</a></li>
        <li><a href="#">About us </a></li>
      </ul>
    </nav>
      <p></br></p>
      <p></br></p>
      <p></br></p>
      <div class="container-fluid">
        <!--this row is for sign up msg-->
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8" id="signup_msg">
          </div>
          <div class="col-md-2"></div>
        </div>
          <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8">
            <div class="panel panel-primary">
              <div class="panel-heading">customer sign up form</div>
            <div class="panel-body">
              <form method="post">
               <div class="row">
                 <div class="col-md-6">
                 <label for="f_name">first name</lable>
                   <input type="text" id="f_name" name="f_name" class="form-control">
                 </div>
                 <div class="col-md-6">
                   <label for="l_name">last name</lable>
                     <input type="text" id="l_name" name="l_name" class="form-control">
                 </div>
               </div>
               <div class="row">
                 <div class="col-md-12">
                 <label for="email">email</lable>
                   <input type="text" id="email" name="email" class="form-control">
                 </div>
               </div>
             <div class="row">
                 <div class="col-md-12">
                   <label for="password">password</lable>
                     <input type="password" id="password" name="password" class="form-control">
                 </div>
               </div>
                   <div class="row">
                       <div class="col-md-12">
                         <label for="repassword">re enter password</lable>
                           <input type="password" id="repassword" name="repassword" class="form-control">
                       </div>
                     </div>
                     <div class="row">
                         <div class="col-md-12">
                           <label for="phone">mobile</lable>
                             <input type="text" id="phone" name="phone" class="form-control">
                         </div>
                       </div>
                       <div class="row">
                           <div class="col-md-12">
                             <label for="address1">address1</lable>
                               <input type="text" id="address1" name="address1" class="form-control">
                           </div>
                         </div>
                         <div class="row">
                             <div class="col-md-12">
                               <label for="address2">address2</lable>
                                 <input type="text" id="address2" name="address2" class="form-control">
                             </div>
                           </div>
                           <div class="row">
                         <div class="col-md-12">
                           <input style="float:right;" value="sign up" type="button"id="signup_btn" name="signup_btn" class="btn btn-primary">
                           </div>
                         </div>
     </div>
      </form>
      <div class="panel-heading"></div>
      </div>
         </div>
   <div class="col-md-2"></div>
  </div><!--end.row-->

      </div><!--end.fluid-->

    </body>
    </html>
