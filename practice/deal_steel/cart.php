<?php session_start();
if (!isset($_SESSION["uid"])) {
  header("location:index.php");
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>steel deal</title>
    <link rel="stylesheet"href="css/bootstrap.min.css">
    <link rel="stylesheet"href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
    <script src="main.js"></script>

    <style>
#myNavbar{
      margin-left: 150px;
      color: #fff;
    }
.brand{
     margin-left: 20px;
   }
.navbar{
     padding: 5px;
     border-radius: 0px;
     background:#000000;
   }
.navbar-nav{
    padding-top: 15px;
}
#myNavbar a{
color:#acaca4;
}
.dropdown-menu{
  padding-top: 0px;
}

    </style>
  </head>
  <body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="https://www.steel.com">
<img src="product_images/logo.jpg" class="brand"alt="steel deal" width="70" height="70"  border="0">
</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav text-color">
        <li><a href="#">Home</a></li>
        <li><a href="#">About us </a></li>
      </ul>
    </div>
  </nav>
    <div class="container-fluid ">
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8" id="cart_msg">

        </div>
      </div class="col-md-2"></div>
      </div>
      <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
          <div class="panel panel-primary">
            <div class="panel-heading">cart checkout</div>
            <div class="panel-body">
              <div class="row">
              <div class="col-md-2"><b>Action</b></div>
              <div class="col-md-2"><b>product image</b></div>
              <div class="col-md-2"><b>product name</b></div>
              <div class="col-md-2"><b>Qty</b></div>
              <div class="col-md-2"><b>product price</b></div>
              <div class="col-md-2"><b>price in $</b></div>
            </div>
            <div id="cart_checkout">
            </div>
          <!--<div class="row">
            <div class="col-md-2">
              <div class="btn-group">
              <a href="#" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
              <a href="#" class="btn btn-primary"><span class="glyphicon glyphicon-ok-sign"></span></a>
             </div>
            </div>
            <div class="col-md-2"><img src='product_images/images.jpg'></div>
            <div class="col-md-2">product name</div>
            <div class="col-md-2"><input type='text' class='form-control'value='1'></div>
            <div class="col-md-2"><input type='text' class='form-control'value='1'disabled></div>
            <div class="col-md-2"><input type='text' class='form-control'value='5000'disabled></div>
          </div>-->
          <!--  <div class="row">
              <div class="col-md-8">
              </div>
              <div class="col-md-4">
                <b>Total $50000</b>
              </div>
            </div>-->
              </div>
            <div class="panel-footer">copyright</div>
          </div>
        </div>
        <div class="col-md-2"></div>
      </div>
    </div>
  </body>
  </html>
