$("document").ready(function(){
  cat();
  brand();
  product();
  //select display acording cat//
  function cat(){
    $.ajax({
      url:"action.php",
      method:"POST",
      data:{'category':1},
      success:function(data){
        //console.log(data);
        $("#get_category").html(data);
      }
    })
  }
  //select display acording brand//
  function brand(){
    $.ajax({
      url:"action.php",
      method:"POST",
      data:{'brand':1},
      success:function(data){
        $("#get_brand").html(data);
      }
    })
  }
//select display acording products//
  function product(){
    $.ajax({
      url:"action.php",
      method:"POST",
      data:{'getproduct':1},
      success:function(data){
        //console.log(data);
  $("#get_products").html(data);
      }
    })
  }
//select display acording categories id show for products//
$("body").delegate(".category", "click",function(event){
  event.preventDefault();
  console.log('pass' );
  var cid=$(this).attr('cid')
  $.ajax({
    url:"action.php",
    method:"POST",
    data:{get_selected_category:1,cat_id:cid},
    success:function(data){
      //console.log(data);
    $("#get_products").html(data);
    }
  });
});

//select display acording categories id show for brand products//
$("body").delegate(".brand", "click",function(event){
  event.preventDefault();
  //console.log('pass' );
  var bid=$(this).attr('bid')
  $.ajax({
    url:"action.php",
    method:"POST",
    data:{selectedbrand:1,brand_id:bid},
    success:function(data){
      //console.log(data);
    $("#get_products").html(data);
    }
  });
});

//this is for search btn for product//
$("#search_btn").click(function(){
  var keyword =$("#search").val();
  if (keyword != ""){
 $.ajax({
   url : "action.php",
   method : "POST",
   data : {search:1,keyword:keyword},
   success : function(data){
     $("#get_products").html(data);
   }
 })
  }
})
//this is for sign up//
$("#signup_btn").click(function(event){
  event.preventDefault();
  $.ajax({
    url : "registered.php",
    method : "POST",
    data : $("form").serialize(),
    success : function(data){
    $("#signup_msg").html(data);
    }
  })
})

  $("#login").click(function(event){
    event.preventDefault();
    var email = $("#email").val();
    var pass = $("#password").val();
    $.ajax({
      url:"login.php",
      method:"POST",
      data:{userlogin:1,useremail:email,userpassword:pass},
      success:function(data){
          window.location.href="profilelogin.php";
      }
    })
  })

  $("body").delegate("#product", "click",function(event){
    event.preventDefault();
    var p_id = $(this).attr('pid');
    $.ajax({
      url : "action.php",
      method : "POST",
      data : {addToproduct:1,pro_id:p_id},
      success:function(data){
     $("#product_msg").html(data);
      }
    });
  });
$("#cart_container").click(function(event){
  event.preventDefault();
  $.ajax({
    url : "action.php",
    method : "POST",
    data : {get_addproductto_cart :1},
    success:function(data){
   $("#cart_msg").html(data);
    }
  });
})
cart_checkout();
function cart_checkout(){
  $.ajax({
    url :"action.php",
  method:"POST",
  data :{cart_checkout:1},
  success: function(data){
    $("#cart_checkout").html(data);
  }
  })
}

$("body").delegate(".qty","keyup",function(){
  var pid = $(this).attr("pid");
  var qty = $("#qty-"+pid).val();
  var price = $("#price-"+pid).val();
  var total= qty * price;
  $("#total-"+pid).val(total);
})

//removed delete product from cart//
$("body").delegate(".remove","click",function(event){
event.preventDefault();
var pid = $ (this).attr("remove_id");
$.ajax({
  url:"action.php",
  method:"POST",
  data:{removefromecart:1,removeId:pid},
  success:function(data){
 $("#cart_msg").html(data);
 cart_checkout();
  }
})
})

$("body").delegate(".update","click",function(event){
event.preventDefault();
var pid = $ (this).attr("update_id");
var qty = $("#qty-"+pid).val();
var price = $("#price-"+pid).val();
var total = $("#total-"+pid).val();
$.ajax({
  url:"action.php",
  method:"POST",
  data:{updateproduct:1,updateid:pid,qty:qty,price:price,total:total,},
  success:function(data){
 $("#cart_msg").html(data);
 cart_checkout();
  }
})
})
//for pagination//
/*page();
function page(){
  $.ajax({
    url:"action.php",
    method:"POST",
    data :{page:1},
    success:function(data){
      alert(data);
    }
  })
}*/
})
