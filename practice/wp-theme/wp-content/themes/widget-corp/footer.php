</div> <!-- END page-wrap -->
		<div id="footer">
			<footer class="group">
				<div id="quote">One wheel turns another</div>
				<div id="copyright">&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?></div>
			</footer>

		</div>

	</div>

	<?php wp_footer(); ?>

	<!-- Don't forget analytics -->

</body>

</html>
