<?php get_header(); ?>
<div id="body_content">
  <?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
  ?>
  <div class="single-post_box">
  <div id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
  <h2 class="post_cat"><?php the_category(); ?></h2>
    <div class="single-post_img">
      <?php
      if ( has_post_thumbnail() ) {
        the_post_thumbnail();
      }
      ?>
    </div><!--post_img-->
    <div class="content_wrapper">
    <div class="post_title">
      <h2><a href="<?php the_permalink();?>"><?php the_title() ?></a></h2>
    </div><!--post_title-->
    <div class="single-post_content">
      <?php the_content();?>
    </div><!--post_content-->
    <div class="nextprev">
      <div class="prev"><a href="<?php the_permalink() ?>"><?php previous_post_link();?></a></div>
      <div class="next"><a href="<?php the_permalink() ?>"><?php next_post_link(); ?></a></div>
    </div><!--nextprev-->
  </div><!--content_wrapper-->
  </div><!--cat-post-->
  </div><!--cat-date-->
  </div><!--post_box-->
  <?php
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>
</div><!--end content-->
<?php get_footer(); ?>
