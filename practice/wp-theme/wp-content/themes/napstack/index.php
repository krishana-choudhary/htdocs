<?php get_header(); ?>
<div id="body_content">
  <div id="sidebar">
  <?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
  ?>
  <div class="post_box">
  <div id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
    <div class="post_title">
      <h2><a href="<?php the_permalink();?>"><?php the_title() ?></a></h2>
    </div><!--post_title-->
    <div class="post_img">
      <?php
      if ( has_post_thumbnail() ) {
        the_post_thumbnail();
      }
      ?>
    </div><!--post_img-->
    <div class="post_content">
      <?php substr(the_excerpt(),0,200);?>
      <div class="read_btn">
        <a href="<?php the_permalink() ?>">Read More</a>
      </div>
      <?php the_category(); ?>
    </div><!--post_content-->
  </div><!--cat-date-->
  </div><!--post_box-->
  <?php
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>
</div><!--sidebar-->
<div id="right-sidebar">
   <?php dynamic_sidebar('Main Sidebar'); ?>
</div>
</div><!--end content-->
<?php get_footer(); ?>
