<?php
register_nav_menus( array(
  'primary' => 'Top Menu',
) );
//post thumbnail
add_theme_support( 'post-thumbnails' );


//widgets
if (function_exists('register_sidebar')) {
  register_sidebar( array(
  'name' => 'Right Sidebar',
  'before_title'  => '<h2>',
  'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
  'name' => 'Left Sidebar',
  'before_title'  => '<h2>',
  'after_title'   => '</h2>',
  ) );
}
