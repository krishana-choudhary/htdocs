<?php
/*
Template Name:Left Sidebar
*/

 get_header(); ?>
  <div id="leftsidebar-wrapper">
  <?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
  ?>

  <div class="leftsidebar">
      <div class="leftsidebar-padding">
        <?php dynamic_sidebar('Left Sidebar'); ?>
      </div><!--leftsidebar-padding-->
  </div><!--leftsidebar-->
    <div class="leftsidebar_content">
      <div class="leftsidebar-padding">
        <?php the_title(); ?>
      <?php the_content();?>
    </div><!--leftsidebar-padding-->
    </div><!--leftsidebar_content-->
    
  <?php
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>
</div><!--leftsidebar-wrapper-->
<?php get_footer(); ?>
