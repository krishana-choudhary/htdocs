<?php
/**
 * Register a db_gene post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function register_gene_db() {
  $labels = array(
    'name'               => __( 'Genes', 'geneftx-api' ),
    'singular_name'      => __( 'Gene', 'geneftx-api' ),
    'menu_name'          => __( 'Genes', 'geneftx-api' ),
    'name_admin_bar'     => __( 'Gene', 'geneftx-api' ),
    'add_new'            => __( 'Add New', 'gene', 'geneftx-api' ),
    'add_new_item'       => __( 'Add New Gene', 'geneftx-api' ),
    'new_item'           => __( 'New Gene', 'geneftx-api' ),
    'edit_item'          => __( 'Edit Gene', 'geneftx-api' ),
    'view_item'          => __( 'View Gene', 'geneftx-api' ),
    'all_items'          => __( 'All Genes', 'geneftx-api' ),
    'search_items'       => __( 'Search Genes', 'geneftx-api' ),
    'parent_item_colon'  => __( 'Parent Genes:', 'geneftx-api' ),
    'not_found'          => __( 'No genes found.', 'geneftx-api' ),
    'not_found_in_trash' => __( 'No genes found in Trash.', 'geneftx-api' )
  );

  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Description.', 'geneftx-api' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'db-gene' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor','thumbnail','custom-fields' ),
    'menu_icon'          => 'dashicons-networking',
  );

  register_post_type( 'db_gene', $args );
}
add_action( 'init', 'register_gene_db' );

              /*END db_gene*/
              register_post_type( 'custom_css', array(
	                'labels' => array(
	                        'name'          => __( 'Custom CSS' ),
	                        'singular_name' => __( 'Custom CSS' ),
	                ),
	                'public'           => false,
	                'hierarchical'     => false,
	                'rewrite'          => false,
	                'query_var'        => false,
	                'delete_with_user' => false,
	                'can_export'       => true,
	                '_builtin'         => true, /* internal use only. don't use this when registering your own post type. */
	                'supports'         => array( 'title', 'revisions' ),
	                'capabilities'     => array(
	                        'delete_posts'           => 'edit_theme_options',
	                        'delete_post'            => 'edit_theme_options',
	                        'delete_published_posts' => 'edit_theme_options',
	                        'delete_private_posts'   => 'edit_theme_options',
	                        'delete_others_posts'    => 'edit_theme_options',
	                        'edit_post'              => 'edit_css',
	                        'edit_posts'             => 'edit_css',
	                        'edit_others_posts'      => 'edit_css',
	                        'edit_published_posts'   => 'edit_css',
	                        'read_post'              => 'read',
	                        'read_private_posts'     => 'read',
	                        'publish_posts'          => 'edit_theme_options',
	                ),
	        ) );

 ?>
