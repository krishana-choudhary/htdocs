<?php

/**
 * Fired during plugin deactivation
 *
 * @link       www ?.com
 * @since      1.0.0
 *
 * @package    Easy_Testimonial
 * @subpackage Easy_Testimonial/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Easy_Testimonial
 * @subpackage Easy_Testimonial/includes
 * @author     Krishana <Krishana@napstack.com>
 */
class Easy_Testimonial_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
