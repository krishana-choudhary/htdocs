<?php

function slider_func(){
  wp_enqueue_style( 'style-css', get_stylesheet_uri() );
   //wp_enqueue_style('jquery-bxslider',get_template_directory_uri().'/css/jquery.bxslider.css');
   //wp_enqueue_script('jquery');
   //wp_enqueue_script('jquery-bxslider',get_template_directory_uri().'/js/jquery.bxslider.js');
  // wp_enqueue_script('script-js',get_template_directory_uri().'/js/script.js');
}
add_action('wp_enqueue_scripts','slider_func');

register_nav_menus( array(
  'primary' => 'Top Menu',
) );
//post thumbnail
add_theme_support( 'post-thumbnails' );

//widgets
if (function_exists('register_sidebar')) {
  register_sidebar( array(
  'name' => 'Right Sidebar',
  'before_title'  => '<h2>',
  'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
  'name' => 'Left Sidebar',
  'before_title'  => '<h2>',
  'after_title'   => '</h2>',
  ) );
}
//registered custom post type
function post_slider( $atts ) {
	$atts = shortcode_atts( array(
	), $atts, 'slider' );
  ?>

  <div id="pics">
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/IMG-20171011-WA0045.jpg" alt="" width="200" height="200" />
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/IMG-20171011-WA0058_1 (1).jpg" width="200" height="200" />
    <img src="<?php echo get_stylesheet_directory_uri();?>/img/IMG-20171026-WA0027 (1).jpg" width="200" height="200" />
</div>
<?php
}
add_shortcode( 'slider', 'post_slider' );
