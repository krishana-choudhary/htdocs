<?php get_header(); ?>
<div id="body_content">
  <div id="sidebar">
  <?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
  ?>
  <div class="page_box">
    <div class="post_title">
      <h2><a href="<?php the_permalink();?>"><?php the_title() ?></a></h2>
    </div><!--post_title-->
    <div class="page_content">
      <?php the_content();?>
    </div><!--post_content-->
  </div><!--post_box-->
  <?php
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>
</div><!--sidebar-->
<div id="right-sidebar">
   <?php dynamic_sidebar('Right Sidebar'); ?>
</div>
</div><!--end content-->
<?php get_footer(); ?>
