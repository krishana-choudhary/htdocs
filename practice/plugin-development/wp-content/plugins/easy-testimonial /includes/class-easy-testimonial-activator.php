<?php

/**
 * Fired during plugin activation
 *
 * @link       www ?.com
 * @since      1.0.0
 *
 * @package    Easy_Testimonial
 * @subpackage Easy_Testimonial/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Easy_Testimonial
 * @subpackage Easy_Testimonial/includes
 * @author     Krishana <Krishana@napstack.com>
 */
class Easy_Testimonial_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
