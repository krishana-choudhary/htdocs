<?php
/*
Plugin Name: WPPS Filter
Plugin URI: http://www.napstack.com
Description: just for demo
Author: Napstack
Version: 1.0
Author URI: krishana@napstack.com
*/


/**
* Capitalize a string
* @param  String - $title - Title of the post
* @return String - Capitalized String
*/
function capital_letter($title){
  return ucwords($title);
}
add_filter('the_title','capital_letter');

/**
* Concatinate Thankyou Message
* @param  String - $content - Post Content
* @return String - Concatinated String
*/
function say_thanks($content){
  $content .= 'Thankyou For Reading';
  return ($content);
}
add_filter('the_content', 'say_thanks');

/**
* Custom Menu Page Removing Description
* @return String - Remove Menu Page
*/
function custom_menu_page_removing() {
  remove_menu_page( 'tools.php' );
}
add_action( 'admin_menu', 'custom_menu_page_removing' );

/**
* Wpautop Control Menu Description
* @return String -  Add Sub Menu
*/
function wpautop_control_menu() {

  $parent_slug = 'options-general.php';
  $page_title  = 'Napstack-control';
  $menu_title  = 'Napstack control';
  $capability  = 'manage_options';
  $menu_slug   = 'Napstack-control-menu';
  $function    = 'Napstack_control_options';

  add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
}
add_action('admin_menu', 'wpautop_control_menu');

/**
* Add Post Page
* @return String -  Post Page
*/
function my_plugin_menu() {
  add_posts_page('My Plugin Posts Page', 'My Plugin', 'read', 'my-unique-identifier', 'my_plugin_function');
}
add_action('admin_menu', 'my_plugin_menu');


function my_plugin_function(){
  echo "hello";
}

/**
* Create Post Type Description
* @return String - Registere Post Type
*/
function create_post_type() {

  register_post_type( 'napstack_product',
  array(
    'labels' => array(
      'name' => __( ' Napstack Products' ),
      'singular_name' => __( 'Napastac Product' ),
    ),
      'public' => true,
      'has_archive' => true,
    )
  );
}
add_action( 'init', 'create_post_type' );

/** End Ist Test of plugin Development **/
/**
*    load data and display  categories
* @param  string - $content category
* @return string -   for all categories
*     this content is custome by self
*/
// function load_terms($content){
//
//    $id = get_the_id();
//
//    if ( !is_singular('post') ) { return $content; }
//
//    $terms = get_the_terms( $id, 'category');
//
//    $cats  = array();
//
//    foreach( $terms as $term) {
//
//     $cats[] = $term->term_id;
//    }
//
//    $args = array(
//      'posts_per_page' => 3,
//      'category__in' => $cats
//    );
//
//    $loop = new WP_Query($args);
//
//    if ( $loop->have_posts() ) {
//      $content .= '
//      <h2> You Also Might Like... </h2>
//      <ul class="related-category-posts">';
//
//      while( $loop->have_posts() ) {
//         $loop->the_post();
//         $content .= '<li><a href="' .get_permalink(). '">' .get_the_title(). '</a></li>';
//      }
//      $content .= '</ul>';
//
//      wp_reset_postdata();
//    }
//
//    return $content;
//  }
//   add_filter('the_content','load_terms');


//
//  add_filter('the_content', function($content) {
//    $id = get_the_id();
//    if (!is_singular('post') ) {
//      return $content;
//    }
//
//   $terms = get_the_terms( $id, 'category');
//   $cats  = array();
//
//   foreach( $terms as $term) {
//      //$cats[] = $term->cat_ID;
//     $cats[] = $term->term_id;
//   }
//
//    $loop = new WP_Query(
//      array(
//        'posts_per_page' => 3,
//        'category__in' => $cats
//      )
//  );
//
// //  var_dump( $loop );
//
// if ( $loop->have_posts() ) {
//     $content .= '
//     <h2> You Also Might Like... </h2>
//     <ul class="related-category-posts">';
//
//     while( $loop->have_posts() ) {
//       $loop->the_post();
//       $content .= '
//     <li>
//     <a href="' .get_permalink(). '">' .get_the_title(). '</a>
//     </li>';
//   }
//   $content .= '</ul>';
//   wp_reset_query();
// }
// return $content;
// });

function show_terms($content){

  $id = get_the_id();

  if (!is_singular('post')) {return $content;}

  $terms = get_the_terms($id,'category');
  /**$cats  = array();

  foreach ($terms as $term){
  $cats[] = $term->term_id;
}**/

$args  = array(
  'posts_per_page' => 3,
  //'category__in' => $cats,
  'orderby'  =>  'rand'
);
// The Query
$query1 = new WP_Query($args);
if ( $query1->have_posts() ) {
  $content.=
  '<h2>hello this is wordpress site</h2>
  <ul class="related-category-posts">';

  while ( $query1->have_posts() ) {
    $query1->the_post();
    $content.='
    <li>
    <a href="'.get_permalink().'">'. get_the_title() . '</a>
    </li>';
  }
  $content.=
  '</ul>';
  wp_reset_postdata();
}
return $content;
}
add_filter('the_content','show_terms');


# Enable Gravity Forms Password Field
add_filter( 'gform_enable_password_field', '__return_true' );

/**
 * Remove custom-forms class on signup form pages
 * @param  array - $classes - Default body classes
 * @return array - modified body classes
 */
function lr_remove_classes($classes = []) {

	if( !is_page() || !in_array(get_the_ID(), ['1775', '1781'] ) ) return $classes;

	$key = array_search('custom-forms', $classes);

	if( $key ) unset( $classes[$key] );

	return $classes;
}
add_filter('body_class', 'lr_remove_classes', 999);
