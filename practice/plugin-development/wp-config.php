<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'plugin_development');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'kwLt0eza-^<xeUc-LKn/E1u@3}BkubGU3sFx>_6jgj:KhwS!dXq#n~|(2IUZIIH3');
define('SECURE_AUTH_KEY',  '{0T&4EbV,^XlzyH9VuQ5IDbooCst;{nywP&xdx!n1f:^@[.Q/}c0$i}Y_rR6GL`m');
define('LOGGED_IN_KEY',    '`MoFEwKJ(Z|}K(1ccv/i5A)!ikHW@j%&RFv8S,fK>^uvUUHugN]H+y[#DY)T=tQ/');
define('NONCE_KEY',        '{;RQw0Y]4!}AE}8|V%nF=/l TC(C8az!8+F/)}C5:(BESpP/:dCiag[@a?5-b_q3');
define('AUTH_SALT',        'D!ml@MX#urHMBR}e{5H*l_A05tl-@Jfv]U(uFo=VYhc8/ILDb7E=Ig*s<(Wrf8s>');
define('SECURE_AUTH_SALT', 'l@&IRMDs<k0;o{,O5wIes-ufDJrHzv|?Q6Rp@R{<7ApPmdk#Ms!RfMvGFU1Qj2PG');
define('LOGGED_IN_SALT',   'wfBU:;Hc0r)[[SKq If%]#Lh#07U$y=`VJz`8j+U3eA*$dmTU{iENQ# ZD(aT-GQ');
define('NONCE_SALT',       '?zcIpZ`iABJt=zl6*{i*fbl(Dfo;ZYYI?HJsQN<] ysl+)(6qo26G }Zx9B~>Dt2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
