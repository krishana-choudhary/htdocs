-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Feb 10, 2018 at 01:55 PM
-- Server version: 5.6.35
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_bms`
--

-- --------------------------------------------------------

--
-- Table structure for table `bms`
--

CREATE TABLE `bms` (
  `id` int(11) NOT NULL,
  `fname` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `ac` varchar(200) NOT NULL,
  `pin` varchar(200) NOT NULL,
  `phone` varchar(200) NOT NULL,
  `address` varchar(255) NOT NULL,
  `img` text NOT NULL,
  `bal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bms`
--

INSERT INTO `bms` (`id`, `fname`, `lname`, `email`, `ac`, `pin`, `phone`, `address`, `img`, `bal`) VALUES
(1, 'krishana', 'kumar', 'krishana@gmail.com', '10', '1234', '1234567', '124,jyoti nagar (Jalandhar)', '', ''),
(2, 'krishana', 'kumar', 'krishana@napstack.com', '10', '1234', '86999959326', '124,jyoti nagar (Jalandhar)', '', ''),
(3, 'krishana', 'kumar', 'krishana@napstack.com', '11', '1234', '123456789', '124,jyoti nagar (Jalandhar)', '', ''),
(4, 'krishana', 'kumar', 'krishana@napstack.com', '12345678900', '1234', '123456789', '124,jyoti nagar (Jalandhar)', '', ''),
(5, 'krishana', 'kumar', 'kumard@gmail.com', '1234567890', '1234', '86999959326', '124,jyoti nagar (Jalandhar)', '', ''),
(6, 'napstack', 'solution', 'n@napstack.com', '1111111111', '2222', '86999959326', 'INDIA (PUNjab)', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bms`
--
ALTER TABLE `bms`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bms`
--
ALTER TABLE `bms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
