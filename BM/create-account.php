<?php session_start(); ?>
<?php include_once "db-connection.php"; ?>
<?php include_once "header-title.php"; ?>
<?php
if ((isset($_POST['signup']))) {
	$a = $_POST['fname'];
	$b = $_POST['lname'];
	$c = $_POST['email'];
	$d = $_POST['ac'];
	$e = strlen($d);
	$f = $_POST['pin'];
	$g = strlen($f);
	$h = $_POST['phone'];
	$i = $_POST['address'];

  if (empty($a)) {
  $msg="name fields is requered";
	}
	elseif (empty($b)) {
	$msg="last name fields is requered";
	}
	elseif (!filter_var($c, FILTER_VALIDATE_EMAIL)) {
	$msg="enter valid email address";
	}
	elseif (empty($d)) {
	$msg="account is required";
	}
	elseif ($e < 10) {
	$msg=" your account is not proper character it should be 10 digits";
	}
	elseif (empty($f)) {
	$msg=" pin is required";
	}
	elseif ($g < 4) {
	$msg=" your pin is not proper character it should be 4 digits";
	}
	elseif (empty($h)) {
	$msg="phone no is required";
	}
	else {
	  $query = "INSERT INTO bms (fname, lname, email, ac, pin, phone, address) VALUES ('$a','$b','$c','$d','$f','$h','$i')";
	   $fire = mysqli_query($con,$query);
	   $_SESSION['fname']=$a;
	   $_SESSION['pass']="you are signed up";
	   header('location:index.php');
	}
	}

?>
<nav class="navbar navbar-inverse">
<div class="container-fluid">
	<div class="navbar-header">
		<a class="navbar-brand" href="#">BMS</a>
	</div>
	<div class="collapse navbar-collapse" id="myNavbar">
		<ul class="nav navbar-nav text-color">
			<li><a href="login.php">Home</a></li>
		</ul>
	</nav>
		<p></br></p>
		<p></br></p>
		<p></br></p>
		<div class="container-fluid">
			<!-- this row is for sign up msg-->
<div class="row">
<div class="col-md-2">
	<?php if (isset($msg)):?>
		<div class="alert alert-success"> <?php echo  $msg ?></div>
	<?php endif; ?>
</div>
<div class="col-md-8">
<div class="panel panel-primary">
<div class="panel-heading">customer sign up form</div>
<div class="panel-body">

<form action="create-account.php"method="post">

<div class="row">
<div class="col-md-6">
<label for="f_name">first name</lable>
<input type="text" id="fname" name="fname" class="form-control">
</div>
<div class="col-md-6">
<label for="l_name">last name</lable>
<input type="text" id="lname" name="lname" class="form-control">
</div>
</div>
<div class="row">
<div class="col-md-12">
<label for="email">email</lable>
<input type="text" id="email" name="email" class="form-control">
</div>
</div>
<div class="row">
<div class="col-md-12">
<label for="Acount">Account</lable>
<input type="text" id="ac" name="ac" class="form-control">
</div>
</div>
<div class="row">
<div class="col-md-12">
<label for="Pin">Pin</lable>
<input type="text" id="pin" name="pin" class="form-control">
</div>
</div>
<div class="row">
<div class="col-md-12">
<label for="phone">Phone</lable>
<input type="text" id="phone" name="phone" class="form-control">
</div>
</div>
<div class="row">
<div class="col-md-12">
<label for="address1">address</lable>
<input type="text" id="address" name="address" class="form-control">
</div>
</div>
<div class="row">
<div class="col-md-12">
	<input type="submit" name="signup" value="Create" class="btn-sm btn btn-primary" style="float:right;">
</div>
</div>
</div>

</form>

<div class="panel-heading"></div>
</div>
</div>
<div class="col-md-2"></div>
</div><!--end.row-->
</div><!--end.fluid-->
</body>
</html>
