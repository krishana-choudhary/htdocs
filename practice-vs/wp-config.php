<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_practice');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tXDg)%H_lFzX( d|;/G`SVp`i.w|cvk5oRvDG-BuU<aqC)tJ9r;VR/4@5R iNRn%');
define('SECURE_AUTH_KEY',  'zc/o2i,YwcQNgAQ<=(`w)BC+yv0%zBu]QAPugP$>|yv#hU+-_vNg;[pGq^BVEyx^');
define('LOGGED_IN_KEY',    'KZzKKsFL|ne(s/GMGi?1U+qAc`hM!n5G]hf)0S(kb7x+}Q{YeTaM@1W!-}?f zYU');
define('NONCE_KEY',        'F%UD#Cqre=z+>U}rZrLw#TK3^Y{lI.2cHM+)*RVWNU/d6vGa1El6*#K`hDQ$Ya~{');
define('AUTH_SALT',        '0Tc~5am+y>v,NF,d $8_jZH ;_v4k-^Ia-G%_L wGUJm@MZ{F)e}dJ ]YVp@(5jB');
define('SECURE_AUTH_SALT', 'h1p_o|>xsl^ViP959,lsyTomc8wXl&J5l-MS::e-uq@#/8L*/4>yEn/L2FCg|VoW');
define('LOGGED_IN_SALT',   'rJhP fFsk)|q6Zpb3q%wDRe#sN9wAx%.Vh{EnrZ<F*rv<H9WMJq|~m(s*;e,dtu,');
define('NONCE_SALT',       '^U|>KCUQkIR, wdTFc{k_)KbJ9Ge,M>QVi7p,lwBFIW_!4$]h45kzL1T=/o9^!ti');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
