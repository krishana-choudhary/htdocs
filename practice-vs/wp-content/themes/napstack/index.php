<?php

$args = [
  'posts_per_page' => 3,
  'posst_type' => 'post',
  'post_type' => 'page',
  // 'pagename' => 'sample-page',
  //'p' => 1016
  'page_id' => 155

];

$query = new WP_Query( $args );

if( $query->have_posts() ) :
  while( $query->have_posts() ) : $query->the_post();
  ?><h1><?php echo get_the_title(); ?> | <?php echo get_the_content(); ?> | <small><?php echo get_the_author(); ?></small></h1>
  <?php
  endwhile;
else:
  ?><h1>No Posts Found</h1><?php
endif;

wp_reset_postdata();
