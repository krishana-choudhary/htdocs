<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'git_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7`#,I_T<V~`Nmin$)4=p%JdR-E!N^UaO,8Z.PMZ.7YveoKoBfg[Z501cGhI1Cw:s');
define('SECURE_AUTH_KEY',  'n-T1l!W5I:V!LL1en]cjZK3!X@!UQ;raheaR|b;$QZBP6AnJ0G L0=q>V}Cs@~~b');
define('LOGGED_IN_KEY',    '(cZLcds88+~e^+%S:z!A,GTF5v#Reun^#PjZ6Mir0?6!-=bdtWT4a5xYq$e814@t');
define('NONCE_KEY',        ' g)*_2Z?}4_yLs6z|y*})T/aYccKa1&B55!}lelshK MC9.fQ@s%XR(No7FcHn:h');
define('AUTH_SALT',        'qyqcRqK%pyC=QK>LZUvH#0@pR{1A*v_6s4eSzN`GOfPe7CRK5wO`%FEIu}2,I8wm');
define('SECURE_AUTH_SALT', 'kv[!o-?~W%fj*Q_61gM2]*tWpe3pmO#!*+LgLG0h)TiXxP_WB).i[l.r`S~~<4.R');
define('LOGGED_IN_SALT',   '&[}#3vm9GPB/*o7:L:c?axdX(rsevDK@Y+]Y)QU)ebGT%<JeN HlCQ&I-0Xn8!R6');
define('NONCE_SALT',       'TB$0JMl uwo6k:T`Ndw%1|*n5%UbdF+z]F9oMwy%9H))Bk.|YY44gRHuB hv:aZn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
