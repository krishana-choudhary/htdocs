<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'classic_curves');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'iU:KMSh|2j7-)(xgVbll!]%T126x9;l3{$>ZwJ$k KTe>_ZiElW@(<1lXkThP#L;');
define('SECURE_AUTH_KEY',  'MWiPv4`k)waoLb:Vhlc].U-kJ5rl+XU@A+bIFDr8r$ECV=2(B)Ud<wH-U$_C`CxY');
define('LOGGED_IN_KEY',    'Y6lX]~`&7t4c>bQzzRFMm?B;Qt2>Zo7*HNmOxp+H9{Gx5IIZnkP$LKNfiLnl3`Hu');
define('NONCE_KEY',        'bh+-tD6DWu^F6.WfjpiuES}X-C>~pWb)|;/YI}fl&QHI&^#jwr!`B3u=+L/oh[<2');
define('AUTH_SALT',        'xMhio|[ii^f-MID`==@YMjjuo,.v#6?pNbQV5GI;$X*rwUZ$&qCve:o[{+x_<=w!');
define('SECURE_AUTH_SALT', 'Mmchygiv*@7xIZF8i)ulo0WaM<A*&.I@WJ%(|@  %C&WjEBG7%uh%lKjO%!eN]~Q');
define('LOGGED_IN_SALT',   'ePLack,n97?X>rkU.)9MyGAxsAeYY}[jW)n/brI:<h@0f7R8gjPC>Insf4P>h9H1');
define('NONCE_SALT',       '&b}p[L?v=Mc@B%_&H4:Ao7#|ex&6F/ybzR>#Dx9gw!ZFG(NNra1RghoilqoJ&VLm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
