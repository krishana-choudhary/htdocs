<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'napstack_theme');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ht!;KhOlJkH`t79nxE|T (=q6NQfJCzHVJ/0B,avQe#xo^@ue{fT:M(lWl,L}K+u');
define('SECURE_AUTH_KEY',  '-YLJ(eE$*;?~+tN+KL18p$Oo q!uCw#l5VA<R2(p81%kmZ(K:(Hs4l6t1.2Jd%5L');
define('LOGGED_IN_KEY',    'ruuC:}FJO}V5`z`!wT!|4D^ve-&)Nz^(vd76;_6:LBt=9 s~iM?tOZzqr[->!Nov');
define('NONCE_KEY',        'xBo<3ZNA?Pq- to!g^#XbHgg_ Wy4NXu[)aF >vbLzl@t| e@>UW*!+M`LY4Y7#o');
define('AUTH_SALT',        'MVz|!bY&775]lJJ?c=a}eVM`RZq<!l~3)9y7;@~~%A~Jt}|w7%7:;xj/a>.CaeF?');
define('SECURE_AUTH_SALT', ' py/}5{q~ Nxig[O9t+2v]5WZ;q8RgE.V!57, G]@V6].+PAV%.]<%l^|E3Jvr/z');
define('LOGGED_IN_SALT',   'sfE$,mXQgeD>8X:zjmBv-<9v8ia_ai$<L|!Af@kk=miriOmjptEd74@D|YD$I+:[');
define('NONCE_SALT',       'J7_}F)C:UcPoTaos60,qW;j{?dy:8`ug$P@NRIGHApLkw7}ZezI]/+<^Jin=ivWp');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
