<?php
 	/*
 		Plugin Name:ADD sub menu
		Plugin URI:www.napstack.com
    Description:Just for demo plugin for add sub menu
		Author:Napstcak
		Author URI:www.napstack.com
 	 */
 	define('PLUGIN_DIR_PATH',plugin_dir_path(__FILE__));
 	define('PLUGIN_URL',plugins_url());
  define('PLUGIN_VER','1.0');

   function menu_page_cb(){
   add_menu_page(
     "custompage",
     "Custom Page",
     "manage_options",
     "custom-page1",
     "menu_page_func_cb",
     "dashicons-format-aside",
     6 );

   add_submenu_page(
    'custom-page1',
    'addnew',
    'Add New',
    'manage_options',
    'custom-page1',
    'menu_page_func_cb');

    add_submenu_page(
    'custom-page1',
    'Add All Pages',
    'All Pages ',
    'manage_options',
    'all-pages',
    'menu_page_func_second');

    }

   add_action("admin_menu","menu_page_cb");

   // this is for admin menu page
   function menu_page_func_cb(){
    include_once (PLUGIN_DIR_PATH . 'views/add-new.php');   }

   // this for second sub sub menu
   function menu_page_func_second(){
    include_once (PLUGIN_DIR_PATH . 'views/all-page.php');   }

    /*
    * Css & Js file include for amin sub menu page
    */

   function my_load_scripts() {
   wp_enqueue_style( 'my_css',    PLUGIN_URL.'/sub-menu/assets/css/style.css',PLUGIN_VER );
   wp_enqueue_script( 'admin_js',PLUGIN_URL.'/sub-menu/assets/js/admin.js', PLUGIN_VER );

   // Localize the script with new data
   $object_array = array(
    'title' => 'Napstack',
    'Author' => 'krishana',
    'ajaxurl'=>admin_url("ajax.php")
   );
   wp_localize_script( 'admin_js', 'object_name', $object_array );
 }
   add_action('init', 'my_load_scripts');



   function my_load_front() {
   wp_enqueue_script( 'custom_js',PLUGIN_URL.'/sub-menu/assets/js/script.js', PLUGIN_VER );
 }
   add_action('wp_enqueue_scripts', 'my_load_front');

/*--------------------------------------------------------------------------------
                          custom database
---------------------------------------------------------------------------------*/

// // table generating code copy paste
// function custom_plugin_tables(){
// global $wpdb;
// require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
// $sql_query_to_create_table = "CREATE TABLE `wp_krishan` (
//  `id` int(11) NOT NULL AUTO_INCREMENT,
//  `name` varchar(150) DEFAULT NULL,
//  `email` varchar(150) DEFAULT NULL,
//  `phone` varchar(150) DEFAULT NULL,
//  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
//  PRIMARY KEY (`id`)
// ) ENGINE=MyISAM DEFAULT CHARSET=latin1"; /// sql query to create table
//
// dbDelta($sql_query_to_create_table);
// }
// register_activation_hook (__FILE__,'custom_plugin_tables');

 //second tabel with codex coding with codex function

// function second_table () {
// global $wpdb;
// $table_name = $wpdb->prefix . "napstack";
// $charset_collate = $wpdb->get_charset_collate();
// $sql = "CREATE TABLE IF NOT EXISTS $table_name (
//   id mediumint(9) NOT NULL AUTO_INCREMENT,
//   name varchar(150)  NOT NULL,
//   email varchar(150)  NOT NULL,
//   phone varchar(150)  NOT NULL'
//   created_at timestamp ON UPDATE CURRENT_TIMESTAMP,
//   UNIQUE KEY id  (id)
// ) $charset_collate;";
// require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
// dbDelta( $sql );
// }
// register_activation_hook( __FILE__, 'second_table' );

//  create table own self by show create table function

function create_table(){
global $wpdb;
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
$table_name = $wpdb->prefix . "krishana";
if (count($wpdb-get_var('SHOW TABLES LIKE "wp_krishana"')) == 0){
$sql_table = "CREATE TABLE IF NOT EXISTS $table_name(
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(150) NOT NULL,
 `email` varchar(150) NOT NULL,
 `phone` varchar(150) NOT NULL,
 `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
dbDelta($sql_table);
}
}
register_activation_hook (__FILE__,'create_table');

//Drop table when plugin will be deactivate
function delete_table_deactivate(){
     global $wpdb;
     $table_name = $wpdb->prefix . 'krishana';
     $sql = "DROP TABLE IF EXISTS $table_name";
     $wpdb->query($sql);
     $the_post_id = get_option("custom_plugin_page_id");
     if(!empty($the_post_id)){
        wp_delete_post($the_post_id,true);
     }
}
register_uninstall_hook( __FILE__, 'delete_table_deactivate' );

//Delete table when plugin will be delete
function delete_table_delete_plugin() {
     global $wpdb;
     $table_name = $wpdb->prefix . 'krishana';
     $sql = "DROP TABLE IF EXISTS $table_name";
     $wpdb->query($sql);
     $the_post_id = get_option("custom_plugin_page_id");
     if(!empty($the_post_id)){
        wp_delete_post($the_post_id,true);
     }
}
register_deactivation_hook( __FILE__, 'delete_table_delete_plugin' );

function create_page(){
  // code for create page
  $page = array();
  $page['post_title']= "Custom Plugin Online Web Tutor";
  $page['post_content']= "Learning Platform for Wordpress Customization for Themes, Plugin and Widgets";
  $page['post_status'] = "publish";
  $page['post_slug'] = "custom-plugin-online-web-tutor";
  $page['post_type'] = "page";

  $post_id = wp_insert_post($page); // post_id as return value

  add_option("custom_plugin_page_id",$post_id);  // wp_options table from the name of custom_plugin_page_id
}
register_activation_hook(__FILE__,"create_page");

 ?>
