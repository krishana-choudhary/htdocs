<?php
/**
 * The template for displaying all pages
 */
 /* Template Name: contact
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package napstackunderscores
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="main" class="container site-main">
		 <div class=" contact-footer col-md-5">
       <?php page_call(); ?>
       <?php
 			while ( have_posts() ) : the_post();
 				get_template_part( 'template-parts/contact', 'page' );
 			endwhile; // End of the loop.
 			?>

    </div>
      <div class="col-md-7">
      <div class="google-map">
     <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style>
     <div class='embed-container'><iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387144.0075834208!2d-73.97800349999999!3d40.7056308!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY!5e0!3m2!1sen!2sus!4v1394298866288' width='600' height='450' frameborder='0' style='border:0'></iframe>
     </div>
   </div>
 </div>

    </div><!-- #main -->
	</div><!-- #primary -->
</br>

<?php
get_footer();
