<?php
function portfoio_thumb( $atts ) {
$atts = shortcode_atts( array(
), $atts, 'portfoios_bg' );

  $args = array(
    'post_type'     => 'portfolio',
    'posts_per_page'=>  4
  );

  $the_query = new WP_Query( $args );

  ob_start(); ?>
  <div class="<?php echo $rtl_classes; ?>">
    <?php if ( $the_query->have_posts() ) : ?>
    <div class="owl-carousel test-carousel">
      <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
      <div class="post-background" style="background-image: url(<?php echo get_the_post_thumbnail_url();?>)"></div>
      <?php endwhile; ?>

    <?php else : ?>

      <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
    </div><!--End .cube-carousel-post-->


    <?php endif; ?>
    <?php wp_reset_postdata(); ?>
  </div><!--End .cube-carousel-wrap-->
<?php
   return ob_get_clean();

}
add_shortcode( 'portfoios_bg', 'portfoio_thumb' );
?>
