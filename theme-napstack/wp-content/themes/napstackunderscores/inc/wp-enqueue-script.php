<?php
/**
 * Enqueue scripts and styles.
 */
function napstackunderscores_scripts() {
	wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/js/jquery.js');
	wp_enqueue_style( 'napstackunderscores-style', get_stylesheet_uri() );

	wp_enqueue_script( 'napstackunderscores-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'napstackunderscores-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_style('bootstrap-css',get_template_directory_uri().'/css/bootstrap.min.css');
	wp_enqueue_script( 'bootstrap-jquery', get_template_directory_uri() . '/js/bootstrap.min.js');
	wp_enqueue_style('slippry-css',get_template_directory_uri().'/css/slippry.css');
	wp_enqueue_script( 'slippry-jquery', get_template_directory_uri() . '/js/slippry.min.js');
	wp_enqueue_script( 'script-js', get_template_directory_uri() . '/js/script.js');
	wp_enqueue_style( 'owl-css', get_template_directory_uri() . '/css/owl.carousel.min.css');
	wp_enqueue_style( 'owl-min', get_template_directory_uri() . '/css/owl.theme.default.css');
	wp_enqueue_style( 'f-a', get_template_directory_uri() . '/css/font-awesome.mins.css');
	wp_enqueue_script( 'owl-jquery', get_template_directory_uri() . '/js/owl.carousel.min.js');
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/js/owl.carousel.js');
	wp_enqueue_script( 'shy', get_template_directory_uri() . '/js/sly.min.js');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'napstackunderscores_scripts' );
?>
