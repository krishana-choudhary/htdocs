<?php
/* date current year */

function current_year_cb() {

	return date('Y');
}
add_shortcode('current_year', 'current_year_cb');

/* this is calling function */
function current_year_cb2(){
	$var1 = date('Y');
	return $var1;
}
/* ancestor function is for child pages */
function get_top_ancestors_id(){
	global $post;
  if($post->post_parent){
	$ancestors = array_reverse(get_post_ancestors($post->ID));
	return $ancestors[0];
}
	return $post->ID;
}

/* excerpt length */
function ld_custom_excerpt_length() {
    return 20;
}
add_filter( 'excerpt_length', 'ld_custom_excerpt_length');

// add post-formats to post_type 'page'
add_action('init', 'my_theme_slug_add_post_formats_to_page', 11);

function my_theme_slug_add_post_formats_to_page(){
    add_post_type_support( 'page', 'post-formats' );
    register_taxonomy_for_object_type( 'post_format', 'page' );
}

add_theme_support( 'post-formats', array( 'aside', 'gallery','link' ) );


/**
 * Implement the Custom Header feature.
 */

require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Customizer additions.
 */
require get_template_directory(). '/inc/page-info.php';

/* custom post type */
require get_template_directory() . '/inc/register-post-type.php';

/* shortcode-portfolio */
require get_template_directory().'/inc/shortcode-portfolio.php';

/* wp-enqueue-scripts */
require get_template_directory().'/inc/wp-enqueue-script.php';

/* registered-functionality */
require get_template_directory().'/inc/register-functionality.php';


/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}



// This function is use for as a all hero section this is for only test this functions iis not in use this theme
 	function ebor_pluto_hero_header( $title = '',$subtitle =''  ){
    $title = ( $title ) ? '<h3 class="hero-title uppercase">' .$title. '</h3>' : '';
    $subtitle = ( $subtitle ) ? '<p class="hero-sub-title uppercase">' .$subtitle. '</p>' : '';

    $output = sprintf('
      <section class="wrapper hero-background-image">
        <div class="container">
          <div class="row">

              <div class="col-md-12 text-center">
                  %s
                  %s
              </div>

          </div>
      </div>
    </section>', $title, $subtitle);

 		return $output;

 	}
