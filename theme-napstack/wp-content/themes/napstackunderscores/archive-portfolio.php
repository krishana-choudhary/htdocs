<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package napstackunderscores
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="main" class=" archive-page container site-main">
			<div class="col-md-2">
			</div>
    <div class="col-md-8">
			hello custom archive
		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();?>
			<div class="portfolio-thumb col-md-4">
				<a href="<?php the_permalink();?>">
			 <?php the_post_thumbnail(); ?>
			</a>
		</div><?Php
			endwhile;
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

	</div><!-- #main -->
</div>
	</div><!-- #primary -->
<div class="col-md-2">
</div>
<?php
get_footer();
