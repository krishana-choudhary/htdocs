<?php
// page include on function.php
// define('FC_PATH', get_stylesheet_directory());
// //nothing
//
// //page-include
// include(FC_PATH . '/vc_blocks/vc_section_title_block.php');

 /**
  * Callback function for [section_titl] shortcode
  *
  * @param array - $atts - array of passed attributes
  * @param string - $content - content passed in between shortcide tags
  * @return string - shortcode generated HTML
  */
 function section_title_cb($atts,$content) {

 	$atts = shortcode_atts( array(
		'title'     => '',
 		'background_title'=> '',
 		'align'     => 'left'
 	), $atts, 'section_title' );
	ob_start(); ?>

	 <div class="section-title-wrapper <?php echo 'align-' . $atts['align'];?>">

			 <h3 class="background-title"><?php echo $atts['background_title']; ?></h3>

				<h2 class="section-title"><?php echo $atts['title']; ?></h2>

			<div class="section-content">

				<?php echo $content; ?>

			</div>
		</div><!-- END .section-wrapper -->
	<?php
	return ob_get_clean();

 }
 add_shortcode('section_title', 'section_title_cb');

 /**
  * The VC Functions
  */
 function section_title_vc() {
 	vc_map(
 		array(
 			"icon" => 'foundry-vc-block',
 			"name" => __("Section Title", 'foundry'),
 			"base" => "section_title",
 			"category" => __('Foundry WP Theme', 'foundry'),
 			'description' => 'Section Module with icon and text.',
 			"params" => array(
			array(
				"type" => "dropdown",
				"heading" => __("Align", 'foundry'),
				"param_name" => "align",
				"value" => array(
					'Left' => 'left',
					'Center' => 'center',
					'Right' => 'right'
				),
				'description' => 'Alignment of the section.'
				),
				array(
					"type" => "textfield",
					"heading" => __("Background Title", 'foundry'),
					"param_name" => "background_title",
					'description' => 'Background title of the section.'
				),
				array(
					"type" => "textfield",
					"heading" => __("Title", 'foundry'),
					"param_name" => "title",
					'description' => 'Title of the section.'
				),
				array(
					"type" => "textarea_html",
					"heading" => __("Block Content", 'foundry'),
					"param_name" => "content",
					'holder' => 'div'
				),
 			)
 		)
 	);
 }
 add_action( 'vc_before_init', 'section_title_vc' );



// content with img

/**
 * Callback function for [blurb] shortcode
 *
 * @param array - $atts - array of passed attributes
 * @param string - $content - content passed in between shortcide tags
 * @return string - shortcode generated HTML
 */
function blurb_cb($atts = [], $content = '') {

 $atts = shortcode_atts( array(
   'thumb'     => '',
   'bg'        => 'pink',
   'size'      => 'small',
   'align'     => 'left'
 ), $atts, 'cube_blurb' );

 $thumbnail = ( $atts['thumb'] ) ? wp_get_attachment_url($atts['thumb']) : get_stylesheet_directory_uri() . '/img/placeholder-thumb.png';

 ob_start(); ?>

  <div class="blurb-wrapper <?php echo 'align-' . $atts['align'] . ' ' . 'size-' . $atts['size']; ?>">
    <div class="blurb-inner">
      <div class="blurb-icon" style="background-image:url(<?php echo $thumbnail; ?>);"></div>
      <div class="blurb-content <?php echo 'bg-' . $atts['bg']; ?>">
        <?php echo $content; ?>
      </div><!-- END .blurb-content -->
    </div><!-- END .blurb-inner -->
 </div><!-- END .blurb-wrapper -->
 <?php
 return ob_get_clean();

}
add_shortcode('cube_blurb', 'blurb_cb');

/**
 * The VC Functions
 */
function cube_blurb_vc() {
 vc_map(
   array(
     "icon" => 'foundry-vc-block',
     "name" => __("Cube Blurb", 'foundry'),
     "base" => "cube_blurb",
     "category" => __('Foundry WP Theme', 'foundry'),
     'description' => 'Blurb Module with icon and text.',
     "params" => array(
       array(
         "type" => "attach_image",
         "heading" => __("Icon Image", 'foundry'),
         "param_name" => "thumb"
       ),
       array(
         "type" => "dropdown",
         "heading" => __("Content Background color", 'foundry'),
         "param_name" => "bg",
         "value" => array(
           'Pink' => 'pink',
           'White'=> 'white',
           'Shadow'=>'shadow',
           'Grey' => 'grey'
         ),
         'description' => 'Background color of the blurb.'
       ),
       array(
         "type" => "dropdown",
         "heading" => __("Size", 'foundry'),
         "param_name" => "size",
         "value" => array(
           'Small' => 'small',
           'Medium' => 'medium',
           'Large' => 'large'
         ),
         'description' => 'Size of the blurb.'
       ),
       array(
         "type" => "dropdown",
         "heading" => __("Align", 'foundry'),
         "param_name" => "align",
         "value" => array(
           'Left' => 'left',
           'Center' => 'center',
           'Right' => 'right'
         ),
         'description' => 'Alignment of the blurb.'
       ),
       array(
         "type" => "textarea_html",
         "heading" => __("Block Content", 'foundry'),
         "param_name" => "content",
         'holder' => 'div'
       ),
     )
   )
 );
}
add_action( 'vc_before_init', 'cube_blurb_vc' );
