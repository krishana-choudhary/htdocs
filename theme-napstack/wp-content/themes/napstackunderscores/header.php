<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package napstackunderscores
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'napstackunderscores' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="small-header">
			<div class="row">
			<div class="container">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="site-branding-1">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-2',
							'menu_id'        => 'custom-Primary',
						) );
					?>
				</div><!-- .site-branding -->
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="blog-title">
       <h1><a href="<?php the_permalink();?>"><?php bloginfo( 'name' ); ?></a></h1>
        <p><?php bloginfo('description')?>
			</div>
		</div>
	</div>
</div>
		</div><!--end small-header-->


		<nav id="site-navigation" class="main-navigation">
       <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'napstackunderscores' ); ?></button>
			 <div class="row">
			<div class="container">
				<div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>
		</div>
		</div>
	</div>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->


	<div id="content" class="container site-content">
