<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package napstackunderscores
 */

?>

	</div><!-- #content -->
<div class="footer-bg">
	<footer id="colophon" class="site-footer container">
		<div class="site-info">
			 <div class="col-md-4">
				<?php

					if( !is_page(64) ) {
						dynamic_sidebar( 'footer 1' );

					} else {
						dynamic_sidebar('footer 4');
					}
					?></div>
				<div class="col-md-4">
				  <?php dynamic_sidebar( 'footer 2' ); ?>
				</div>

				<div class="col-md-4">
				  <?php dynamic_sidebar( 'footer 3' ); ?>
				</div>

	   <!-- this function is default php <p class="center">copyright &copy <?php echo date("Y"); ?> Napstack</p>-->
	   <!-- this is shortcode functuon <p class="center">copyright &copy  <?php echo do_shortcode('[current_year]'); ?> Napstack</p>-->
		 <p>copyright &copy <?php echo current_year_cb2(); ?> Napstack</p>


	  </div><!-- .site-info -->
	 </footer><!-- #colophon -->
	</div><!-- #page -->


<?php wp_footer(); ?>

</body>
</html>
