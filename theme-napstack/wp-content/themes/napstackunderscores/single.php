<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package napstackunderscores
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="main" class="container site-main">
     <div class="col-md-2">
		 </div>
		 <div class="col-md-8">
			 <!-- <p>this is for author post 1 </p> -->
			 <!-- <?php echo get_avatar(get_the_author_meta('ID')); ?>
			 <?php echo get_the_author_meta('nickname'); ?> -->

			 <!-- <p>this is for author post 2</p> -->
			 <!-- <?php $args = array(
				 'author'=> get_the_author_meta('ID'),
				 'posts_per_page'=>3,
				 'post__not_in'=>array(get_the_ID())
			 );
			 $the_query = new WP_Query( $args );

			 if ($the_query->have_posts()):?>
			 <div class="other-posts">
			 <h4>other posts by <?php echo get_the_author_meta('nickname') ?></h4>
		 </div>--><?php

		 			//this is author post 3
			// 	while ($the_query->have_posts() ): $the_query->the_post();?>
			<!-- // 	<div class="other-posts">
			// 	<a href="<?php the_permalink()?>">
			// 		<?php the_title(); ?>
			// 	</a>
			// </div> -->

		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'template-parts/content', get_post_type() ); ?>
<?php			
			the_post_navigation([
            'prev_text'=> 'Prev <i class="fa fa-arrow-left" aria-hidden="true"></i>',
            'next_text'=> '<i class="fa fa-arrow-right" aria-hidden="true">Next</i>',
        ]);

		//  	the_post_navigation([
		//  		'prev_text' => 'Prev <i class="ti-arrow-left"></i>',
		//  		'next_text' => '<i class="ti-arrow-right"></i> Next',
		//  ]);?><?php

            // this function is also pagignation with text
			//the_post_navigation();

		 endwhile; // End of the loop.?>
	 <?php endif; ?>

	   <!--this is for author post 4-->
	 <!-- <?php if (count_user_posts(get_the_author_meta('ID')) > 3): ?>
		 <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'))?>">read more</a>-->

	 <?php endif; ?>
		<div class="col-md-2">
		</div>
	</div>
	</div><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
