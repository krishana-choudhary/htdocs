<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package napstackunderscores
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="main" class="container site-main">
    <div class="col-md-2">
    </div>

  <div class="col-md-8">
   <div class="cat-main-title">
     <h4>This Is From Single Portfolio Post</h4>
    </div>
		<?php
		while ( have_posts() ) : the_post();?>
		<div class="portfoilo-title"><h3><?php the_title(); ?></h3></div>
		<div class="portfoilo-thumb"><?php the_post_thumbnail(); ?></div>
		<?php
		endwhile; // End of the loop.
		?>
  </div>
	
<div class="col-md-2">
</div>
	</div><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
