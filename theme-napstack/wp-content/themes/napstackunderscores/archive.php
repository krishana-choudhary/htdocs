<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package napstackunderscores
 */
 if (is_category()) {
	 echo 'this is category:archive';
 }elseif (is_author()){
 	echo 'this is author:archive';
}elseif (is_month()) {
	echo 'this is month:archive';
}elseif (is_day()) {
	echo 'this is date:archive';
} else {
	echo 'archive';
}
get_header(); ?>

	<div id="primary" class="content-area">
		<div id="main" class=" archive-page container site-main">
			<div class="col-md-2">
			</div>
    <div class="col-md-8">
			hello archive
		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="archive-description">', '</div>' );
				?>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

	</div><!-- #main -->
</div>
	</div><!-- #primary -->
<div class="col-md-2">
</div>
<?php
get_footer();
