<?php
// with perameter you need to put on after url / ?sourceurl=goosedogdesigns
// llike this - tallcube.com/your-questions-answered/do-you-only-work-with-businesses-in-san-francisco/?sourceurl=goosedogdesigns

	if( !isset( $_GET['sourceurl'] ) ) return;

	if( $_GET['sourceurl'] != 'goosedogdesigns' ) return;

	?>
	<div class="foundry_modal text-center image-bg overlay" data-time-delay="1000" modal-link="3585"><i class="ti-close close-modal"></i>
		<div class="background-image-holder">
			<img width="800" height="534" src="http://29apzu3lfdp42eosoq34qlff.wpengine.netdna-cdn.com/wp-content/uploads/sites/14/2015/07/Unknown-12.jpeg" class="background-image" alt="" srcset="http://29apzu3lfdp42eosoq34qlff.wpengine.netdna-cdn.com/wp-content/uploads/sites/14/2015/07/Unknown-12.jpeg 800w, http://29apzu3lfdp42eosoq34qlff.wpengine.netdna-cdn.com/wp-content/uploads/sites/14/2015/07/Unknown-12-500x334.jpeg 500w, http://29apzu3lfdp42eosoq34qlff.wpengine.netdna-cdn.com/wp-content/uploads/sites/14/2015/07/Unknown-12-768x513.jpeg 768w, http://29apzu3lfdp42eosoq34qlff.wpengine.netdna-cdn.com/wp-content/uploads/sites/14/2015/07/Unknown-12-600x400.jpeg 600w"
			  sizes="(max-width: 800px) 100vw, 800px" />
		</div>

		<div class="wpb_text_column wpb_content_element ">
			<div class="wpb_wrapper">
				<h3 style="text-align: center;">Looking for Goose Dog Designs?</h3>
				<p class="lead mb48" style="text-align: center;">You've come to the right place. The Goose Dog has a new coat and a new name.

Welcome to Tall Cube. We're glad you could make it.</p>
			<a href="#" class="close-modal btn" style="position:relative;right:0;">Got it!</a>
			</div>
		</div>
	</div>
	<?php
}
add_action('wp_footer', 'gdd_redirect_popup');

?>
