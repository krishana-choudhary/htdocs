<?php
/**
* The template for displaying all pages
*/
/* Template Name: homepage
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package napstackunderscores
*/

get_header(); ?>
<div id="primary" class="content-area">
 <div id="main" class="container site-main">
  <div class=" sidebar-1 col-md-8"><?php
    while ( have_posts() ) : the_post();
    get_template_part('template-parts/home', 'page');
    endwhile; // End of the loop.
    ?>
    <div class="slider">
    <div class="home-slider">
     <ul id="slippry">
      <li><a href="#slide1"><img src="<?php echo get_template_directory_uri()?>/img/image-1.jpg" alt="<?php the_title();?>"></a></li>
      <li><a href="#slide2"><img src="<?php echo get_template_directory_uri()?>/img/image-2.jpg"  alt="<?php the_title();?>"></a></li>
      <li><a href="#slide3"><img src="<?php echo get_template_directory_uri()?>/img/image-4.jpg" alt="<?php the_title();?>"></a></li>
      </ul>
    </div>
    </div>
    </br>
  <!--slider from post-->
  <div class="cat-main-title">This Is From Post Portfolio </div>
   <div class="slider-2">
    <ul id="slippry-1"><?php
      $args=array(
      'post_type'=>'portfolio',
      'posts_per_page'=>3
      );
      $the_query = new WP_Query( $args );
      if ( $the_query->have_posts() ):
      while ( $the_query->have_posts() ): $the_query->the_post();?>
      <?php $thumbnail =get_post_thumbnail_id(); ?>
      <?php $id = wp_get_attachment_image_src( $thumbnail,'full', true )?>

      <li class="img">
        <a href="#slide1"><img src="<?php echo $id[0] ?>"alt="<?php the_title();?>">
        </a>
      </br>
      <div class="content"><?php the_content(); ?></div>
      </li>
      <?php
      endwhile;
      endif;?>
    </ul>
  </div>

</br>

  <div class="cate-post">
    <div class="cat-main-title">
      <h4>This Is From Category Posts</h4>
    </div>

      <?php
      $args=array('cat'=>2,'posts_per_page'=>2);
      $the_query = new WP_Query( $args );
      while ($the_query->have_posts() ) :$the_query->the_post();?>

      <div class="cat-title col-md-8">
      <h3><?php the_title(); ?></h3>
      <p><?php the_content(); ?>
      </div>
      <div class="cat-thumb col-md-4">
      <?php the_post_thumbnail(); ?>

      </div><?php
      endwhile; // End of the loop.
      ?>
  </div>

  <div class="page-post">
    <div class="page-main-title-2 col-md-12">
      <h4>This Is From Page Id</h4>
    </div>

      <?php
      $args=array('page_id'=>61);
      $the_query = new WP_Query( $args );
      while ($the_query->have_posts() ) :$the_query->the_post();
      ?>

      <div class="page-title col-md-8">
      <h3><?php the_title(); ?></h3>
      <p><?php the_content(); ?>
      </div>

      <div class="page-thumb col-md-4">
      <?php the_post_thumbnail(); ?>
      </div><?php
      endwhile; // End of the loop.
      ?>
  </div>
</div><!--end col-8-->

  <div class="home-sidebar sidebar-1 col-md-4">
    <?php get_sidebar(); ?>
  </div>

</div><!--end container-->

<!--slider from - post-->
  <div class="container">
    <div class="col-md-8">
      <div class="owl-2">
        <div class="cat-main-title">This Is From Post Portfolio-bg</div>

        <?php
        $args=array(
        'post_type'=>'portfolio',
        'posts_per_page'=>3
        );
        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ):?>
        <div class="owl-carousel test-carousel">
          <?php
        while ( $the_query->have_posts() ): $the_query->the_post();?>
        <div class="post-background" style="background-image: url(<?php echo get_the_post_thumbnail_url();?>)"></div>
        <?php
        endwhile;
        ?></div><?php
        endif;?>

    </div><!-- owl- -->
  </div><!-- col-md-8 -->

<div class="col-md-4">
</div>
</div><!--end .container-->
  <div class="container">
    <div class="col-md-8">
      <div class="col-md-8">

      <?php
      //function classes with array foreach
      function get_add_classes($width){
      $class ='';
      if ($width >= 0 && $width <= 20) { $class ='warning';}

      elseif ($width > 21 && $width <= 40) {$class ='info';}

      elseif ($width > 41 && $width <= 60) {$class ='success';}

      return $class;
      }

      $items =[
      ['label' =>'hello','value' => 5],
      ['label' =>'hello world','value' => 10],
      ['label' =>'hello world again','value' => 15],
      ['label' =>'hello world again & again','value' => 20],
      ];

      foreach ($items as $item ):
      ?>
      <div class="progress">
      <div class="progress-bar progress-bar-<?php echo get_add_classes($item['value']);?>" role="progressbar" aria-valuenow="70"
      aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $item['value'];?>%">
      </div>
      </div>
      <p><small><?php echo $item['label'];?></small></p>
      <?php
      endforeach;
      ?>
         </div><!--end-col-8-->

     <div class="col-md-4">
      <?php

         $args=array(
         'post_type'=>'portfolio',
         'posts_per_page'=>3,
         //'tax_query' => array(                     //(array) - use taxonomy parameters (available with Version 3.1).
         //   'relation' => 'AND',                      //(string) - Possible values are 'AND' or 'OR' and is the equivalent of ruuning a JOIN for each taxonomy
         //     array(
         //       'taxonomy' => 'portfolio_category',                //(string) - Taxonomy.
         //       'field' => 'id',                    //(string) - Select taxonomy term by ('id' or 'slug')
         //       'terms' => array( 7 ),    //(int/string/array) - Taxonomy term(s).
         //       'include_children' => true,           //(bool) - Whether or not to include children for hierarchical taxonomies. Defaults to true.
         //       'operator' => 'IN'                    //(string) - Operator to test. Possible values are 'IN', 'NOT IN', 'AND'.
         //     )
         //   ),
         );

        $the_query = new WP_Query( $args );
        if ( $the_query->have_posts() ):?>
         <div class="portfolio-cat"> <?php
          while ( $the_query->have_posts() ): $the_query->the_post();?>

          <div class="box-category">
           <?php $term = wp_get_post_terms(get_the_ID(), 'portfolio_category');?>
           <?php foreach( $term as $ter ) : ?>
							<li class="term-item"><a href="<?php echo get_term_link($ter); ?>"><?php echo $ter->name; ?></a></li>
						<?php endforeach; ?>
            </div>


          <?php endwhile;
         ?></div><?php
        endif;?>

        <?php
          $test = array('Krishana'=>1,'Napstack'=>2,'Com'=>3);
          $result = '';
        ?>
        <?php foreach ($test as $key => $key1) : ?>
          <?php $result .= $key.$key1 ?>
        <?php endforeach; ?>
        <?php echo $result ?>

        <?php
        $args=array(
        'post_type'=>'portfolio',
        'posts_per_page'=>3,
        'post__not_in' => [get_the_id()]        );

       $the_query = new WP_Query( $args );
       if ( $the_query->have_posts() ):?>
        <div class="portfolio-cat"><?php
         while ( $the_query->have_posts() ): $the_query->the_post();?>
         <div class="box-category">
          <?php $term = wp_get_post_terms(get_the_ID(),'portfolio_category');?>
          <?php foreach( $term as $ter ) : ?>
             <li class="term-item"><a href="<?php echo get_term_link($ter); ?>"><?php echo $ter->name; ?></a></li>
           <?php endforeach; ?>
           </div>
           <?php endwhile;
          ?></div><?php
         endif;?>
         <?php

         //get_term function use
         $termss = get_terms( array(
       		'taxonomy' => 'portfolio_category',
       		'hide_empty' => true,
       	) );?>
          <?php foreach( $termss as $termi ) : ?>
            <?php echo $termi->name; ?>
          <?php endforeach; ?>

      </div><!-- col-4 -->
    </div><!-- col-8 -->
</div><!-- container -->

<!-- <div class="section-4" style="background-image:url(<?php echo get_template_directory_uri()?>/img/image-1.jpg)">
  <p> hello </p>
</div> -->
</div><!-- #main -->
</div><!-- #primary -->

<?php
get_footer();
