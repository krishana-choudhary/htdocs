<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package napstackunderscores
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
    <div class="napstack-title">
			<h3><?php the_title(); ?></h3>
		</div>
		<div class="napstack-title">
			<?php the_content(); ?>
		</div>
	</header><!-- .entry-header -->

	<footer class="entry-footer">
		<?php napstackunderscores_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
