<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package napstackunderscores
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		endif;

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php napstackunderscores_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->
	<?php if (is_single()){
     napstackunderscores_post_thumbnail();
	 }else {
		 ?><div class="postss-img">
		<?php napstackunderscores_post_thumbnail(); ?>
	</div><?php
	 }
	 ?>
	<div class="entry-content">
		<?php
		  if (is_single()) {
			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'napstackunderscores' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );}
			else {?>

			  <div class="ecerpt-link">
					<?php
						/*
						* Custom Excerpt Length WordPress using wp_trim_excerpt()
						*/
						$content = get_the_content();
						echo wp_trim_words( $content , '10' );
						?>

					<p>
						<!-- this is used by functons -->
					<?php echo get_the_excerpt();?>

					 <?php the_content('read More &raquo;'); ?>

					<a href="<?php the_permalink() ?>">Read More</a>
				</p>

				</div>
				<?php the_category(); ?>

				<div class="custom-excerpt">
					<?php
						$a = get_the_excerpt();
						$b = 'this is for get test';
						$c = $a.$b;
						echo $c ;
				 ?>
				</div>

				<?php $var1 = get_the_category();?>
				<?php $var2 = '<i class="fa fa-wordpress" aria-hidden="true"></i>';?>
				<?php $var3 = 'This Is your Post Category: ';?>

				<?php if ($var1): ?>
				<?php
					foreach ($var1 as $key) {
						$var3.=$var2 ." ". $key->cat_name;
					 }
					 echo $var3."</br>";
					endif; ?>

			 	 <?php $var1 = get_the_category();?>
				 <?php $var2 = ',';?>
				 <?php $var3 = '';?>

				 <?php if ($var1): ?>
				 <?php
					 foreach ($var1 as $key) {
						 $var3.=$key->cat_name.$var2;
						}
						echo trim($var3,$var2);

				?>
				<?php endif; ?>

				<?php }
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'napstackunderscores' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php napstackunderscores_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
