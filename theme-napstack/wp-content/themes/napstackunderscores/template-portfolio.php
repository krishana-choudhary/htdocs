<?php
/**
 * The template for displaying all pages
 */
 /* Template Name: portfolio
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package napstackunderscores
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="main" class="container site-main">
    <div class="cate-post col-md-8">
      <div class="cat-main-title">
        <h4>This Is From Portfolio Template Posts</h4>
      </div>
     <?php
     $queryvar = get_query_var('paged');

     $args=array(
       'post_type'=>'portfolio',
        'posts_per_page'=>3,
        'paged'=>$queryvar
   );
     $the_query = new WP_Query( $args );

     while ($the_query->have_posts() ) :$the_query->the_post();
       ?>
       <div class="portfolio-thumb col-md-4">
         <a href="<?php the_permalink();?>">
        <?php the_post_thumbnail(); ?>
      </a>
      <?php the_title(); ?>
       </div><?php
     endwhile; // End of the loop.?>
     <?php
      // this is for custom pagination
      echo paginate_links(array(
        'total'=>$the_query->max_num_pages,
        'prev_text'=> __('<i class="fa fa-arrow-left" aria-hidden="true">Prev</i>'),
        'next_text'=> __('<i class="fa fa-arrow-right" aria-hidden="true">Next</i>'),
      ));

      ?>
     <!-- <p>Pagignation with next previous text</p> -->
     <!-- <div class="post-link">
     <?php next_posts_link('Next &raquo;',$the_query->max_num_pages); ?>
     <?php previous_posts_link('Previous &raquo;'); ?>
     </div> -->


     </div><!--end col-8-->
			<div class="home-sidebar sidebar-1 col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
