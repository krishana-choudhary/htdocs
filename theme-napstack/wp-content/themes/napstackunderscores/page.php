<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package napstackunderscores
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="main" class="container site-main">
     <div class=" sidebar-1 col-md-2">
		 </div>
		 <div class=" sidebar-1 col-md-8">
			<?php
			page_call();
			while ( have_posts() ) : the_post();

				?>
				<div id="main-content">
					<div class="content-inner">
					<?php
				get_template_part( 'template-parts/content', 'page' );
				?>
					</div>
				</div>
				<?php

			endwhile; // End of the loop.
		 ?><div class="page-list">
			<?php
				$args = array(
					'child_of' => get_top_ancestors_id(),
					'title_li' => ''
				);
				wp_list_pages($args);
  	?>
    </div>
		</div>
			<div class=" sidebar-1 col-md-2">
			</div>
		</div><!-- #main -->
	</div><!-- #primary -->

	<script type="text/javascript">

			jQuery('.page-list li a').on('click', function(event) {

					var item = jQuery(this),

							href = item.attr('href') + ' .content-inner';

					$('#main-content').load(href);

					event.preventDefault();
			});

	</script>
<?php
get_footer();
