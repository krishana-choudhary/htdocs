<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'prac_gene_wpbb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'n_CCJ!s9:QaHRB)9*sSvX@RF $sC88zU5.y{zrIPBL.g#!94p~:Y}G[/h:Tk`;D[');
define('SECURE_AUTH_KEY',  '~G7(f{BCnt2{I-hygt^}r2F:ulS;H1KX9qPf-m5cdL`={]yPwzr7{r0D |QAW*~H');
define('LOGGED_IN_KEY',    'n&ii0=52UuR]eg;Qn(r )g:^<uYmQW6<eeD8+Xkxap~N8`8CWs~BTWMZ>1P4:lK(');
define('NONCE_KEY',        's/lVH?UKMLjV/jv6Qyh58_i$&w@vKvq#U 717aKz:HieiLjSU/ymKUg/f-bK8aC(');
define('AUTH_SALT',        'B168!~ /Y?=}+C@eb~]Z,*UQq b!EvZp%5DR.wa]qLx1!r8ycDu<oXRn=AlD75nx');
define('SECURE_AUTH_SALT', 'yD;sjSgMRzrH^x]RF:Tv>8cTq^pwu%,7FU9b(pv9knsuXSfQzN1[s$}+N %v3JOA');
define('LOGGED_IN_SALT',   ')Eb6jIH1}/EpKyy)DfzGDA}$u{%/ubc=TY58=<yAAEyyRH]Atvk DzpGD8g!Ay1s');
define('NONCE_SALT',       '!)To^G):%a)=p,&rlZ^_b<_`zNSaB%NH/{l31#@7&^uHhwpkd-XkabIe6MPW;`m6');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
