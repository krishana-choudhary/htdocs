<?php

/**
 * Fired during plugin activation
 *
 * @link       krishana@napstack.com
 * @since      1.0.0
 *
 * @package    Prac_Gene_Wppb
 * @subpackage Prac_Gene_Wppb/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Prac_Gene_Wppb
 * @subpackage Prac_Gene_Wppb/includes
 * @author     Napstack <krishana@napstack.com>
 */
class Prac_Gene_Wppb_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
