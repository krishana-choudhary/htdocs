<?php

/**
 * Fired during plugin deactivation
 *
 * @link       krishana@napstack.com
 * @since      1.0.0
 *
 * @package    Prac_Gene_Wppb
 * @subpackage Prac_Gene_Wppb/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Prac_Gene_Wppb
 * @subpackage Prac_Gene_Wppb/includes
 * @author     Napstack <krishana@napstack.com>
 */
class Prac_Gene_Wppb_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
