<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       krishana@napstack.com
 * @since      1.0.0
 *
 * @package    Prac_Gene_Wppb
 * @subpackage Prac_Gene_Wppb/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Prac_Gene_Wppb
 * @subpackage Prac_Gene_Wppb/includes
 * @author     Napstack <krishana@napstack.com>
 */
class Prac_Gene_Wppb_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'prac-gene-wppb',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
