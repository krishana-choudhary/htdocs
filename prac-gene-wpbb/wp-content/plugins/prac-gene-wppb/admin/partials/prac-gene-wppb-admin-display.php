<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       krishana@napstack.com
 * @since      1.0.0
 *
 * @package    Prac_Gene_Wppb
 * @subpackage Prac_Gene_Wppb/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
