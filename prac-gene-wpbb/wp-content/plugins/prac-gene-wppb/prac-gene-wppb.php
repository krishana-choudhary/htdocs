<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              krishana@napstack.com
 * @since             1.0.0
 * @package           Prac_Gene_Wppb
 *
 * @wordpress-plugin
 * Plugin Name:       Prac Gene Wppb
 * Plugin URI:        www.napstack.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Napstack
 * Author URI:        krishana@napstack.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       prac-gene-wppb
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-prac-gene-wppb-activator.php
 */
function activate_prac_gene_wppb() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-prac-gene-wppb-activator.php';
	Prac_Gene_Wppb_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-prac-gene-wppb-deactivator.php
 */
function deactivate_prac_gene_wppb() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-prac-gene-wppb-deactivator.php';
	Prac_Gene_Wppb_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_prac_gene_wppb' );
register_deactivation_hook( __FILE__, 'deactivate_prac_gene_wppb' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-prac-gene-wppb.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_prac_gene_wppb() {

	$plugin = new Prac_Gene_Wppb();
	$plugin->run();

}
run_prac_gene_wppb();
