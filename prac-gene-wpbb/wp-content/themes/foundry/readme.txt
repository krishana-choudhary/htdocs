===================================================================
July 21st 2017 - v2.0.10
-------------------------------------------------------------------

* FIXED: Multiple [gallery] masonry feeds in the same page
* FIXED: Stars display when leaving product rating in WooCommerce
* UPDATED: Increased amount of social icons allowed in header
* UPDATED: Increased amount of social icons allowed in footer
* UPDATED: body class function in header.php is cleaned up
* ADDED: New theme option for header, specify scroll distance before follow header appears
* ADDED: data-fixed-at markup to each header layout in /inc/

===================================================================
June 13th 2017 - v2.0.9
-------------------------------------------------------------------

* FIXED: header CTA button going invisible on scroll
* FIXED: Screen reader text
* FIXED: Double arrow on select items in WooCommerce Checkout
* ADDED: new header layout - centered header without utility bar
* ADDED: new header layout - transparent centered header without utility bar
* ADDED: New blog layout, big alternating sections with featured images.

===================================================================
May 19th 2017 - v2.0.8
-------------------------------------------------------------------

* FIXED: Twitter feeds
* UPDATED: Visual composer minimum version
  
===================================================================
April 21st 2017 - v2.0.7
-------------------------------------------------------------------

* UPDATED: Visual composer minimum version
* UPDATED: Ebor Framework minimum version
* UPDATED: WooCommerce minimum version
* UPDATED: WooCommerce template files for 3.x
* UPDATED: WooCommerce product images now use the new zoom gallery
* ADDED: Support for new WooCommerce
* ADDED: Theme auto updates (via Ebor Framework update)