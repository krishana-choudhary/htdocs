<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '>=w?DWxvBXi$|#wC>6V)R?ZF=#AnbPI$c-d< n(%dpp#`dn/.:>/.Rw`r=;;LWWA');
define('SECURE_AUTH_KEY',  'IR~zRYr^^!y]A]M_%QijH7j!l+&rc-B~ALEVRI<+*~Pr>XR{qugEG)Xu#@Mp^/i6');
define('LOGGED_IN_KEY',    'wMboL A+Ko(&kOzEf70n.7`O^x%cT9fR9!7M`8!+#K3g6:e}Ie@/7eQ%($q,(kLX');
define('NONCE_KEY',        ':h(t=gvO-G,a-|o+6?=ylrSx}4/;]j|XeH@r0(fynBGd$5seQs2@<bA,P.7?o5.L');
define('AUTH_SALT',        ';v>c4Vwop]J}dHa+xY*Yl[6P*_n?mk)iYuKv^yYw]|TCNU6{DH&AQQq3B!!2@s2?');
define('SECURE_AUTH_SALT', '*$h&Q}.CyGG%36): p:rFRvSQY*#nBe;6B*OiF^{-|y7Qf*b8pt1Y]id$%p!tyeJ');
define('LOGGED_IN_SALT',   'g&,ca7v:t-a%btzY6MXRL 2Z(w4_icKznPh*@.KWqjlOHV4A7vYj>Q?qlU5KUH:m');
define('NONCE_SALT',       '6lagI,lt3dtJxHW~}DR;Pn,zpK+ysrPTpfZ%T;G+CLkC+)K1nyP~J r#OwLK,/u!');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
