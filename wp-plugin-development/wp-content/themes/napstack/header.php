

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
  <title><?php bloginfo('name');?> | <?php wp_title();?></title>
    <link rel=" stylesheet" href="<?php bloginfo('stylesheet_url')?>"/>
    <?php wp_head(); ?>
  </head>
  <body>
    <div id="wrapper">
      <div id="header">
        <h2><?php bloginfo('name') ?></h2>
        <h4><?php bloginfo('description') ?></h4>
        <div id="menu">
          <?php $defaults = array(
      'theme_location'  => 'primary',
  );?>
  <?php wp_nav_menu( $defaults ); ?>
        </div><!--end menu-->
      </div><!--end header-->
