<?php
/*
Template Name:Right Sidebar
*/

 get_header(); ?>
  <div id="leftsidebar-wrapper">
  <?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
  ?>

  <div class="rightsidebar">
      <div class="leftsidebar-padding">
        <?php dynamic_sidebar('right Sidebar'); ?>
      </div><!--rightsidebar-padding-->
  </div><!--rightsidebar-->
    <div class="rightsidebar_content">
      <div class="leftsidebar-padding">
        <?php the_title(); ?>
      <?php the_content();?>
    </div><!--rightsidebar-padding-->
  </div><!--rightsidebar_content-->

  <?php
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>

</div><!--leftsidebar-wrapper-->
<?php get_footer(); ?>
