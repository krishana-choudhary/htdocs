<?php
/*
Template Name:Both Sidebar
*/

 get_header(); ?>
  <div id="leftsidebar-wrapper">
  <?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
  ?>

  <div class="bt-lt-sidebar">
      <div class="leftsidebar-padding">
        <?php dynamic_sidebar('Left Sidebar'); ?>
      </div><!--leftsidebar-padding-->
  </div><!--bt-lt-tsidebar-->
    <div class="btsidebar_content">
      <div class="leftsidebar-padding">
        <?php the_title(); ?>
      <?php the_content();?>
    </div><!--leftsidebar-padding-->
  </div><!--btsidebar_content-->

    <div class="bt-rt-sidebar">
        <div class="leftsidebar-padding">
          <?php dynamic_sidebar('Right Sidebar'); ?>
        </div><!--leftsidebar-padding-->
    </div><!--bt-rt-sidebar-->

  <?php
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>
</div><!--leftsidebar-wrapper-->
<?php get_footer(); ?>
