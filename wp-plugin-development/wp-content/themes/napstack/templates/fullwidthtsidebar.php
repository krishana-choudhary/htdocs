<?php
/*
Template Name: Fullwidth
*/

 get_header(); ?>
  <div id="leftsidebar-wrapper">
  <?php
if ( have_posts() ) :
	while ( have_posts() ) : the_post();
  ?>

    <div class="leftsidebar-padding">
      <?php the_title(); ?>
      <?php the_content();?>
    </div><!--rightsidebar-padding-->
  <?php
	endwhile;
else :
	echo wpautop( 'Sorry, no posts were found' );
endif;
?>

</div><!--leftsidebar-wrapper-->
<?php get_footer(); ?>
