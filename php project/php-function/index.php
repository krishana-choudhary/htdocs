<!docktype html>
<html>
<head>
<title>php</title>
<meta charsset=utf-8>
</head>
<body>
<p>array</P>
<?php
$students=array('krishana'=>99,'kumar'=>100);
echo "$students[krishana]"."</br>.</br>";

 echo"array with multiple value"."</br>";
$students1=array('krishana'=>array('age'=>24,'marks'=>26,'class'=>'Ba'));
echo $students1['krishana']['age']."</br>";

 echo "foreach with array multiple condition value"."<br/>";
 $a=array('krishana','age','class');
 foreach ($a as $key1) {
   echo "$key1"."</br>";
 }
 echo"foreeach with array with key and value"."</br>";
 $students2=array('krishana'=>25,'kumar'=>24,'choudhary'=>60,'ram'=>50);
 foreach ($students2 as $key => $value)
 {
 echo $key." has got secure" .$value."marks</br>";
 }

echo "switch</br>";
$b="php";
switch ($b) {
  case 'php':
    echo "Language is php</br>";
    break;
   case 'javascript':
   echo "Language is javascript";
   break;
  default:
    echo "No languages is selected</br>";
    break;
}
echo "function</br>";
function calling_function(){
echo "hello world</br>";
}
calling_function();

echo "return function</br>";
function c(){
  return 2;
}
$d=c();
echo "$d</.br>"+4;

echo "function with arguments with logic</br>";
function perameters_logic($e,$f,$i){
  $g=$e + $f - $i;
  return $g;
}
$h=perameters_logic(10,50,30);
echo "$h</br>";

echo "inbilt function of php strlen<br>";
$j="My";
$h=strlen($j);
if ($h<=5) {
  echo "password shoud we more than long caracters</br>";
};

echo "<p>explode function breack to sting in array</p>";
$jpg = 'krishana.jpg';
$breack =explode('.',$jpg);
$break = end($breack);
if ($break == 'jpg') {
  echo "image is jpg";
} else {
  echo "image is not jpg";
}

echo "<p>explode function breack to sting in array</p>";
$jpg = 'krishana.jpg';
$breack =explode('.',$jpg);
$breack=0;
if ($breack==0) {
  echo "is krishana";
} else {
  echo "image is jpg";
}
echo "<p>implode function breack to array in string</p>";
$ram = array('krishana','kumar','chaoudhary','is','fine',);
echo implode(" ",$ram)."</br>";

//continue function
echo "<p>continue function with if statement</p></br>";
for ($i=0; $i <=10 ; $i++) {
  if ($i==5) {
    echo "continue";
    continue;
  }
  echo "$i";
}

//continue function
echo "<p>breack function with if statement</p></br>";
for ($i=0; $i <=10 ; $i++) {
  if ($i==5) {
    echo "breack</br>";
    break;
  }
  echo "$i</br>";
}

//current and next  function
echo "<p>current and next function is use with while loop</br>";
$a=array('krishana','ram','shyam','choudhary');
echo current($a);
echo next($a);
echo current($a);
 while ($ab=current($a)) {
  echo $ab."</br>";
  next($a)."</br>";
};
//function without arguments
echo "<p>function without arguments</p></br>";
function dropdown(){
  echo "<select>";
  for ($i=0; $i <=10 ; $i++) {
    echo "<option>$i</option>";
  }
  echo "</select>";
}
dropdown();

//function with arguments
echo "<p>function with arguments</p></br>";
function dropdown1($a,$b){
  echo "<select>";
  echo "<option>$i</option>";
  for ($i=$a; $i <=$b ; $i++) {
  }
  echo "</select>";
}
dropdown1(1,50);
dropdown1(111,450);

echo "<p>hello php date </p>";
function todaydate($sd){
  $d = date("Y")." " .$sd;
  return $d;
};
$c = todaydate('hello');
echo "$c.</br>";

echo "global function </br>";
$as ="hello this is global var";

function globals(){
  global $as;
  return $as;
}
$dx = globals();
echo $dx;
?>
<form action="submit.php" method="post">
Name: <input type="text" name="name"><br>
</br>
E-mail: <input type="text" name="email"><br>
<input type="submit">
</form>
</body>
</html>
